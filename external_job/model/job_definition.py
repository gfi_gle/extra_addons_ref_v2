# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from base64 import b64encode
import datetime
import logging
import os
import sys

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools.misc import DEFAULT_SERVER_DATE_FORMAT,\
    DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools import config

from .util import get_secure_filename

if sys.platform == 'win32':
    import pbs as sh
else:
    import sh

_logger = logging.getLogger(__name__)


class job_definition(osv.Model):
    _name = 'external_job.job_definition'

    _columns = {
        'name': fields.char(
            u"Name",
            size=128,
            required=True,
        ),
        'command_line': fields.char(
            u"Command Line",
            size=512,
            help=u"The program to call",
            required=True,
        ),
        'arguments_base': fields.char(
            u"Arguments",
            size=2048,
        ),
        'short_desc': fields.char(
            u"Short Description",
            size=256,
            translate=True,
        ),
        'long_desc': fields.text(
            u"Long Description",
        ),
        'output_filename_base': fields.char(
            u"Output File Name",
            size=128,
            help=u"The desired file pattern . For example:"
            u" %(job_definition.name)s-%(datetime.today)s.csv. If there is no"
            u" value, no output file is created.",
        ),
        'model_id': fields.many2one(
            'ir.model',
            string=u"Model",
            ondelete="cascade",
            help=u"The data model our job is expecting to process, used in the"
            u"job runner wizard.",
        ),
        'input_content': fields.char(
            string=u"Content of the input file",
            help=u"A single argument, if the argument is a list, there will be"
            u" one value per line. Without this argument, if no input file is "
            u"given to run, no output file is created. If provided any input "
            u"file will be ignored.",
        ),
        'force_commit': fields.boolean(
            u"Force Commit",
            help=u"Only needed when the external job need to see the job log "
            u"in the database. Avoid as much as possible.",
        ),
    }

    def run_job(
        self, cr, uid, ids, job_args=None, in_file_name=None,
        context=None
    ):
        """This callback method is called by a user pressing a button
        in the form view. When the user presses the button we run the command
        and collect the data back for him.

        :param job_args: arguments for the job, added as a section of the dict
            of the string substitution
        :type job_args: dict
        :param in_file_name: name of the temporary input file to use (instead
            of generating one if needed)
        :type in_file_name: string
        :param out_file_name: name of the temporary output file to use (instead
            of generating one if needed)
        :type out_file_name: string
        :return: a view change, only make sense when provided with a single id
        """
        joblog_ids = []
        joblog_osv = self.pool['external_job.job_log']
        if type(ids) in (int, long):
            ids = [ids]
        for job in self.browse(cr, uid, ids, context=context):

            command = job.command_line
            job_log_values = {
                'job_definition_id': job.id,
            }

            if job.output_filename_base:
                out_file_name = get_secure_filename()
                job_log_values['out_file_name'] = out_file_name
            if job.input_content:
                if in_file_name:
                    _logger.warn(
                        "Ignoring provided filename %s in run job",
                        in_file_name)
                in_file_name = get_secure_filename()

            if in_file_name:
                job_log_values['in_file_name'] = in_file_name

            joblog_id = joblog_osv.create(
                cr, uid, job_log_values, context=context)
            joblog_ids.append(joblog_id)
            joblog = joblog_osv.browse(cr, uid, joblog_id, context=context)
            arg_dict = ArgumentDict(job, joblog, context, job_args, cr)

            if job.input_content:
                # TODO handle exceptions
                with open(in_file_name, 'w+') as f:
                    for value in arg_dict[job.input_content]:
                        f.write("%s\n" % value)

            # Make sure a commit of our transaction before giving the newly
            # created id to our external process or it won't be able to find it
            if job.force_commit:
                cr.commit()

            args = []
            if job.arguments_base:
                args.append(job.arguments_base % arg_dict)

            try:
                run = sh.Command(command)
            except sh.CommandNotFound:
                raise osv.except_osv(
                    _("Error"),
                    _("Command %s not found") % command)

            if job.arguments_base:
                out = run((job.arguments_base % arg_dict).split())
            else:
                out = run()

            # cleanup temporary files
            if in_file_name:
                os.remove(in_file_name)

            write_args = {
                'end_date': fields.datetime.now(),
                'stdout': out,
                'state': 'done',
            }
            joblog_osv.write(cr, uid, joblog_id, write_args, context=context)

            if job.output_filename_base:
                with open(out_file_name, 'rb') as outfile:
                    outfile_data = outfile.read()

                if not outfile_data:
                    outfile_data = ''

                outfile_data = b64encode(outfile_data)

                attachment_osv = self.pool.get('ir.attachment')
                fname = job.output_filename_base % arg_dict
                create_args = {
                    'name': fname,
                    'datas': outfile_data,
                    'datas_fname': fname,
                    'res_id': joblog_id,
                    'res_model': 'external_job.job_log',
                }
                attachment_osv.create(cr, uid, create_args, context=context)

                os.remove(out_file_name)

        if len(joblog_ids) == 1:
            return {
                'name': 'Job Log',
                'type': 'ir.actions.act_window',
                'res_model': 'external_job.job_log',
                'res_id': joblog_ids[0],
                'view_type': 'form',
                'view_mode': 'form',
                'context': context,
                'target': 'new',
            }
        # TODO check that this is correct
        return {
            'name': 'Job Log',
            'type': 'ir.actions.act_window',
            'res_model': 'external_job.job_log',
            'res_ids': joblog_ids,
            'view_type': 'tree',
            'view_mode': 'tree',
            'context': context,
            'target': 'new',
        }


class ArgumentDict(object):
    """Used to create arguments of a job.
    """
    def __init__(self, job_definition, joblog, context, run, cr):
        self.job_definition = job_definition
        self.joblog = joblog
        self.context = context
        self.run = run
        self.cr = cr

    # incorrect but does not seem to be used
    def __len__(self):
        return 3 + len(self.context) + len(self.run)

    def __getitem__(self, key):
        parts = key.split('.')
        if parts[0] == 'job_log':
            # dangerous?
            return getattr(self.joblog, parts[1])
        if parts[0] == 'job_definition':
            # dangerous?
            return getattr(self.job_definition, parts[1])
        if parts[0] == 'context':
            if self.context:
                return self.context[parts[1]]
        if parts[0] == 'run':
            return self.run[parts[1]]
        if parts[0] == 'config':
            if parts[1] == 'options':
                if parts[2] == 'db_port':
                    return (
                        config.options[parts[2]]
                        if config.options[parts[2]]
                        else 5432
                    )
                else:
                    return config.options[parts[2]]
            else:
                return config.misc.get(parts[1], {}).get(parts[2])
        if parts[0] == 'cr':
            if parts[1] == 'dbname':
                return self.cr.dbname
        if parts[0] == 'datetime':
            if parts[1] == 'now':
                return datetime.datetime.today().strftime(
                    DEFAULT_SERVER_DATETIME_FORMAT)
            elif parts[1] == 'today':
                return datetime.datetime.now().date().strftime(
                    DEFAULT_SERVER_DATE_FORMAT)

    # does not seem to be used
    def __contains__(self, item):
        if item in [
            'job_log.id', 'job_log.in_file_name',
            'job_log.out_file_name'
        ]:
            return True
        parts = item.split('.')
        if parts[0] == 'job_definition':
            # TODO do real test
            return True
        if parts[0] == 'context':
            return self.context and parts[1] in self.context
        if parts[0] == 'run':
            return self.run and parts[1] in self.run
        if parts[0] == 'config':
            if parts[1] == 'options':
                return parts[2] in config.options
            else:
                return parts[2] in config.misc.get(parts[1], {})
        if parts[0] == 'cr':
            if parts[1] == 'dbname':
                return True
        return False

    # XXX __iter__ not used apparently
