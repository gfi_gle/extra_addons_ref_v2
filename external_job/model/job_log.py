# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014-2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
import datetime
import logging

from openerp.osv import fields, osv
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _

_logger = logging.getLogger(__name__)


class job_log(osv.Model):
    _name = 'external_job.job_log'

    def _get_filename(self, cr, uid, ids, field_name, arg, context=None):
        attachment_osv = self.pool['ir.attachment']
        search_args = [
            '&',
            ('res_model', '=', 'external_job.job_log'),
            ('res_id', 'in', ids),
        ]
        attachement_ids = attachment_osv.search(
            cr, uid, search_args, context=context)
        result = dict.fromkeys(ids)
        read_args = ['datas', 'datas_fname', 'res_id']
        attachments = attachment_osv.read(
            cr, uid, attachement_ids, read_args, context=context)
        for attachment in attachments:
            result[attachment.get('res_id')] = attachment.get('datas_fname')

        return result

    def _get_attachment(self, cr, uid, ids, field_name, arg, context=None):
        attachment_osv = self.pool['ir.attachment']
        search_args = [
            '&',
            ('res_model', '=', 'external_job.job_log'),
            ('res_id', 'in', ids),
        ]
        attachement_ids = attachment_osv.search(
            cr, uid, search_args, context=context)
        result = dict.fromkeys(ids)
        read_args = ['datas', 'datas_fname', 'res_id']
        attachments = attachment_osv.read(
            cr, uid, attachement_ids, read_args, context=context)
        for attachment in attachments:
            result[attachment.get('res_id')] = attachment.get('datas')

        return result

    _columns = {
        'job_definition_id': fields.many2one(
            'external_job.job_definition',
            u"Job Definition",
            ondelete='cascade',
            required=True,
        ),

        'start_date': fields.datetime(
            u"Start Date",
            readonly=True,
        ),
        'end_date': fields.datetime(
            u"End Date",
            readonly=True,
        ),

        'in_file_name': fields.char(
            u"Temporary In File Name",
            size=512,
            readonly=True,
        ),
        'out_file_name': fields.char(
            u"Temporary Out File Name",
            size=512,
            readonly=True,
        ),

        'out_file': fields.function(
            _get_attachment,
            type='binary',
            string=u"Output File",
        ),
        'filename': fields.function(
            _get_filename,
            type='char',
            string=u"Filename",
        ),

        'stdout': fields.text(
            u"StdOut",
            readonly=True,
        ),
        'stderr': fields.text(
            u"StdErr",
            readonly=True,
        ),
        'state': fields.selection(
            [
                ('started', "Started"),
                ('done', "Done"),
                ('exception', "Exception"),
            ],
            u"State",
        ),
    }

    _defaults = {
        'state': lambda *a: 'started',
        'start_date': lambda *a: fields.datetime.now(),
    }

    def name_get(self, cr, uid, ids, context=None):
        # TODO There might be a method that grab the lang and timezone from
        # the context and format a datetime correctly.
        res = []
        if context and 'lang' in context:
            language_osv = self.pool['res.lang']
            lang_id = language_osv.search(
                cr, uid, [('code', '=', context['lang'])], context=context)
            lang_r = language_osv.read(
                cr, uid, lang_id, ['date_format', 'time_format'])[0]
            lang_format = lang_r['date_format'] + ' ' + lang_r['time_format']
        else:
            lang_format = '%Y-%m-%d %H:%M:%S'
        for r in self.read(cr, uid, ids, ['start_date']):
            job_datetime = datetime.datetime.strptime(
                r['start_date'], DEFAULT_SERVER_DATETIME_FORMAT)
            job_datetime = fields.datetime.context_timestamp(
                cr, uid, job_datetime, context)
            job_datetime = datetime.datetime.strftime(
                job_datetime, lang_format)
            res.append((r['id'], _(u"%s’s Job") % job_datetime))
        return res
