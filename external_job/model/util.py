# -*- encoding: utf-8 -*-
import os
import tempfile


def get_secure_filename():
    """Create a temporary file as securely as possible.
    Make sure it is closed and return the filename for easy usage. The caller
    is responsible for removing the file when it's done.
    """

    file_handle, filename = tempfile.mkstemp()
    tmpfile = os.fdopen(file_handle, 'r')
    tmpfile.close()

    return filename
