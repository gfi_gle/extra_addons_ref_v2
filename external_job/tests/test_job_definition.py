###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
import unittest2

from openerp import SUPERUSER_ID
import openerp.tests
from openerp.tests.common import TransactionCase


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class test(TransactionCase):

    def setUp(self):
        """Create a manager user with the group erp manager
        """
        super(test, self).setUp()
        users_osv = self.registry('res.users')
        cr, uid = self.cr, self.uid
        context = {}
        self.users = {}
        self.users['super'] = SUPERUSER_ID
        manager_values = {
            'name': 'manager',
            'login': 'manager',
            'groups_id': [(4, self.ref('base.group_erp_manager'))],
        }
        self.users['manager'] = users_osv.create(
            cr, uid, manager_values, context=context)

    def test_create(self):
        """Create a new job definition with an erp manager.
        Assert the job definition is created (id>0).
        """
        cr, uid = self.cr, self.uid
        context = {}
        job_definition_osv = self.registry('external_job.job_definition')
        job_definition_values = {
            'name': 'test',
            'command_line': 'ls',
        }
        job_def_id = job_definition_osv.create(
            cr, self.users['manager'], job_definition_values, context=context)
        self.assertLess(0, job_def_id)

    def test_run_job_echo_1(self):
        """Run an echo 1 job and assert that the result id is in the output.
        """
        cr, uid = self.cr, self.uid
        context = {}
        job_definition_osv = self.registry('external_job.job_definition')
        job_log_osv = self.registry('external_job.job_log')
        echo_job = self.ref('external_job.job_definition_echo_1')
        echo_args = {
        }
        res = job_definition_osv.run_job(
            cr, uid, echo_job, job_args=echo_args, context=context)
        job_log_id = res['res_id']
        job_log = job_log_osv.browse(cr, uid, job_log_id, context)
        self.assertEqual("%d\n" % job_log_id, job_log.stdout)

    def test_run_job_echo_2(self):
        """Run an echo 2 job and assert that the output is correct.
        """
        cr, uid = self.cr, self.uid
        context = {}
        job_definition_osv = self.registry('external_job.job_definition')
        job_log_osv = self.registry('external_job.job_log')
        echo_job = self.ref('external_job.job_definition_echo_2')
        echo_args = {
        }
        res = job_definition_osv.run_job(
            cr, uid, echo_job, job_args=echo_args, context=context)
        job_log_id = res['res_id']
        job_log = job_log_osv.browse(cr, uid, job_log_id, context)
        self.assertEqual("echo 2\n", job_log.stdout)

    def test_run_job_cat(self):
        """Run a cat job and assert the output is the content of the file
        """
        cr, uid = self.cr, self.uid
        context = {
            'values': [1, 2, 3],
        }
        job_definition_osv = self.registry('external_job.job_definition')
        job_log_osv = self.registry('external_job.job_log')
        cat_job = self.ref('external_job.job_definition_cat')
        cat_args = {
        }
        res = job_definition_osv.run_job(
            cr, uid, cat_job, job_args=cat_args, context=context)
        job_log_id = res['res_id']
        job_log = job_log_osv.browse(cr, uid, job_log_id, context)
        self.assertEqual("1\n2\n3\n", job_log.stdout)

if __name__ == '__main__':
        unittest2.main()
