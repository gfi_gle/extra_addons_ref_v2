.. _README:

External Job
============

This is a module for Odoo to run external programs. 

It is meant to be used in one of this way:

- run button on the job definition (but it can only be used by a member of the admin group)
- run_job method launched programatically, it can be in the code, by the scheduler, or a queued task (see queue module)

A job definition indicate a program to launch with his expected arguments. The arguments will be obtained by formating a dictionary (see the job definition view for the details).

A job log is the result of a call to the job definition. It is either in process if the job has been started, or done/crashed.
