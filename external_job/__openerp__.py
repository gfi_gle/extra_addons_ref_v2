# -*- coding: utf-8 -*-
##############################################################################
#
#    External Jobs, for OpenERP
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': "External Jobs",
    'version': '2.0',
    'author': "XCG Consulting",
    'category': "Tools",
    'description': """
Run jobs outside of the OpenERP realm/processes.

Dependencies:
- sh
    $ pip install sh
""",
    'website': "https://odoo.consulting/",
    'depends': [
        'document',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/menu.xml',

        'wizards/jobrunner.xml',

        'views/job_definition.xml',
        'views/job_log.xml',
    ],
    'demo': [
        'demo/external_job.job_definition.csv'
    ],
    'installable': True,
    'external_dependencies': {
        'python': [
            'sh',
        ]
    },
}
