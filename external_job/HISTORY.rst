=======
History
=======

.. _2.0:

2.0.0 (wip)
-----------

Added documentation, tests, streamlined usage, added migration for old jobs.
