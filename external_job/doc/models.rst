Models
======

Job Definition
--------------

.. automodule:: openerp.addons.external_job.model.job_definition
    :members:
    :undoc-members:

Job Log
-------

.. automodule:: openerp.addons.external_job.model.job_log
    :members:
    :undoc-members:

