# -*- encoding: utf-8 -*-
from openerp.osv import osv, fields


class jobrunner(osv.TransientModel):
    """a job runner is responsible to propose a choice to the user
    filtered contextualy to only propose jobs that have a definition
    related to the current object
    """

    _name = 'external_job.jobrunner'

    _columns = {
        'job_definition_id': fields.many2one(
            "external_job.job_definition",
            string="Job definition",
        ),
    }

    def run_job(self, cr, uid, ids, context=None):

        wizard = self.browse(cr, uid, ids[0], context=context)
        job_id = wizard.job_definition_id.id
        job_osv = self.pool.get('external_job.job_definition')
        return job_osv.run_job(cr, uid, [job_id], context=context)
