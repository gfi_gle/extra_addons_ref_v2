# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
import logging

__name__ = "Migrate the old arguments to the new format"

_logger = logging.getLogger(__name__)

def migrate(cr, installed_version):
    if not installed_version:
        return
    cr.execute(
        'SELECT id, arguments, config_arguments FROM external_job_job_definition;'
    )
    values = cr.fetchall()
    for id, args, conf_args in values:
        update_argument_base = []
        # implied argument was job log id for 1.0 and dbname + job log id for
        # 1.2.4 but there is no way to determine that so pick the 1.0 version
        update_argument_base.append('%(job_log.id)s')
        # migrate arguments column (no transformation)
        if args:
            update_argument_base += args.split()
        # migrate config_arguments (transform)
        if conf_args:
            for conf_arg in conf_args.split():
                update_argument_base.append(
                    '%%(config.options.%s)s' % conf_arg)
        arguments_base = ' '.join(update_argument_base)
        cr.execute(
            'UPDATE external_job_job_definition SET arguments_base=\''
            '%(arguments_base)s\' WHERE ID=%(id)s;' %
            dict(arguments_base=arguments_base, id=id)
        )
     
    # Old code had this hard coded: resource_ids in input file
    cr.execute(
        'UPDATE external_job_job_definition SET input_content=\'context.resource_ids\';'
    )
    # old code had this hard coded
    cr.execute(
        'UPDATE external_job_job_definition SET output_filename_base=\'%(job_definition.name)s-%(datetime.today)s.\' || file_extension;'
    )
    # delete old columns
    cr.execute('ALTER TABLE external_job_job_definition DROP COLUMN arguments;')
    cr.execute('ALTER TABLE external_job_job_definition DROP COLUMN config_arguments;')
