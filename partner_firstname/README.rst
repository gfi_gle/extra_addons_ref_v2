Partner first names for Odoo
============================

This package adds first and last names to non-company Odoo partners.

Taken from OCA: <https://github.com/OCA/partner-contact/tree/8.0/partner_firstname>.
Git revision: 69cbd8171b.

Patches in the "patches" directory have been applied.
