Accounting improvements
=======================

Improvements on top of the base Odoo accounting:

- Better analytics.

- Better handle multiple currencies.

- Disallow copies thanks to ``base_no_copy``.

- Payment batches.

- Default payment methods.

- Misc.


Installation
------------

- If the Odoo database contains lots of partners, pre-create computed columns
  by running the ``scripts/create_partner_computed_columns.sql`` script.

- Install this addon as usual.
