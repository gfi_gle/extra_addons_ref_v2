# -*- coding: utf-8 -*-
##############################################################################
#
##############################################################################
{
    "name": "Accounting improvements",
    "version": "2.1.0",
    "author": "XCG Consulting",
    "category": 'Accounting',
    "description": """
Accounting improvements
=======================

Improvements on top of the base Odoo accounting:

- Better analytics.

- Better handle multiple currencies.

- Disallow copies thanks to ``base_no_copy``.

- Misc.

- Payment batches.

- Default payment methods.


Installation
------------

- If the Odoo database contains lots of partners, pre-create computed columns
  by running the ``scripts/create_partner_computed_columns.sql`` script.

- Install this addon as usual.
    """,
    'website': 'http://www.openerp-experts.com',
    'init_xml': [],
    "depends": [
        'base',
        'account_accountant',
        'account_voucher',
        'account_payment',
        'account_sequence',
        'mail',
        'analytic_structure',
        'base_no_copy',
        'queue',
        'report_webkit',
    ],
    "data": [

        'menu.xml',

        'data/ir_config_parameter.xml',
        'data/partner_data.xml',
        'data/ir_sequence.xml',
        'data/queues.xml',

        'wizard/account_reconcile_view.xml',
        'wizard/email_remittance.xml',
        'account_config_settings.xml',
        'account_move_line_search_unreconciled.xml',
        'account_move_line_tree.xml',
        'account_move_view.xml',
        'account_view.xml',
        'account_voucher.xml',
        'partner_view.xml',
        'payment_batch.xml',
        'payment_batch_selection.xml',
        'payment_selection.xml',
        'payment_suggestion.xml',
        'account_move_line_journal_items.xml',
        'account_move_line_journal_view.xml',
        'account_menu_entries.xml',
        'res_company.xml',
        'data/voucher_report_header.xml',
        'report/payment_suggestion.xml',
        'report/remittance_letter.xml',
        'data/remittance_letter_email_template.xml',
        'security/ir.model.access.csv',
        'security/record_rules.xml',
        'workflow/payment_batch.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
