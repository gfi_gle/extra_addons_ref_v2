-- Create partner computed columns; useful to avoid letting Odoo create them, which might take too
-- long on a database that already contains a lot of partners.

-- Note: It is assumed partner accounts are not incoherent and do not fall under the checks
-- implemented by these fields.

ALTER TABLE res_partner ADD COLUMN customer_account_check boolean;
COMMENT ON COLUMN res_partner.customer_account_check IS 'Check Customer Account';

ALTER TABLE res_partner ADD COLUMN supplier_account_check boolean;
COMMENT ON COLUMN res_partner.supplier_account_check IS 'Check Supplier Account';

UPDATE res_partner SET customer_account_check = FALSE, supplier_account_check = FALSE;
