from openerp import api
from openerp import fields
from openerp import models


_DCPM_KEY = 'account_streamline.default_client_payment_method'
_DSPM_KEY = 'account_streamline.default_supplier_payment_method'


class AccountConfigSettings(models.TransientModel):
    """Inherit from account.config.settings to add default payment methods.
    This is only here for easier access; settings are not actually stored by
    this (transient) model. Instead, they are kept in sync with global settings
    the keys of which start with "account_streamline.". See comments in the
    definition of the "res.config.settings" model for details.
    """

    _inherit = 'account.config.settings'

    accstreamline_dcpm_id = fields.Many2one(
        comodel_name='account.journal',
        string='Default incoming client payment method',
        domain=[('type', 'in', ['bank', 'cash'])],
        ondelete='set null',
        help=(
            'Payment method to use by default in debit payment batches and '
            'client vouchers.'
        ),
    )

    accstreamline_dspm_id = fields.Many2one(
        comodel_name='account.journal',
        string='Default outgoing supplier payment method',
        domain=[('type', 'in', ['bank', 'cash'])],
        ondelete='set null',
        help=(
            'Payment method to use by default in payment batches and supplier '
            'vouchers.'
        ),
    )

    @api.model
    def get_default_accstreamline_dpmethods(self, fields):
        """Read default payment method settings. This function is called when
        the form is shown.
        """

        ret = {}

        if 'accstreamline_dcpm_id' in fields:
            ret['accstreamline_dcpm_id'] = self.read_accstreamline_dcpm().id
        if 'accstreamline_dspm_id' in fields:
            ret['accstreamline_dspm_id'] = self.read_accstreamline_dspm().id

        return ret

    @api.model
    def read_accstreamline_dcpm(self):
        """Read the accstreamline_dcpm setting. Use the admin account to bypass
        security restrictions.
        :rtype: Odoo "account.journal" record set.
        """

        return self._read_accstreamline_dpmethod(_DCPM_KEY)

    @api.model
    def read_accstreamline_dspm(self):
        """Read the accstreamline_dspm setting. Use the admin account to bypass
        security restrictions.
        :rtype: Odoo "account.journal" record set.
        """

        return self._read_accstreamline_dpmethod(_DSPM_KEY)

    @api.multi
    def set_accstreamline_dcpm(self):
        """Update the default client payment method setting. This function is
        called when saving the form.
        """

        setting_value = unicode(self.accstreamline_dcpm_id.id or 0)
        self._set_accstreamline_dpmethod(_DCPM_KEY, setting_value)

    @api.multi
    def set_accstreamline_dspm(self):
        """Update the default supplier payment method setting. This function is
        called when saving the form.
        """

        setting_value = unicode(self.accstreamline_dspm_id.id or 0)
        self._set_accstreamline_dpmethod(_DSPM_KEY, setting_value)

    def _read_accstreamline_dpmethod(self, setting_key):
        """Read the specified default payment method setting. Use the admin
        account to bypass security restrictions.
        :type setting_key: String.
        :rtype: Odoo "account.journal" record set.
        """

        config_record = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', setting_key)],
            limit=1,
        )
        if not config_record:
            return self.env['account.journal']
        journal_id = int(config_record.value)
        if not journal_id:
            return self.env['account.journal']
        return self.env['account.journal'].browse(journal_id)

    def _set_accstreamline_dpmethod(self, setting_key, setting_value):
        """Set the specified default payment method setting to the specified
        value.
        :type setting_key: String.
        :type setting_value: String.
        """

        existing_config_record = self.env['ir.config_parameter'].search(
            [('key', '=', setting_key)],
            limit=1,
        )

        if existing_config_record:
            existing_config_record.value = setting_value

        else:
            # The setting doesn't exist; create it.
            self.env['ir.config_parameter'].create(
                {'key': setting_key, 'value': setting_value},
            )
