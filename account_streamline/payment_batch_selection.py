from collections import defaultdict
import logging

from openerp import models, fields, api, _, exceptions

PAYMENT_MODE_SELECTION = [
    ('supplier_invoices', _('Supplier invoices')),
    ('client_refunds', _('Client refunds')),
    ('both', _('Both')),
]

DEBIT_PAYMENT_MODE_SELECTION = [
    ('client_invoices', _('Client invoices')),
    ('supplier_refunds', _('Supplier refunds')),
    ('both', _('Both')),
]


log = logging.getLogger(__name__)


class PaymentBatchSelection(models.TransientModel):
    _name = 'account.payment_batch.selection'

    def translate_tuple_list(self, list):
        return [
            (tpl[0], _(tpl[1]))
            for tpl in list
        ]

    def get_mode_selection(self):

        context = self.env.context
        option = 'payment'

        if 'default_option' in context.keys():
            option = context['default_option']

        if option == 'payment':
            selection = self.translate_tuple_list(PAYMENT_MODE_SELECTION)
        elif option == 'debit_payment':
            selection = self.translate_tuple_list(DEBIT_PAYMENT_MODE_SELECTION)

        return selection

    date_maturity = fields.Date(
        string='Due date',
        help=(
            'Date based on which to filter accounting entries (we take '
            'entries with a due date <= the one entered here).'
        ),
        required=True,
    )

    def _default_journal_id(self):
        return (
            self.env['account.config.settings'].read_accstreamline_dcpm()
            if self.env.context.get('default_option') == 'debit_payment'
            else self.env['account.config.settings'].read_accstreamline_dspm()
        )

    journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Payment Method',
        domain=[('type', 'in', ('bank', 'cash'))],
        default=_default_journal_id,
        required=True,
    )

    option = fields.Selection(
        selection=[
            ('payment', 'Payments'),
            ('debit_payment', 'Debit payments'),
        ],
        string='Option',
        help=(
            'Whether the banking operations corresponding to this batch are '
            'payments or debit payments.'
        ),
        required=True,
        readonly=True,
    )

    mode = fields.Selection(
        selection=get_mode_selection,
        string='Mode',
        help='How to filter accounting entries.',
        required=True,
    )

    @api.model
    def create(self, vals):

        context = dict(self.env.context).copy()

        if 'option' in vals.keys():
            context.update({'default_option': vals['option']})
            vals.pop('option')

        if (
            (
                'default_option' not in context.keys() or
                context['default_option'] not in [
                    'payment', 'debit_payment'
                ]
            ) and
            (
                'option' not in vals.keys() or
                vals['option'] not in ['payment', 'debit_payment']
            )
        ):
            context.update({'default_option': 'payment'})

        return (
            super(PaymentBatchSelection, self.with_context(context)).create(
                vals
            )
        )

    @api.multi
    def write(self, vals):

        if 'option' in vals.keys():
            raise exceptions.ValidationError(_(
                'It is not possible for a payment batch wizard, to modify its '
                'purpose, whether it is dedicated to payments or debit '
                'payments. This option is definitely set on creation.'
            ))

        ret = super(PaymentBatchSelection, self).write(vals)

        return ret

    @api.model
    def get_preferred_bank_account_id(
        self, partner, entry, partner_bank_cache,
    ):
        """Get the preferred bank account of the specified partner, based on
        the specified accounting entry.

        Made to be overridden. The default implementation always returns the
        most recently modified bank of the partner, regardless of the
        accounting entry.

        :type partner: Odoo "res.partner record set.
        :type entry: Odoo "account.move.line" record set.
        :type partner_bank_cache: Dictionary partner ID -> bank ID.
        :return: Odoo "res.partner.bank" record ID.
        """

        partner_banks = partner_bank_cache.get(partner.id)

        if partner_banks is None:
            # First pass on this partner; save into the cache.
            partner_banks = partner_bank_cache[partner.id] = (
                self.env['res.partner.bank'].search(
                    [('partner_id', '=', partner.id)], order='write_date desc',
                    limit=1,
                ).id
            )

        # Use the cached bank.
        return partner_banks

    @api.multi
    def sale_payment_method_filter(self, entry_search_domain):
        """Hook for other modules, in order to filter accounting document lines
        on payment methods, which are associated to debit payments.
        """
        self.ensure_one()
        return entry_search_domain

    @api.multi
    def partner_filter(self, entry_search_domain):
        """Hook for other modules, in order to filter accounting document lines
        on partners specific fields.
        """
        self.ensure_one()
        return entry_search_domain

    @api.multi
    def generate_batch(self):
        """Create a payment batch with accounting entries found based on the
        specified parameters.
        """

        self.ensure_one()

        # Gather the accounting entries to include:
        # - Filter by date.
        # - Filter by account type: all supplier lines, and client lines that
        # are for refunds.
        # - Not already included in a batch.
        # - Not marked with specific flags (made to delay payment).

        entry_search_domain = [  # noqa
            '|',
                ('reconcile_id', '=', False),
                ('reconcile_partial_id', '!=', False),
        ]

        if self.mode == 'supplier_invoices':
            entry_search_domain += [
                ('account_id.type', '=', 'payable'),
                ('move_id.journal_id.type', '=', 'purchase'),
            ]

        elif self.mode == 'client_refunds':
            entry_search_domain += [
                ('account_id.type', '=', 'receivable'),
                ('move_id.journal_id.type', '=', 'sale_refund'),
            ]

        elif self.mode == 'both' and self.option == 'payment':
            entry_search_domain += [  # noqa
                '|',
                    '&',
                        ('account_id.type', '=', 'payable'),
                        ('move_id.journal_id.type', '=', 'purchase'),
                    '&',
                        ('account_id.type', '=', 'receivable'),
                        ('move_id.journal_id.type', '=', 'sale_refund'),
            ]

        elif self.mode == 'client_invoices':
            entry_search_domain += [
                ('account_id.type', '=', 'receivable'),
                ('move_id.journal_id.type', '=', 'sale'),
            ]

        elif self.mode == 'supplier_refunds':
            entry_search_domain += [
                ('account_id.type', '=', 'payable'),
                ('move_id.journal_id.type', '=', 'purchase_refund'),
            ]

        elif self.mode == 'both' and self.option == 'debit_payment':
            entry_search_domain += [  # noqa
                '|',
                    '&',
                        ('account_id.type', '=', 'receivable'),
                        ('move_id.journal_id.type', '=', 'sale'),
                    '&',
                        ('account_id.type', '=', 'payable'),
                        ('move_id.journal_id.type', '=', 'purchase_refund'),
            ]

        entry_search_domain += [
            ('state', '=', 'valid'),
            ('move_id.state', '=', 'posted'),
            ('date_maturity', '<=', self.date_maturity),
            ('is_not_payment_batchable', '=', False),
            ('payment_batch_id', '=', False),
            ('is_in_voucher', '=', False),
        ]

        # Hook for other modules, in order to filter accounting document lines
        # on payment methods, which are associated to debit payments.
        if self.option == 'debit_payment':
            entry_search_domain = (
                self.with_context(self.env.context).sale_payment_method_filter(
                    entry_search_domain
                )
            )
            entry_search_domain = (
                self.with_context(self.env.context).partner_filter(
                    entry_search_domain
                )
            )

        log.info('Payment batch selection: Looking for acc entries...')
        lines = self.env['account.move.line'].search(entry_search_domain)

        if not lines:
            raise exceptions.Warning('No lines found.')

        log.info('Payment batch selection: Got %d acc entries.', len(lines))

        # Split lines by partner and by preferred bank account.
        batch_lines = defaultdict(dict)

        # Prepare a cache to avoid too many partner bank lookups.
        partner_bank_cache = {}

        for line in lines:
            partner = line.partner_id
            bank_id = self.get_preferred_bank_account_id(
                partner, line, partner_bank_cache,
            )
            subdict = batch_lines[(partner.id, bank_id)]
            subdict['bank_id'] = bank_id
            subdict['partner_id'] = partner.id
            subdict.setdefault('line_ids', []).append(line.id)

        log.info('Payment batch selection: Partner / bank lookups done.')

        # Create a payment batch
        pb = self.env['account.payment_batch'].create({
            'journal_id': self.journal_id.id,
            'line_ids': [
                (0, 0, {
                    'partner_id': line_data['partner_id'],
                    'line_ids': [
                        (0, 0, {'move_line_id': ml})
                        for ml in line_data['line_ids']
                    ],
                    'bank_id': line_data['bank_id'],
                })
                for line_data in batch_lines.itervalues()
            ],
            'date_maturity': self.date_maturity,
            'option': self.option,
        })

        lines.write({'payment_batch_id': pb.id})

        log.info('Payment batch selection: Batch %s created.', pb.name)

        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.payment_batch',
            'name': _("Payment Batch"),
            'view_mode': 'form,tree',
            'view_type': 'form',
            'res_id': pb.id,
        }
