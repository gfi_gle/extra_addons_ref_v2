import ast
import base64
import io
import urllib
import zipfile

from openerp import _
from openerp import api
from openerp import exceptions
from openerp import models
from openerp import fields

import openerp.addons.decimal_precision as dp


# The amount of batch lines beyond which batch creation & validation are moved
# to queued tasks.
QUEUE_BATCHLINE_THRESHOLD = 20


msg_invalid_line_type = _('Account type %s is not usable in payment vouchers.')
msg_invalid_partner_type_supplier = _('Partner %s is not a supplier.')
msg_invalid_partner_type_customer = _('Partner %s is not a customer.')
msg_define_dc_on_journal = _(
    'Please define default credit/debit accounts on the journal "%s".')
msg_already_reconciled = _(
    'The line %s is already reconciled.'
)


class PaymentBatch(models.Model):
    _name = 'account.payment_batch'
    _inherit = ['mail.thread']

    # Utility field filled in when the batch creation would otherwise take too
    # long; a queued task will then peek into this field and fill the batch.
    batchlines_to_create = fields.Text(
        string='batchlines_to_create (Utility)',
        readonly=True,
    )

    creation_queue_task_id = fields.Many2one(
        comodel_name='queue.task',
        string='Creation queue task',
        ondelete='set null',
        copy=False,
        help='Task queued to create this batch.',
        readonly=True,
    )

    validation_queue_task_id = fields.Many2one(
        comodel_name='queue.task',
        string='Validation queue task',
        ondelete='set null',
        copy=False,
        help='Task queued to validate this batch.',
        readonly=True,
    )

    name = fields.Char(
        string=u"Number",
        required=True,
        size=8,
        default='PB00000',
        track_visibility='onchange',
    )
    date_maturity = fields.Date(
        string="Execution Date",
        required=True,
        track_visibility='onchange',
    )
    journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Payment Method',
        required=True,
        domain=[('type', 'in', ['bank', 'cash'])],
    )
    line_ids = fields.One2many(
        comodel_name='account.payment_batch.line',
        inverse_name='batch_id',
        string="Lines",
    )
    state = fields.Selection(
        selection=[
            ('draft', 'Draft'),
            ('paid', 'Paid')
        ],
        string='State',
        track_visibility='onchange',
    )
    voucher_ids = fields.Many2many(
        comodel_name='account.voucher',
        relation='account_voucher_account_payment_batch_rel_',
        column1='voucher_id',
        column2='payment_batch_id',
        string="Vouchers",
    )
    validate_date = fields.Datetime(
        u"Validation Date",
        readonly=True,
        track_visibility='onchange',
    )
    validate_user_id = fields.Many2one(
        'res.users',
        u"Validator",
        readonly=True,
        track_visibility='onchange',
    )

    option = fields.Selection(
        selection=[
            ('payment', 'Payments'),
            ('debit_payment', 'Debit payments'),
        ],
        string='Option',
        help=(
            'Whether the banking operations corresponding to this batch are '
            'payments or debit payments.'
        ),
        required=True,
        readonly=True,
        default='payment',
    )

    @api.constrains('state')
    @api.one
    def _ensure_bank_account(self):
        """Ensure bank accounts are set when outside of the "draft" state."""

        if not self.state or self.state == 'draft':
            return

        for pbline in self.line_ids:
            if not pbline.bank_id:
                raise exceptions.ValidationError(_(
                    'Bank accounts must be set on every line.'
                ))

    @api.multi
    def control_voucher_generation(self):

        self.ensure_one()

        if self.option == 'debit_payment':

            for line in self.line_ids:

                move_lines = line.line_ids.mapped('move_line_id')

                total_credit = 0

                for ml in move_lines:

                    sign = (2 * (ml.debit > 0) - 1)
                    total_credit += (ml.debit or ml.credit) * sign

                if total_credit <= 0:
                    raise exceptions.ValidationError(_(
                        'The total amounts of the generated vouchers must be '
                        'in credit, for it is a debit payment from the '
                        'customer.'
                    ))

        return True

    @api.model
    def create(self, vals):
        """Override to:
        - Generate a sequence code.
        - Move batch line creations to a queued task when there are too many.
        """

        # Generate a sequence code.
        vals['name'] = self.env['ir.sequence'].next_by_code(self._name)

        # Move batch line creations to a queued task when there are too many.
        move_to_queue = False
        batchlines_to_create = vals.get('line_ids', [])
        if len(batchlines_to_create) > QUEUE_BATCHLINE_THRESHOLD:
            vals.pop('line_ids')
            vals['batchlines_to_create'] = unicode(batchlines_to_create)
            move_to_queue = True

        pbatch = super(PaymentBatch, self).create(vals)

        # Move batch line creations to a queued task when there are too many.
        if move_to_queue:
            qtask = self.env['queue.task'].create({
                'args_interpolation': """{{
                    'batch_id': {batch_id!r},
                    'batch_action': {batch_action!r},
                }}""".format(batch_id=pbatch.id, batch_action='create_blines'),
                'queue_id': self.env.ref(
                    'account_streamline.accstreamline_payment_batch_queue'
                ).id,
            })
            pbatch.creation_queue_task_id = qtask
            pbatch.message_post(
                body=_('Registered a delayed task to create the batch.'),
                subject=_('Batch creation'),
            )

        return pbatch

    @api.multi
    def run_queued_action(self, batch_action):
        """Run a queued action.
        :param batch_action: One of:
        - create_blines: Create batch lines (based on the
          "batchlines_to_create" field).
        - validate_batch: Validate the batch.
        """

        self.ensure_one()

        if batch_action == 'create_blines':
            self.run_queued_action_create_blines()

        elif batch_action == 'validate_batch':
            self.run_queued_action_validate_batch()

        return True

    @api.multi
    def run_queued_action_create_blines(self):
        """Run a queued action: Create batch lines (based on the
          "batchlines_to_create" field).
        """

        self.ensure_one()

        try:
            batchlines_to_create = ast.literal_eval(self.batchlines_to_create)

        except Exception as error:
            self.message_post(
                body=_('Could not fill batch lines.'),
                subject=_('Batch creation'),
            )
            raise error

        self.write({
            'batchlines_to_create': False,  # Clear out.
            'line_ids': batchlines_to_create,
        })

        self.message_post(
            body=(_('%d batch lines have been created.') %
                  len(batchlines_to_create)),
            subject=_('Batch creation'),
        )

    @api.multi
    def run_queued_action_validate_batch(self):
        """Run a queued action: Validate the batch.
        """

        self.ensure_one()

        try:
            self.generate_vouchers()

        except Exception as error:
            self.message_post(
                body=_('Could not validate the batch.'),
                subject=_('Batch validation'),
            )
            raise error

        self.message_post(
            body=_('The batch has been validated.'),
            subject=_('Batch validation'),
        )

    @api.multi
    def voucher_creation_hook(self, pbatchline, values):
        """"Hook to let other addons customize the voucher creation.

        :type pbatchline: Odoo "account.payment_batch.line" record set.

        :param values: Data used to create an Odoo "account.voucher" record.
        :type values: Dictionary.
        """

        pass  # Empty on purpose.

    @api.multi
    def wkf_draft(self):
        self.state = 'draft'
        return True

    @api.multi
    def _generate_report_action(self, voucher_ids):
        """Generate a Payment Suggestion report action
        """

        context = self.env.context.copy()

        # active_ids contains move-line ids. remove them or the payment
        # suggestion object will use them by default.
        if 'active_ids' in self.env.context:
            del context['active_ids']

        # no way to call this function in a v8 form
        return self.pool.get('payment.suggestion').print_payment_suggestion(
            self.env.cr, self.env.uid, voucher_ids, context=context
        )

    @api.multi
    def generate_vouchers(self):

        # TODO Docstring

        voucher_obj = self.env['account.voucher']
        avl_obj = self.env['account.voucher.line']
        voucher_ids = []

        for line in self.line_ids:
            partner = line.partner_id
            journal = self.journal_id
            move_lines = line.line_ids.mapped('move_line_id')

            if not move_lines:
                continue

            vals = {
                'payment_batch_line_id': line.id,
                'partner_id': partner.id,
                'journal_id': journal.id,
                'payment_option': 'without_writeoff',
                'partner_bank_id': line.bank_id.id,
                'amount': line.subtotal,
                'type': 'payment' if self.option == 'payment' else 'receipt',
                # Payment batches are for payment, if the option is set on
                # 'payment'. Otherwise, if the option is set on 'debit
                # payment', these are for receipts.

                # Define "pre_line" to ensure the voucher is aware of the
                # lines we are going to add; otherwise it doesn't show all
                # of them.
                'pre_line': True,
            }

            account = (
                journal.default_credit_account_id or
                journal.default_debit_account_id
            )
            if not account:
                raise exceptions.Warning(
                    _('Error!'),
                    msg_define_dc_on_journal % journal.name
                )

            vals['account_id'] = account.id

            # Hook to let other addons customize the voucher creation.
            self.voucher_creation_hook(line, vals)

            voucher = voucher_obj.create(vals)
            voucher_ids.append(voucher.id)

            # For payments, the generated vouchers must be in debit. and for
            # debit payments, in credit.
            # For payments, the generated vouchers are supplier payments in
            # debit. Credit lines for suppliers must be in debit in the
            # vouchers.
            # For debit payments, the generated vouchers are customer payments
            # in credit. Debit lines for customers must be in credit in the
            # vouchers.

            # now that we have a voucher id we'll add our lines to it
            for ml in move_lines:
                avl = avl_obj.create({
                    'name': ml.name,
                    'voucher_id': voucher.id,

                    # Voucher lines must use the same account as the move
                    # lines, in order to be able to reconcile them with the
                    # move lines created during the validation of the voucher.
                    'account_id': ml.account_id.id,
                    'type': 'dr' if ml.credit else 'cr',
                    'move_line_id': ml.id,
                })

                # Those values are not in the create for some unknown reasons.
                # Maybe we should check if they really need to be written after
                #  the creation process
                avl.write({
                    'reconcile': True,
                    'amount': avl.amount_unreconciled,
                })
                # Mark the lines as batched
                ml.payment_batch_id = self.id

        # Store the vouchers in the batch
        self.voucher_ids = [(6, 0, voucher_ids)]

        # Validate the vouchers - Use the admin account as this might have
        # various effects on invoices / etc. Not a security issue as
        # payment batch access rights have already been checked.
        self.voucher_ids.sudo().signal_workflow('proforma_voucher')

    @api.multi
    def wkf_paid(self):
        """- Create payment vouchers, either directly or through a queued task.
        - Mark the batch as paid.
        """

        self.ensure_one()

        qtask = self.creation_queue_task_id
        if qtask and qtask.state != 'done':
            raise exceptions.Warning(_(
                'Please wait for the creation task to be done before '
                'validating.'
            ))

        if len(self.line_ids) > QUEUE_BATCHLINE_THRESHOLD:
            qtask = self.env['queue.task'].create({
                'args_interpolation': """{{
                    'batch_id': {batch_id!r},
                    'batch_action': {batch_action!r},
                }}""".format(batch_id=self.id, batch_action='validate_batch'),
                'queue_id': self.env.ref(
                    'account_streamline.accstreamline_payment_batch_queue'
                ).id,
            })
            self.validation_queue_task_id = qtask
            self.message_post(
                body=_('Registered a delayed task to validate the batch.'),
                subject=_('Batch validation'),
            )

        else:
            # Under the threshold -> No queued task.
            self.generate_vouchers()

        self.write({
            'validate_date': fields.Datetime.now(),
            'validate_user_id': self.env.uid,
            'state': 'paid',
        })
        return True

    @api.multi
    def unlink(self):
        """Override to:
        - Disallow deleting paid batches.
        - Also delete batch lines.
        """

        if self.filtered(lambda batch: batch.state == 'paid'):
            raise exceptions.Warning(_('Paid batches cannot be deleted.'))

        self.mapped('line_ids').unlink()

        return super(PaymentBatch, self).unlink()


class PaymentBatchLine(models.Model):
    _name = 'account.payment_batch.line'

    _order = 'order_field ASC'

    batch_id = fields.Many2one(
        comodel_name='account.payment_batch',
        string="Batch",
        required=True,
        ondelete='cascade',
    )

    @api.depends('bank_id.acc_number', 'bank_id.bank_bic')
    @api.multi
    def _get_order_field(self):
        """Make the order depend on bank accounts."""
        for pbline in self:
            pbline.order_field = (
                (pbline.bank_id.bank_bic or u'') +
                (pbline.bank_id.acc_number or u'')
            )

    order_field = fields.Char(
        compute=_get_order_field,
        string='Order field',
        help='Field used to order lines based on bank accounts.',
        store=True,
    )

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string="Partner",
        required=True
    )
    subtotal = fields.Float(
        string="Subtotal",
        compute='_get_line_values',
        digits=dp.get_precision('Account'),
        store=True,
    )
    nb_lines = fields.Integer(
        string=u"Number of Move Lines",
        compute='_get_line_values',
        store=True,
    )
    line_ids = fields.One2many(
        'account.payment_batch.move.line',
        'batch_line_id',
        string='Move Lines',
    )
    bank_id = fields.Many2one(
        "res.partner.bank",
        "Bank account",
    )

    bank_iban = fields.Char(
        related=('bank_id', 'acc_number'),
        string='IBAN',
        help='The IBAN set on the bank account set on this line.',
        readonly=True,
        store=True,
    )

    bank_bic = fields.Char(
        related=('bank_id', 'bank_bic'),
        string='BIC',
        help='The BIC of the bank account set on this line.',
        readonly=True,
        store=True,
    )

    bank_create_date = fields.Datetime(
        related=('bank_id', 'create_date'),
        string=u"Bank Account Created on",
        readonly=True,
        store=False,
    )

    bank_write_date = fields.Datetime(
        related=('bank_id', 'write_date'),
        string=u"Bank Account Last Updated on",
        readonly=True,
        store=False,
    )

    batch_state = fields.Selection(
        related=('batch_id', 'state')
    )

    @api.constrains('bank_id')
    @api.one
    def _ensure_bank_account(self):
        """Ensure bank accounts are set when outside of the "draft" state."""

        if (
            not self.bank_id and
            self.batch_id.state and
            self.batch_id.state != 'draft'
        ):
            raise exceptions.ValidationError(_(
                'Bank accounts must be set on every line.'
            ))

    @api.depends('line_ids')
    @api.one
    def _get_line_values(self):
        """
        For payments, the generated vouchers must be in debit. and for
        debit payments, in credit.
        For payments, the generated vouchers are supplier payments in
        debit. Credit lines for suppliers must be in debit in the
        vouchers.
        For debit payments, the generated vouchers are customer payments
        in credit. Debit lines for customers must be in credit in the
        vouchers.

        The subtotal written on the batch line is the total to pay in the
        associated voucher.
        For payment, it is a credit for the supplier, to which will be
        subtracted a debit in the voucher.
        For debit payments it is a debit for the customer, to which will be
        subtracted a credit in the voucher.
        """

        multiplier = 1 if self.batch_id.option == 'payment' else -1

        for line in self:
            line.subtotal = sum(line.line_ids.mapped(
                lambda l: (l.credit - l.debit) * multiplier
            ))
            line.nb_lines = len(line.line_ids)

    @api.multi
    def save(self):
        return True

    @api.multi
    def open_batch_line_form(self):
        self.ensure_one()
        res = self.env['ir.actions.act_window'].for_xml_id(
            'account_streamline', 'account_payment_batch_line_action'
        )
        res['res_id'] = self.id
        res['view_mode'] = 'form'
        return res

    @api.multi
    def unlink(self):
        """Override to:
        - Also delete accounting entry wrappers.
        """

        self.mapped('line_ids').unlink()

        return super(PaymentBatchLine, self).unlink()


class PaymentBatchMoveLine(models.BaseModel):
    _name = 'account.payment_batch.move.line'

    move_line_id = fields.Many2one(
        'account.move.line',
        u"Move Line",
        required=True,
        delegate=True,
        ondelete='cascade'
    )

    batch_line_id = fields.Many2one(
        'account.payment_batch.line',
        u"Batch Line",
        required=True,
        ondelete='restrict'  # Needs to go through the unlink method
    )

    attachment_ids = fields.Many2many(
        compute='_get_attachment_ids',
        comodel_name='ir.attachment',
        string=u"File Attachment",
        readonly=True,
        store=False,
    )

    attachment_data = fields.Binary(
        compute='_get_attachment_data',
        string=u"File Attachment",
        readonly=True,
        store=False,
    )

    attachment_fname = fields.Binary(
        compute='_get_attachment_fname',
        string=u"File Attachment",
        readonly=True,
        store=False,
    )

    @api.one
    def _get_attachment_ids(self):
        attachments = self.env['ir.attachment']
        for obj in self.move_id, self.move_id.object_reference:
            if obj:
                attachments |= attachments.search([
                    ('res_model', '=', obj._name), ('res_id', '=', obj.id)
                ])
        self.attachment_ids = attachments

    @api.one
    def _get_attachment_data(self):
        attachments = self.attachment_ids
        if len(attachments) < 1:
            res = False
        elif len(attachments) == 1:
            res = attachments.datas
        else:
            zip_buffer = io.BytesIO()
            with zipfile.ZipFile(zip_buffer, 'w') as zip_file:
                for att in attachments:
                    zip_file.writestr(att.name, base64.b64decode(att.datas))
            res = base64.b64encode(zip_buffer.getvalue())
            zip_buffer.close()
        self.attachment_data = res

    @api.one
    def _get_attachment_fname(self):
        attachments = self.attachment_ids
        if len(attachments) < 1:
            res = ''
        elif len(attachments) == 1:
            res = attachments.name
        else:
            res = '{}.zip'.format(self.move_id.name)
        self.attachment_fname = res

    @api.multi
    def download_attachments(self):
        self.ensure_one()
        params = urllib.urlencode({
            'model': self._name, 'id': self.id, 'field': 'attachment_data',
            'filename_field': 'attachment_fname'
        })
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/binary/saveas?{}'.format(params),
            'target': 'self',
        }

    @api.multi
    def unlink(self):
        self.mapped('move_line_id').write({'payment_batch_id': False})
        return super(PaymentBatchMoveLine, self).unlink()
