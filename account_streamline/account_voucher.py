from openerp import _
from openerp import api
from openerp import models
from openerp.osv import fields as odoo7_fields
from openerp.osv import osv as odoo7_osv
from openerp.tools import float_compare


class AccountingVoucher(models.Model):
    """Customize accounting vouchers:
    - Default payment method.

    We also customize this model in an Odoo 7 way; see further below.
    """

    _inherit = 'account.voucher'

    @api.model
    def default_get(self, fields_list):
        """Override to set default payment methods when we have some.

        Rather than forcing our journal via a default value, we provide it
        through the context; this plays well with various default getters in
        the base "account_voucher" module that expect it there in the context.
        """

        default_get_context = self.env.context.copy()

        default_journal = (
            self.env['account.config.settings'].read_accstreamline_dcpm()
            if self.env.context.get('type') == 'receipt'
            else self.env['account.config.settings'].read_accstreamline_dspm()
        )
        if default_journal:
            default_get_context['journal_id'] = default_journal.id

        return (
            super(AccountingVoucher, self.with_context(default_get_context))
            .default_get(fields_list)
        )


class AccountingVoucher_Odoo7(odoo7_osv.Model):
    _inherit = "account.voucher"

    def _get_iban(self, cr, uid, ids, field_name, arg, context=None):
        """Empty on purpose - to be overridden."""
        return {voucher_id: '' for voucher_id in ids}

    def _get_rem_letter_bot(self, cr, uid, ids, field_name, arg, context=None):
        """Empty on purpose - to be overridden."""
        return {voucher_id: '' for voucher_id in ids}

    _columns = {
        'iban': odoo7_fields.function(
            _get_iban,
            method=True,
            type='char',
        ),

        'remittance_letter_bottom': odoo7_fields.function(
            _get_rem_letter_bot,
            method=True,
            type='text',
        ),

        'partner_email': odoo7_fields.related(
            'partner_id',
            'email',
            type='char',
            string=_('Partner email'),  # TODO No need for "_".
        ),
        'payment_batch_line_id': odoo7_fields.many2one(
            'account.payment_batch.line',
            string=u"Payment Batch Line",
            readonly=True,
        ),
        'payment_batch_id': odoo7_fields.related(
            'payment_batch_line_id',
            'batch_id',
            type='many2one',
            obj='account.payment_batch',
            string=u"Payment Batch",
            readonly=True,
            store=False,
        ),
    }

    def print_payment_suggestion(self, cr, uid, ids, context=None):
        ''' Create a payment_suggestion object to which the report should be
        attached; let it handle the rest of the logic. '''

        return (self.pool.get('payment.suggestion')
                .print_payment_suggestion(cr, uid, ids, context=context))

    def voucher_move_line_create(
        self, cr, uid, voucher_id, line_total, move_id, company_currency,
        current_currency, context=None,
    ):
        """Create one account move line, on the given account move, per voucher
        line where amount is not 0.0.
        It returns Tuple with tot_line what is total of difference between
        debit and credit and a list of lists with ids to be reconciled with
        this format (total_deb_cred,list_of_lists).

        :param voucher_id: Voucher id what we are working with
        :param line_total: Amount of the first line, which correspond to the
        amount we should totally split among all voucher lines.
        :param move_id: Account move wher those lines will be joined.
        :param company_currency: id of currency of the company to which the
        voucher belong
        :param current_currency: id of currency of the voucher
        :return: Tuple build as (remaining amount not allocated on voucher
        lines, list of account_move_line created in this method)
        :rtype: tuple(float, list of int)
        """

        if context is None:
            context = {}
        move_line_obj = self.pool.get('account.move.line')
        currency_obj = self.pool.get('res.currency')
        tax_obj = self.pool.get('account.tax')
        tot_line = line_total
        rec_lst_ids = []

        date = self.read(
            cr, uid, voucher_id, ['date'], context=context,
        )['date']
        ctx = context.copy()
        ctx.update({'date': date})
        voucher = self.pool.get('account.voucher').browse(
            cr, uid, voucher_id, context=ctx)
        voucher_currency = (
            voucher.journal_id.currency or
            voucher.company_id.currency_id
        )
        ctx.update({
            'voucher_special_currency_rate': (
                voucher_currency.rate * voucher.payment_rate
            ),
            'voucher_special_currency': (
                voucher.payment_rate_currency_id and
                voucher.payment_rate_currency_id.id or False
            ),
        })
        prec = self.pool.get('decimal.precision').precision_get(
            cr, uid, 'Account')
        for line in voucher.line_ids:
            # create one move line per voucher line where amount is not 0.0 AND
            # (second part of the clause) only if the original move line was
            # not having debit = credit = 0 (which is a legal value)
            if not line.amount and not (
                line.move_line_id and
                not float_compare(
                    line.move_line_id.debit, line.move_line_id.credit,
                    precision_rounding=prec,
                ) and
                not float_compare(
                    line.move_line_id.debit, 0.0,
                    precision_rounding=prec,
                )
            ):
                continue
            # convert the amount set on the voucher line into the currency of
            # the voucher's company this calls res_curreny.compute() with the
            # right context, so that it will take either the rate on the
            # voucher if it is relevant or will use the default behaviour
            amount = self._convert_amount(
                cr, uid,
                line.untax_amount or line.amount,
                voucher.id,
                context=ctx,
            )
            # if the amount encoded in voucher is equal to the amount
            # unreconciled, we need to compute the currency rate difference
            if line.amount == line.amount_unreconciled:
                if not line.move_line_id:
                    raise odoo7_osv.except_osv(
                        _('Wrong voucher line'),
                        _("The invoice you are willing to pay is not valid "
                          "anymore."),
                    )
                sign = voucher.type in ('payment', 'purchase') and -1 or 1
                currency_rate_difference = sign * (
                    line.move_line_id.amount_residual - amount
                )
            else:
                currency_rate_difference = 0.0
            move_line = {
                'journal_id': voucher.journal_id.id,
                'period_id': voucher.period_id.id,
                'name': line.name or '/',
                'account_id': line.account_id.id,
                'move_id': move_id,
                'partner_id': voucher.partner_id.id,
                # curency_id cannot be False, either it is the currency on the
                # move line, either it is the company currency
                'currency_id': line.move_line_id and (
                    company_currency != line.move_line_id.currency_id.id and
                    line.move_line_id.currency_id.id
                ) or company_currency,
                'analytic_account_id': (
                    line.account_analytic_id and line.account_analytic_id.id
                    or False
                ),
                'quantity': 1,
                'credit': 0.0,
                'debit': 0.0,
                'date': voucher.date
            }
            if amount < 0:
                amount = -amount
                if line.type == 'dr':
                    line.type = 'cr'
                else:
                    line.type = 'dr'

            if (line.type == 'dr'):
                tot_line += amount
                move_line['debit'] = amount
            else:
                tot_line -= amount
                move_line['credit'] = amount

            if voucher.tax_id and voucher.type in ('sale', 'purchase'):
                move_line.update({
                    'account_tax_id': voucher.tax_id.id,
                })

            if move_line.get('account_tax_id', False):
                tax_data = tax_obj.browse(
                    cr, uid, [move_line['account_tax_id']], context=context,
                )[0]
                if not (tax_data.base_code_id and tax_data.tax_code_id):
                    raise odoo7_osv.except_osv(
                        _('No Account Base Code and Account Tax Code!'),
                        _(
                            "You have to configure account base code and "
                            "account tax code on the '%s' tax!"
                        ) % tax_data.name
                    )

            # compute the amount in foreign currency
            foreign_currency_diff = 0.0
            amount_currency = False
            if line.move_line_id:
                # We want to set it on the account move line as soon as the
                # original line had a foreign currency amount_currency must be
                # set, even when move line currency equals company currency
                if line.move_line_id.currency_id:
                    # we compute the amount in that foreign currency.
                    if line.move_line_id.currency_id.id == current_currency:
                        # if the voucher and the voucher line share the same
                        # currency, there is no computation to do
                        sign = (
                            (move_line['debit'] - move_line['credit']) < 0 and
                            -1 or 1
                        )
                        amount_currency = sign * (line.amount)
                    else:
                        # if the rate is specified on the voucher, it will be
                        # used thanks to the special keys in the context
                        # otherwise we use the rates of the system
                        amount_currency = currency_obj.compute(
                            cr, uid, company_currency,
                            line.move_line_id.currency_id.id,
                            move_line['debit'] - move_line['credit'],
                            context=ctx,
                        )
                if line.amount == line.amount_unreconciled:
                    sign = voucher.type in ('payment', 'purchase') and -1 or 1
                    foreign_currency_diff = sign * (
                        abs(amount_currency) -
                        line.move_line_id.amount_residual_currency
                    )

            move_line['amount_currency'] = amount_currency
            voucher_line = move_line_obj.create(cr, uid, move_line)
            rec_ids = [voucher_line, line.move_line_id.id]

            if not currency_obj.is_zero(
                cr, uid, voucher.company_id.currency_id,
                currency_rate_difference,
            ):
                # Change difference entry in company currency
                exch_lines = self._get_exchange_lines(
                    cr, uid, line, move_id, currency_rate_difference,
                    company_currency, current_currency, context=context)
                new_id = move_line_obj.create(cr, uid, exch_lines[0], context)
                move_line_obj.create(cr, uid, exch_lines[1], context)
                rec_ids.append(new_id)

            if (
                line.move_line_id and
                line.move_line_id.currency_id and
                not currency_obj.is_zero(
                    cr, uid, line.move_line_id.currency_id,
                    foreign_currency_diff,
                )
            ):
                # Change difference entry in voucher currency
                move_line_foreign_currency = {
                    'journal_id': line.voucher_id.journal_id.id,
                    'period_id': line.voucher_id.period_id.id,
                    'name': _('change') + ': ' + (line.name or '/'),
                    'account_id': line.account_id.id,
                    'move_id': move_id,
                    'partner_id': line.voucher_id.partner_id.id,
                    'currency_id': line.move_line_id.currency_id.id,
                    'amount_currency': (-1 * foreign_currency_diff),
                    'quantity': 1,
                    'credit': 0.0,
                    'debit': 0.0,
                    'date': line.voucher_id.date,
                }
                new_id = move_line_obj.create(
                    cr, uid, move_line_foreign_currency, context=context,
                )
                rec_ids.append(new_id)

            if line.move_line_id.id:
                rec_lst_ids.append(rec_ids)

        return (tot_line, rec_lst_ids)

    def cancel_voucher(self, cr, uid, ids, context=None):
        """Unmark the native move lines
        """
        vouchers = self.browse(cr, uid, ids, context=context)

        move_line_ids = []
        for voucher in vouchers:
            move_line_ids.extend(
                [line.move_line_id.id for line in voucher.line_dr_ids]
            )
            move_line_ids.extend(
                [line.move_line_id.id for line in voucher.line_cr_ids]
            )

        self.pool.get('account.move.line').write(
            cr, uid,
            move_line_ids,
            {'payment_batch_id': False},
            context=context
        )

        return super(AccountingVoucher_Odoo7, self).cancel_voucher(
            cr, uid, ids, context=context
        )
