import time

import openerp.exceptions
import openerp.fields
import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid

AMOUNT = 42.0


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_accounts(self):
        """Create accounting accounts for use in further tests.
        """

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': time.strftime('%Y-01-01'),
            'date_stop': time.strftime('%Y-12-31'),
            'period': 'month',
        }).execute()

        top_normal_account = self.env['account.account'].search(
            [('type', '=', 'other')],
            limit=1,
        )
        self.assertTrue(top_normal_account)

        top_payable_account = self.env['account.account'].search(
            [('type', '=', 'payable')],
            limit=1,
        )
        self.assertTrue(top_payable_account)

        top_receivable_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )
        self.assertTrue(top_receivable_account)

        (
            self.memory.normal_account,
            self.memory.payable_account,
            self.memory.receivable_account,
        ) = self.createAndTest('account.account', [
            {
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': top_normal_account.id,
                'reconcile': True,
                'type': top_normal_account.type,
                'user_type': top_normal_account.user_type.id,
            },
            {
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': top_payable_account.id,
                'reconcile': True,
                'type': top_payable_account.type,
                'user_type': top_payable_account.user_type.id,
            },
            {
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': top_receivable_account.id,
                'reconcile': True,
                'type': top_receivable_account.type,
                'user_type': top_receivable_account.user_type.id,
            },
        ])

    def test_0001_find_accounting_journals(self):
        """Ensure we have accounting journals for use in further tests.
        """

        self.memory.payment_journal = self.env['account.journal'].search(
            [('type', '=', 'bank')], limit=1,
        )
        self.memory.purchase_journal = self.env['account.journal'].search(
            [('type', '=', 'purchase')], limit=1,
        )
        self.memory.sales_journal = self.env['account.journal'].search(
            [('type', '=', 'sale')], limit=1,
        )
        self.memory.purchase_refund_journal = \
            self.env['account.journal'].search(
                [('type', '=', 'purchase_refund')], limit=1,
            )
        self.assertTrue(self.memory.payment_journal)
        self.assertTrue(self.memory.purchase_journal)
        self.assertTrue(self.memory.sales_journal)
        self.assertTrue(self.memory.purchase_refund_journal)

    def test_0002_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partners = self.createAndTest('res.partner', [
            {
                'name': genUuid(),
            },
        ])

    def test_0100_create_accounting_entries(self):
        """Create and validate accounting entries.
        """

        OPERATION_TYPES = ['credit', 'debit']

        def mirror(operation):
            # TODO Better variable name.
            l = OPERATION_TYPES  # noqa: E741
            if operation in l:
                return [
                    op
                    for op in l
                    if op != operation
                ][0]
            else:
                return False

        # Keep the accounting entries to pay here, so we can check them later
        # on.
        self.memory.acc_entries_to_pay = self.env['account.move.line']

        # Create accounting documents, that will hold accounting entries.
        acc_docs = self.createAndTest('account.move', [
            {
                'journal_id': getattr(
                    self.memory, '%s_journal' % journal
                ).id,
            } for journal in ['purchase', 'sales', 'sales', 'purchase_refund']
        ])

        self.memory.acc_entries_to_pay = []

        for acc_doc, operation, account in zip(
            acc_docs, ['debit', 'credit', 'debit', 'credit'],
            ['payable', 'receivable', 'receivable', 'payable']
        ):

            # Add accounting entries into each accounting document.
            acc_entries = self.createAndTest('account.move.line', [
                {
                    'account_id': self.memory.normal_account.id,
                    operation: AMOUNT,
                    'date_maturity': openerp.fields.Date.today(),
                    mirror(operation): 0.0,
                    'move_id': acc_doc.id,
                    'name': genUuid(),
                },
                {
                    'account_id': (
                        getattr(self.memory, '%s_account' % account).id
                    ),
                    operation: 0.0,
                    'date_maturity': openerp.fields.Date.today(),
                    mirror(operation): AMOUNT,
                    'move_id': acc_doc.id,
                    'name': genUuid(),
                    'partner_id': self.memory.partners[0].id,
                },
            ])

            self.memory.acc_entries_to_pay.append(acc_entries[1])

            # Validate the accounting document.
            self.assertEqual(acc_doc.state, 'draft')
            acc_doc.button_validate()
            self.assertEqual(acc_doc.state, 'posted')

        self.assertEqual(len(self.memory.acc_entries_to_pay), 4)

    def test_0200_create_payment_batch(self):
        """Create a payment batch by simulating a dialog box; ensure it
        contains accounting documents validated so far.
        """

        self.memory.payment_batch = []

        dlgs = self.createAndTest(
            'account.payment_batch.selection',
            [
                {
                    'date_maturity': openerp.fields.Date.today(),
                    'journal_id': self.memory.payment_journal.id,
                    'mode': 'both',
                },
                {
                    'date_maturity': openerp.fields.Date.today(),
                    'journal_id': self.memory.payment_journal.id,
                    'mode': 'client_invoices',
                    'option': 'debit_payment',
                },
                {
                    'date_maturity': openerp.fields.Date.today(),
                    'journal_id': self.memory.payment_journal.id,
                    'mode': 'supplier_refunds',
                    'option': 'debit_payment',
                },
            ],
        )

        for dlg, option in zip(dlgs, ['debit_payment', 'payment', 'payment']):

            with self.assertRaises(openerp.exceptions.ValidationError):
                dlg.option = option

            dlg_ret = dlg.with_context(unit_tests=True).generate_batch()

            # The dialog box returns an action to display the new payment
            # batch; pick it from there.
            # 'res_id': payment_batch_id,
            payment_batch_id = dlg_ret['res_id']
            self.assertTrue(payment_batch_id)
            payment_batch = self.env['account.payment_batch'].browse(
                payment_batch_id
            )

            for line in payment_batch.line_ids:

                if all([
                    (
                        line_id.move_line_id.partner_id.id !=
                        self.memory.partners[0].id
                    )
                    for line_id in line.line_ids
                ]):
                    line.unlink()

            self.memory.payment_batch.append(payment_batch)

        options = ['payment', 'debit_payment', 'debit_payment']

        # Check the payment batch.
        for payment_batch, acc_entry, option in zip(
            self.memory.payment_batch,
            [
                set([self.memory.acc_entries_to_pay[0].id]),
                set([
                    self.memory.acc_entries_to_pay[1].id,
                    self.memory.acc_entries_to_pay[2].id
                ]),
                set([self.memory.acc_entries_to_pay[3].id]),
            ],
            options
        ):

            self.assertEqual(
                set(
                    payment_batch.line_ids.mapped('line_ids').
                    mapped('move_line_id').ids
                ),
                acc_entry
            )

            self.assertEqual(payment_batch.state, 'draft')
            self.assertEqual(payment_batch.option, option)
            self.assertFalse(payment_batch.voucher_ids)

    def test_0201_ensure_payment_batch_bank_account_required(self):
        """Ensure the payment batch cannot be validated without setting up a
        bank account.
        """

        for i, payment_batch in enumerate(self.memory.payment_batch):
            if i != 1:
                with self.assertRaises(openerp.exceptions.ValidationError):
                    payment_batch.signal_workflow('pay')

    def test_0202_set_payment_batch_bank_account(self):
        """Create a bank account and set it onto the payment batch.
        """

        bank_account, = self.createAndTest(
            'res.partner.bank',
            [{
                'acc_number': genUuid(),
                'partner_id': self.memory.partners[0].id,
                'state': 'bank',
            }],
        )

        for payment_batch in self.memory.payment_batch:
            for pbline in payment_batch.line_ids:
                pbline.bank_id = bank_account.id

    def test_0203_validate_payment_batch(self):
        """Validate the payment batch; ensure payment vouchers are created.
        """

        i = 0
        for payment_batch, line_type, voucher_type in zip(
            self.memory.payment_batch,
            ['dr', 'cr', 'cr'],
            ['payment', 'receipt', 'receipt'],
        ):
            i += 1

            if i == 2:

                # Check the control on the debit amount of vouchers generated
                # for debit payments, which must be positive.
                with self.assertRaises(openerp.exceptions.ValidationError):
                    payment_batch.signal_workflow('pay')

                line_to_delete = [
                    m_line
                    for line in payment_batch.line_ids
                    for m_line in line.line_ids
                    if m_line.move_line_id.credit > 0
                ][0]

                line_to_delete.unlink()

                payment_batch.signal_workflow('pay')

            self.assertEqual(payment_batch.state, 'paid')

            voucher = payment_batch.voucher_ids[0]

            self.assertEqual(voucher.type, voucher_type)

            line = getattr(voucher, 'line_%s_ids' % line_type)[0]

            self.assertEqual(line.amount, AMOUNT)
            self.assertEqual(line.amount_unreconciled, 0)

            self.assertEqual(voucher.writeoff_amount, 0)

        # Ensure accounting entries we expected to be paid have been paid.
        for i, acc_entry in enumerate(self.memory.acc_entries_to_pay):
            if i != 2:
                self.assertTrue(acc_entry.reconcile_id)

    def test_0204_ensure_payment_batch_bank_account_required_line_change(self):
        """Ensure a bank account cannot be unset once the payment batch is out
        of the "draft" state.
        """

        for payment_batch in self.memory.payment_batch:
            with self.assertRaises(openerp.exceptions.ValidationError):
                payment_batch.line_ids[0].bank_id = False
