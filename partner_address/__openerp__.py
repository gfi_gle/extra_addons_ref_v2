# -*- coding: utf-8 -*-
##############################################################################
#
#    Partner Address, for OpenERP
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#
#    Anael LORIMIER <anael.lorimier@xcg-consulting.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "partner_address",
    "version": "1.1",
    "author": "XCG",
    "category": '',
    "description": """
    This module add french region/departments/cities/postal code to partner address.
    This module brings a list of all cities and postal code from France
    It also implements a better way to display postal address using AFNOR standard

    Created by XCG Consulting: http://www.xcg-consulting.fr
    """,
    'website': 'http://www.xcg-consulting.fr',
    'init_xml': [],
    "depends": ['base'],
    'images': [],
    "data": [
        'views/res_city.xml',
        'views/res_department.xml',
        'views/res_partner.xml',

        'security/ir.model.access.csv',

        'wizards/address_validation.xml',
        'wizards/address_confirmation.xml',
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': True,
    'css': [],
    'js': ['static/src/js/phone_widget.js'],
    'qweb': ['static/src/xml/phone_widget.xml'],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
