from openerp.osv import osv, fields


class res_partner_wizard_address_confirmation(osv.TransientModel):
    _name = 'res.partner.wizard_address_confirmation'

    _columns = {
        'address': fields.text(string=u"Address"),
    }

    def validate(self, cr, uid, ids, context=None):
        return {'ir.actions.act_window_close'}
