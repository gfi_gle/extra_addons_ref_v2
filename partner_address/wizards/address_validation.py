from openerp.osv import osv, fields


class res_partner_wizard_address_validation(osv.TransientModel):
    _name = 'res.partner.wizard_address_validation'

    _columns = {
        'city_zip_ids': fields.one2many(
            'res.partner.wizard_address_validation.city_zip',
            'wizard_id',
            string=u"City/ZIP",
        ),
    }

    def validate(self, cr, uid, ids, context=None):
        form = self.browse(cr, uid, ids, context=context)[0]

        city_zip_ids = form.city_zip_ids

        selected_city_zip = [
            city_zip
            for city_zip
            in city_zip_ids
            if city_zip.is_selected
        ]

        if len(selected_city_zip) != 1:
            raise osv.except_osv(
                _(u"Error"),
                _(u"Please select only one City/ZIP.")
            )

        city_zip = selected_city_zip[0]
        active_obj = self.pool[context['active_model']]
        active_id = context['active_id']
        active_obj.write(
            cr, uid,
            active_id, {
                'city': city_zip.city,
                'zip': city_zip.zip_code
            },
            context=context
        )
        active_obj.validate_address(cr, uid, [active_id], context=context)
        return {'ir.actions.act_window_close'}



class res_partner_wizard_address_validation_city_zip(osv.TransientModel):
    _name = 'res.partner.wizard_address_validation.city_zip'

    _columns = {
        'is_selected': fields.boolean(string=u"Select"),
        'zip_code': fields.char(string=u"ZIP Code"),
        'city': fields.char(string=u"City"),
        'wizard_id': fields.many2one(
            'res.partner.wizard_address_validation',
            string=u"Wizard",
        ),
    }