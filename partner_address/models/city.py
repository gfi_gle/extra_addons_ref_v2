# coding=utf-8
from openerp.osv import osv, fields
from openerp.tools.translate import _


_enum_city_status = [
    ('commune', _(u"Simple Commune")),
    ('country_seat_canton', _(u"Country Seat Canton")),
    ('prefecture', _(u"Prefecture")),
    ('regional_prefecture', _(u"Regional Prefecture")),
    ('subprefecture', _(u"Subprefecture")),
    ('capital', _(u"Capital")),
]


class res_zip_code(osv.Model):
    _name = 'res.zip_code'

    _rec_name = 'code'

    _columns = {
        'code': fields.char(string=u"Code", size=8)
    }


class res_city(osv.Model):
    _name = 'res.city'

    _columns = {
        'name': fields.char(string=u"Name", size=256),
        'insee_code': fields.char(string=u"INSEE Code", size=5),
        'zip_code': fields.many2many(
            'res.zip_code',
            'res_city_zip_code_rel_',
            'city_id',
            'zip_code_id',
            string=u"Zip Code",
        ),
        'department_id': fields.many2one('res.department', string=u"Department"),
        'region_id': fields.related(
            'department_id',
            'region_id',
            string=u"Region",
            type='many2one',
            store=False,
            readonly=True,
            obj='res.country.state',
        ),
        # Status: Is this relevant ?
        'status': fields.selection(_enum_city_status, string=u"Status"),
        'average_altitude': fields.integer(string=u"Average Altitude", help=u"In meters"),
        'surface_area': fields.integer(string=u"Surface Area", help=u"In km²"),
        'population': fields.float(string=u"Population", help=u"By thousand"),
        'geometry_id': fields.many2one('res.geometry', string=u"Geometry"),
    }