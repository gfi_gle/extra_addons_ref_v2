from openerp.osv import osv, fields


class res_department(osv.Model):
    _name = 'res.department'

    _columns = {
        'name': fields.char(string=u"Name", size=256),
        'region_id': fields.many2one('res.country.state', string=u"Region"),
        'code': fields.char(string=u"Code", size=16),
    }