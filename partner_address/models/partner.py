# coding=utf-8
from openerp.osv import osv, fields
from openerp.tools.translate import _


class res_partner(osv.Model):
    _name = 'res.partner'
    _inherit = 'res.partner'

    def _get_standardized_address(self, cr, uid, ids, name, args, context=None):
        brs = self.browse(cr, uid, ids, context=context)
        res = {}
        for br in brs:
            # We get the country relative address format
            if not br.country_id:
                res[br.id] = u""
                continue
            addr_format = br.country_id.address_format
            if br.use_parent_address:
                parent = br.parent_id
                text = unicode(parent.name.upper()) + "\n"
                text += unicode(br.name.upper()) + "\n"
                city_cedex = unicode(parent.standardized_city or "") + u" "
                if parent.has_cedex:
                    city_cedex += u" " + unicode(parent.cedex)
                text += addr_format % {
                    'street': unicode(parent.standardized_street or ""),
                    'street2': unicode(parent.standardized_street2 or ""),
                    'city': city_cedex,
                    'zip': unicode(parent.standardized_zip_code or ""),
                    'country_name': unicode(parent.country_id.name or "").upper(),
                    'state_code': unicode(parent.state_id.code or "").upper(),
                    'state_name': unicode(parent.state_id.name or "").upper(),
                }
            else:
                text = unicode(br.name.upper()) + "\n"
                city_cedex = unicode(br.standardized_city or "") + u" "
                if br.has_cedex:
                    city_cedex += u" " + unicode(br.cedex)
                text += addr_format % {
                    'street': unicode(br.standardized_street or ""),
                    'street2': unicode(br.standardized_street2 or ""),
                    'city': city_cedex,
                    'zip': unicode(br.standardized_zip_code or ""),
                    'state_code': unicode(br.state_id.code or "").upper(),
                    'state_name': unicode(br.state_id.name or "").upper(),
                    'country_name': unicode(br.country_id.name or "").upper(),
                    }
            res[br.id] = text
        return res

    _columns = {
        'has_cedex': fields.boolean(string=u"Has Cedex"),
        'cedex': fields.char(string=u"Cedex", size=64),
        'standardized_street': fields.char(string=u"Standardized Street", size=128),
        'standardized_street2': fields.char(string=u"Standardized Street 2", size=128),
        'standardized_zip_code': fields.char(string=u"Standardized ZIP Code", size=64),
        'standardized_city': fields.char(string=u"Standardized City", size=128),
        'standardized_address_block': fields.function(
            _get_standardized_address,
            type='text',
            string=u"Standardized Address"
        ),
    }

    @staticmethod
    def _set_standardized_address(vals):
        if 'street' in vals:
            vals['standardized_street'] = vals['street'] and vals['street'][:38].upper()
        if 'street2' in vals:
            vals['standardized_street2'] = (vals['street2'] and vals['street2'][:38].upper()) or u""
        if 'city' in vals:
            vals['standardized_city'] = vals['city'] and vals['city'][:38].upper()
        if 'zip' in vals:
            vals['standardized_zip_code'] = vals['zip'] and vals['zip'][:5]

    def create(self, cr, uid, vals, context=None):
        self._set_standardized_address(vals)
        return super(res_partner, self).create(cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        self._set_standardized_address(vals)
        return super(res_partner, self).write(cr, uid, ids, vals, context=context)

    def validate_address(self, cr, uid, ids, context=None):
        for i in ids:
            vals = self.read(cr, uid, i, ['street', 'street2', 'city', 'zip'], context=context)
            self.write(cr, uid, i, vals, context=context)
        return True

    def _get_city_list(self, cr, uid, ids, context):
        # Get the list of city with the same zip code and city name
        #TODO: Should we keep ids, or just get one id...
        br = self.browse(cr, uid, ids[0], context=context)
        city_osv = self.pool['res.city']
        res = []
        if br.standardized_city and not br.standardized_zip_code:
            # If we only get the city name, search first with name, then name%, then %name%
            cr.execute('SELECT id FROM res_city WHERE name ilike \'%s\'' % br.standardized_city)
            res.extend([x[0] for x in cr.fetchall()])
            cr.execute('SELECT id FROM res_city WHERE name ilike \'%s%%\'' % br.standardized_city)
            res.extend([x[0] for x in cr.fetchall()])
            cr.execute('SELECT id FROM res_city WHERE name ilike \'%%%s%%\'' % br.standardized_city)
            res.extend([x[0] for x in cr.fetchall()])

        else:
            # Get zips
            cr.execute('SELECT id FROM res_zip_code WHERE code ilike \'%s%%\'' % br.standardized_zip_code)
            zip_ids = [x[0] for x in cr.fetchall()]
            if br.standardized_zip_code and not br.standardized_city:
                # If we only get the zip code, search with zip%
                res.extend(city_osv.search(cr, uid, ['zip_code', 'in', zip_ids], context=context))
            else:
                cr.execute('SELECT id FROM res_city WHERE name ilike \'%s\'' % (
                    br.standardized_city,
                ))
                res.extend([x[0] for x in cr.fetchall()])
                cr.execute('SELECT id FROM res_city WHERE name ilike \'%s%%\'' % (
                    br.standardized_city,
                ))
                res.extend([x[0] for x in cr.fetchall()])
                cr.execute('SELECT id FROM res_city WHERE name ilike \'%%%s%%\'' % (
                    br.standardized_city,
                ))
                res.extend([x[0] for x in cr.fetchall()])

        seen = set()
        seen_add = seen.add
        res = [x for x in res if not (x in seen or seen_add(x))]
        return res

    def launch_validation_wizard(self, cr, uid, ids, context=None):
        if len(ids) > 1:
            raise osv.except_osv(
                _(u"Programming Error"),
                _(u"This method should not be called with multiple ids.")
            )
        self.validate_address(cr, uid, ids, context)
        city_list = self._get_city_list(cr, uid, ids, context)
        partner = self.browse(cr, uid, ids, context=context)[0]
        if not city_list:
            # Return a message to prevent the user that his entries are not in our database.
            # Ask him if he wants to keep it.
            context['default_address'] = partner.standardized_address_block
            action = {
                'type': 'ir.actions.act_window',
                'res_model': 'res.partner.wizard_address_confirmation',
                'name': _(u"Address Confirmation"),
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'context': context,
            }
        else:
            city_obj = self.pool['res.city']
            # Display the propositions to the user and let him choose one, including the original.
            context['default_city_zip_ids'] = []
            for city in city_obj.browse(cr, uid, city_list):
                context['default_city_zip_ids'].extend([
                    (0, 0, {'city': city.name, 'zip_code': zc.code})
                    for zc in city.zip_code
                ])
            action = {
                'type': 'ir.actions.act_window',
                'name': _(u"Address Validation"),
                'res_model': 'res.partner.wizard_address_validation',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'context': context,
            }
        return action
