from openerp.osv import osv, fields


# noinspection PyPep8Naming
class res_geometry(osv.Model):
    _name = 'res.geometry'

    _columns = {
        '2d_point': fields.char(string=u"2d Point"),
        'shape': fields.char(string=u"Shape"),
    }