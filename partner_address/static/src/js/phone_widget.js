openerp.partner_address = function (instance) {
    "use strict";

    instance.web.form.widgets.add('phone', 'instance.partner_address.FieldPhone');

    instance.partner_address.FieldPhone = instance.web.form.FieldChar.extend({
        template: 'FieldPhone',
        initialize_content: function() {
            this._super();
            var $button = this.$el.find('button');
            $button.click(this.on_button_clicked);
            this.setupFocus($button);
        },
        render_value: function() {
            if (!this.get("effective_readonly")) {
                this._super();
            } else {
                this.$el.find('a')
                        .attr('href', 'tel:' + this.get('value'))
                        .text(this.get('value') || '');
            }
        },
        on_button_clicked: function() {
            if (!this.get('value') || !this.is_syntax_valid()) {
                this.do_warn(_t("Phone Error"), _t("Can't call an invalid phone number"));
            } else {
                location.href = 'tel:' + this.get('value');
            }
        }
    });
};