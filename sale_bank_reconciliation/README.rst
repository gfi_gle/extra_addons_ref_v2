Sales bank reconciliation
=========================

Add a step in sales orders for certain payment methods in which they await bank
statements that can then be matched against sales order lines to reconcile
them.
