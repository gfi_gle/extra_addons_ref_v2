from collections import OrderedDict
from openerp import _
from openerp import api
from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class SalesOrder(models.Model):
    """Adapt sales orders to bank reconciliation tracking.
    """

    _inherit = 'sale.order'

    # The reversed n:n field exists in sales bank trackers.
    bank_tracker_ids = fields.Many2many(
        comodel_name='sale.bank_tracker',
        relation='sale_bank_tracker_sale_order_rel',
        column1='order_id',
        column2='tracker_id',
        string='Bank trackers',
        copy=False,
        help='The bank trackers linked to this sales orders.',
        readonly=True,
    )

    # Override to add an element into the selection list.
    state = fields.Selection(
        selection_add=[('awaiting_payment', 'Awaiting payment')],
    )

    def fields_get(
        self, cr, uid, allfields=None, context=None, write_access=True,
    ):
        """Override to:
        - Properly translate added selection items (Odoo bug).
        """

        ADDED_SEL_ITEMS = {
            'state': {
                'awaiting_payment': _('Awaiting payment'),
            },
        }

        ret = super(SalesOrder, self).fields_get(
            cr, uid, allfields=allfields, context=context,
            write_access=write_access,
        )

        # ret: { field-name: { 'selection': sel-items } }

        for field_name, sel_items in ADDED_SEL_ITEMS.iteritems():

            field_info = ret.get(field_name)
            if field_info:

                sel_data = field_info.get('selection')
                if sel_data:

                    # sel_data is a list of tuples; convert to a dictionary to
                    # update it, then back to a list of tuples.
                    sel_data = OrderedDict(sel_data)
                    sel_data.update(sel_items)
                    field_info['selection'] = sel_data.items()

        return ret

    @api.multi
    def wkf_awaiting_payment(self):
        """Switch the state to "awaiting_payment".
        """

        self.state = 'awaiting_payment'

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        """Override to adapt automatic payment instructions to the case here,
        where the sales bank tracker controls invoice payment. See the override
        in the "sale_invoice_payment" addon for more information.
        """

        ret = super(SalesOrder, self)._prepare_invoice(
            cr, uid, order, lines, context=context,
        )

        payment_method = order.payment_method_id
        if (
            payment_method and
            payment_method.bank_tracked and
            payment_method.pay_sales_invoices
        ):
            # Disable per-invoice payment.
            ret.pop('payment_journal_id', None)
            ret.pop('payment_write_off_account_id', None)

            # Assign bank trackers, so payment can happen through them.
            ret['sales_bank_tracker_ids'] = [
                (6, 0, order.bank_tracker_ids.ids),
            ]

        return ret
