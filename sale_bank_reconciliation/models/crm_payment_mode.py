from openerp import fields
from openerp import models


class PaymentMode(models.Model):
    """Adapt payment methods to bank reconciliation tracking.
    """

    _inherit = 'crm.payment.mode'

    bank_tracked = fields.Boolean(
        string='Bank tracked',
        help=(
            'Require bank trackers to unlock sales that use this payment mode.'
        ),
    )
