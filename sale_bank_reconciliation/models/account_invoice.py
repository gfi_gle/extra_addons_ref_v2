from openerp import _
from openerp import api
from openerp import fields
from openerp import models


class AccountingInvoice(models.Model):
    """Handle payments via sales bank trackers.
    """

    _inherit = 'account.invoice'

    sales_bank_tracker_ids = fields.Many2many(
        comodel_name='sale.bank_tracker',
        relation='invoice_sales_bank_tracker_rel',
        column1='invoice_id',
        column2='tracker_id',
        string='Sales bank trackers',
        copy=False,
        help='The sales bank trackers linked to this invoice.',
        readonly=True,
    )

    @api.multi
    def invoice_validate(self):
        """Override to pay the invoice when a payment journal is defined.
        """

        ret = super(AccountingInvoice, self).invoice_validate()
        self.pay_via_sales_bank_trackers()
        return ret

    @api.one
    def pay_via_sales_bank_trackers(self):
        """Pay the invoice via sales bank trackers, when they are defined.
        """

        self.sales_bank_tracker_ids.pay_when_needed()
