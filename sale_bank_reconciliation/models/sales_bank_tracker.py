from openerp import api
from openerp import fields
from openerp import models
from openerp import exceptions
from openerp import _

import openerp.addons.decimal_precision as dp


class SalesBankTracker(models.Model):
    """Track bank reconciliation in relation to sales orders.
    """

    _name = 'sale.bank_tracker'

    _inherit = ['mail.thread']

    @api.depends('amount', 'sales_order_ids')
    @api.one
    def _get_total_amounts(self):
        """Sum sales order amounts and compute a delta."""

        self.sales_total_amount = sum(
            self.sales_order_ids.mapped('amount_total')
        )

        self.amount_gap = abs(self.amount - self.sales_total_amount)

    amount = fields.Float(
        string='Tracker amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help='Amount tracked by this sales bank tracker.',
        required=True,
    )

    amount_gap = fields.Float(
        compute=_get_total_amounts,
        string='Amount gap',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help=(
            'Delta between the amount tracked by this sales bank tracker and '
            'the sum from sales orders.'
        ),
        store=True,
    )

    client_id = fields.Many2one(
        comodel_name='res.partner',
        string='Client',
        domain=[('customer', '=', True)],
        ondelete='restrict',
        help='Client to filter sales orders on.',
    )

    date = fields.Date(
        string='Date',
        help='Date when this payment was registered.',
    )

    description = fields.Text(
        string='Description',
        help='Miscellaneous description of this sales bank tracker.',
    )

    @api.depends('sales_order_ids')
    @api.one
    def _has_sales_orders(self):
        self.has_sales_orders = bool(self.sales_order_ids)

    # Utility field used in views.
    has_sales_orders = fields.Boolean(
        compute=_has_sales_orders,
    )

    name = fields.Char(
        string='Name',
        help='The name of this sales bank tracker.',
        required=True,
        track_visibility='always',
    )

    sales_order_ids = fields.Many2many(
        comodel_name='sale.order',
        relation='sale_bank_tracker_sale_order_rel',
        column1='tracker_id',
        column2='order_id',
        string='Sales orders',
        help='Sales orders this bank tracker is for.',
    )

    sales_total_amount = fields.Float(
        compute=_get_total_amounts,
        string='Sales total amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help=(
            'Amount computed by summing total amounts of the sales orders '
            'this bank tracker is for.'
        ),
        store=True,
    )

    state = fields.Selection(
        selection=[
            ('draft', 'Draft'),
            ('amount_gap_validation', 'Amount gap validation'),
            ('validated', 'Validated'),
            ('canceled', 'Canceled'),
        ],
        string='State',
        help=(
            'State of this sales bank tracker. The "Amount gap validation" '
            'state is reached when the allowed amount gap has been exceeded.'
        ),
        readonly=True,
        track_visibility='onchange',
        default='draft',
    )

    @api.constrains('sales_order_ids')
    @api.one
    def _ensure_single_client(self):
        """Ensure all sales orders are from the same client.
        """

        if len(self.sales_order_ids.mapped('partner_id')) > 1:
            raise exceptions.ValidationError(_(
                'The selected sales orders are not all from the same client.'
            ))

    @api.onchange('client_id')
    def _handle_client_change(self):
        """- Update the sales order filter.
        - Fill sales orders based on the selected client.
        """

        domain = [
            ('state', '=', 'awaiting_payment'),
            ('payment_method_id.bank_tracked', '=', True),
        ]

        # The client is optional.
        if self.client_id:
            domain.append(('partner_id', '=', self.client_id.id))

            # When a client is selected, fill sales orders.
            self.sales_order_ids = self.env['sale.order'].search(domain)

        return {'domain': {'sales_order_ids': domain}}

    @api.onchange('sales_order_ids')
    def _handle_sales_order_change(self):
        """Update the client selector when sales orders have been selected.
        """

        if self.sales_order_ids:
            partners = self.sales_order_ids.mapped('partner_id')

            if len(partners) > 1:

                return {'warning': {
                    'message': _(
                        'The selected sales orders are not all from the same '
                        'client.'
                    ),
                    'title': _('Invalid sales orders'),
                }}

            self.client_id = partners

    @api.model
    def create(self, vals):
        """Override to fill the "client" field based on sales orders."""

        tracker = super(SalesBankTracker, self).create(vals)
        tracker._fill_client(vals)
        return tracker

    @api.multi
    def write(self, vals):
        """Override to fill the "client" field based on sales orders."""

        ret = super(SalesBankTracker, self).write(vals)
        self._fill_client(vals)
        return ret

    @api.multi
    def is_above_amount_gap(self):
        """Find out whether one of the trackers has a tracked amount too far
        from the expected one.
        """

        allowed_gap = self._get_allowed_gap()

        return bool(
            self.filtered(lambda tracker: tracker.amount_gap > allowed_gap)
        )

    @api.one
    def pay_when_needed(self):
        """Generate a payment when all sales invoices are validated.
        """

        if self.state != 'validated':
            return

        # Ensure all sales invoices have been validated.
        for sales_order in self.sales_order_ids:
            invoices = sales_order.invoice_ids
            if not invoices:
                return
            if invoices.filtered(lambda invoice: invoice.state == 'draft'):
                return

        first_order = self.sales_order_ids[0]

        amount = self.amount
        journal = first_order.payment_method_id.pay_sales_invoices_journal_id
        partner = self.env['res.partner']._find_accounting_partner(
            first_order.partner_id,
        )

        # Init accessors with various potentially useful context values.
        context = {
            'invoice_id': self.id,
            'invoice_type': first_order.invoice_ids[0].type,
            'journal_id': journal.id,
            'partner_id': partner.id,
            'payment_expected_currency': first_order.currency_id.id,
            'type': 'receipt',
        }
        voucher_obj = self.env['account.voucher'].with_context(context)
        vline_obj = self.env['account.voucher.line'].with_context(context)

        # Values to create the voucher with.
        voucher_values = {
            'account_id': journal.default_debit_account_id.id,
            'amount': amount,
            'currency_id': first_order.currency_id.id,
            'journal_id': journal.id,
            'partner_id': partner.id,
            'payment_option': 'without_writeoff',
            'pre_line': True,
            'reference': first_order.name,
            'type': 'receipt',
        }

        # When a write-off accounting account is defined, allow write-offs.
        if (
            first_order.payment_method_id.
            pay_sales_invoices_write_off_account_id
        ):
            voucher_values.update({
                'comment': _('Write-off'),
                'payment_option': 'with_writeoff',
                'writeoff_acc_id': (
                    first_order.payment_method_id.
                    pay_sales_invoices_write_off_account_id.id
                ),
            })

        voucher = voucher_obj.create(voucher_values)

        # Include lines by hand, to avoid the voucher selection magic. Done in
        # a secondary step so workflows update correctly.
        # Mostly imitate account.voucher::recompute_voucher_lines for the
        # details of account.voucher.line creation.
        for line in self.sales_order_ids.mapped('invoice_ids.move_id.line_id'):
            if (
                line.state != 'valid' or
                line.account_id.type != 'receivable' or
                line.reconcile_id
            ):
                continue
            vline_vals = {
                'account_id': line.account_id.id,
                'amount': line.amount_residual_currency,
                'currency_id': line.currency_id.id,
                'date_due': line.date_maturity,
                'date_original': line.date,
                'move_line_id': line.id,
                'name': line.name,
                'reconcile': True,
                'type': 'cr',
                'voucher_id': voucher.id,
            }
            vline_obj.create(vline_vals)
        voucher.signal_workflow('proforma_voucher')

    @api.one
    def wkf_draft(self):
        """Switch the state to "draft".
        """

        if self.state != 'draft':  # It's the default.
            self.state = 'draft'

    @api.one
    def wkf_amount_gap_validation(self):
        """Switch the state to "amount_gap_validation".
        """

        self.state = 'amount_gap_validation'

    @api.one
    def wkf_validated(self):
        """- Switch the state to "validated".
        - Fill amounts to pay.
        - Unlock sales orders.
        """

        if not self.sales_order_ids:
            raise exceptions.Warning('No order lines to pay')

        if self.is_above_amount_gap():
            raise exceptions.Warning(_(
                'The allowed gap is %s.' % self._get_allowed_gap()
            ))

        self.state = 'validated'

        self.sales_order_ids.signal_workflow('validate')

    @api.one
    def wkf_canceled(self):
        """Switch the state to "canceled".
        """

        self.state = 'canceled'

    @api.one
    def _fill_client(self, values):
        """Fill the "client" field based on sales orders.
        :param values: The data being updated.
        """

        if 'sales_order_ids' not in values:
            # Not updating the sales order list.
            return

        sales_orders = self.sales_order_ids
        if not sales_orders:
            return

        # Take the first client; a constraint guarantees it is the same across
        # all sales orders.
        self.client_id = sales_orders[0].partner_id

    def _get_allowed_gap(self):
        return float(self.env.ref(
            'sale_bank_reconciliation.sales_bank_tracking_allowed_gap'
        ).value)
