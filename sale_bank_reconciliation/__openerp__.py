##############################################################################
#
#    Sales bank reconciliation for Odoo
#    Copyright (C) 2016 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Sales bank reconciliation',
    'description': '''
Sales bank reconciliation
=========================

Add a step in sales orders for certain payment methods in which they await bank
statements that can then be matched against sales order lines to reconcile
them.
''',
    'version': '0.1',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'mail',
        'sale_invoice_payment',
        'sale_payment_method',
        'sale_streamline',
    ],

    'data': [
        'security/ir.model.access.csv',

        'menu.xml',

        'data/ir.config_parameter.xml',

        'views/crm_payment_mode.xml',
        'views/sales_bank_tracker.xml',

        'workflows/sales_bank_tracker.xml',
        'workflows/sales_order.xml',
    ],

    'installable': True,
}
