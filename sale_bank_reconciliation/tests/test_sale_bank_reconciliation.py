import time

import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_account(self):
        """Create accounting account for use in further tests.
        """

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': time.strftime('%Y-01-01'),
            'date_stop': time.strftime('%Y-12-31'),
            'period': 'month',
        }).execute()

        parent_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )
        self.assertTrue(parent_account)

        self.memory.account, = self.createAndTest(
            'account.account',
            [{
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': parent_account.id,
                'reconcile': True,
                'type': parent_account.type,
                'user_type': parent_account.user_type.id,
            }],
        )

    def test_0001_create_payment_methods(self):
        """Create payment methods and store them for further use.
        """

        # Find accounting journals.
        payment_journal = self.env['account.journal'].search(
            [('type', '=', 'bank')], limit=1,
        )
        sales_journal = self.env['account.journal'].search(
            [('type', '=', 'sale')], limit=1,
        )
        self.assertTrue(payment_journal)
        self.assertTrue(sales_journal)

        self.memory.payment_methods = self.createAndTest(
            'crm.payment.mode',
            [
                {
                    'bank_tracked': True,
                    'invoicing_journal_id': sales_journal.id,
                    'name': genUuid(),
                    'pay_sales_invoices': True,
                    'pay_sales_invoices_account_id': self.memory.account.id,
                    'pay_sales_invoices_journal_id': payment_journal.id,
                },
            ],
        )

    def test_0002_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partners = self.createAndTest(
            'res.partner',
            [
                {
                    'customer': True,
                    'name': genUuid(),
                },
            ],
        )

    def test_0003_create_products(self):
        """Create products for use in further tests.
        """

        self.memory.products = self.createAndTest(
            'product.product',
            [
                {
                    'name': genUuid(),
                    'type': 'service',
                },
            ],
        )

    def test_0010_create_sales_orders(self):
        """Create sales orders and store them for further use.
        """

        def buildSalesOrder():
            return {
                'order_policy': 'prepaid',  # To generate an invoice.
                'partner_id': self.memory.partners[0].id,
                'payment_method_id': self.memory.payment_methods[0].id,
            }

        # Create the sales orders.
        self.memory.sales_orders = self.createAndTest(
            'sale.order',
            [
                buildSalesOrder(), buildSalesOrder(),
            ],
        )

        # Add lines into the sales orders.
        for sales_order in self.memory.sales_orders:
            self.createAndTest(
                'sale.order.line',
                [
                    {
                        'name': genUuid(),
                        'order_id': sales_order.id,
                        'price_unit': 1.0,
                        'product_id': self.memory.products[0].id,
                    },
                ],
            )

        # Attempt to validate them and ensure they get stuck into "awaiting
        # payment".
        for sales_order in self.memory.sales_orders:
            self.assertEqual(sales_order.state, 'draft')
            sales_order.signal_workflow('order_confirm')
            self.assertEqual(sales_order.state, 'awaiting_payment')

    def test_0020_create_sales_bank_trackers(self):
        """Create a sales bank tracker.
        """

        def salesOrderTester(test_instance, asked_value, recorded_value):
            test_instance.assertEqual(recorded_value.ids, asked_value[0][2])

        sales_order_below_gap = self.memory.sales_orders[0]
        sales_order_above_gap = self.memory.sales_orders[1]

        (
            self.memory.tracker_below_gap,
            self.memory.tracker_above_gap,
            self.memory.tracker_to_cancel,
        ) = self.createAndTest(
            'sale.bank_tracker',
            [
                {  # tracker_below_gap
                    'amount': 42.0,  # < default allowed gap (50.0)
                    'name': genUuid(),
                    'sales_order_ids': [(6, 0, [sales_order_below_gap.id])],
                },
                {  # tracker_above_gap
                    'amount': 142.0,  # > default allowed gap (50.0)
                    'name': genUuid(),
                    'sales_order_ids': [(6, 0, [sales_order_above_gap.id])],
                },
                {  # tracker_to_cancel
                    'amount': 42.0,  # < default allowed gap (50.0)
                    'name': genUuid(),
                    'sales_order_ids': [(6, 0, [sales_order_below_gap.id])],
                },
            ],
            custom_testers={
                'sales_order_ids': salesOrderTester,
            },
        )

        # Ensure a client has been deduced.
        client = self.memory.partners[0]
        self.assertEqual(self.memory.tracker_below_gap.client_id.id, client.id)
        self.assertEqual(self.memory.tracker_above_gap.client_id.id, client.id)
        self.assertEqual(self.memory.tracker_to_cancel.client_id.id, client.id)

    def test_0021_validate_sales_bank_trackers(self):
        """Validate sales bank trackers and ensure the effects on sales orders
        are as expected.
        """

        # The tracker below the allowed gap should directly become validated.
        tracker = self.memory.tracker_below_gap
        self.assertEqual(tracker.state, 'draft')
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'validated')

        # The tracker above the allowed gap should need 2 validations.
        tracker = self.memory.tracker_above_gap
        self.assertEqual(tracker.state, 'draft')
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'amount_gap_validation')
        tracker.amount = 42.0  # < default allowed gap (50.0)
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'validated')

        # The sales order should have now been unlocked.
        sales_order = self.memory.sales_orders[0]
        self.assertNotEqual(sales_order.state, 'awaiting_payment')

    def test_0022_validate_sales_invoice(self):
        """Validate the sales invoice and ensure it automatically gets paid.
        """

        sales_order = self.memory.sales_orders[0]
        invoice = sales_order.invoice_ids[0]

        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'paid')

        # The sales order should have noticed that and should now be "done".
        self.assertEqual(sales_order.state, 'done')

    def test_0030_cancel_sales_bank_trackers(self):
        """Play with the cancellation process of sales bank trackers.
        """

        # draft -> canceled -> draft.
        tracker = self.memory.tracker_to_cancel
        self.assertEqual(tracker.state, 'draft')
        tracker.signal_workflow('cancel')
        self.assertEqual(tracker.state, 'canceled')
        tracker.signal_workflow('back_to_draft')
        self.assertEqual(tracker.state, 'draft')

        # Ensure a canceled tracker may not be validated.
        tracker.signal_workflow('cancel')
        self.assertEqual(tracker.state, 'canceled')
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'canceled')
        tracker.signal_workflow('back_to_draft')
        self.assertEqual(tracker.state, 'draft')

        # Ensure a validated tracker may not be canceled.
        tracker = self.memory.tracker_below_gap
        self.assertEqual(tracker.state, 'validated')
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'validated')
