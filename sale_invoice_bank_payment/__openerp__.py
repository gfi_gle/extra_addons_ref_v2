##############################################################################
#
#    Sales invoice bank payment for Odoo
#    Copyright (C) 2016-2017 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Sales invoice bank payment',
    'description': '''
Sales invoice bank payment
==========================

Match bank payments against invoices; reconcile relevant entries.
''',
    'version': '0.1',
    'category': 'Accounting',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'account_invoice_streamline',
        'sale_payment_method',
    ],

    'data': [

        'data/ir.config_parameter.xml',

        'menu.xml',

        'views/crm_payment_mode.xml',
        'views/sales_invbanktracker.xml',
        'views/account_config_settings.xml',

        'workflows/sales_invbanktracker.xml',

        'security/ir.model.access.csv',
    ],

    'installable': True,
}
