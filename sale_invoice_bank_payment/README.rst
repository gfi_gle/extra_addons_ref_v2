Sales invoice bank payment
==========================

Match bank payments against invoices; reconcile relevant entries.
