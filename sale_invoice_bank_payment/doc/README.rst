Sale Invoice Bank Payment
=========================

* Category: Accounting

* Quick summary:
Match bank payments against invoices; reconcile relevant entries.

* Version:
8.0.1.0

* Dependencies:
account_invoice_streamline
sale_payment_method

* Repo owner or admin: XCG Consulting
