from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

from openerp.addons.sale_invoice_bank_payment.util import (
    extract_field_value_from_write_dictionary,
    extract_fields_values_from_write_dictionary, extract
)

import openerp.addons.decimal_precision as dp


class SaleInvoiceBankTracker(models.Model):
    """Track bank reconciliation in relation to sales invoices.
    """

    _name = 'sale.invoice_bank_tracker'
    _description = 'Bank tracker'

    _inherit = ['mail.thread']

    bank_tracker_payment_method_id = fields.Many2one(
        company_dependent=True,
        comodel_name='crm.payment.mode',
        string='Bank tracker payment method',
        help=(
            'The payment method, and the associated accounting journal, used '
            'in payments generation from bank trackers.'
        ),
    )

    @api.depends('tracker_line_ids.amount_retained')
    @api.one
    def set_total_retained(self):

        for record in self:

            if record.tracker_line_ids:
                record.total_retained = sum(
                    self.tracker_line_ids.mapped('amount_retained')
                )

    @api.depends(
        'tracker_line_ids.amount_retained',
        'tracker_line_ids.invoice_id.residual'
    )
    @api.one
    def _compute_data(self):

        for record in self:

            total = all([
                line.amount_retained == line.residual
                for line in record.tracker_line_ids
            ])

            record.computed_payment_type = 'total' if total else 'partial'

    @api.depends('state')
    def set_tracker_validated(self):
        for record in self:
            record.tracker_validated = (record.state == 'validated')

    total_retained = fields.Float(
        compute=set_total_retained,
        string=_('Total retained'),
        help=(
            'Amount tracked by this invoices bank tracker. The sum of all '
            'the retained amounts on each invoice defined in the tracker '
            'lines.'
        ),
        store=True,
    )

    payment_type = fields.Selection(
        selection=[('partial', 'Partial'), ('total', 'Total')],
        string='Payment type',
    )

    computed_payment_type = fields.Selection(
        selection=[('partial', 'Partial'), ('total', 'Total')],
        string='Payment type',
        compute=_compute_data,
        store=True,
    )

    @api.depends('tracker_line_ids.invoice_id.residual')
    @api.one
    def _get_total_invoice_amounts(self):

        """Sum sale invoice amounts and compute a delta."""

        for record in self:

            record.computed_invoice_total_amount = sum(
               record.account_invoice_ids.mapped('residual')
            )

    @api.depends(
        'invoice_total_amount',
        'tracker_line_ids.amount_retained',
    )
    @api.one
    def _get_total_amounts(self):

        """Sum sale invoice amounts and compute a delta."""

        for record in self:

            record.amount_gap = abs(
                record.total_retained - record.invoice_total_amount
            )

    amount = fields.Float(
        string='Tracker amount',
        digits=dp.get_precision('Account'),  # Like invoice amounts.
        help=(
            'Amount tracked by this invoices bank tracker. The sum of all '
            'the retained amounts on each invoice defined in the tracker '
            'lines.'
        ),
        related='total_retained',
        readonly=True,
        store=True,
    )

    amount_gap = fields.Float(
        compute=_get_total_amounts,
        string='Amount gap',
        digits=dp.get_precision('Account'),  # Like invoices amounts.
        help=(
            'Delta between the amount tracked by this invoices bank tracker '
            'and the sum from invoices.'
        ),
        store=True,
    )

    client_id = fields.Many2one(
        comodel_name='res.partner',
        string='Client',
        domain=[('customer', '=', True)],
        ondelete='restrict',
        help='Client to filter invoices on.',
    )

    date = fields.Date(
        string='Date',
        help='Date when this payment was registered.',
    )

    description = fields.Text(
        string='Description',
        help='Miscellaneous description of this invoices bank tracker.',
    )

    @api.depends('client_id')
    @api.one
    def _has_client_id(self):

        for record in self:
            record.has_client_id = bool(record.client_id)

    @api.depends('tracker_line_ids.invoice_id')
    @api.one
    def _has_account_invoice(self):

        for record in self:
            record.has_account_invoice = bool(record.account_invoice_ids)

    # Utility fields used in views.
    has_account_invoice = fields.Boolean(
        compute=_has_account_invoice,
    )

    has_client_id = fields.Boolean(
        compute=_has_client_id,
    )

    invoice_type = fields.Selection(
        selection=[
            ('out_invoice', _('Invoice')),
            ('out_refund', _('credit note'))
        ],
        string='Invoice type',
    )

    tracker_validated = fields.Boolean(
        compute=set_tracker_validated,
        readonly=True,
    )

    @api.depends('tracker_line_ids.invoice_id')
    @api.one
    def update_account_invoice_ids(self):

        for record in self:
            record.account_invoice_ids = \
                record.tracker_line_ids.mapped('invoice_id')

    name = fields.Char(
        string='Name',
        help='The name of this invoices bank tracker.',
        required=True,
        track_visibility='always',
    )

    tracker_line_ids = fields.One2many(
        comodel_name='sale.invoice_bank_tracker.line',
        inverse_name='tracker_id',
        string='Tracker lines',
        help=(
            'Sale invoices and their retained amounts this bank tracker is '
            'for.'
        ),
    )

    account_invoice_ids = fields.Many2many(
        compute=update_account_invoice_ids,
        comodel_name='account.invoice',
        relation='sale_invbanktracker_invoice_rel',
        column1='tracker_id',
        column2='account_invoice_id',
        string='Sale invoices',
        help='Sale invoices this bank tracker is for.',
        readonly=True,
        store=True,
    )

    computed_invoice_total_amount = fields.Float(
        compute=_get_total_invoice_amounts,
        string='Invoices total amount',
        digits=dp.get_precision('Account'),  # Like invoices amounts.
        help=(
            'Amount computed by summing total amounts of the invoices '
            'this bank tracker is for.'
        ),
        store=True,
    )

    invoice_total_amount = fields.Float(
        string='Invoices total amount',
        digits=dp.get_precision('Account'),  # Like invoices amounts.
        help=(
            'Amount computed by summing total amounts of the invoices '
            'this bank tracker is for.'
        ),
        readonly=True,
    )

    def _get_default_currency_id(self):
        """Set the currency to the user's default currency"""
        return self.env.user.company_id.currency_id

    currency_id = fields.Many2one(
        comodel_name='res.currency',
        string="Currency",
        default=_get_default_currency_id,
        required=True,
    )

    state = fields.Selection(
        selection=[
            ('draft', 'Draft'),
            ('amount_gap_validation', 'Amount gap validation'),
            ('validated', 'Validated'),
        ],
        string='State',
        help=(
            'State of this invoices bank tracker. The "Amount gap validation" '
            'state is reached when the allowed amount gap has been exceeded.'
        ),
        readonly=True,
        track_visibility='onchange',
        default='draft',
    )

    @api.constrains('currency_id', 'tracker_line_ids')
    def _ensure_same_currency(self):

        for record in self:

            currencies = record.account_invoice_ids.mapped('currency_id')
            if currencies and record.currency_id not in currencies:
                raise ValueError(
                    _(
                        "Currencies in selected invoices don't match "
                        "{currency}."
                    ).format(currency=self.currency_id.name)
                )

    @api.constrains('tracker_line_ids')
    @api.one
    def _ensure_single_client(self):
        """Ensure all invoices are from the same client.
        """

        for record in self:

            if len(record.account_invoice_ids.mapped('partner_id')) > 1:
                raise exceptions.ValidationError(_(
                    'The selected invoices are not all from the same client.'
                ))

    @api.onchange('tracker_line_ids')
    @api.one
    def _handle_account_invoice_change(self):
        """Update the client selector when invoices have been selected.
        """

        if self.tracker_line_ids:
            partners = \
                self.tracker_line_ids.mapped('invoice_id').mapped('partner_id')

            if len(partners) > 1:

                return {'warning': {
                    'message': _(
                        'The selected invoices are not all from the same '
                        'client.'
                    ),
                    'title': _('Invalid invoices'),
                }}
            self.client_id = partners

    @api.model
    def create(self, vals):
        """Override to fill the "client" field based on invoices."""

        tracker = super(SaleInvoiceBankTracker, self).create(vals)
        tracker._fill_client(vals)
        return tracker

    @api.multi
    def write(self, vals):
        """Override:
        - to fill the "client" field based on invoices.
        - transfer the values of some hidden computed fields to displayed
        fields, for which no computation must be done at the validated state.
        """

        ret = []

        for record in self:

            # Propagate the values of the total invoice amount and payment type
            # function fields to their displayed values.
            values = extract(record.transfer_computed_fields_values(vals))

            ret.append(super(SaleInvoiceBankTracker, record).write(values))

            record._fill_client(values)

            # Propagate the value of the cost overrun function field to its
            # displayed value.
            if record.state != 'validated':
                for line in record.tracker_line_ids:
                    if line.computed_cost_overrun != line.cost_overrun:
                        line.cost_overrun = line.computed_cost_overrun

        return all(ret)

    @api.multi
    def is_above_amount_gap(self):
        """Find out whether one of the trackers has a tracked amount too far
        from the expected one.
        """

        allowed_gap = self._get_allowed_gap()

        return bool(
            self.filtered(lambda tracker: tracker.amount_gap > allowed_gap) and
            allowed_gap != -1
        )

    @api.one
    def pay_invoices(self):
        """Generate a payment when all invoices are validated.
        """

        if self.state != 'validated':
            raise exceptions.Warning(_('Non-validated bank tracker.'))

        # Ensure all accounting invoices are ready for payment.
        if self.account_invoice_ids.filtered(
            lambda invoice: invoice.state == 'draft'
        ):
            raise exceptions.Warning(_('Some invoices are still draft.'))
        if self.account_invoice_ids.filtered(
            lambda invoice: invoice.state == 'paid'
        ):
            raise exceptions.Warning(_(
                'Some invoices have already been paid.'
            ))

        # Gather some data.
        first_invoice = self.account_invoice_ids[0]
        payment_method = self.bank_tracker_payment_method_id

        if not payment_method:
            raise exceptions.Warning(_(
                'No payment method has been defined for bank trackers.'
            ))

        amount = self.amount
        journal = payment_method.sales_invoices_bank_payment_journal_id
        partner = self.env['res.partner']._find_accounting_partner(
            first_invoice.partner_id,
        )

        # Init accessors with various potentially useful context values.
        context = {
            'invoice_id': self.id,
            'invoice_type': first_invoice.type,
            'journal_id': journal.id,
            'partner_id': partner.id,
            'payment_expected_currency': first_invoice.currency_id.id,
            'type': 'receipt',
        }
        voucher_obj = self.env['account.voucher'].with_context(context)
        vline_obj = self.env['account.voucher.line'].with_context(context)

        # Values to create the voucher with.
        voucher_values = {
            'account_id': journal.default_debit_account_id.id,
            'amount': amount,
            'currency_id': first_invoice.currency_id.id,
            'journal_id': journal.id,
            'partner_id': partner.id,
            'payment_option': 'without_writeoff',
            'pre_line': True,
            'reference': first_invoice.name,
            'type': 'receipt',
        }

        # When a write-off accounting account is defined, allow write-offs.
        if payment_method.sales_invoices_bank_payment_write_off_account_id:
            voucher_values.update({
                'comment': _('Write-off'),
                'payment_option': 'with_writeoff',
                'writeoff_acc_id': (
                    payment_method.
                    sales_invoices_bank_payment_write_off_account_id.id
                ),
            })

        voucher = voucher_obj.create(voucher_values)

        # Include lines by hand, to avoid the voucher selection magic. Done in
        # a secondary step so workflows update correctly.
        # Mostly imitate account.voucher::recompute_voucher_lines for the
        # details of account.voucher.line creation.
        for tracker_line in self.tracker_line_ids:

            invoice = tracker_line.invoice_id

            lines = invoice.move_id.line_id.filtered(
                lambda r: not (
                    r.state != 'valid' or
                    r.account_id.type != 'receivable' or
                    r.reconcile_id
                )
            )

            if not lines:
                continue

            line = lines[0]

            vline_vals = {
                'account_id': line.account_id.id,
                'amount': tracker_line.amount_retained,
                'currency_id': line.currency_id.id,
                'date_due': line.date_maturity,
                'date_original': line.date,
                'move_line_id': line.id,
                'name': line.name,
                'reconcile': True,
                'type': 'cr' if self.invoice_type == 'out_invoice' else 'dr',
                'voucher_id': voucher.id,
            }
            vline_obj.create(vline_vals)
        voucher.signal_workflow('proforma_voucher')

    @api.one
    def wkf_draft(self):
        """Switch the state to "draft".
        """

        if self.state != 'draft':  # It's the default.
            self.state = 'draft'

    @api.one
    def wkf_amount_gap_validation(self):
        """Switch the state to "amount_gap_validation".
        """

        self.state = 'amount_gap_validation'

    @api.one
    def wkf_validated(self):
        """- Switch the state to "validated".
        - Fill amounts to pay.
        - Unlock sale invoices.
        """

        if not self.account_invoice_ids:
            raise exceptions.Warning(_('No invoice to pay.'))

        if self.is_above_amount_gap():
            raise exceptions.Warning(_(
                'The allowed gap is %s.' % self._get_allowed_gap()
            ))

        self.state = 'validated'

        self.pay_invoices()

    @api.one
    def _fill_client(self, values):
        """Fill the "client" field based on sale invoices.
        :param values: The data being updated.
        """

        if 'tracker_line_ids' not in values:
            # Not updating the account invoice list.
            return

        account_invoice = self.tracker_line_ids.mapped('invoice_id')
        if not account_invoice:
            return

        # Take the first client; a constraint guarantees it is the same across
        # all account invoices.
        self.client_id = account_invoice[0].partner_id

    @api.one
    def transfer_computed_fields_values(self, vals):
        """Propagate the values of some hidden computed fields to displayed
        fields, for which no computation must be done at the validated state.
        """

        values = vals.copy()

        fields = [
            'state', 'invoice_total_amount', 'computed_invoice_total_amount',
            'payment_type', 'computed_payment_type'
        ]

        fields_dict = extract(extract_fields_values_from_write_dictionary(
            self, values, fields
        ))

        state = extract(fields_dict['state'])
        invoice_total_amount = extract(fields_dict['invoice_total_amount'])
        computed_invoice_total_amount = \
            extract(fields_dict['computed_invoice_total_amount'])
        payment_type = extract(fields_dict['payment_type'])
        computed_payment_type = extract(fields_dict['computed_payment_type'])

        if state != 'validated':
            if invoice_total_amount != computed_invoice_total_amount:
                values['invoice_total_amount'] = computed_invoice_total_amount

            if payment_type != computed_payment_type:
                values['payment_type'] = computed_payment_type

        return values

    def _get_allowed_gap(self):
        return float(self.env.ref(
            'sale_invoice_bank_payment.'
            'sale_invoice_bank_payment_tracking_allowed_gap'
        ).value)

    @api.multi
    def fill_tracker_lines(self):
        """Create tracker lines for all available invoices associated to a
        client.
        """

        self.ensure_one()

        if self.client_id:
            domain = [
                ('state', '=', 'open'),
                ('type', '=', self.invoice_type),
                ('partner_id', '=', self.client_id.id),
            ]

            invoices = \
                self.env['account.invoice'].search(domain)

            self.tracker_line_ids = [(5,)]

            self.tracker_line_ids = [
                (0, 0, {
                    'invoice_id': invoice.id,
                    'amount_retained': invoice.residual,
                }) for invoice in invoices
            ]
