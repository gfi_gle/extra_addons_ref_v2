from openerp import api, _
from openerp import fields
from openerp import models


class PaymentMode(models.Model):
    """Add settings into payment methods for sales invoice payment.
    """

    _inherit = 'crm.payment.mode'

    bank_tracked = fields.Boolean(
        string='Bank tracked',
        help=(
            'Require bank trackers to unlock invoices that use this payment '
            'mode.'
        ),
    )

    sales_invoices_bank_payment_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Sales invoices bank payment - Payment journal',
        domain=[('type', 'in', ('bank', 'cash'))],
        ondelete='restrict',
        help=(
            _('The accounting journal to use when paying invoices generated '
              'from sales orders that use this payment mode. Only bank / cash '
              'accounting journals may be selected here.')
        ),
    )

    sales_invoices_bank_payment_write_off_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Sales invoices bank payment - Write-off accounting account',
        domain=[('reconcile', '=', True), ('type', '=', 'other')],
        ondelete='restrict',
        help=(_(
          'The accounting account to use for the write-off when paying '
          'invoices generated from sales orders that use this payment mode. '
          'Only "other" accounting accounts that allow reconciliation may '
          'be selected here.'
        )),
    )

    @api.onchange('bank_tracked')
    def _handle_sales_invoices_bank_payment_change(self):
        """When the "pay sales invoices" box is disabled, clear out its related
        fields.
        """

        if not self.bank_tracked:
            self.sales_invoices_bank_payment_journal_id = False
            self.sales_invoices_bank_payment_write_off_account_id = False
