from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class SaleInvoiceBankTrackerLine(models.Model):
    """Track bank reconciliation in relation to sales invoices.
    """

    _name = 'sale.invoice_bank_tracker.line'

    def set_default_amount_retained(self):
        return self.invoice_id.residual

    @api.constrains('tracker_id', 'invoice_id')
    def tracker_invoice_unicity(self):

        for record in self:

            if len(
                record.tracker_id.tracker_line_ids.
                filtered(lambda r: r.invoice_id.id == self.invoice_id.id)
            ) > 1:
                raise ValueError(
                    _(
                        "There must be only one tracker line per invoice."
                    )
                )

            t = record.tracker_id
            i = record.invoice_id

            valid_invoice = (
                (
                    (
                        t.tracker_validated and
                        i.state in ['open', 'paid']
                    ) or

                    i.state == 'open'

                ) and i.type == t.invoice_type and
                (

                    i.partner_id.id == t.client_id.id or

                    not t.has_client_id
                )
            )

            if not valid_invoice:
                raise exceptions.ValidationError(_(
                    'An invalid invoice has been inserted.\n \n'
                    'An invoice must be in the open state. \n'
                    'It must be an invoice or a refund accordingly to the '
                    'type of invoice, which has been selected. \n'
                    'It must have been made in the name of the client, who '
                    'has been selected.'
                ))

    @api.depends('invoice_id.residual', 'amount_retained')
    def set_cost_overrun(self):
        """Declare a cost overrun, if the retained amount is higher than the
        invoice residual.
        """

        for record in self:
            record.computed_cost_overrun = \
                (record.amount_retained > record.invoice_id.residual)

    tracker_id = fields.Many2one(
        comodel_name='sale.invoice_bank_tracker',
        string='Bank tracker',
        required=True,
    )

    amount_retained = fields.Float(
        string='Retained amount',
        digits=dp.get_precision('Account'),  # Like invoice amounts.
        help=(
            'Amount tracked by this invoices bank tracker line on the '
            'associated invoice.'
        ),
        required=True,
        default=set_default_amount_retained,
    )

    invoice_type = fields.Selection(
        selection=[
            ('out_invoice', _('Invoice')),
            ('out_refund', _('credit note'))
        ],
        readonly=True,
    )

    tracker_validated = fields.Boolean(readonly=True)

    client_id = fields.Many2one(
        comodel_name='res.partner',
        readonly=True,
    )

    has_client_id = fields.Boolean(
        readonly=True,
    )

    invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Invoice',
        delegate=True,
        required=True,
    )

    cost_overrun = fields.Boolean(
        string='Cost overrun',
        readonly=True,
        help=(
            'Indicates, if the retained amount for this invoice has exceeded '
            'its residual.'
        ),
    )

    computed_cost_overrun = fields.Boolean(
        compute=set_cost_overrun,
        string='Cost overrun',
        readonly=True,
        store=True,
        help=(
            'Indicates, if the retained amount for this invoice has exceeded '
            'its residual.'
        ),
    )

    @api.onchange('invoice_id')
    @api.one
    def onchange_invoice_id(self):
        """Update the retained amount, when an invoice has been selected.
        """
        if self.invoice_id:
            self.amount_retained = self.invoice_id.residual
