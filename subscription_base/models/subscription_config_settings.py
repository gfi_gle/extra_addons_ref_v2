from openerp import api
from openerp import models


class SubscriptionConfigSettings(models.TransientModel):
    """Configuration view for subscription settings.

    This is only here for easier access; the settings are not actually stored
    by this (transient) model. Instead, they are kept in sync with the relevant
    global / per-company settings. See comments in the definition of the
    "res.config.settings" model for details.
    """

    _inherit = 'res.config.settings'

    _name = 'subscription.config.settings'
    _description = 'Subscription config settings'

    @api.model
    def get_property_fields(self):
        """List the property fields, per model.
        Override to include yours.
        :rtype: Dictionary.
        """

        return {
        }

    # ===

    # TODO Define fields here.

    # ===

    @api.model
    def _get_property(self, model, field_name):
        """Get the specified property.

        :return: Odoo "ir.property" record set.
        """

        return self.env['ir.property'].search([
            ('res_id', '=', False),  # Only global ones.
            ('fields_id.model_id.model', '=', model),
            ('fields_id.name', '=', field_name),
        ], limit=1)

    @api.model
    def _read_property_id(self, model, field_name):
        """Get the record the specified property points to, if it is defined.

        :return: Record ID or False.
        """

        prop = self._get_property(model, field_name)
        if not prop.value_reference:
            return False
        model_name, record_id_str = prop.value_reference.split(',')
        return int(record_id_str)

    @api.multi
    def get_default_property_fields(self, fields):
        """Read property fields. This function is called when the form is
        shown.
        """

        return {
            field: self._read_property_id(model, field)
            for model, field_list in self.get_property_fields().iteritems()
            for field in field_list
        }

    @api.multi
    def set_property_fields(self):
        """Update property fields. This function is called when saving the
        form.
        """

        for model, field_list in self.get_property_fields().iteritems():
            for field in field_list:

                value = getattr(self, field)

                # Find out whether a property already exists.
                prop = self._get_property(model, field)

                if value:
                    # Create or update the property.
                    value = '%s,%s' % (value._name, value.id)
                    if prop:
                        prop.value_reference = value
                    else:
                        prop.create({
                            'fields_id': self.env['ir.model.fields'].search([
                                ('model_id.model', '=', model),
                                ('name', '=', field),
                            ], limit=1).id,
                            'name': 'default_%s_%s' % (model, field),
                            'type': 'many2one',
                            'value_reference': value,
                        })

                elif prop:
                    # Delete the property.
                    prop.unlink()
