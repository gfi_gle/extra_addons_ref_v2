IBAN management improvements
============================

Improve IBAN management in Odoo:

- Clean up bank views for a more IBAN-centric system.
- Ensure BICs have a valid format.


BIC format
----------

- 6 letters.
- 2 letters / digits.
- Optionally, 3 letters / digits.
