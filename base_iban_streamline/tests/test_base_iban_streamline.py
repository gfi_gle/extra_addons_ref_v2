import openerp.exceptions
import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_check_bic(self):
        """Ensure the BIC has a valid format.
        """

        def tryBic(bic):
            self.createAndTest('res.bank', [{'bic': bic, 'name': genUuid()}])

        def tryInvalidBic(bic):
            with self.assertRaises(openerp.exceptions.ValidationError):
                tryBic(bic)

        # Valid ones.

        tryBic('AAAAZZ11')
        tryBic('AAAAZZAA')

        tryBic('AAAAZZ11AAA')
        tryBic('AAAAZZ11222')

        tryBic('AAAAZZBBAAA')
        tryBic('AAAAZZBB222')

        # Invalid ones.

        tryInvalidBic('AAAAZZ1')
        tryInvalidBic('AAAAZZ')
        tryInvalidBic('AAAAZ')
        tryInvalidBic('AAAZZ')
        tryInvalidBic('A')
        tryInvalidBic('')

        tryInvalidBic('1111ZZ11')
        tryInvalidBic('AAAA2211')

        tryInvalidBic('AAAAZZ11AAAA')
        tryInvalidBic('AAAAZZ11AA')

        tryInvalidBic('-AAAZZ11')
        tryInvalidBic('AAAAZZ-1')

        # Should fail as it is a dupe.
        tryInvalidBic('AAAAZZ11')
