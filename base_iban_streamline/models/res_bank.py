from openerp import _
from openerp import api
from openerp import exceptions
from openerp import models
import re


class Bank(models.Model):
    """Adapt the bank model to an IBAN-centric system.
    """

    _inherit = 'res.bank'

    @api.constrains('bic')
    @api.one
    def _check_bic(self):
        """- Ensure the BIC has a valid format.
        - Ensure it is unique.
        """

        if not self.bic:
            raise exceptions.ValidationError(_('BIC undefined.'))

        if len(self.search([('bic', '=', self.bic)], limit=2)) > 1:
            raise exceptions.ValidationError(_('BIC already in use.'))

        # Don't bother pre-compiling the regex as this field is not likely to
        # change much (it's a set-up process).

        regex = (
            '^'  # Exact match.
            '[A-Za-z]{6}'  # 6 letters.
            '[A-Za-z0-9]{2}'  # 2 letters / digits.
            '([A-Za-z0-9]{3})?'  # Optionally, 3 letters / digits.
            '$'  # Exact match.
        )

        if re.match(regex, self.bic) is None:
            raise exceptions.ValidationError(_(
                'The "%s" BIC is not valid.'
            ) % self.bic)
