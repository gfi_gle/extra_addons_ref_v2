# -*- coding: utf-8 -*-
##############################################################################
#
#    Base Iban Streamline, for OpenERP
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': "IBAN management improvements",
    'description': """
IBAN management improvements
============================

Improve IBAN management in Odoo:

- Clean up bank views for a more IBAN-centric system.
- Ensure BICs have a valid format.


BIC format
----------

- 6 letters.
- 2 letters / digits.
- Optionally, 3 letters / digits.
""",
    'version': "8.0.3.0.0",
    'category': "Hidden/Dependency",
    'author': "XCG Consulting",
    'website': "http://odoo.consulting/",
    'depends': [
        'base_iban',
    ],
    'data': [
        'views/res_bank.xml',
        'views/res_partner_bank.xml',
    ],
    'installable': True,
}
