- Bank accounts: Name the "tracking" group.


8.0.3.0.0
=========

- Ensure BICs are valid.

- Ensure BICs are unique.


2.0
===

- Improve bank views for a more IBAN-centric system.


1.0
===

- Initial version.
