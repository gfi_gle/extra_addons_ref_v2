from openerp import api
from openerp import fields
from openerp import models

import logging
_logger = logging.getLogger(__name__)

class AccountConfigSettings(models.TransientModel):
    """Configuration view for check tracker settings.
    """

    _inherit = 'account.config.settings'

    @api.model
    def get_check_tracker_payment_method(self):
        """Specify the property field.
        :rtype: Dictionary.
        """

        ret = {}

        ret.update({
            'sale.invoice_check_tracker': [
                'check_tracker_payment_method_id',
            ],
        })

        return ret

    # ===
    # Settings in the check tracker model.
    # ===

    check_tracker_payment_method_id = fields.Many2one(
        comodel_name='crm.payment.mode',
        string='Check tracker payment method',
        help=(
            'The payment method, and the associated accounting journal, used '
            'in payments generation from check trackers.'
        ),
    )

    # ===

    @api.model
    def _get_property(self, model, field_name):
        """Get the specified property.

        :return: Odoo "ir.property" record set.
        """

        return self.env['ir.property'].search([
            ('res_id', '=', False),  # Only global ones.
            ('fields_id.model_id.model', '=', model),
            ('fields_id.name', '=', field_name),
        ], limit=1)

    @api.model
    def _read_property_id(self, model, field_name):
        """Get the record the specified property points to, if it is defined.

        :return: Record ID or False.
        """

        prop = self._get_property(model, field_name)
        if not prop.value_reference:
            return False
        model_name, record_id_str = prop.value_reference.split(',')
        return int(record_id_str)

    @api.multi
    def get_default_check_tracker_payment_method(self, fields):
        """Read property fields. This function is called when the form is
        shown.
        """

        return {
            field: self._read_property_id(model, field)
            for model, field_list in (
                self.get_check_tracker_payment_method().iteritems()
            )
            for field in field_list
        }

    @api.multi
    def set_check_tracker_payment_method(self):
        """Update property fields. This function is called when saving the
        form.
        """

        _logger.debug("------------------/////////////////////------------------------------test")

        for model, field_list in (
            self.get_check_tracker_payment_method().iteritems()
        ):
            for field in field_list:

                value = getattr(self, field)

                # Find out whether a property already exists.
                prop = self._get_property(model, field)

                if value:
                    # Create or update the property.
                    value = '%s,%s' % (value._name, value.id)
                    if prop:
                        prop.value_reference = value
                    else:
                        prop.create({
                            'fields_id': self.env['ir.model.fields'].search([
                                ('model_id.model', '=', model),
                                ('name', '=', field),
                            ], limit=1).id,
                            'name': 'default_%s_%s' % (model, field),
                            'type': 'many2one',
                            'value_reference': value,
                        })

                elif prop:
                    # Delete the property.
                    prop.unlink()
