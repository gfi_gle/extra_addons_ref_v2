from openerp import fields
from openerp import models


class AccountInvoice(models.Model):
    """Add a field to permit to filter invoices, function of the selection of a
    client, before linking these to tracker lines.
    """

    _inherit = 'account.invoice'

    # This field must always be set as 'True'.
    for_check_tracker_filter = fields.Boolean(default=True)
