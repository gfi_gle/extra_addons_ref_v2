from openerp import api
from openerp import fields
from openerp import models
from openerp import exceptions
from openerp import _

import openerp.addons.decimal_precision as dp


class CheckPaymentStatementBatch(models.Model):
    """Payment statement or batch of check trackers.
    """

    _name = 'sale.invoice_check_tracker.batch'
    _description = 'Check trackers batch'
    _rec_name = 'number'

    _inherit = ['mail.thread']

    @api.depends('check_tracker_ids.amount')
    def get_total_amount(self):

        for record in self:

            record.total_amount = \
                sum(record.check_tracker_ids.mapped(
                    lambda r: r.amount * (
                        1 if r.invoice_type == 'out_invoice' else -1
                    )
                ))

    number = fields.Char(
        string='Name',
        help='The name of this batch.',
        required=True,
        track_visibility='always',
    )

    check_tracker_ids = fields.One2many(
        comodel_name='sale.invoice_check_tracker',
        inverse_name='batch_id',
        string='Checks',
        help='The checks linked to this batch',
        readonly=True,
    )

    payment_method_id = fields.Many2one(
        comodel_name='payment.mode',
        string='Payment method',
        help=(
            'The payment method on which account is registered the '
            'payment statement.'
        ),
        required=True,
    )

    company_id = fields.Many2one(
        comodel_name='res.company',
        string='Company',
        required=True,
        delegate=True,
        ondelete='cascade',
    )

    bank_id = fields.Many2one(
        comodel_name='res.partner.bank',
        related='payment_method_id.bank_id',
        string='Bank account',
        store=True,
    )

    date = fields.Date(
        string='Date',
        help=(
            'Check date, up to which all non-registered checks have been '
            'registered on the payment statement.'
        ),
        required=True,
    )

    total_amount = fields.Float(
        compute=get_total_amount,
        digits=dp.get_precision('Account'),
        help=(
            'Total amount of the payment statement. The sum of all the check '
            'amounts.'
        ),
        string='Total amount',
        store=True,
        readonly=True,
    )

    @api.model
    def create(self, vals):

        # Automatically fill the company from the payment method.
        if 'payment_method_id' in vals.keys():

            payment_method = self.env['payment.mode'].browse(
                vals['payment_method_id']
            )

            vals['company_id'] = payment_method.company_id.id

        batch_id = super(CheckPaymentStatementBatch, self).create(vals)

        return batch_id
