from openerp import api, _
from openerp import fields
from openerp import models


class PaymentMode(models.Model):
    """Add settings into payment methods for check invoice payment.
    """

    _inherit = 'crm.payment.mode'

    check_tracked = fields.Boolean(
        string='Check tracked',
        help=(
            'Pay accounting invoices that use this payment mode via check '
            'trackers.'
        ),
    )

    check_tracked_payment_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Check payments - Payment journal',
        domain=[('type', 'in', ('bank', 'cash'))],
        ondelete='restrict',
        help=(
            'The accounting journal to use when paying invoices that use this '
            'payment mode. Only bank / cash accounting journals may be '
            'selected here.'
        ),
    )

    check_tracked_write_off_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Check payments - Write-off accounting account',
        domain=[('reconcile', '=', True), ('type', '=', 'other')],
        ondelete='restrict',
        help=(
            'The accounting account to use for the write-off when paying '
            'invoices that use this payment mode. Only "other" accounting '
            'accounts that allow reconciliation may be selected here.'
        ),
    )

    @api.onchange('check_tracked')
    def _handle_check_tracked_change(self):
        """When the "check tracked" box is disabled, clear out its related
        fields.
        """

        if not self.check_tracked:
            self.check_tracked_payment_journal_id = False
            self.check_tracked_write_off_account_id = False
