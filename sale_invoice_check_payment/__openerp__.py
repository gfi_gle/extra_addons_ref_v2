##############################################################################
#
#    Sales invoice check payment for Odoo
#    Copyright (C) 2017 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Sales invoice check payment',
    'description': '''
Sales invoice check payment
===========================

- Register checks to pay sales invoices.

- Generate check bank deposit batches.
''',
    'version': '0.1',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'account_invoice_streamline',
        'mail',
        'sale_payment_method',
        'sale_streamline',
        'report_py3o',
    ],

    'data': [
        'security/ir.model.access.csv',

        'menu.xml',

        'data/ir.config_parameter.xml',

        'views/crm_payment_mode.xml',
        'views/sales_invchecktracker.xml',
        'views/sales_invchecktracker_batch.xml',
        'views/account_config_settings.xml',

        'workflows/sales_invchecktracker.xml',

        'wizard/check_payment_statement.xml',

        'reports/reports.xml',

    ],

    'installable': True,
}
