Change watcher for Odoo
=======================

This package provides base methods to watch for changes in any Odoo collection.


Use
---

Code changes
~~~~~~~~~~~~

Have the Odoo package inherit from the "change_watcher" Odoo package, then just
make Odoo collections inherit from "change_watcher.base"::

    class TestCollection(models.Model):
        _name = 'test'
        _inherit = 'change_watcher.base'

This system also works fine with existing Odoo collections::

    class AccMove(models.Model):
        _inherit = [
            'account.move',
            'change_watcher.base',
        ]
        _name = 'account.move'

Enable
~~~~~~

With the above, change watching is available but not enabled. To activate, add
the relevant Odoo collections in the "Models under watch" list.


Utilities
---------

Some code utilities (generally useful when watching for changes) are available
in the "util" directory.
