from openerp import api
from openerp import fields
from openerp import models
import uuid


class Base(models.Model):
    """Base methods to watch for changes in any Odoo collection. This
    collection is meant to be overridden by any collection willing to watch for
    changes.

    Implementers have to override the "handle_change_watcher_event" method.
    """

    _name = 'change_watcher.base'

    EXTERNAL_XREF_FIELD = None

    def _default_change_watcher_force_change(self):
        return uuid.uuid4().hex

    change_watcher_force_change = fields.Char(
        string='Utility field to force change watcher events',
        size=64,
        copy=False,
        default=_default_change_watcher_force_change,
    )

    change_watcher_state = fields.Selection(
        selection=[
            ('creating', 'Creating'),
            ('idle', 'Idle'),
            ('updating', 'Updating'),
        ],
        string='Change watcher state',
        copy=False,
        default='creating',
        required=True,
    )

    def handle_change_watcher_event(self):
        """Called when the change watcher has detected a change. To be
        overridden.
        """

        raise NotImplementedError()

    @api.multi
    def write(self, vals):
        """Overwrite to signal a change."""

        if vals is None:
            vals = {}
        if 'change_watcher_state' not in vals:
            vals['change_watcher_state'] = 'updating'

        return super(Base, self).write(vals)

    def wkf_change_watcher_creating(self):
        """Signal the change event and let the workflow handle the rest (it
        will transition to the "idle" activity).
        """

        if self.change_watcher_state != 'idle':
            self.handle_change_watcher_event()

    def wkf_change_watcher_idle(self):
        """Switch the change watcher state to "idle".
        """

        # Use the admin account here as security rules affecting the object
        # might have changed after an update operation; but we still want to
        # track the change.
        self.sudo().change_watcher_state = 'idle'

    def wkf_change_watcher_updating(self):
        """Signal the change event and let the workflow handle the rest (it
        will transition back to the "idle" activity).
        """

        if self.change_watcher_state != 'idle':
            self.handle_change_watcher_event()
