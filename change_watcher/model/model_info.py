from openerp import api
from openerp import fields
from openerp import models
import uuid

import openerp.workflow.helpers as WorkflowHelpers
from openerp.workflow.instance import WorkflowInstance
from openerp.workflow.service import WorkflowService


class ModelInfo(models.Model):
    """Information about an Odoo collection that has change watcher
    capabilities (by inheriting from "change_watcher.base").
    """

    _name = 'change_watcher.model_info'

    enabled = fields.Boolean(
        string='Enabled',
        readonly=True,
    )

    model_id = fields.Many2one(
        comodel_name='ir.model',
        string='Model',
        ondelete='cascade',
        required=True,
    )

    workflow_id = fields.Many2one(
        comodel_name='workflow',
        string='Workflow',
        ondelete='set null',
    )

    @api.multi
    def enable_change_watch(self):
        """Create the change watcher workflow linked to the Odoo collection and
        put existing records under change watch.
        """

        self.ensure_one()

        activity_obj = self.env['workflow.activity'].sudo()
        transition_obj = self.env['workflow.transition'].sudo()
        workflow_obj = self.env['workflow'].sudo()

        # Create the workflow object.
        self.workflow_id = workflow_obj.create({
            'name': '"%s" change watcher workflow' % self.model_id.name,
            'on_create': True,
            'osv': self.model_id.model,
        })

        # Create workflow activities.
        creating_activity = activity_obj.create({
            'action': 'wkf_change_watcher_creating()',
            'flow_start': True,
            'kind': 'function',
            'name': 'creating',
            'wkf_id': self.workflow_id.id,
        })
        idle_activity = activity_obj.create({
            'action': 'wkf_change_watcher_idle()',
            'kind': 'function',
            'name': 'idle',
            'wkf_id': self.workflow_id.id,
        })
        updating_activity = activity_obj.create({
            'action': 'wkf_change_watcher_updating()',
            'kind': 'function',
            'name': 'updating',
            'wkf_id': self.workflow_id.id,
        })

        # Create workflow transitions.
        transition_obj.create({
            'act_from': creating_activity.id,
            'act_to': idle_activity.id,
            'condition': 'True',
        })
        transition_obj.create({
            'act_from': idle_activity.id,
            'act_to': updating_activity.id,
            'condition': "change_watcher_state == 'updating'",
        })
        transition_obj.create({
            'act_from': updating_activity.id,
            'act_to': idle_activity.id,
            'condition': 'True',
        })

        # Put existing records under change watch.
        # Imitate openerp.workflow.service.WorkflowService.create but for 1
        # workflow only.

        # Add the new workflow into the workflow cache.
        WorkflowService.CACHE.setdefault(self.env.cr.dbname, {})
        cached_workflow_ids = (
            WorkflowService.CACHE[self.env.cr.dbname]
            .get(self.model_id.model, None)
        ) or []
        cached_workflow_ids.append(self.workflow_id.id)

        existing_records = self.env[self.model_id.model].sudo().search([])
        for record in existing_records:

            # Ensure no creation / update event is generated.
            record.change_watcher_state = 'idle'

            # Let's do it.
            WorkflowInstance.create(
                WorkflowHelpers.Session(self.env.cr, self.env.user.id),
                WorkflowHelpers.Record(self.model_id.model, record.id),
                self.workflow_id.id,
            )

        # Done!
        self.enabled = True

    @api.multi
    def disable_change_watch(self):
        """Disable the change watcher workflow linked to the Odoo collection,
        along with its instance objets.
        """

        self.ensure_one()

        self.workflow_id.sudo().unlink()

        self.enabled = False

    @api.multi
    def signal_all_records(self):
        """Signal change events for all existing records.
        """

        self.ensure_one()

        new_uuid = uuid.uuid4().hex

        existing_records = self.env[self.model_id.model].sudo().search([])
        for record in existing_records:
            record.change_watcher_force_change = new_uuid
