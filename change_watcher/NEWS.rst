* Don't copy fields we add.


8.0.1.1 - 2016-07-12
====================

* Minor fixes.

* Add utilities previously provided by other addons.


8.0.1.0 - 2015-08-17
====================

* Initial implementation.
