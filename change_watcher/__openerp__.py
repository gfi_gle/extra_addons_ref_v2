##############################################################################
#
#    Change watcher for Odoo
#    Copyright (C) 2015 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Change watcher',
    'description': '''
Change watcher for Odoo
=======================

This package provides base methods to watch for changes in any Odoo collection.


Use
---

Code changes
~~~~~~~~~~~~

Have the Odoo package inherit from the "change_watcher" Odoo package, then just
make Odoo collections inherit from "change_watcher.base"::

    class TestCollection(models.Model):
        _name = 'test'
        _inherit = 'change_watcher.base'

This system also works fine with existing Odoo collections::

    class AccMove(models.Model):
        _inherit = [
            'account.move',
            'change_watcher.base',
        ]
        _name = 'account.move'

Enable
~~~~~~

With the above, change watching is available but not enabled. To activate, add
the relevant Odoo collections in the "Models under watch" list.


Utilities
---------

Some code utilities (generally useful when watching for changes) are available
in the "util" directory.
''',
    'version': '8.0.1.1',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'base',
    ],

    'data': [
        'security/ir.model.access.csv',

        'views/model_info.xml',
    ],

    'installable': True,
}
