"""Help with the management of external references in Odoo.
"""

from openerp import _
from openerp import exceptions
import uuid


def get_ref(record_set, xref):
    """Get an Odoo record set of the entry about the specified external
    reference.

    :type xref: str.
    """

    data_obj = record_set.sudo().env['ir.model.data']

    return data_obj.search([('name', '=', xref)], limit=1)


def browse_ref(record_set, xref):
    """Get an Odoo record set of the record pointed to by the specified
    external reference.

    :type xref: str.
    :return: The found record, or False.
    """

    ref = get_ref(record_set, xref)
    if not ref:
        return False

    return record_set.env[ref.model].browse(ref.res_id)


def all_refs(record_set):
    """Find all possible external references on the specified Odoo record.
    Return a record set, with the xrefs created with save_ref placed first.
    """

    data_obj = record_set.sudo().env['ir.model.data']

    refs = data_obj.search([
        ('model', '=', record_set._name),
        ('res_id', '=', record_set.id),
    ])
    return refs.sorted(key=lambda r: r.module != '__change_watcher_xref')


def ref(record_set):
    """Return the external reference of the specified Odoo record. The
    "get_external_id" method prepends the module name, which is not what we
    want.

    :rtype: str.
    """
    refs = all_refs(record_set)
    if refs:
        return refs[0].name
    else:
        return refs


def save_ref(record_set, xref):
    """Save an external reference to the specified Odoo record.
    """

    data_obj = record_set.sudo().env['ir.model.data']
    data_obj.create({
        'name': xref,
        'model': record_set._name,
        'module': '__change_watcher_xref',
        'res_id': record_set.id,
    })


def ensure_ref(record_set):
    """Either return the existing external reference of the specified
    record or generate one.

    :rtype: str.
    """

    if not record_set:
        return None

    xref = ref(record_set)

    if not xref:
        # No external reference; generate one.
        external_xref_field = getattr(record_set._model, 'EXTERNAL_XREF_FIELD')
        if external_xref_field:
            xref = getattr(record_set, external_xref_field)
        if not xref:
            xref = uuid.uuid4().hex
        save_ref(record_set, xref)

        # This should always work but we'd rather make sure.
        xref = ref(record_set)
        if not xref:
            raise exceptions.Warning(_(
                'No external reference for the "%s" record in "%s".'
            ) % (record_set.id, record_set._name))

    return xref
