# encoding: utf-8
"""Utilities related to Odoo data updates.
"""

from openerp import models


def would_change(record, data):
    """Determine whether the specified record would be updated when loading the
    specified data. Useful not to launch any update.

    :type record: Odoo record set.
    :type data: Dict.

    :return: Whether the record would change.
    :rtype: Boolean.
    """

    for key, value in data.iteritems():

        if type(value) is str:
            value = value.decode('utf-8')
        prev_value = getattr(record, key, None)

        # Handle relational fields (Odoo record-sets).
        if isinstance(prev_value, models.BaseModel):
            if isinstance(value, (tuple, list)):
                if (
                    len(value) == 1 and
                    isinstance(value[0], (tuple, list)) and
                    len(value[0]) == 3 and
                    value[0][0] == 6 and
                    isinstance(value[0][2], (tuple, list))
                ):
                    # Special case for (6, 0, IDs) "magic" tuples.
                    same = value[0][2] == prev_value.ids
                else:
                    # Compare assuming this is just an ID list.
                    same = value == prev_value.ids
            else:
                same = value == prev_value.id

        else:
            same = value == prev_value

        if not same:
            return True

    return False
