from openerp import _
from openerp import api
from openerp import exceptions
from openerp import models


class BaseNoCopyMixin(models.AbstractModel):
    """Include this model within models inherited by an object to benefit from
    default behaviors set here.

    Current defaults:
    - Block copies. 2 ways to bypass:
        * Via a ``_base_allow_copy`` class member.
        * Via a ``base_allow_copy`` context key.
    """

    _name = 'base.no_copy_mixin'

    # By default copying an object is blocked; set to true in a derived class
    # to allow.
    _base_allow_copy = False

    @api.multi
    def copy(self, default=None):
        """Override to disallow copying."""

        if self._base_allow_copy or self.env.context.get('base_allow_copy'):
            return super(BaseNoCopyMixin, self).copy(default=default)

        raise exceptions.Warning(_(
            'Copying this object is not supported.'
        ))
