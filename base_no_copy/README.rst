No copy mixin
=============

Odoo mixin to disallow object copying for a model by default. May be bypassed
(see below).


Use
---

Code changes
~~~~~~~~~~~~

Have the Odoo module inherit from this ``base_no_copy`` module, then just make
an Odoo model inherit from ``base.no_copy_mixin``::

    class TestModel(models.Model):
        _name = 'test'
        _inherit = 'base.no_copy_mixin'

This system also works fine with existing Odoo models::

    class AccMove(models.Model):
        _inherit = [
            'account.move',
            'base.no_copy_mixin',
        ]
        _name = 'account.move'

Allow copying
~~~~~~~~~~~~~

- A ``_base_allow_copy`` member may be set on a derived class to globally
  bypass::

    class TestModel(models.Model):
        _inherit = 'test'
        _base_allow_copy = True

- Alternatively, a ``base_allow_copy`` context key may be used to bypass in
  specific cases::

    obj.with_context(base_allow_copy=True).copy()


History / Rationale
-------------------

This was initially implemented via a metaclass, but that made it difficult to
selectively combine / enable several features. Mixins, on the other hand, are
quite easy to add to a model regardless of that model's metaclass.


UI changes
----------

None for now.
