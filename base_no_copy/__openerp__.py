##############################################################################
#
#    No copy mixin for Odoo
#    Copyright (C) 2017 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'No copy mixin',
    'description': '''
No copy mixin
=============

Odoo mixin to disallow object copying for a model by default. May be bypassed
(see below).


Use
---

Code changes
~~~~~~~~~~~~

Have the Odoo module inherit from this ``base_no_copy`` module, then just make
an Odoo model inherit from ``base.no_copy_mixin``::

    class TestModel(models.Model):
        _name = 'test'
        _inherit = 'base.no_copy_mixin'

This system also works fine with existing Odoo models::

    class AccMove(models.Model):
        _inherit = [
            'account.move',
            'base.no_copy_mixin',
        ]
        _name = 'account.move'

Allow copying
~~~~~~~~~~~~~

- A ``_base_allow_copy`` member may be set on a derived class to globally
  bypass::

    class TestModel(models.Model):
        _inherit = 'test'
        _base_allow_copy = True

- Alternatively, a ``base_allow_copy`` context key may be used to bypass in
  specific cases::

    obj.with_context(base_allow_copy=True).copy()


History / Rationale
-------------------

This was initially implemented via a metaclass, but that made it difficult to
selectively combine / enable several features. Mixins, on the other hand, are
quite easy to add to a model regardless of that model's metaclass.


UI changes
----------

None for now.
''',
    'version': '1.0',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'base',
    ],

    'data': [
    ],

    'installable': True,
}
