import openerp

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_copy_allowed(self):
        """Create a model that does not inherit from "base.no_copy_mixin" and
        ensure copying is allowed (default state).
        """

        # Create a test model and insert an object.
        model_name = self._create_model(
            inherit_from_mixin=False, bypass_copy_block=False,
        )
        obj = self.env[model_name].create({})

        # Copying should be allowed (default state).
        self._ensure_objcount(model_name, 1)
        obj.copy()
        self._ensure_objcount(model_name, 2)

    def test_0001_copy_blocked(self):
        """Create a model that inherits from "base.no_copy_mixin" and ensure
        copying is blocked.
        """

        # Create a test model and insert an object.
        model_name = self._create_model(
            inherit_from_mixin=True, bypass_copy_block=False,
        )
        obj = self.env[model_name].create({})

        # Copying should raise an exception.
        with self.assertRaises(openerp.exceptions.Warning):
            obj.copy()

    def test_0002_copy_bypassed_class_member(self):
        """Create a model that inherits from "base.no_copy_mixin" and that asks
        for a copy bypass (by defining a "_base_allow_copy" member) and ensure
        copying is then allowed.
        """

        # Create a test model and insert an object.
        model_name = self._create_model(
            inherit_from_mixin=True, bypass_copy_block=True,
        )
        obj = self.env[model_name].create({})

        # Copying should be allowed (bypassed via class member).
        self._ensure_objcount(model_name, 1)
        obj.copy()
        self._ensure_objcount(model_name, 2)

    def test_0003_copy_bypassed_context_key(self):
        """Create a model that inherits from "base.no_copy_mixin" and attempt
        a copy after having set a "base_allow_copy" context key; ensure copying
        is then allowed.
        """

        # Create a test model and insert an object.
        model_name = self._create_model(
            inherit_from_mixin=True, bypass_copy_block=False,
        )
        obj = self.env[model_name].create({})

        # Copying should be allowed (bypassed via context key).
        self._ensure_objcount(model_name, 1)
        obj.with_context(base_allow_copy=True).copy()
        self._ensure_objcount(model_name, 2)

    def _create_model(self, inherit_from_mixin, bypass_copy_block):
        """Create a model that inherits from "base.no_copy_mixin".

        :param inherit_from_mixin: Whether the test model should inherit from
        "base.no_copy_mixin".
        :type inherit_from_mixin: Boolean.

        :param bypass_copy_block: Whether the test model should contain a
        "_base_allow_copy" member to allow copying.
        :type bypass_copy_block: Boolean.

        :return: The name of the created model.
        :rtype: String.
        """

        model_name = 'test_model_%s' % genUuid(max_chars=32)

        class TestModel(openerp.models.Model):
            """Create a test model. Inspired by the one in the
            "ir.model::instanciate" method, except we also play with model
            inheritance.
            """

            _name = model_name
            _module = False
            _custom = True

        if inherit_from_mixin:
            TestModel._inherit = 'base.no_copy_mixin'
        if bypass_copy_block:
            TestModel._base_allow_copy = True

        # The following is mostly copied from "ir.model::create".
        TestModel._build_model(self.registry, self.env.cr)
        self.registry.setup_models(self.env.cr, partial=True)
        model_context = self.env.context.copy()
        model_context.update({
            'field_name': model_name,
            'field_state':  'manual',
            'select': '0',
            'update_custom_fields': True,
        })
        self.registry[model_name]._auto_init(self.env.cr, model_context)
        self.registry[model_name]._auto_end(self.env.cr, model_context)
        openerp.modules.registry.RegistryManager.signal_registry_change(
            self.env.cr.dbname,
        )

        return model_name

    def _ensure_objcount(self, model_name, expected_count):
        """Check the object count for the specified model.
        :type model_name: String.
        :type expected_count: Integer.
        """

        self.assertEqual(self.env[model_name].search_count([]), expected_count)
