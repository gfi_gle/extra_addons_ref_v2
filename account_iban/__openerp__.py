# -*- coding: utf-8 -*-
##############################################################################
#
#    Account Iban, for OpenERP
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': "IBAN & accounting integration improvements",
    'description': """
IBAN & accounting integration improvements
==========================================

Improvements on the default IBAN integration with accounting Odoo packages.

- Links between bank accounts and accounting vouchers.

- Bank accounts know their last payment date.

- Payment batches:

    * Show bank account last payment dates.

    * Highlight when the bank account has been modified since the last payment
      date.

- Partner view improvements for a better IBAN integration.
""",
    'version': "0.2",
    'category': "Hidden/Dependency",
    'author': "XCG Consulting",
    'website': "http://odoo.consulting/",
    'depends': [
        'account_streamline',
        'base_iban_streamline',
    ],
    'data': [
        'views/account_voucher.xml',
        'views/payment_batch_line.xml',
        'views/res_partner.xml',
        'views/res_partner_bank.xml',
    ],
    'installable': True,
}
