IBAN & accounting integration improvements
==========================================

Improvements on the default IBAN integration with accounting Odoo packages.

- Links between bank accounts and accounting vouchers.

- Bank accounts know their last payment date.

- Payment batches:

    * Show bank account last payment dates.

    * Highlight when the bank account has been modified since the last payment
      date.

- Partner view improvements for a better IBAN integration.
