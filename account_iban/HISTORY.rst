UNRELEASED
==========

- Links between bank accounts and accounting vouchers.

- Bank accounts know their last payment date.

- Payment batches show bank account last payment dates.


0.2
===

- Partner view improvements for a better IBAN integration.
