from openerp import api
from openerp import fields
from openerp import models


class AccountingVoucher(models.Model):
    """- Add bank information into accounting vouchers.
    - Update the last payment date of bank accounts at validation.
    """

    _inherit = 'account.voucher'

    partner_bank_id = fields.Many2one(
        comodel_name='res.partner.bank',
        string='Partner bank',
        ondelete='set null',
        help='The bank paying this accounting voucher.',
    )

    @api.multi
    def proforma_voucher(self):
        """Override to:
        - Update the last payment date of bank accounts at validation.
        """

        ret = super(AccountingVoucher, self).proforma_voucher()

        for voucher in self:
            bank_account = voucher.partner_bank_id
            if not bank_account:
                continue

            # Use the admin account to bypass security rules.
            bank_account = bank_account.sudo()

            # Save the update date to restore it later on.
            bank_account_update_date = bank_account.write_date

            bank_account.last_payment_date = fields.Datetime.now()

            # Restore the previous update date: In this particular case, we
            # don't want the update date of the bank account to change.
            self.env.cr.execute('UPDATE res_partner_bank SET write_date = %s',
                                (bank_account_update_date,))
            bank_account.modified(('write_date',))

        return ret
