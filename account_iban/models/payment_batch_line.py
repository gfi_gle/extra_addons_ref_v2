from openerp import api
from openerp import fields
from openerp import models


class PaymentBatchLine(models.Model):
    """- Add bank information into payment batch lines.
    - Determine whether the bank account has been modified since the last
    payment, so as to highlight such lines (of use to people who validate
    payments).
    """

    _inherit = 'account.payment_batch.line'

    bank_last_payment_date = fields.Datetime(
        related=('bank_id', 'last_payment_date'),
        string='Bank account - Last payment date',
        help='The last payment date of the selected bank account.',
        readonly=True,
        store=True,
    )

    @api.depends('bank_id.write_date', 'bank_id.last_payment_date')
    @api.one
    def _get_bank_modified_since_last_payment(self):
        """Consider a bank modified since the last payment when:
        - The bank account has just been created (no last payment date).
        - The last bank update date comes after the last bank payment date.
        """

        self.bank_modified_since_last_payment = (
            not self.bank_id.last_payment_date or
            self.bank_id.write_date > self.bank_id.last_payment_date
        )

    bank_modified_since_last_payment = fields.Boolean(
        compute=_get_bank_modified_since_last_payment,
        string='Bank account - Modified since the last payment',
        help=(
            'Whether the bank account has been modified since the last '
            'payment, so as to highlight such lines (of use to people who '
            'validate payments). Consider a bank modified since the last '
            'payment when:\n'
            '- The last bank update date comes after the last bank payment '
            'date.\n'
            '- The bank account has just been created (no last payment date).'
        ),
        store=True,
    )
