from openerp import fields
from openerp import models


class ResPartnerBank(models.Model):
    """Show payment information in bank accounts.
    """

    _inherit = 'res.partner.bank'

    account_voucher_ids = fields.One2many(
        comodel_name='account.voucher',
        inverse_name='partner_bank_id',
        string='Accounting vouchers',
        domain=[('state', '=', 'posted')],
        help='The accounting vouchers making use of this bank account.',
        readonly=True,
    )

    last_payment_date = fields.Datetime(
        string='Last payment date',
        help=(
            'The most recent payment date; updated at accounting voucher '
            'validation.'
        ),
        readonly=True,
    )
