# -*- encoding: utf-8 -*-
##############################################################################
#
#    NAF Code Partner Segmentation, for OpenERP
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'NAF Code partner segmentation',
    'version': '1.1.1',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'category': 'Accounting & Finance',
    'description': '''This module imports the French official NAF \
nomenclature of partner activities as partner categories and attach them to \
the NACE categories.
''',
    'depends': [
        'base',
        'l10n_eu_nace',
    ],
    'demo_xml': [],
    'data': [
        'data/res.partner.category.csv',
        'partner_view.xml',
    ],
    'active': True,
    'installable': True
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
