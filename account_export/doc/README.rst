Account Export
==============

* Category: Accounting

* Quick summary:
Exports of accounting document lines batches.
Exports of partner batches.

* Version:
8.0.1.0

* Dependencies:
  account_streamline

* Repo owner or admin: XCG Consulting
