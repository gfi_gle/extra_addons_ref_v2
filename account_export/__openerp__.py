# -*- coding: utf-8 -*-
##############################################################################
#
#    Account Export, for OpenERP
#    Copyright (C) 2017 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Account Export",
    "version": '8.0.1.0',
    "author": "XCG Consulting",
    "category": 'Accounting',
    "description": """
Account Export
==============

Exports of accounting document lines batches.
Exports of partner batches.

Configuration
-------------

No specific configuration.
    """,
    'website': 'http://odoo.consulting/',
    'init_xml': [],
    "depends": [
        'document',
        'account_streamline',
        'partner_unique_id',
    ],
    "data": [
        'security/ir.model.access.csv',

        'data/ir_cron.xml',

        'views/menu.xml',

        'views/account_move_line_view.xml',
        'views/account_move_line_batch_view.xml',
        'views/res_partner_info_view.xml',
        'views/res_partner_info_batch_view.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
