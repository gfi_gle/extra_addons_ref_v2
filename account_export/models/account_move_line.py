# -*- coding: utf-8 -*-

import datetime
from openerp import models
from openerp import api
from openerp import fields
from openerp import exceptions
from openerp import _

from openerp.addons.account_export.util.timezone import (
    system_timezone, system_timezone_now,
)

import logging

log = logging.getLogger('account.move.line')


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    batch_id = fields.Many2one(
        'account.move.line_batch',
        string=u"Batch",
    )

    # This field is meant to be read-only but we leave it open to allow data
    # imports. It is not shown in any view, but were it so, it would then be
    # read- only there.
    reconciliation_marker = fields.Char(
        string='Reconciliation marker',
        help=(
            'Reconciliation marker, used by exports. Filled in when the '
            'accounting document is ready for an external reconciliation.'
        ),
        copy=False,
    )

    @api.multi
    def filter_exportable_lines(self):
        return self.filtered(
            lambda r: not r.batch_id and r.move_id.state == 'posted'
        )

    @api.model
    def export_all(self, filename, journal_codes, max_date):
        """Export all accounting entries there are to export, provided they
        reside within the specified accounting journals.

        :param filename: Filename; may contain date / time formatters.
        :type filename: String.

        :type journal_codes: List.
        """

        if not isinstance(journal_codes, (list, tuple)) or not journal_codes:
            raise exceptions.Warning(
                'Accounting entry export: Invalid journal codes.'
            )

        log.info(u"Exports de lots d'écritures comptables")

        domain = [('journal_id.code', 'in', journal_codes)]

        now = system_timezone_now()

        log.info(u'Date : %s. Fuseau horaire : %s.', now, system_timezone)

        if max_date == 'prev_day':

            limit = (now - datetime.timedelta(days=1)).\
                strftime('%Y-%m-%d 23:59:59')

            domain.append(('date', '<=', limit))

            log.info(
                u"Exports de lots d'écritures comptables - Chaque jour : "
                u"avant le %s", limit
            )

        elif max_date == 'prev_month':

            max_day = (
                datetime.datetime(now.year, now.month, 1, 23, 59, 59) -
                datetime.timedelta(days=1)
            )

            limit = max_day.strftime('%Y-%m-%d 23:59:59')

            domain.append(('date', '<=', limit))

            log.info(
                u"Exports de lots d'écritures comptables - Provisions "
                u"- Chaque mois : avant le %s", limit
            )

        elif max_date != 'none':

            raise exceptions.Warning(_(u'Invalid date limit argument.'))

        exportable_lines = self.search(domain).filter_exportable_lines()

        log.info(u"%s lignes à exporter.", len(exportable_lines))

        if exportable_lines:
            exportable_lines.export(filename)

            log.info(u"Terminé : Fichier d'export %s", filename)

    @api.multi
    def export(self, filename):
        """Export the specified accounting entries.

        :param filename: Filename; may contain date / time formatters.
        :type filename: String.
        """

        if not self:
            raise exceptions.Warning(
                "The selected lines can't be packed into a batch."
            )

        # Generate the batch.
        return self.env['account.move.line_batch'].create({
            'name': system_timezone_now().strftime(filename),
            'line_ids': [(6, 0, self.ids)],
        })

    @api.multi
    def generate_batch(self):
        """Generate a account move line batch for the
         selected account move lines.
        """

        batch = self.browse(
            self.env.context['active_ids']
        ).filter_exportable_lines().export()

        # Show the generated batch.
        return {
            'context': self.env.context,
            'name': _('Account move line batch'),
            'res_id': batch.id,
            'res_model': 'account.move.line_batch',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
        }
