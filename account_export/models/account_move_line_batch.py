import base64
import codecs
import functools
from openerp import _
from openerp import api
from openerp import fields
from openerp import models
import StringIO

from openerp.addons.account_export.util.unicode_csv import (
    UnicodeWriter,
)


class AccountMoveLineBatch(models.Model):
    """Maintain links to multiple account move lines.
    """

    _name = 'account.move.line_batch'

    name = fields.Char(
        string='Name',
        help='The name of this batch.',
        required=True,
    )

    @api.depends('line_ids')
    @api.one
    def _get_line_count(self):
        """Count account move lines covered by the specified batch.
        """

        self.line_count = len(self.line_ids.ids)

    line_count = fields.Integer(
        compute=_get_line_count,
        string='Account move line count',
        help='The amount of account move line covered by this batch.',
        readonly=True,
        store=True,
    )

    line_ids = fields.One2many(
        comodel_name='account.move.line',
        inverse_name='batch_id',
        string='Account move lines',
        help='The account move lines covered by this batch.',
        readonly=True,
    )

    def get_exported_fields(self):
        """Override to customize the fields that are to be exported.
        :return: List of "ExportedField" instances.
        """

        today = fields.Date.today()

        # Get analytic fields of accounting entries.
        aentry_analytics = (
            self.env['analytic.structure'].get_structures('account_move_line')
        )

        def formatPeriod(period):
            """Format the period code (01/2016 -> 201601)."""
            if not period:
                return u''
            month, year = period.code.split('/')
            return year + month

        # Soft dependency on the "account_transaction_date" module: Only export
        # the field when it is available.
        optional_transaction_date_field = []
        if hasattr(self.env['account.move.line'], 'transaction_date'):
            optional_transaction_date_field.append(AccountMoveLineField(
                'transaction_date',
            ))

        # Soft dependency on the "sale_payment_method" module: Only export the
        # field when it is available.
        optional_sales_payment_method_field = []
        if hasattr(self.env['account.move.line'], 'sales_payment_method_id'):
            optional_sales_payment_method_field.append(ExportedField(
                '', 'sales_payment_method_id.technical_name',
                label=_('Payment method'), typ='char',
            ))

        return [
            AccountMoveLineField('company_id'),
            AccountMoveLineField('id'),
            AccountMoveLineField('move_state'),
            ExportedField('account.journal', 'journal_id.code',
                          label=_('Journal')),
            ExportedField(
                '', '', label=_('Period'), typ='char',
                value_getter=lambda field, record, field_type: (
                    formatPeriod(record.period_id)
                ),
            ),
        ] + optional_transaction_date_field + [
            AccountMoveLineField('date'),
            AccountMoveLineField('internal_sequence_number'),
            AccountMoveLineField('move_id'),
            AccountMoveLineField('ref'),
            ExportedField('account.account', 'account_id.code',
                          label=_('Account')),
            AccountMoveLineField('name'),
            AccountMoveLineField('debit'),
            AccountMoveLineField('debit_curr'),
            AccountMoveLineField('credit'),
            AccountMoveLineField('credit_curr'),
            AccountMoveLineField('currency_id'),
            AccountMoveLineField('date_maturity'),
            AccountMoveLineField('partner_id'),
            ExportedField('res.partner', 'partner_id.partner_info_id'),
            ExportedField(
                '', '', label=_('Tax account'), typ='char',
                value_getter=lambda field, record, field_type: (
                    record.tax_code_id.code or u''
                ),
            ),
        ] + [
            # Analytics.
            AnalyticCodeField(
                'a%s_id.name' % a_structure.ordering,
                label=a_structure.nd_id.name,
                typ='char',
            )
            for a_structure in aentry_analytics
        ] + [
            AccountMoveLineField('date_reconcile'),
            AccountMoveLineField('reconcile_ref'),
            ExportedField(
                '', 'extraction_date', label=_('Extraction date'),
                typ='date', value_getter=lambda *a: today,
            ),
            AccountMoveLineField('reconciliation_marker'),
        ] + optional_sales_payment_method_field

    def generate_export(self):
        """Override to customize export generation.
        By default, build a CSV file with 1 line per account move line.
        """

        exported_fields = self.get_exported_fields()

        # Usually this runs via an automated task so there is no lang info.
        export_context = self.env.context.copy()
        if not export_context.get('lang'):
            export_context['lang'] = self.env.user.lang

        # Get information about the fields to export.
        # Odoo 7 style to call "fields_get" without error...
        field_info = {
            model: self.pool[model].fields_get(self.env.cr, self.env.user.id, [
                field.name for field in exported_fields if field.model == model
            ], context=export_context)
            for model in (
                'account.account',
                'account.journal',
                'account.move.line',
                'res.partner',
            )
        }

        # Labels - either explicit or deduced from field information.
        data = [[
            field.label or field_info[field.model][field.name]['string']
            for field in exported_fields
        ]]

        # The actual data.
        data.extend([
            [
                field.get_value(
                    field, line,
                    field.typ or field_info[field.model][field.name]['type'],
                )
                for field in exported_fields
            ]
            for line in self.line_ids
        ])

        # Prepare the CSV data.
        csv_stream = StringIO.StringIO()
        csv_stream.write(codecs.BOM_UTF8)  # Make Excel happy...
        csv_writer = UnicodeWriter(csv_stream, delimiter=';', quotechar='"')
        csv_writer.writerows(data)
        csv_data = csv_stream.getvalue()
        csv_stream.close()

        return csv_data

    def get_export_name(self):
        """Override to define the filename of the export.
        By default, use the name of the batch and build a CSV file.
        :rtype: String.
        """

        return self.name + '.csv'

    @api.model
    def create(self, vals):
        """Generate an export file when creating a account move line batch.
        """

        batch = super(AccountMoveLineBatch, self).create(vals)

        self.env['ir.attachment'].create({
            'datas_fname': batch.get_export_name(),
            'datas': base64.b64encode(batch.generate_export()),
            'name': batch.get_export_name(),
            'res_id': batch.id,
            'res_model': self._name,
        })

        return batch

    @api.multi
    def generate_file(self):

        self.ensure_one()

        att_obj = self.env['ir.attachment']

        data = self.generate_export()
        name = self.get_export_name()

        files = att_obj.search([
            ('res_model', '=', self._name),
            ('res_id', '=', self.id),
        ])

        files.unlink()

        att_obj.create({
            'datas_fname': name,
            'datas': base64.b64encode(data),
            'name': name,
            'res_id': self.id,
            'res_model': self._name,
        })

        return True


class ExportedField(object):
    """Information about an exported field.
    """

    def __init__(self, model, path, label=None, typ=None, value_getter=None):
        """Init the field information holder.

        :param model: Technical name of the Odo model this field is in.

        :param path: Technical path to the field, from a account move line -
        may contain nested field names, separated by dots.

        :param label: Custom label for the exported field; by default, the Odoo
        label will be used.

        :param typ: Type of the exported field; by default, the Odoo field
        type will be used.

        :param value_getter: Custom value getter function; by default, the
        value will directly be fetched from the Odoo record.
        Signature: def value_getter(exported_field, record, field_type).
        """

        self.model = model
        self.path = path
        self.label = label
        self.typ = typ
        self.get_value = value_getter or self.default_value_getter

        # Extract the name of the this field in its model (after the last dot).
        last_dot_pos = self.path.rfind('.')
        if last_dot_pos > 0:
            self.name = self.path[last_dot_pos + 1:]
        else:
            self.name = self.path

    @staticmethod
    def default_value_getter(self, record, field_type):
        """By default, just call "getattr" to read the field and format
        accordingly.

        Although this is a static method, the first parameter is effectively a
        "self".
        """

        return ExportedField.format_value(
            functools.reduce(getattr, self.path.split('.'), record),
            field_type,
        )

    @staticmethod
    def format_value(value, field_type):
        """Format a value according to an Odoo field type, so it shows up
        nicely in the exported file.

        :return: The formatted value.
        """

        if field_type == 'boolean':
            # Return bools as they are.
            return value

        if field_type in ('float', 'integer'):
            # Replace "." by "," into numbers...
            return str(value).replace('.', ',')

        if field_type in ('many2one', 'one2many', 'many2many'):
            # Call the "name_get" method to stringify relational values.
            if not value:
                return u''
            names = dict(value.name_get())
            return u', '.join(names[val_iter.id] for val_iter in value)

        # Stringify values of fields in unhandled types and encode them as
        # Unicode.
        if value in (None, False):
            return u''
        return unicode(value)


# Define shortcuts for some model fields.


class AccountMoveLineField(ExportedField):
    def __init__(self, *args, **kwargs):
        ExportedField.__init__(self, 'account.move.line', *args, **kwargs)


class AnalyticCodeField(ExportedField):
    def __init__(self, *args, **kwargs):
        ExportedField.__init__(self, 'analytic.code', *args, **kwargs)
