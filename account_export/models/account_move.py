from openerp import api
from openerp import models


class AccountMove(models.Model):
    """Catch accounting document validations to fill reconciliation markers.
    """

    _inherit = 'account.move'

    @api.multi
    def post(self):
        """Override to fill reconciliation markers, when applicable. This is
        enough to be overridden as the "account_sequence" addon, which fills
        the "internal_sequence_number" field we are going to use, only does so
        at this step.
        """

        ret = super(AccountMove, self).post()

        for doc in self:
            for entry in doc.line_id:

                # A reconciliation marker has already been supplied; ignore.
                if entry.reconciliation_marker:
                    continue

                # Only care about client & supplier accounts.
                if entry.account_id.type not in ('payable', 'receivable'):
                    continue

                entry.reconciliation_marker = doc.internal_sequence_number

        return ret
