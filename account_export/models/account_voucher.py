from openerp import api
from openerp import models


class AccountingVoucher(models.Model):
    """Catch accounting voucher validations to fill reconciliation markers.
    """

    _inherit = 'account.voucher'

    @api.model
    def voucher_move_line_create(
        self, voucher_id, line_total, move_id, company_currency,
        current_currency,
    ):
        """Override to fill reconciliation markers, when applicable. Propagate
        the reconciliation marker of the accounting entry being paid to the
        payment accounting entry.
        """

        ret = super(AccountingVoucher, self).voucher_move_line_create(
            voucher_id, line_total, move_id, company_currency,
            current_currency,
        )

        # ret: amount, [ (payment_entry, paid_entry (, payment_entry)* ) ]
        # Very defensive coding here...

        def ensureList(var, size):
            return isinstance(var, (tuple, list)) and len(var) >= size

        if not ensureList(ret, 2):
            return ret

        payment_list = ret[1]
        if not ensureList(payment_list, 1):
            return ret

        for payment_data in payment_list:
            if not ensureList(payment_data, 2):
                continue

            # The paid entry is in 2nd position.
            paid_entry = self.env['account.move.line'].browse(payment_data[1])

            # Generated payment entries (all except the 2nd one).
            payment_entries = self.env['account.move.line'].browse([
                entry_id
                for index, entry_id in enumerate(payment_data)
                if index != 1
            ])

            # Take the marker from the paid entry.
            marker = paid_entry.reconciliation_marker

            # Propagate the marker to payment entries.
            for entry in payment_entries:
                # Only care about client & supplier accounts.
                if entry.account_id.type in ('payable', 'receivable'):
                    entry.reconciliation_marker = marker

        return ret
