from pytz import timezone
import datetime

from openerp.tools import config


system_timezone = timezone(
    config.options.get('timezone') or 'Europe/Paris'
)


def timezone_datetime(date_time, zone):
    """Converts a UTC date and time into a corresponding date and time in the
    given time zone.
    """
    utc_datetime = timezone('UTC').localize(date_time)
    return utc_datetime.astimezone(zone)


def system_timezone_datetime(date_time):
    """Converts a UTC date and time into a corresponding date and time in the
    information system time zone.
    """
    return timezone_datetime(date_time, system_timezone)


def system_timezone_now():
    return system_timezone_datetime(datetime.datetime.utcnow())
