from openerp import models, fields, api


class PaymentBatch(models.Model):
    _name = 'account.payment_batch'
    _inherit = 'account.payment_batch'

    sepa_batch_id = fields.Many2one(
        comodel_name='account.voucher.sepa_batch',
        string="SEPA Batch",
        track_visibility="onchange"
    )

    @api.multi
    def generate_sepa_batch(self):
        self.ensure_one()
        return self.voucher_ids.with_context(
            active_ids=self.voucher_ids.ids,
            payment_batch_id=self.id
        ).launch_wizard_sepa()

    @api.multi
    def pay_sepa(self):
        self.ensure_one()
        self.signal_workflow('pay')
        return self.generate_sepa_batch()
