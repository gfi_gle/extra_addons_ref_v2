# -*- coding: utf-8 -*-
# flake8: noqa

from . import account_sepa_purpose
from . import account_sdd_mandate
from . import account_voucher
from . import sepa_batch
from . import payment_batch
from . import res_bank
from . import res_partner
from . import res_partner_bank
