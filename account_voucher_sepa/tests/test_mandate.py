# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import openerp.tests


class Singleton(type):
    """
    This is a neat singleton pattern. This was found in a comment on this page:
    http://www.garyrobinson.net/2004/03/python_singleto.html

    to use this, example :
    # >>> class Toto(object):
    # ...     __metaclass__ = Singleton
    # ...     def __init__(self, foo):
    # ...         self.foo = foo
    #
    # >>> Toto('bar').foo
    # 'bar'
    #
    # >>> Toto().foo
    'bar'
"""
    def __init__(cls, name, bases, dic):
        super(Singleton, cls).__init__(name, bases, dic)
        cls.instance = None

    def __call__(mcs, *args, **kw):
        if mcs.instance is None:
            mcs.instance = super(Singleton, mcs).__call__(*args, **kw)

        return mcs.instance


class TestMemory(object):
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class TestBase(openerp.tests.SingleTransactionCase):

    _name = 'TestBase'

    def setUp(self):
        super(TestBase, self).setUp()
        self.memory = TestMemory()

    def test_0001_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        # TODO Use test helpers (createAndTest).

        # Find a bank (one should exist by default).
        bank = self.env['res.bank'].search([], limit=1)
        self.assertTrue(bank)

        self.memory.partner = self.env['res.partner'].create({
            'bank_ids': [(0, 0, {
                'acc_number': 'GR1601101250000000012300695',
                'bank': bank.id,
                'state': 'iban',
            })],
            'customer': True,
            'name': 'abcd',  # TODO genUuid()
            'supplier': True,
        })
        self.assertTrue(self.memory.partner)

    def test_0010_create_mandate(self):
        partner = self.memory.partner

        values = {"identification": 'test',
                  "creditor_company_id": '1',
                  "date_of_signature": '2015-12-11',
                  "ultimate_debtor_id": partner.id,
                  "ultimate_creditor_id": partner.id,
                  "debtor_id": partner.id,
                  }

        self.memory.mandate = self.env['account.sdd.mandate'].create(values)

    def test_0011_create_second_mandate(self):
        partner = self.memory.partner

        values = {"identification": 'test_test',
                  "creditor_company_id": '1',
                  "date_of_signature": '2015-12-11',
                  "ultimate_debtor_id": partner.id,
                  "ultimate_creditor_id": partner.id,
                  "debtor_id": partner.id,
                  }

        self.memory.mandate2 = self.env['account.sdd.mandate'].create(values)

    def test_0013_compare_mandate(self):

        # TODO Create sequences, or these will always be null.
        pass

        # self.assertNotEqual(
        #     self.memory.mandate.sequence_number,
        #     self.memory.mandate2.sequence_number)
