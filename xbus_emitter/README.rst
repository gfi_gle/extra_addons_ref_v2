Xbus emitter for Odoo
=====================

Emit messages to Xbus from Odoo.

2 sending methods are available:

- The "zmq" method has additional requirements and lets Odoo do the job.

- The "notify" method only notifies an external process.

Dependencies for the "zmq" method:

- zmq_rpc (available on pypi)::

    $ pip install zmq_rpc
