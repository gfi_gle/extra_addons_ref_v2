8.0.3.0 - UNRELEASED
====================

* The "zmq" sending method is now optional; add a "notify" method.


8.0.2.0 - 2016-06-17
====================

* Delayed mode; UI to manage messages.


8.0.1.0 - 2015-07-07
====================

* Initial implementation.
