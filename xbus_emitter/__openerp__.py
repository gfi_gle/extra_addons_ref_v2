##############################################################################
#
#    Xbus emitter for Odoo
#    Copyright (C) 2015 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Xbus emitter',
    'description': '''
Xbus emitter for Odoo
=====================

Emit messages to Xbus from Odoo.

2 sending methods are available:

- The "zmq" method has additional requirements and lets Odoo do the job.

- The "notify" method only notifies an external process.

Dependencies for the "zmq" method:

- zmq_rpc (available on pypi)::

    $ pip install zmq_rpc
''',
    'version': '8.0.3.0',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'base',
    ],

    'data': [
        'security/ir.model.access.csv',

        'menu.xml',

        'data/job_runner_cron_task.xml',

        'views/xbus_emitter.xml',
        'views/xbus_emitter_job.xml',
    ],

    'installable': True,
}
