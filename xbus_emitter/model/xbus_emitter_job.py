import json
from openerp import _
from openerp import api
from openerp import fields
from openerp import models


class XbusEmitterJob(models.Model):
    """Message to be emitted (or already emitted) to Xbus.
    """

    _name = 'xbus.emitter.job'

    _order = 'id DESC'

    emitter_id = fields.Many2one(
        comodel_name='xbus.emitter',
        string='Xbus emitter',
        ondelete='cascade',
        help='The Xbus emitter profile to send the message with.',
        required=True,
    )

    event_type = fields.Char(
        string='Xbus event type',
        help='The event type to use when sending items to Xbus.',
        required=True,
    )

    log = fields.Text(
        string='Log',
        help='Information about the emission of this message.',
        readonly=True,
    )

    items = fields.Text(
        string='Items',
        help='The data to sent to Xbus, JSON encoded.',
        required=True,
    )

    state = fields.Selection(
        selection=[
            ('to_send', 'To send'),
            ('sent_success', 'Sent (success)'),
            ('sent_error', 'Sent (error)'),
        ],
        string='State',
        required=True,
        track_visibility='onchange',
        default='to_send',
    )

    @api.model
    def send_jobs(self):
        """Run jobs that have not yet been sent."""

        # Explicit ordering to send messages in the right order.
        # Ignore the "notify" sending method here as an external process is in
        # charge of running this from time to time.
        for job in self.search([
            ('emitter_id.sending_method', '!=', 'notify'),
            ('state', '=', 'to_send'),
        ], order='id ASC'):

            try:
                job.emitter_id.send_items(
                    job.event_type, json.loads(job.items), delayed=False,
                )

                job.write({'log': _('Success!'), 'state': 'sent_success'})

            except Exception as error:
                job.write({
                    'log': _('Error when sending data to Xbus: %s') % error,
                    'state': 'sent_error',
                })

        return True
