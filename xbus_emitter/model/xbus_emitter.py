import json
import logging
from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

# The zmq sending method is optional.
try:
    import zmq
    from zmq_rpc import ZmqRpcClient
    from zmq_rpc import ZmqRpcError
    has_zmq = True
except ImportError:
    has_zmq = False


log = logging.getLogger(__name__)


class XbusEmitter(models.Model):
    """Configuration profile to emit Xbus messages from Odoo.
    """

    _name = 'xbus.emitter'

    _order = 'priority ASC'

    front_url = fields.Char(
        string='Front URL',
        size=128,
        help='URL to the Xbus broker front.',
    )

    login = fields.Char(
        string='Login',
        size=64,
        help='Emitter login; must exist in the list of emitters inside Xbus.',
    )

    password = fields.Char(
        string='Password',
        size=64,
        help='Emitter password for the specified login.',
    )

    priority = fields.Integer(
        string='Priority',
        required=True,
        help=(
            'Priority to sort emitter configuration objects; the lowest '
            'priority is the most important.'
        ),
        default=100,
    )

    sending_method = fields.Selection(
        selection=[
            ('zmq', 'zmq'),
            ('notify', 'notify'),
        ],
        string='Sending method',
        help=(
            'How messages using this emitter should be sent to Xbus. The '
            '"zmq" method has additional requirements and lets Odoo do the '
            'job; the "notify" method only notifies an external process.'
        ),
        required=True,
        default='zmq',  # For back-compat.
    )

    timeout = fields.Integer(
        string='Timeout',
        required=True,
        help=(
            'Time to wait during Xbus calls before throwing an error (in '
            'milliseconds).'
        ),
        default=5000,
    )

    @api.constrains('front_url', 'login', 'password', 'sending_method')
    @api.one
    def _check_zmq_fields(self):
        """Ensure fields that are relevant to the zmq sending method have
        been filled.
        """

        if self.sending_method != 'zmq':
            return
        if not self.front_url or not self.login or not self.password:
            raise exceptions.Warning(_(
                'Fields required for the "zmq" sending method: Front URL, '
                'Login, Password.'
            ))

    @api.one
    def name_get(self):
        return self.id, '%s@%s' % (self.login, self.front_url)

    @api.one
    def send_items(self, event_type, items, delayed=False):
        """Send the data to Xbus, inside one event with the specified event
        type.

        :param event_type: Name of an Xbus event type.
        :type event_type: String.

        :type items: Iterable.

        :param delayed: Whether the emission should only happen after the
        database transaction is committed; a scheduled task will run the
        emission in that case. Errors, if any, will be logged.
        :type delayed: Boolean.

        :return: The result of an "end_event" Xbus API call in immediate
        (non-delayed) mode; the ID of a job in delayed mode.
        """

        if delayed:
            # Just add to the job queue.

            emitter_job_id = self.env['xbus.emitter.job'].create({
                'emitter_id': self.id,
                'event_type': event_type,
                'items': json.dumps(items),
            }).id

            if self.sending_method == 'notify':
                self._send_notification()

            return emitter_job_id

        else:
            # Not delayed; run now and return the result.

            if self.sending_method != 'zmq':
                raise exceptions.Warning(_(
                    'Immediate emissions are only supported for the "zmq" '
                    'sending method.'
                ))

            if not has_zmq:
                raise exceptions.Warning('Missing zmq_rpc import.')

            try:
                return self._send_items_zmq(event_type, items)

            except (zmq.ZMQBaseError, ZmqRpcError) as error:
                raise exceptions.Warning(_(
                    'Error when sending data to Xbus: %s'
                ) % error)

    def _send_items_zmq(self, event_type, items):
        """Implementation of the Xbus data sending function via zmq that can
        throw.

        :param event_type: Name of an Xbus event type.
        :type event_type: String.
        :type items: Iterable.
        :raise zmq.ZMQBaseError: zmq error.
        :return: The result of an "end_event" Xbus API call.
        """

        log.debug('Establishing RPC connection...')
        conn = ZmqRpcClient(self.front_url, timeout=self.timeout)
        log.debug('RPC connection OK')
        token = conn.login(self.login, self.password)
        log.debug('Got connection token: %s', token)

        envelope_id = conn.start_envelope(token)
        log.debug('Envelope created: %s', envelope_id)

        event_id = conn.start_event(token, envelope_id, event_type, 0)
        log.debug('Event created: %s - event type: %s', event_id, event_type)

        for item in items:
            conn.send_item(
                token, envelope_id, event_id, conn.packer.pack(item)
            )
        log.debug('Data sent (event ID: %s)', event_id)

        ret = conn.end_event(token, envelope_id, event_id)
        log.debug('Event %s done: return value: %s', event_id, ret)

        conn.end_envelope(token, envelope_id)
        log.debug('Envelope %s closed', envelope_id)

        conn.logout(token)
        log.debug('Logged out; terminating')

        conn.close()
        log.debug('Done.')

        return ret

    def _send_notification(self):
        """Run a "NOTIFY" postgresql command.
        Since we only tell the external process there is new data in our table,
        the channel name is going to be the table name.
        The payload is the ID of this emitter.

        Ref: <https://www.postgresql.org/docs/current/static/sql-notify.html>.
        """

        self.env.cr.execute("NOTIFY %s, '%s'" % (
            self.env['xbus.emitter.job']._table,  # channel
            self.id,  # payload
        ))
