bcr
===

.. todo:: document

This module...

Parameters
----------

.. graphviz:: params.dot

.. automodule:: openerp.addons.bcr.model.account
        :members:
        :undoc-members:
        :private-members:

.. automodule:: openerp.addons.bcr.model.computation_setting
        :members:
        :undoc-members:
        :private-members:

