# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (www.xcg-consulting.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.osv import fields

ACCOUNTS_DISPLAYS = [
        ('asset', 'Asset'),
        ('liability', 'Liability'),
        ('mix', 'Mixed Rule'),
]


class account_account(osv.Model):
    """Adds an option on accounts, in order to precise, if these should be
    considered as asset or liability accounts when generating a profit and loss
    balance.
    """

    _inherit = 'account.account'
    """Account model"""

    _columns = {

        'balance_sheet_display': fields.selection(
            ACCOUNTS_DISPLAYS,
            string=u"Balance sheet display",
            help=(
                u'This option is used in the BCR module for profit and loss '
                u'accounts balance sheets. It precises, if the child accounts '
                u'should be considered as assets or liabilities accounts, or '
                u'if a mixed rule should be applied.'
            ),
            select=True,
        ),

    }
    """Fields"""

    _defaults = {
         'balance_sheet_display': 'mix',
    }
