##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class BCR_ProfitLossBalanceSheet(osv.Model):
    """Profit & loss - Balance Sheet
    Each subtotal and total of the profit & loss balance sheet is listed among
    the hierarchical result objects. To the former are associated the initial
    computation setting, the analytic balance, the current and former fiscal
    years, and the company.
    """

    _name = 'bcr.plbs'

    _columns = {

        'computation_setting_id': fields.many2one(
            'bcr.computation_setting',
            string=u"Computation Setting",
            readonly=True,
        ),

        'name': fields.related(
            'computation_setting_id',
            'name',
            type='char',
            readonly=True,
            string=u"Name",
        ),

        'analytic_balance_id': fields.many2one(
            'bcr.analytic_balance',
            string=u"Analytic Balance",
            readonly=True,
        ),

        'current_fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            string=u"Current Fiscal Year",
            readonly=True,
        ),

        'former_fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            string=u"Former Fiscal Year",
            readonly=True,
        ),

        'company_id': fields.related(
            'current_fiscalyear_id',
            'company_id',
            type='many2one',
            obj='res.company',
            string=u"Company",
            readonly=True,
        ),

        'hierarchical_result_ids': fields.one2many(
            'bcr.plbs.hierarchy',
            'plbs_id',
            string=u"Hierarchical results",
            readonly=True,
        ),
    }


class BCR_ProfitLossBalanceHierarchy(osv.Model):
    """Profit & loss - Balance Sheet Result
    This final hierarchical result object is a subtotal or a total of the
    profit & loss balance sheet. It is corresponding to a computation hierarchy
    object. It gives the total debit and credit net amounts, and the total
    asset and liability amounts, on all accounts corresponding to its analytic
    codes and its parent hierarchical results, as defined in the computation
    hierarchy object. The four results are given for the current and the former
    fiscal years, along their differences between both.
    The hierarchical result object also lists the analytic balance lines,
    associated to its analytic codes, and the other hierarchical results,
    associated to its parent objects, that had to be summed to give its data.
    """

    _name = 'bcr.plbs.hierarchy'

    _columns = {

        'computation_hierarchy_id': fields.many2one(
            'bcr.computation_hierarchy',
            string=u"Computation Hierarchy Object",
            readonly=True,
        ),

        'code': fields.related(
            'computation_hierarchy_id',
            'code',
            type='char',
            readonly=True,
            string=u"Code",
        ),

        'name': fields.related(
            'computation_hierarchy_id',
            'name',
            type='char',
            readonly=True,
            string=u"Name",
        ),

        'plbs_id': fields.many2one(
            'bcr.plbs',
            string=u"Profit & Loss - Balance Sheet",
            readonly=True,
        ),

        'company_id': fields.related(
            'plbs_id',
            'current_fiscalyear_id',
            'company_id',
            type='many2one',
            obj='res.company',
            string=u"Company",
            readonly=True,
        ),

        'analytic_balance_line_ids': fields.many2many(
            'bcr.analytic_balance.line',
            relation='bcr_plbs_hierarchy_analytic_balance_line_rel',
            column1='plbs_hierarchy_id',
            column2='analytic_balance_line_id',
            string=u"Analytic balance lines",
            readonly=True,
        ),

        'plbs_hierarchy_line_ids': fields.many2many(
            'bcr.plbs.hierarchy',
            'bcr_plbs_hierarchy_plbs_hierarchy_line_rel',
            'child_plbs_hierarchy_id',
            'parent_plbs_hierarchy_id',
            string=u"Parents hierarchical lines",
            readonly=True,
        ),

        'current_net_profit_loss_debit': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Current Net (debit, Profit and loss account)",
            readonly=True,
        ),
        'current_net_profit_loss_credit': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Current Net (credit, Profit and loss account)",
            readonly=True,
        ),
        'current_asset_amount': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Current Asset amount",
            readonly=True,
        ),
        'current_liability_amount': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Current Liability Amount",
            readonly=True,
        ),

        'former_net_profit_loss_debit': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Former Net (debit, Profit and loss account)",
            readonly=True,
        ),
        'former_net_profit_loss_credit': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Former Net (credit, Profit and loss account)",
            readonly=True,
        ),
        'former_asset_amount': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Former Asset amount",
            readonly=True,
        ),
        'former_liability_amount': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Former Liability Amount",
            readonly=True,
        ),

        'differential_net_profit_loss_debit': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Differential Net (debit, Profit and loss account)",
            readonly=True,
        ),
        'differential_net_profit_loss_credit': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Differential Net (credit, Profit and loss account)",
            readonly=True,
        ),
        'differential_asset_amount': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Differential Asset amount",
            readonly=True,
        ),
        'differential_liability_amount': fields.float(
            digits_compute=dp.get_precision('Account'),
            string=u"Differential Liability Amount",
            readonly=True,
        ),
    }

    _defaults = {
        'current_net_profit_loss_debit': 0.0,
        'current_net_profit_loss_credit': 0.0,
        'current_asset_amount': 0.0,
        'current_liability_amount': 0.0,
        'former_net_profit_loss_debit': 0.0,
        'former_net_profit_loss_credit': 0.0,
        'former_asset_amount': 0.0,
        'former_liability_amount': 0.0,
        'differential_net_profit_loss_debit': 0.0,
        'differential_net_profit_loss_credit': 0.0,
        'differential_asset_amount': 0.0,
        'differential_liability_amount': 0.0,
    }
