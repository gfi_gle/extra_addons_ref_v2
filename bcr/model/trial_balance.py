##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import orm, fields
import openerp.addons.decimal_precision as dp


class TrialBalance(orm.Model):
    """Overload of the trial balance class to make compute the credit and debit
    net amounts over the fiscal year, as the asset and liability amounts,
    whether the account is an asset or a liability account, or follows a mixed
    rule.
    """

    _inherit = 'trial_balance'

    _columns = {
        'total_net_profit_loss_debit': fields.float(
            string=u"Net (debit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
        ),
        'total_net_profit_loss_credit': fields.float(
            string=u"Net (credit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
        ),
        'total_asset_amount': fields.float(
            string=u"Asset amount",
            digits_compute=dp.get_precision('Account'),
        ),
        'total_liability_amount': fields.float(
            string=u"Liability Amount",
            digits_compute=dp.get_precision('Account'),
        ),
    }


class TrialBalanceLine(orm.Model):
    """Overload of the trial balance line class to make compute the credit and
    debit net amounts over the fiscal year, as the asset and liability amounts,
    whether the account is an asset or a liability account, or follows a mixed
    rule.
    """

    _inherit = 'trial_balance.line'

    _columns = {
        'net_profit_loss_debit': fields.float(
            string=u"Net (debit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
        ),
        'net_profit_loss_credit': fields.float(
            string=u"Net (credit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
        ),
        'asset_amount': fields.float(
            string=u"Asset amount",
            digits_compute=dp.get_precision('Account'),
        ),
        'liability_amount': fields.float(
            string=u"Liability Amount",
            digits_compute=dp.get_precision('Account'),
        ),
    }
