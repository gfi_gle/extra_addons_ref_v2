##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _


class BCR_ComputationHierarchy(osv.Model):
    """This object corresponds to a total or a subtotal of the final profit and
    loss balance. It lists the analytic codes, that corresponds to the trial
    balance lines, that are to sum to give the associated final result.
    This type of object also defines a computation hierarchy, that sets how the
    different linked subtotals and totals are to be summed together to give the
    final result. Therefore, each object is associated to parents and children
    objects of the same class, to build this hierarchy.
    The top parents are to be linked to a computation setting object. Their
    children can be however only linked to their parents. This permits to these
    objects to be inserted in other computation settings.
    """

    _name = 'bcr.computation_hierarchy'

    def find_relatives(
        self, cr, uid, ids, res, class_name, field_name, context=None
    ):
        """From the IDs of given source objects, searches all relative objects
        linked to the source objects through a relation. This relation is the
        field of the latters browse records, to which belong the relatives.
        Also identifies all the relatives of the further generations. Returns
        their IDs set.
        :param res: integer set (source object IDs)
        :param class_name: string (source object class name)
        :param field_name: string (relative objects field name in the source
        object browse record)
        :returns: res: integer set (source and relative objects IDs)
        """

        all_items = set()
        all_items.update(res)
        done = set()

        while(len(all_items) > 0):
            n = list(all_items)[0]
            item = self.pool[class_name].browse(
                cr, uid, [n], context=context
            )[0]

            objs = getattr(item, field_name)

            if objs:
                s = set([
                    parent.id
                    for parent in objs
                ])
            else:
                s = set()

            if len(done) == 0:
                done = set([n])
            else:
                done.update(set([n]))

            s -= done

            if s is not None:
                res.update(s)
                all_items.update(s)

            all_items -= done

        return res

    def filter_hierarchy(self, cr, uid, ids, context=None):
        """Filter method to store values of computed dimension ID for hierarchy
        objects. Identifies the parents of a hierarchy object, for which
        dimension change is possible, for having been moved to another tree.
        :param ids: integer list (bcr.computation_hierarchy object IDs)
        :returns: result: integer list (bcr.computation_hierarchy object IDs)
        """

        result = set()
        for hierarchical in self.browse(cr, uid, ids, context=context):
            res = set()
            output = set()

            if hierarchical.child_hierarchy_ids:
                res = set([
                    child.id
                    for child in hierarchical.child_hierarchy_ids
                ])

                output = self.pool['bcr.computation_hierarchy'].find_relatives(
                    cr, uid, ids, res, 'bcr.computation_hierarchy',
                    'child_hierarchy_ids', context=context
                )

            if output:
                result.update(output)

        result.update(ids)

        return list(result)

    def filter_setting(self, cr, uid, ids, context=None):
        """Filter method to store values of computed dimension ID for hierarchy
        objects. Identifies all the hierarchy objects of the tree of a
        computation setting object.
        :param ids: integer list (bcr.computation_hierarchy object IDs)
        :returns: result: integer list (bcr.computation_hierarchy object IDs)
        """

        result = set()
        for setting in self.browse(cr, uid, ids, context=context):
            res = set()
            output = set()

            if setting.hierarchy_ids:
                res = set([
                    hierarchical.id
                    for hierarchical in setting.hierarchy_ids
                ])

                output = self.pool['bcr.computation_hierarchy'].find_relatives(
                    cr, uid, ids, res, 'bcr.computation_hierarchy',
                    'child_hierarchy_ids', context=context
                )

            if output:
                result.update(output)

        return list(result)

    def get_item_dimension(
        self, cr, uid, ids, computation_setting_id, parent_hierarchy_id,
        context=None
    ):
        """Returns the analytic dimension of the tree, to which a hierarchy
        object is associated, and which is its authorized analytic dimension.
        :param computation_setting_id: integer (bcr.computation_setting object
        ID)
        :param parent_hierarchy_id: integer (bcr.computation_hierarchy object
        ID)
        :returns: dimension_id: integer (analytic.dimension object ID)
        """

        if computation_setting_id:
            setting = self.pool['bcr.computation_setting'].browse(
                cr, uid, [computation_setting_id], context=context
            )[0]
            dimension_id = setting.dimension_id.id
        elif parent_hierarchy_id:
            hierarchical = self.pool['bcr.computation_hierarchy'].browse(
                cr, uid, [parent_hierarchy_id], context=context
            )[0]
            setting = hierarchical.computation_setting_id
            if setting:
                dimension_id = setting.dimension_id.id
            elif hierarchical.parent_hierarchy_ids:
                dimension_id = self.get_item_dimension(
                    cr, uid, ids, False,
                    hierarchical.parent_hierarchy_ids[0].id,
                    context=context
                )
            else:
                dimension_id = False
        else:
            dimension_id = False

        return dimension_id

    def onchange_parent_hierarchy_ids(
        self, cr, uid, ids, computation_setting_id, parent_hierarchy_ids,
        analytic_code_ids, context=None
    ):
        """'On change' method to set a filter on parent hierarchy objects.
        Parents, that are only allowed, are of the same analytic dimension ID
        than the tree to which belongs the object.
        :param computation_setting_id: integer (bcr.computation_setting object
        ID)
        :param parent_hierarchy_ids: integer list (bcr.computation_hierarchy
        object IDs)
        :param analytic_code_ids: integer list (analytic.code IDs)
        :returns: dictionary (
        keys:
        'domain': integer parent's authorized_dimension_id matches the
        authorized analytic dimension ID of the object.
        'values: integer list of the IDs of the authorized parent computation
        hierarchy objects)
        """

        ret = {}

        if parent_hierarchy_ids:
            if parent_hierarchy_ids[0][2]:
                parents = parent_hierarchy_ids[0][2][0]
            else:
                parents = False
        else:
            parents = False

        authorized_dimension_id = self.get_item_dimension(
            cr, uid, ids,
            computation_setting_id,
            parents,
            context=context
        )

        if authorized_dimension_id:
            ret['domain'] = {
                'parent_hierarchy_ids': [
                    ('authorized_dimension_id', '=', authorized_dimension_id)
                ]
            }

            if parent_hierarchy_ids:
                answer = []
                parent_hierarchies = [
                    parent_hierarchy
                    for parent_hierarchy in parent_hierarchy_ids
                    if parent_hierarchy[0] == 6
                ][0]
                parent_list = parent_hierarchies[2]
                for parent in self.pool['bcr.computation_hierarchy'].browse(
                    cr, uid, parent_list, context=context
                ):

                    if (
                        parent.authorized_dimension_id.id !=
                        authorized_dimension_id
                    ):
                        parent_list.remove(parent.id)

                answer = [
                    parent_hierarchies[0], parent_hierarchies[1], parent_list
                ]

                parent_hierarchy_ids.remove(parent_hierarchies)

                if parent_list:
                    parent_hierarchy_ids.append(answer)

                ret['value'] = {
                    'parent_hierarchy_ids': parent_hierarchy_ids
                }

        else:
            ret['domain'] = {
                'parent_hierarchy_ids': []
            }

        return ret

    def onchange_analytic_code_ids(
        self, cr, uid, ids, computation_setting_id, parent_hierarchy_ids,
        analytic_code_ids, context=None
    ):
        """'On change' method to set a filter on analytic codes. Those that are
        only allowed, are of the same analytic dimension ID than the tree to
        which belongs the object.
        :param computation_setting_id: integer (bcr.computation_setting object
        ID)
        :param parent_hierarchy_ids: integer list (bcr.computation_hierarchy
        object IDs)
        :param analytic_code_ids: integer list (analytic.code IDs)
        :returns: dictionary (
        keys:
        'domain': integer analytic code dimension_id matches the
        authorized analytic dimension ID of the object.
        'values: integer list of the IDs of the authorized analytic codes
        (analytic.code))
        """

        ret = {}

        if parent_hierarchy_ids:
            if parent_hierarchy_ids[0][2]:
                parents = parent_hierarchy_ids[0][2][0]
            else:
                parents = False
        else:
            parents = False

        authorized_dimension_id = self.get_item_dimension(
            cr, uid, ids,
            computation_setting_id,
            parents,
            context=context
        )

        if authorized_dimension_id:

            ret['domain'] = {
                'analytic_code_ids': [('nd_id', '=', authorized_dimension_id)]
            }

            if analytic_code_ids:
                answer = []
                analytics = [
                    analytic
                    for analytic in analytic_code_ids
                    if analytic[0] == 6
                ][0]
                code_list = analytics[2]
                for code in self.pool['analytic.code'].browse(
                    cr, uid, code_list, context=context
                ):

                    if code.nd_id.id != authorized_dimension_id:
                        code_list.remove(code.id)

                answer = [analytics[0], analytics[1], code_list]

                analytic_code_ids.remove(analytics)

                if code_list:
                    analytic_code_ids.append(answer)

                ret['value'] = {
                    'analytic_code_ids': analytic_code_ids
                }

        return ret

    def set_authorized_dimension_id(
        self, cr, uid, ids, field_names, args, context=None
    ):
        """When this object is inserted in a computation tree, its analytic
        codes must be of the same analytic.dimension ID than those of the other
        elements of the tree. This method, associated to the authorized
        dimension ID function field, so computes the analytic dimension of the
        tree, as being the authorized analytic dimension for the object. It is
        the analytic dimension ID of the computation setting, from which
        depends the tree.
        :returns: dictionary
        (keys:
        id: integer ID of the computation hierarchy objects for which an
        analytic dimension is to be computed;
        dimension_id: integer analytic.dimension object ID).
        """

        res = {}

        for item in self.browse(cr, uid, ids, context=context):

            if item.parent_hierarchy_ids:
                parent = item.parent_hierarchy_ids[0].id
            else:
                parent = False

            res[item.id] = self.get_item_dimension(
                cr, uid, ids,
                item.computation_setting_id.id, parent,
                context=context
            )

        return res

    _columns = {

        'code': fields.char(
            size=64,
            string=u"Code",
            required=True,
        ),

        'name': fields.char(
            size=256,
            string=u"Total Name",
            required=True,
        ),

        'computation_setting_id': fields.many2one(
            'bcr.computation_setting',
            string=u"Linked Computation Setting",
        ),

        'child_hierarchy_ids': fields.many2many(
            'bcr.computation_hierarchy',
            'bcr_computation_hierarchy_hierarchy_rel',
            'parent_hierarchy_id',
            'child_hierarchy_id',
            string=u"Child hierarchical totals",
        ),

        'parent_hierarchy_ids': fields.many2many(
            'bcr.computation_hierarchy',
            'bcr_computation_hierarchy_hierarchy_rel',
            'child_hierarchy_id',
            'parent_hierarchy_id',
            string=u"Parent hierarchical totals",
        ),

        'analytic_code_ids': fields.many2many(
            'analytic.code',
            'bcr_computation_hierarchy_analytic_code_rel',
            'hierarchy_id',
            'analytic_code_id',
            string=u"Accounts analytic codes",
        ),

        'authorized_dimension_id': fields.function(
            set_authorized_dimension_id,
            obj='analytic.dimension',
            type='many2one',
            string=u"Accounts analytic codes dimension",
            method=True,
            readonly=True,
            store={
                'bcr.computation_hierarchy': (
                    filter_hierarchy,
                    ['computation_setting_id', 'parent_hierarchy_ids'],
                    10
                ),
                'bcr.computation_setting': (
                    filter_setting,
                    ['dimension_id', 'hierarchy_ids'],
                    10
                ),
            },
        ),

    }

    def _check_for_non_recursivity_in_computation_hierarchy(
        self, cr, uid, ids, context=None
    ):
        """Checks for non-recursivity in the computation hierarchy.
        """

        for item in self.browse(cr, uid, ids, context=context):

            res = set([item.id])

            children = list(self.find_relatives(
                cr, uid, ids, res, 'bcr.computation_hierarchy',
                'child_hierarchy_ids', context=context
            ))

            for child in item.parent_hierarchy_ids:
                if child.id in children:
                    return False

        return True

    _constraints = [
        (
            _check_for_non_recursivity_in_computation_hierarchy,
            u"A hierarchical computation object must not be linked to a child "
            u"object as being its child, and reversely, to a parent object as "
            u"being its parent. A hierarchical computation object can not also"
            u" be its own parent or its own child. The computation hierarchy "
            u"must be non-recursive.",
            ['child_hierarchy_ids', 'parent_hierarchy_ids']
        ),
    ]


class BCR_ComputationSetting(osv.Model):
    """This object defines the subtotals and totals hierarchy to compute. It
    is set with an analytic structure dimension. Hierarchical computation
    objects are associated to it, to represent the linked totals and subtotals.
    They must be of the same analytic dimension.
    """

    _name = 'bcr.computation_setting'

    def onchange_dimension_id(self, cr, uid, ids, dimension_id, context=None):
        """This 'on change' method checks, that the selected analytic dimension
        is indeed associated to accounts objects. It defines a corresponding
        filter for the dimension_id field.
        :param dimension_id: integer (analytic.dimension ID)
        :returns: dictionary (
        keys:
        'domain': dimension_id matches
        account.account model analytic.dimension objects IDs;
        'values: False for dimension_id, if not in domain)
        """

        structure_obj = self.pool['analytic.structure']

        analytic_structure_ids = structure_obj.search(
            cr, uid, [('model_name', '=', 'account_account')],
            context=context
        )

        res = []
        ret = {}

        for structure in structure_obj.browse(
            cr, uid, analytic_structure_ids, context=context
        ):

            res.append(structure.nd_id.id)

        ret['domain'] = {
            'dimension_id': [('id', 'in', res)]
        }

        if dimension_id not in res:
            ret['value'] = {
                'dimension_id': False
            }

        return ret

    _columns = {

        'name': fields.char(
            size=256,
            string=u"Name",
            required=True,
        ),

        'dimension_id': fields.many2one(
            'analytic.dimension',
            string=u"Accounts analytic codes dimension",
            required=True,
        ),

        'hierarchy_ids': fields.one2many(
            'bcr.computation_hierarchy',
            'computation_setting_id',
            string=u"Computation Hierarchies",
            help=u"Balance sheet computation analytic codes top level"
            u"hierarchies",
        ),

    }
