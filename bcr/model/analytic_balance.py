##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp


class BCR_AnalyticBalance(osv.Model):
    """This analytic balance is computed from the trial balances of the current
    and former fiscal years. It details the total moves of all the accounts
    linked to the chosen company (through the selection of given fiscal years),
    grouped by account analytic codes.
    """

    _name = 'bcr.analytic_balance'

    _columns = {

        'current_trial_balance_id': fields.many2one(
            'trial_balance',
            string=u"Current fiscal year trial balance",
            readonly=True,
        ),

        'company_id': fields.related(
            'current_trial_balance_id',
            'company_id',
            type='many2one',
            obj='res.company',
            string=u"Company",
            readonly=True,
        ),

        'former_trial_balance_id': fields.many2one(
            'trial_balance',
            string=u"Former fiscal year trial balance",
            readonly=True,
        ),

        'line_ids': fields.one2many(
            'bcr.analytic_balance.line',
            'analytic_balance_id',
            string=u"Analytic balance lines",
            readonly=True,
        ),
    }


class BCR_AnalyticBalanceLine(osv.Model):
    """An analytic balance line details the total moves of all the accounts
    of a given analytic code. It gives the debit and credit net amounts, and
    the asset and liability amounts, function of the fact that an account is an
    asset or a liability account, or follows a mixed rule. The four results are
    given for the current and the former fiscal years, along their differences
    between both.
    """

    _name = 'bcr.analytic_balance.line'

    _columns = {

        'analytic_balance_id': fields.many2one(
            'bcr.analytic_balance',
            string=u"Analytic Balance",
            readonly=True,
        ),

        'company_id': fields.related(
            'analytic_balance_id',
            'current_trial_balance_id',
            'company_id',
            type='many2one',
            obj='res.company',
            string=u"Company",
            readonly=True,
        ),

        'analytic_code_id': fields.many2one(
            'analytic.code',
            string=u"Analytic code",
            help=u"Grouped accounts analytic code",
            readonly=True,
        ),

        'current_trial_balance_line_ids': fields.many2many(
            'trial_balance.line',
            'bcr_analytic_balance_current_trial_balance_rel',
            'analytic_balance_id',
            'trial_balance_id',
            string=u"Current balance lines",
            help=u"Current fiscal year grouped trial balance lines",
            readonly=True,
        ),

        'former_trial_balance_line_ids': fields.many2many(
            'trial_balance.line',
            'bcr_analytic_balance_former_trial_balance_rel',
            'analytic_balance_id',
            'trial_balance_id',
            string=u"Former balance lines",
            help=u"Former fiscal year grouped trial balance lines",
            readonly=True,
        ),

        'current_net_profit_loss_debit': fields.float(
            string=u"Current Net (debit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'current_net_profit_loss_credit': fields.float(
            string=u"Current Net (credit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'current_asset_amount': fields.float(
            string=u"Current Asset amount",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'current_liability_amount': fields.float(
            string=u"Current Liability Amount",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),

        'former_net_profit_loss_debit': fields.float(
            string=u"Former Net (debit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'former_net_profit_loss_credit': fields.float(
            string=u"Former Net (credit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'former_asset_amount': fields.float(
            string=u"Former Asset amount",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'former_liability_amount': fields.float(
            string=u"Former Liability Amount",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),

        'differential_net_profit_loss_debit': fields.float(
            string=u"Differential Net (debit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'differential_net_profit_loss_credit': fields.float(
            string=u"Differential Net (credit, Profit and loss account)",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'differential_asset_amount': fields.float(
            string=u"Differential Asset amount",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),
        'differential_liability_amount': fields.float(
            string=u"Differential Liability Amount",
            digits_compute=dp.get_precision('Account'),
            readonly=True,
        ),

    }
