.. BCR documentation master file, created by
   sphinx-quickstart on Mon Apr 11 12:38:32 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BCR's documentation!
===============================

.. include:: manifest

Contents:

.. toctree::
   :maxdepth: 2

   README
   NEWS
   tests
   TODO


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

