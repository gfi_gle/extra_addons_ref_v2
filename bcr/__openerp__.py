# -*- coding: utf-8 -*-
##############################################################################
#
#    bcr, for Odoo
#    Copyright (C) 2016 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Profit & Loss - Balance Sheet",
    "version": '1.0',
    "author": "XCG Consulting",
    "category": 'Accounting',
    "description": u"""
        Profit & Loss - Balance Sheet / Bilan - Comptes et Resultats
    """,
    'website': 'http://odoo.consulting/',
    'init_xml': [],
    "depends": [
        'base',
        'account',
        'trial_balance',
        'report_py3o',
        'analytic_structure',
    ],
    "data": [
        'security/ir.model.access.csv',
        'security/security.xml',
        'views/computation_setting.xml',
        'views/account_view.xml',
        'views/trial_balance_view.xml',
        'views/analytic_balance_view.xml',
        'views/plbs_view.xml',
        'wizard/bcr_wizard_view.xml',
        'reports/reports.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
