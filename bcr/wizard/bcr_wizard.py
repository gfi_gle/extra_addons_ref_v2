# -*- coding: utf-8 -*-
##############################################################################
#
#    Trial Balance, for OpenERP
#    Copyright (C) 2015 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import orm, fields
from openerp.tools.translate import _
from openerp import exceptions


class BCRWizard(orm.TransientModel):
    """Generates a profit & loss balance sheet, on the basis of the given
    current and former fiscal years of the user's company, and a computation
    setting.
    """

    _name = 'wizard.bcr'

    def onchange_current_fiscalyear_id(
        self, cr, uid, ids, current_fiscalyear_id, context=None
    ):
        """'On change' method to set a filter on the current and the former
        fiscal years, to make these match the user's company.
        :param current_fiscalyear_id: integer (account.fiscalyear object
        ID)
        :returns: dictionary (
        keys:
        'domain': integer user's company ID matches the company ID of the
        object;
        'values: False for the former fiscal year,
        False for the current fiscal year, if the selected one's company does
        not match the filter.
        """

        user = self.pool['res.users'].browse(
            cr, uid, [uid], context=context
        )[0]

        company_id = user.company_id.id

        res = {}

        res['domain'] = {
            'current_fiscalyear_id': [
                ('company_id', '=', company_id),
            ],
            'former_fiscalyear_id': [
                ('company_id', '=', company_id),
            ],
        }

        res['value'] = {
            'former_fiscalyear_id': False
        }

        if current_fiscalyear_id:
            current_fiscalyear = self.pool['account.fiscalyear'].browse(
                cr, uid, [current_fiscalyear_id], context=context
            )[0]

            if current_fiscalyear.company_id.id != company_id:
                res['value'] = {
                    'current_fiscalyear_id': False
                }

        return res

    _columns = {
        'current_fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            string=u"Current Fiscal Year",
            required=True,
        ),
        'former_fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            string=u"Former Fiscal Year",
            required=True,
        ),
        'computation_setting_id': fields.many2one(
            'bcr.computation_setting',
            string=u"Computation Setting",
            required=True,
        ),
        'include_draft_moves': fields.boolean(
            string=u"Include Draft Moves",
        ),
    }

    def get_start_period(self, cr, uid, ids, fiscalyear_id, context=None):
        """Returns the first fiscal period of the given fiscal year.
        :param fiscalyear_id: integer (account.fiscalyear object ID)
        :returns: integer (account.period object ID)
        """
        return self.pool['account.period'].search(
            cr, uid,
            [
                ('special', '=', False),
                ('fiscalyear_id', '=', fiscalyear_id),
            ],
            order='date_start ASC',
            limit=1,
            context=context
        )[0]

    def get_end_period(self, cr, uid, ids, fiscalyear_id, context=None):
        """Returns the last fiscal period of the given fiscal year.
        :param fiscalyear_id: integer (account.fiscalyear object ID)
        :returns: integer (account.period object ID)
        """
        return self.pool['account.period'].search(
            cr, uid,
            [
                ('special', '=', False),
                ('fiscalyear_id', '=', fiscalyear_id),
            ],
            order='date_stop DESC',
            limit=1,
            context=context
        )[0]

    def get_trial_balance(
        self, cr, uid, ids, wizard, fiscalyear_id, context=None
    ):
        """Returns the trial balance that corresponds to the given fiscal year.
        :param wizard: browse record (wizard.bcr object)
        :param fiscalyear_id: integer (account.fiscalyear object ID)
        :returns: integer (trial.balance object ID)
        """

        trial_balance_wizard_obj = self.pool['wizard.trial_balance']

        trial_balance_wizard_id = trial_balance_wizard_obj.create(
            cr, uid, {
                'fiscalyear_id': fiscalyear_id,
                'centralized_groups': 'disabled',
                'include_draft_moves': wizard.include_draft_moves,
                'period_from': wizard.get_start_period(
                    fiscalyear_id,
                    context=context
                ),
                'period_to': wizard.get_end_period(
                    fiscalyear_id,
                    context=context
                ),
            }, context=context
        )

        trial_balance_wizard = trial_balance_wizard_obj.browse(
            cr, uid, [trial_balance_wizard_id], context=context
        )[0]

        return trial_balance_wizard.generate(
            context=context
        )['res_id']

    def get_asset_amount(self, category, debit, credit):
        """Returns the asset amount that corresponds to the given account
        category, for given debit and credit amounts.
        :param category: string ('asset', 'liability', 'mix')
        :param debit: float
        :param credit: float
        :returns: float
        """

        if category == 'asset':
            r = debit - credit
        elif category == 'liability':
            r = 0
        elif (debit - credit) > 0:
            r = debit - credit
        else:
            r = 0

        return r

    def get_liability_amount(self, category, debit, credit):
        """Returns the liability amount that corresponds to the given account
        category, for given debit and credit amounts.
        :param category: string ('asset', 'liability', 'mix')
        :param debit: float
        :param credit: float
        :returns: float
        """

        if category == 'liability':
            r = credit - debit
        elif category == 'asset':
            r = 0
        elif (credit - debit) > 0:
            r = credit - debit
        else:
            r = 0

        return r

    def edit_trial_balance(
        self, cr, uid, ids, trial_balance_id, context=None
    ):
        """Edits a trial balance, in order to add the debit and credit net
        amounts, and the asset and liability amounts, whether an account is an
        asset or a liability account, or follows a mixed rule.
        :param trial_balance_id: integer (trial.balance object ID)
        :returns: integer (trial.balance object ID)
        """

        trial_balance_obj = self.pool['trial_balance']

        trial_balance = trial_balance_obj.browse(
            cr, uid, [trial_balance_id], context=context
        )[0]

        company_id = trial_balance.fiscalyear_id.company_id.id

        query1 = """
            SELECT account.code, parent.balance_sheet_display
            FROM account_account AS account
            LEFT JOIN account_account as parent
            ON parent.id = account.parent_id
            WHERE account.company_id = %s;
        """

        cr.execute(
            query1, tuple([company_id])
        )

        accounts_infos = cr.fetchall()

        for line in trial_balance.line_ids:

            account_code = line.account_code
            category = [
                info[1]
                for info in accounts_infos
                if info[0] == account_code
            ][0]

            debit = line.end_balance_debit
            credit = line.end_balance_credit

            r = debit - credit

            line.write(
                {
                    'net_profit_loss_debit': r,
                    'net_profit_loss_credit': (-r),
                    'asset_amount': self.get_asset_amount(
                        category, debit, credit
                    ),
                    'liability_amount': self.get_liability_amount(
                        category, debit, credit
                    ),
                }
            )

        query2 = """
            SELECT
                SUM(line.net_profit_loss_debit),
                SUM(line.net_profit_loss_credit),
                SUM(line.asset_amount),
                SUM(line.liability_amount)
            FROM
                trial_balance_line AS line
            LEFT JOIN
                trial_balance AS balance
            ON
                balance.id = line.trial_balance_id
            WHERE
                balance.id = %s
        """

        cr.execute(query2, tuple([trial_balance_id]))

        trial_balance_sums = cr.fetchall()[0]

        trial_balance.write(
            {
                'total_net_profit_loss_debit': trial_balance_sums[0],
                'total_net_profit_loss_credit': trial_balance_sums[1],
                'total_asset_amount': trial_balance_sums[2],
                'total_liability_amount': trial_balance_sums[3],
            }
        )

        return trial_balance

    def get_extended_trial_balance(
        self, cr, uid, ids, wizard, fiscalyear_id, context=None
    ):
        """Returns a trial balance with debit and credit net amounts, and asset
        and liability amounts, from a given fiscal year.
         :param wizard: browse record (wizard.bcr object)
        :param fiscalyear_id: integer (account.fiscalyear object ID)
        :returns: integer (trial.balance object ID)
        """

        trial_balance_id = wizard.get_trial_balance(
            wizard, fiscalyear_id,
            context=context
        )

        trial_balance = wizard.edit_trial_balance(
            trial_balance_id, context=context
        )

        return trial_balance

    def generate_analytic_balance(self, cr, uid, ids, wizard, context=None):
        """Returns an analytic balance.
        An analytic balance is computed from the trial balances of the current
        and former fiscal years. It details the total moves of all the accounts
        linked to the chosen company (through the selection of given fiscal
        years), grouped by account analytic codes.
        :param wizard: browse record (wizard.bcr object)
        :returns: integer (bcr.analytic_balance object ID)
        """

        analytic_balance_obj = self.pool['bcr.analytic_balance']
        analytic_balance_line_obj = self.pool['bcr.analytic_balance.line']

        current_trial_balance = wizard.get_extended_trial_balance(
            wizard, wizard.current_fiscalyear_id.id,
            context=context
        )

        former_trial_balance = wizard.get_extended_trial_balance(
            wizard, wizard.former_fiscalyear_id.id,
            context=context
        )

        analytic_balance_id = analytic_balance_obj.create(
            cr, uid,
            {
                'current_trial_balance_id': current_trial_balance.id,
                'former_trial_balance_id': former_trial_balance.id,
            },
            context=context
        )

        balance_str = "(%s, %s)" % (
            current_trial_balance.id, former_trial_balance.id
        )
        company_id = wizard.current_fiscalyear_id.company_id.id
        dimension_id = wizard.computation_setting_id.dimension_id.id

        query1 = """
            SELECT structure.ordering
            FROM analytic_structure AS structure
            WHERE structure.nd_id = %s AND
            structure.model_name = 'account_account';
        """

        cr.execute(query1, tuple([dimension_id]))
        ordering = cr.fetchall()[0][0]

        query2 = (
            "SELECT code.id, "

            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % current_trial_balance.id +
            "THEN "
            "line.net_profit_loss_debit ELSE 0 END), "

            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % current_trial_balance.id +
            "THEN "
            "line.net_profit_loss_credit ELSE 0 END), "

            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % current_trial_balance.id +
            "THEN "
            "line.asset_amount ELSE 0 END), "

            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % current_trial_balance.id +
            "THEN "
            "line.liability_amount ELSE 0 END), "


            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % former_trial_balance.id +
            "THEN "
            "line.net_profit_loss_debit ELSE 0 END), "

            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % former_trial_balance.id +
            "THEN "
            "line.net_profit_loss_credit ELSE 0 END), "

            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % former_trial_balance.id +
            "THEN "
            "line.asset_amount ELSE 0 END), "

            "SUM(CASE WHEN ("
            "line.account_code = account.code AND " +
            "line.trial_balance_id = %s) " % former_trial_balance.id +
            "THEN "
            "line.liability_amount ELSE 0 END) "

            "FROM account_account AS account "
            "LEFT JOIN analytic_code AS code "
            "ON code.id = account.a" + "%s" % ordering + "_id " +
            "LEFT JOIN trial_balance_line AS line "
            "ON line.account_code = account.code "
            "WHERE account.company_id = %s " % company_id +
            "AND line.trial_balance_id IN " + balance_str + " "
            "GROUP BY code.id "
            "ORDER BY code.id;"
        )

        cr.execute(query2)
        results = cr.fetchall()

        query3 = (
            "SELECT item.id, cod.id, item.trial_balance_id "
            "FROM trial_balance_line AS item "
            "LEFT JOIN account_account AS acc "
            "ON item.account_code = acc.code "
            "LEFT JOIN analytic_code AS cod "
            "ON cod.id = acc.a" + "%s" % ordering + "_id " +
            "WHERE item.trial_balance_id IN " + balance_str + ";"
        )

        cr.execute(query3)
        line_lists = cr.fetchall()

        for result in results:

            if result[0] is None:
                continue

            current_line_list = list(set([
                (4, line_list[0])
                for line_list in line_lists
                if line_list[1] == result[0] and
                line_list[2] == current_trial_balance.id
            ]))

            former_line_list = list(set([
                (4, line_list[0])
                for line_list in line_lists
                if line_list[1] == result[0] and
                line_list[2] == former_trial_balance.id
            ]))

            analytic_balance_line_obj.create(
                cr, uid,
                {
                    'analytic_balance_id': analytic_balance_id,
                    'analytic_code_id': result[0],

                    'current_net_profit_loss_debit': result[1],
                    'current_net_profit_loss_credit': result[2],
                    'current_asset_amount': result[3],
                    'current_liability_amount': result[4],

                    'former_net_profit_loss_debit': result[5],
                    'former_net_profit_loss_credit': result[6],
                    'former_asset_amount': result[7],
                    'former_liability_amount': result[8],

                    'differential_net_profit_loss_debit': (
                        result[1] - result[5]
                    ),
                    'differential_net_profit_loss_credit': (
                        result[2] - result[6]
                    ),
                    'differential_asset_amount': (result[3] - result[7]),
                    'differential_liability_amount': (result[4] - result[8]),

                    'current_trial_balance_line_ids': current_line_list,
                    'former_trial_balance_line_ids': former_line_list,

                },
                context=context
            )

        return analytic_balance_id

    def compute_hierarchy_structure(
        self, cr, uid, ids, all_hierarchy, class_name, relation, context=None
    ):
        """Classifies different elements of a hierarchy, function of their
        respective ranks in the tree. The leaves, or top elements, are first
        identified, and ranked as firsts. Each hierarchical level, immediately
        underneath of the former, is then identified by listing the elements
        that are only related to the elements of the former levels.
        A list of all the tree elements IDs is returned, classifying the
        elements in the order in which their amounts should be computed.
        :param all_hierarchy: integer list (class_name object IDs)
        :param class_name: string (name of the objects class for which
        all_hierarchy is the IDs list)
        :param relation: string (name of the field listing the former levels
        elements IDs that are related to a given element object)
        :returns: (integer rank, integer element ID) tuple list (class_name
        object ID)
        """

        item_obj = self.pool[class_name]
        items = item_obj.browse(cr, uid, all_hierarchy, context=context)

        _dict = {}

        n = 0
        classified = []
        while len(classified) != len(items):
            _dict[n] = []
            for item in items:
                if item.id not in classified:
                    relatives = getattr(item, relation)
                    if relatives:
                        exclude = False
                        for relative in relatives:
                            if relative.id not in classified:
                                exclude = True
                                break
                        if not exclude:
                            _dict[n].append(item.id)
                    else:
                        _dict[0].append(item.id)

            classified += _dict[n]
            n += 1

        n = 0
        hierarchy_structure = []
        for level in _dict.keys():
            for element in _dict[level]:
                n += 1
                hierarchy_structure.append((n, element))

        return hierarchy_structure

    def generate(self, cr, uid, ids, context=None):
        """Generates a profit & loss balance sheet on the basis of the data
        given to the wizard.
        """

        wizard = self.browse(cr, uid, ids, context=context)[0]
        plbs_obj = self.pool['bcr.plbs']
        plbs_hierarchy_obj = self.pool['bcr.plbs.hierarchy']
        hierarchy_obj = self.pool['bcr.computation_hierarchy']

        # Check that there exists a hierarchy associated to the given
        # computation setting.
        leaves = set(hierarchy_obj.search(
            cr, uid,
            [
                (
                    'computation_setting_id', '=',
                    wizard.computation_setting_id.id
                )
            ],
            context=context
        ))

        # If no hierarchy associated to the given computation setting exists,
        # return an exception.
        if not leaves:
            raise exceptions.Warning(
                _(
                    u"No computation hierarchy has been defined in "
                    u"association with this computation setting."
                )
            )

        # Identify all the elements of the hierarchy associated to the given
        #  computation setting.
        all_hierarchy = list(hierarchy_obj.find_relatives(
            cr, uid, ids, leaves, 'bcr.computation_hierarchy',
            'child_hierarchy_ids', context=context
        ))

        # Compute the analytic balance.
        analytic_balance_id = wizard.generate_analytic_balance(
            wizard, context=context
        )

        # Create a profit & loss balance sheet with empty fields.
        plbs_id = plbs_obj.create(
            cr, uid,
            {
                'computation_setting_id': wizard.computation_setting_id.id,
                'analytic_balance_id': analytic_balance_id,
                'current_fiscalyear_id': wizard.current_fiscalyear_id.id,
                'former_fiscalyear_id': wizard.former_fiscalyear_id.id,
            },
            context=context
        )

        # Generate the list of the amount fields names of the profit & loss
        # balance sheet.
        first_part_names = [
            'current_', 'former_', 'differential_'
        ]
        second_part_names = [
            'net_profit_loss_debit', 'net_profit_loss_credit',
            'asset_amount', 'liability_amount'
        ]
        field_names = []
        for a, first in enumerate(first_part_names):
            for b, second in enumerate(second_part_names):
                field_names.append(first + second)

        # Generates a segment of the SQL query, that will sum all the amounts
        # within a group of analytic balance lines, which analytic codes match
        # the hierarchical result configuration.
        # Prepares the segment asking to sum each amounts linked to the amount
        # fields names.
        field_string = ""
        for field_name in field_names:
            field_string += (
                "SUM(" + "line." + field_name + "), "
            )
        field_string = field_string[0:len(field_string) - 2]

        # Generates a segment of the SQL query, that will sum all the amounts
        # within a group of analytic balance lines, which analytic codes match
        # the hierarchical result configuration.
        # Prepares the segment asking to seek all the hierarchical computation
        # configuration objects of the computation setting tree.
        hierarchy_str = "("
        for element in all_hierarchy:
            hierarchy_str += "%s, " % element
        hierarchy_str = hierarchy_str[0:len(hierarchy_str) - 2] + ")"

        # Generates the list of all the elements IDs of the computation setting
        # tree, along their rank in the computation order.
        hierarchy_structure_table = (
            wizard.compute_hierarchy_structure(
                all_hierarchy, 'bcr.computation_hierarchy',
                'child_hierarchy_ids',
                context=context
            )
        )

        # Generates the SQL query, that will sum all the amounts within a group
        # of analytic balance lines, which analytic codes match the
        # hierarchical result configuration.
        query1 = (
            "SELECT hierarchy.id, " + field_string + " "
            "FROM bcr_computation_hierarchy_analytic_code_rel AS rel "
            "LEFT JOIN bcr_computation_hierarchy AS hierarchy "
            "ON rel.hierarchy_id = hierarchy.id "
            "LEFT JOIN analytic_code AS code "
            "ON rel.analytic_code_id = code.id "
            "LEFT JOIN bcr_analytic_balance_line AS line "
            "ON line.analytic_code_id = code.id "
            "WHERE hierarchy.id IN " + hierarchy_str + " "
            "AND line.analytic_balance_id = %s" % analytic_balance_id + " "
            "GROUP BY hierarchy.id "
            "ORDER BY hierarchy.id;"
        )

        # Execute the query and fetch its results.
        cr.execute(query1)
        results1 = cr.fetchall()

        # Generates the SQL query, that will identify all the analytic balance
        # lines, which analytic codes match the hierarchical result
        # configuration.
        query2 = (
            "SELECT line.id, hierarchy.id "
            "FROM bcr_computation_hierarchy_analytic_code_rel AS rel "
            "LEFT JOIN bcr_computation_hierarchy AS hierarchy "
            "ON rel.hierarchy_id = hierarchy.id "
            "LEFT JOIN analytic_code AS code "
            "ON rel.analytic_code_id = code.id "
            "LEFT JOIN bcr_analytic_balance_line AS line "
            "ON line.analytic_code_id = code.id "
            "WHERE hierarchy.id IN " + hierarchy_str + " "
            "AND line.analytic_balance_id = %s" % analytic_balance_id + " "
            "ORDER BY hierarchy.id;"
        )

        # Execute the query and fetch its results.
        cr.execute(query2)
        results2 = cr.fetchall()

        # Create the hierarchical results lines of the profit & loss balance
        # sheet, on the basis of a first computation based on analytic codes
        # only. The results will be summed in their computation order in a
        # later stage.
        for hierarchical in all_hierarchy:

            result = [
                results
                for results in results1
                if results[0] == hierarchical
            ]

            if result:
                result = result[0]

                analytic_line_list = list(set([
                    (4, line_list[0])
                    for line_list in results2
                    if line_list[1] == result[0]
                ]))
            else:
                result = [hierarchical]
                for i in range(12):
                    result.append(0)

                analytic_line_list = False

            plbs_hierarchy_obj.create(
                cr, uid,
                {
                    'plbs_id': plbs_id,
                    'computation_hierarchy_id': result[0],

                    'current_net_profit_loss_debit': result[1],
                    'current_net_profit_loss_credit': result[2],
                    'current_asset_amount': result[3],
                    'current_liability_amount': result[4],

                    'former_net_profit_loss_debit': result[5],
                    'former_net_profit_loss_credit': result[6],
                    'former_asset_amount': result[7],
                    'former_liability_amount': result[8],

                    'differential_net_profit_loss_debit': result[9],
                    'differential_net_profit_loss_credit': result[10],
                    'differential_asset_amount': result[11],
                    'differential_liability_amount': result[12],

                    'analytic_balance_line_ids': analytic_line_list,

                },
                context=context
            )

        # Generates the SQL query, that will identify all the hierarchical
        # results lines, that are to be summed in a formerly determined order
        # to give the final hierarchical results.
        # A pack of child hierarchical results lines is identified for each
        # hierarchical result.
        query3 = (
            "SELECT line.id, parent.id, result.id "
            "FROM bcr_computation_hierarchy_hierarchy_rel AS rel "
            "LEFT JOIN bcr_computation_hierarchy AS child "
            "ON rel.child_hierarchy_id = child.id "
            "LEFT JOIN bcr_computation_hierarchy AS parent "
            "ON rel.parent_hierarchy_id = parent.id "
            "LEFT JOIN bcr_plbs_hierarchy AS line "
            "ON line.computation_hierarchy_id = child.id "
            "LEFT JOIN bcr_plbs_hierarchy AS result "
            "ON result.computation_hierarchy_id = parent.id "
            "WHERE parent.id IN " + hierarchy_str + " "
            "AND line.plbs_id = %s " % plbs_id +
            "AND result.plbs_id = %s;" % plbs_id
        )

        # Execute the query and fetch its results.
        cr.execute(query3)
        results3 = cr.fetchall()

        # Write in the hierarchical results lines of the profit & loss balance
        # sheet, the final sums of their amounts in the formerly determined
        # computation order.
        for level, hierarchical in hierarchy_structure_table:

            hierarchy_line_m2m = [
                (4, hierarchy_list[0])
                for hierarchy_list in results3
                if hierarchy_list[1] == hierarchical
            ]

            hierarchy_line_list = [
                hierarchy_list[0]
                for hierarchy_list in results3
                if hierarchy_list[1] == hierarchical
            ]

            plbs_hierarchy_id = [
                hierarchy_list[2]
                for hierarchy_list in results3
                if hierarchy_list[1] == hierarchical
            ]

            if plbs_hierarchy_id:
                plbs_hierarchy_id = plbs_hierarchy_id[0]

                hierarchy_line_list.append(plbs_hierarchy_id)

                r = []
                for i in range(12):
                    r.append(0)
                for item in plbs_hierarchy_obj.browse(
                    cr, uid, hierarchy_line_list, context=context
                ):
                    for j, field_name in enumerate(field_names):
                        r[j] += getattr(item, field_name)

                plbs_hierarchy_obj.write(
                    cr, uid, [plbs_hierarchy_id],
                    {
                        'plbs_hierarchy_line_ids': hierarchy_line_m2m,

                        'current_net_profit_loss_debit': r[0],
                        'current_net_profit_loss_credit': r[1],
                        'current_asset_amount': r[2],
                        'current_liability_amount': r[3],

                        'former_net_profit_loss_debit': r[4],
                        'former_net_profit_loss_credit': r[5],
                        'former_asset_amount': r[6],
                        'former_liability_amount': r[7],

                        'differential_net_profit_loss_debit': r[8],
                        'differential_net_profit_loss_credit': r[9],
                        'differential_asset_amount': r[10],
                        'differential_liability_amount': r[11],
                    },
                    context=context
                )

        # Return an action pointing to the newly created profit & loss balance
        # sheet object.
        return {
            'type': 'ir.actions.act_window',
            'name': _('Profit & Loss - Balance Sheet'),
            'res_model': 'bcr.plbs',
            'res_id': plbs_id,
            'view_mode': 'form',
            'view_type': 'form',
        }
