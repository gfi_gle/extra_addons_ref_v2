from openerp import api, fields, models, exceptions



class res_service(models.Model):
    _name = 'res.service'

    name = fields.Char(
        'Service',
        translate=True,
    )
