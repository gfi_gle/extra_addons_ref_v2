from openerp import api, fields, models, exceptions


class res_typology(models.Model):
    _name = 'res.typology'

    name = fields.Char(
        'Typology',
        translate=True,
    )
