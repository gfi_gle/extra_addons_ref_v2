# encoding: utf-8
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from openerp import api, fields, models, exceptions


class res_partner(models.Model):
    _inherit = 'res.partner'

    service_id = fields.Many2one(
        'res.service',
        'Service',
    )
    typology_id = fields.Many2one(
        'res.typology',
        'Typology',
        ondelete="restrict",
    )

    def update_lead_data(self, cr, uid, partner_id, vals, context=None):
        self.write(cr, uid, [partner_id], vals, context)

    @api.one
    @api.depends("firstname", "lastname")
    def _compute_name(self):
        """Write the 'name' field according to splitted data."""
        # fix bug unlink
        try:
            super(res_partner, self)._compute_name()
        except:
            pass
    