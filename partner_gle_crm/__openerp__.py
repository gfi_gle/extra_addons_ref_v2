# -*- coding: utf-8 -*-
##############################################################################
#     
#    Better User login for Odoo
#    Copyright (C) 2015 XCG Consulting (http://odoo.consulting)
#    Author: Vincent Hatakeyama
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': 'partner_gle_crm',
    'version': '8.0.1.2',
    'author': 'XCG Consulting',
    'category': '', # TODO add technical
    'description': """
Partner GLE CRM
===============

    """,
    'website': 'http://odoo.consulting',
    'init_xml': [],
    'depends': [
        'crm',
        'partner_firstname', #fix bug unlink
        'partner_naf',
    ],
    'data': [
        'security/user_group.xml',
        'security/ir.model.access.csv',
        'views/res_partner_view.xml',
        'views/menu.xml',
    ],
    'demo': [
        'demo/res.service.csv',
        'demo/res.typology.csv',
        'demo/res.partner.csv',
    ],
    'test': [
    ],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
