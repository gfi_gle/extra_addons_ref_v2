.. Partner GLE CRM documentation master file, created by
   sphinx-quickstart on Wed Apr 13 12:35:36 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Partner GLE CRM's documentation!
===========================================

.. include:: manifest

Contents:

.. toctree::
   :maxdepth: 2

   README
   NEWS
   TODO

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

