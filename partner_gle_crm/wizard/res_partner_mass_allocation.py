# encoding: utf-8
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from openerp import models, fields, api


class crm_lead_mass_allocation(models.TransientModel):
    _name = "res.partner.mass.allocation"

    section_id = fields.Many2one(
        'crm.case.section',
        'Sales Team',
    )
    user_id = fields.Many2one(
        'res.users',
        'Salesperson',
        select=True,
    )

    @api.multi
    def validate(self):
        for partner in self.env['res.partner'].browse(self._context.get("active_ids")):
            vals = {
                'section_id': self.section_id.id,
                'user_id': self.user_id.id,
            }
            partner.write(vals)
        return {}
