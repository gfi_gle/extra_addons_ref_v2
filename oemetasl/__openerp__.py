# -*- coding: utf-8 -*-
##############################################################################
#
#    OEmetaSL, for OpenERP
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Author: Anael LORIMIER <anael.lorimier@xcg-consulting.fr>
#            Vincent Lhote-Hatakeyama <vincent.lhote@xcg-consulting.fr>
#
##############################################################################

{
    'name': "OEmetaSL",
    'version': '0.6',
    'author': "XCG Consulting",
    'category': "Custom Module",
    'description': """Empty on purpose.

This module is now obsolete; see xcg/base_no_copy for an alternative.
    """,
    'website': "http://www.openerp-experts.com/",
    'depends': [
        'base',
    ],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
