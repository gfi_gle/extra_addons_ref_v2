### README ###


### Account Move Reversal for Odoo/OpenERP ###

This module allows to reverse account entries in Odoo

Example
=======

A reversal can be from that certain products or accounting charges
recorded during a year does not concern only one.
For example, a one-year subscription to a magazine subscribed
September 1 must be distributed at 4 months ( 1/3) of the current
fiscal year and 8 months (2/3) the following year.
2/3 of the registered charge will therefore be " reversed " the following year.

You can find a related post for the example at: http://bit.ly/1C7pWNB

Dependencies
============

- account_streamline >8.0.2.1.0