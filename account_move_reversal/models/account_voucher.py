# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://odoo.consulting/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _


class account_voucher(models.Model):

    _inherit = 'account.voucher'

    def reject_voucher(self, cr, uid, ids, context=None):

        move = self.browse(cr, uid, ids, context=context)[0]
        if move.journal_id.is_not_reversable:
            raise Exception(
                _(u"Error"),
                _(u"Reversal is not allowed in this journal.")
            )

        context = context.copy()
        context['active_ids'] = [move.id]

        return {
            'name': 'Select Reversal Date',
            'type': 'ir.actions.act_window',
            'res_model': 'account.move.reversal.date',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': context
        }

    @api.multi
    def _get_reversed_move_ids(self):
        move_obj = self.env['account.move']
        move_line_obj = self.env['account.move.line']
        for voucher in self:
            if voucher.number:
                move_record = move_obj.search(
                        [('name', '=', voucher.number + _('-reversal'))]
                )
                if move_record:
                    move_records = move_line_obj.search(
                        [('move_id', '=', move_record.id)]
                    )

                    if move_records:
                        self.reversed_move_line_ids = [
                            record.id for record in move_records
                        ]
                    else:
                        self.reversed_move_line_ids = []

    @api.one
    @api.depends('reversed_move_line_ids')
    def _get_has_reversed_move_line_ids(self):
        self.has_reversed_move_line_ids = bool(self.reversed_move_line_ids)

    reversed_move_line_ids = fields.One2many(
        'account.move.line',
        'id',
        compute='_get_reversed_move_ids',
        method=True
        )
    has_reversed_move_line_ids = fields.Boolean(
        string='Has some reversed move lines',
        compute='_get_has_reversed_move_line_ids',
        method=True
        )
    state = fields.Selection(
        selection_add=[('rejected', _(u'Rejeté'))],
        help=(
            ' * The \'Draft\' status is used when a user is encoding a new '
            'and unconfirmed Voucher. \n'
            '* The \'Pro-forma\' when voucher is in Pro-forma status,voucher '
            'does not have an voucher number. \n'
            '* The \'Posted\' status is used when user create voucher,a '
            'voucher number is generated and voucher entries are created in '
            'account \n'
            '* The \'Cancelled\' status is used when user cancel voucher. \n'
            '* The \'Rejected\' status is used when user reject payment.'
        ),
    )
