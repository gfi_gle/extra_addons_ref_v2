# -*- coding: utf-8 -*-

from collections import defaultdict
from openerp.osv import osv
from openerp.tools.translate import _


class AccountingDocument(osv.Model):
    """Add reversal capabilities to accounting documents.
    """

    _inherit = 'account.move'

    def button_reverse_move(self, cr, uid, ids, context=None):
        move = self.browse(cr, uid, ids, context=context)[0]
        if move.journal_id.is_not_reversable:
            raise osv.except_osv(
                _(u"Error"),
                _(u"Reversal is not allowed in this journal.")
            )

        context['active_ids'] = [move.id]
        return {
            'name': 'Create Move Reversals',
            'type': 'ir.actions.act_window',
            'res_model': 'account.move.reversal.create',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'context': context
        }

    def reverse_move(
        self, cr, uid, move_id, journal_id, date, doc_name=None, doc_ref=None,
        reconcile=False, context=None,
    ):
        """Create an accounting document that represents the reverse of the
        specified accounting document.
        It is mostly a copy, except:
        - Debit / Credit values are switched.
        - The name & reference are customized
        - The maturity date is changed to the current date.

        :param move_id: The ID of the accounting document to reverse.
        :param journal_id: The ID of the accounting journal that will hold the
        reversed accounting document.
        :param doc_name: Custom name for the reversed accounting document; when
        unspecified, "-reversal" will be appended.
        :param doc_ref: Custom reference for the reversed accounting document;
        when unspecified, "-reversal" will be appended.
        :param reconcile: Whether source and reversed accounting entries are to
        be reconciled.

        :return: ID of the reversed accounting document.
        """

        move_obj = self.pool['account.move']

        move = move_obj.browse(cr, uid, move_id, context=context)
        period_id = self.pool['account.period'].find(
            cr, uid, dt=date, context=context
        )[0]

        # Customize the name & reference of the reversed accounting document.
        reversed_name = (
            '/' if move.name == '/'
            else doc_name if doc_name is not None
            else _('%s-reversal') % move.name
        )
        reversed_ref = (
            '' if not move.ref
            else doc_ref if doc_ref is not None
            else _('%s-reversal') % move.ref
        )

        # Prepare the data to create the reversed accounting document with.
        vals = {
            'name': reversed_name,
            'ref': reversed_ref,
            'period_id': period_id,
            'date': date,
            'journal_id': journal_id,
            'partner_id': move.partner_id.id,
            'narration': move.narration,
            'company_id': move.company_id.id,
        }

        # Dynamically copy fields of move lines.

        move_line_fields = self.pool['account.move.line'].fields_get(
            cr, uid, context=context
        )

        vals['line_id'] = []

        # This tricky part just copy the fields from aml depending on the types
        # of the field. The types are mapped in the below enumeration

        fnct_types = {
            'many2one': lambda val: val.id,
            'one2many': lambda vals: [(6, 0, [v.id for v in vals])],
            'many2many': lambda vals: [(6, 0, [v.id for v in vals])],
            'default': lambda val: val,
        }
        for aml in move.line_id:
            new_line = {}
            for key in move_line_fields:
                # Call a function that will format the value depending on type
                new_line[key] = fnct_types.get(
                    move_line_fields[key]['type'],
                    fnct_types['default']
                )(getattr(aml, key))

            # Some specific changes
            new_line['name'] = aml.name + _('-reversal')
            new_line['debit'] = aml.credit
            new_line['credit'] = aml.debit
            new_line['amount_currency'] = -aml.amount_currency

            vals['line_id'].append((0, 0, new_line))

        reversed_move_id = self.create(cr, uid, vals, context=context)

        if reconcile:
            reversed_doc = move_obj.browse(
                cr, uid, [reversed_move_id], context=context
            )[0]

            # Group entries to reconcile by accounting account.
            entries_by_account = defaultdict(set)
            for acc_doc in (move, reversed_doc):
                for entry in acc_doc.line_id:
                    entries_by_account[entry.account_id.id].add(entry.id)

            # Reconcile each accounting entry set.
            for entry_set in entries_by_account.itervalues():
                if len(entry_set) <= 1:
                    continue
                self.pool['account.move.line'].reconcile(
                    cr, uid, list(entry_set), context=context,
                )

        return reversed_move_id
