# -*- coding: utf-8 -*-

from openerp.osv import fields
from openerp.osv import osv

from openerp.tools.translate import _

from post_function import (
    call_post_function,
    call_post_err_function,
)


class account_move_reversal_create(osv.osv_memory):

    _name = 'account.move.reversal.create'

    _columns = {
        'date': fields.date(
            u"Reversal date",
            required=True,
            default=fields.date.today,
        ),
    }

    def _check_reconciliation(self, cr, uid, move, context):
        for line in move.line_id:
            if line.reconcile_partial_id or line.reconcile_id:
                return True
        return False

    def _create_reversals_no_state(
            self, cr, uid, ids, reversal_date, move_ids, context=None
    ):
        """Applies reversals, note that this method does not depend on the
        'ids' parameter for the reversal date nor does it rely on the context
        to get the move ids.

        This method allows us to apply reversals without creating the wizard in
        the database.
        """
        move_obj = self.pool['account.move']

        for move in move_obj.browse(
            cr, uid, move_ids, context=context
        ):
            move.line_id.invalidate_cache()
            if self._check_reconciliation(cr, uid, move, context):
                raise osv.except_osv(
                    _(u"Error"),
                    _(u"One of the transaction of the journal is reconciled. "
                      u"Reversing the move is forbidden.")
                )

            move_obj.reverse_move(
                cr, uid, move.id, move.journal_id.id, reversal_date,
                reconcile=True, context=context,
            )

        # Call post functions set by caller
        context = context.copy() if context is not None else {}
        call_post_function(cr, uid, context)

    def create_reversals(self, cr, uid, ids, context=None):
        wizard_dicts = self.read(cr, uid, ids, ['date'], context=context)

        try:
            for wizard_dict in wizard_dicts:
                self._create_reversals_no_state(
                    cr, uid, ids, wizard_dict['date'], context['active_ids'],
                    context=context,
                )
        except Exception as e:
            context = context.copy() if context is not None else {}
            call_post_err_function(cr, uid, context)
            raise e

        return {'type': 'ir.actions.act_window_close'}
