# -*- coding: utf-8 -*-
from collections import defaultdict

from openerp import fields, models


class account_move_reversal_date(models.TransientModel):

    _name = 'account.move.reversal.date'

    def _create_reversals_no_state(
            self, cr, uid, ids, reversal_date, voucher_ids, context=None
    ):
        """Applies reversals, note that this method does not depend on the
        'ids' parameter for the reversal date nor does it rely on the context
        to get the voucher ids.

        This method allows us to apply reversals without creating the wizard in
        the database.
        """
        if not context:
            context = {}

        voucher_obj = self.pool['account.voucher']
        move_line_obj = self.pool['account.move.line']
        reconcile_obj = self.pool['account.move.reconcile']
        batch_move_line_obj = self.pool['account.payment_batch.move.line']

        for voucher in voucher_obj.browse(
            cr, uid, voucher_ids, context=context
        ):
            batch_line = voucher.payment_batch_line_id
            if batch_line:
                # Find the reconciled lines in the batch and voucher
                voucher_reconcile = defaultdict(list)
                for ml in voucher.move_ids:
                    if ml.reconcile_id:
                        voucher_reconcile[ml.reconcile_id.id].append(ml.id)
                batch_reconcile = {}
                for bline in batch_line.line_ids:
                    ml_ids = voucher_reconcile[bline.reconcile_id.id]
                    if ml_ids:
                        if ml_ids[0] in batch_reconcile:
                            batch_reconcile[ml_ids[0]].append(bline)
                        else:
                            blines = [bline]
                            for ml_id in ml_ids:
                                batch_reconcile[ml_id] = blines
            else:
                batch_reconcile = False

            for move_line in voucher.move_ids:
                move_line.refresh()
                if move_line.reconcile_id:
                    move_lines = [
                        line.id for line
                        in move_line.reconcile_id.line_id
                        if line.id != move_line.id
                    ]
                    reconcile_obj.unlink(cr, uid, [move_line.reconcile_id.id])
                    if len(move_lines) >= 2:
                        move_line_obj.reconcile_partial(
                            cr, uid, move_lines, 'auto', context=context
                        )
            reversal_move = self.pool['account.move'].reverse_move(
                cr,
                uid,
                voucher.move_id.id,
                voucher.move_id.journal_id.id,
                reversal_date,
                reconcile=True,
                context=context
            )
            if batch_reconcile:
                old_lines, new_lines = [], []
                for reversal_line in reversal_move.line_id:
                    for rec_line in reversal_line.reconcile_id.line_id:
                        batch_move_lines = batch_reconcile.get(rec_line.id)
                        if not batch_move_lines:
                            continue
                        batch_move_line = batch_move_lines.pop()
                        old_lines.append(batch_move_line.move_line_id.id)
                        new_lines.append(reversal_line.id)
                        batch_move_line_obj.write(
                            cr, uid, [batch_move_line.id],
                            {'move_line_id': reversal_line.id},
                            context=context
                        )
                        break
                old_vals = {'payment_batch_id': False}
                move_line_obj.write(
                    cr, uid, old_lines, old_vals, context=context
                )
                new_vals = {'payment_batch_id': batch_line.batch_id.id}
                move_line_obj.write(
                    cr, uid, new_lines, new_vals, context=context
                )

            voucher.state = 'rejected'

        return True

    def create_reversals(self, cr, uid, ids, context=None):
        """
        a part of the following code is a copy of code of the following method:
        addons/account_voucher.account_voucher.cancel_voucher

        """
        if not context:
            context = {}

        wizards = self.browse(cr, uid, ids, context=context)

        for wizard in wizards:
            if wizard.date:
                self._create_reversals_no_state(
                    cr, uid, ids, wizard.date, context['active_ids'],
                    context=context,
                )
        return True

    date = fields.Date(
        u"Reversal date", required=True, default=fields.Date.today
    )
