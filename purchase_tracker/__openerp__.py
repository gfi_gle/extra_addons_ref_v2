# -*- coding: utf-8 -*-
##############################################################################
#
#    Purchase Tracker, for OpenERP
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Purchase Tracker",
    "version": '8.0.1.19',
    "author": "XCG Consulting",
    "category": 'Invoicing / Audit',
    "description": """
Purchase Tracker
================

Tracking of purchase orders and invoices
before they are really keyed in the accounting books

Configuration
-------------

* property_purchase_invoice_journal (in configuration/parameters):
In the first unnamed selector, select "account.journal"; in the second one,
pick the accounting journal the purchase tracker will create invoice lines into
when validated.

* In Configuration>Parameter>System Parameters, configure
purchase_tracker.activate_second_validation if the second validation is needed
(1 or true to activate).
    """,
    'website': 'http://odoo.consulting/',
    'init_xml': [],
    "depends": [
        'mail',
        'account_invoice_streamline',
        'analytic_structure',
        'hr_streamline',
        'purchase_streamline',
    ],
    "data": [
        'security/groups.xml',
        'security/record_rules.xml',
        'data/purchasetracker_data.xml',
        'data/tracker_sequence.xml',
        'views/account_invoice.xml',
        'views/fetchmail_server.xml',
        'views/purchase_order.xml',
        'views/purchase_tracker.xml',
        'views/res_config.xml',
        'workflows/account_invoice.xml',
        'workflows/purchase_tracker.xml',
        'data/ir.config_parameter.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
