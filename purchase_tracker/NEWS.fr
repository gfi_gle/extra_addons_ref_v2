purchase_tracker 8.0.1.17
~~~~~~~~~~~~~~~~~~~~~

* Merge with 8.0.1.14 branch

purchase_tracker 1.9.8

- Correction d'un lien entre factures & trackers.

purchase_tracker 1.8.9

 ∅

purchase_tracker 1.8.1.11

- Séparation des devis et bons de commande différente (uniquement sur ​​l'état «approuvé» ).

purchase_tracker 1.8.1.10

- Ajout d'une colonne 'order status'

purchase_tracker 1.8.1.9

- Retirer la génération manuelle de facture de bons de commande ; l'achat Tracker remplace la fonction d'une meilleure façon.

purchase_tracker 1.8.1.8

- Selection des bons de commande: Uniquement les standards

purchase_tracker 1.8.1.7

- Mise à jour des traductions en français.
- Interdire annulation de commandes avec des trackers qui ont généré des lignes de facture 

purchase_tracker 1.8.1.6

- Filtrer la liste sur les trackers standards par défaut.
- Mise à jour des traductions en français.
- Erreur explicite lorsque la propriété " property_purchase_invoice_journal " est manquante.
- Allowing changing the purchase order's tracker status at creation.
- Permettre de changer le statut de tracker de l'ordre d'achat à la création.

purchase_tracker 1.8.8

- TKT/2014/00541 [imp] Ajout d'une configuration pour désactiver la copie du terme de paiement po partenaire au niveau de l'entreprise .
- [l10n] Mise à jour des traductions en français.
- TKT/2014/00230 [imp] Changement du total pour etre accepté dans le domaine du tracker du bon de commande.

purchase_tracker 1.8.7

- [i18n] [fix]  Correction du champ employée d'un bon de commande
- [fix] Réorganisation des champs amount_balance et amount_untaxed.

purchase_tracker 1.8.6

- TKT/2014/00241 [imp] Ajout d'un regroupement.
- [i18n] Correction de l'affivchage de la sélection.
- TKT/2014/00239 [fix]Correction de l'utilisation du self.

purchase_tracker 1.8.5

- [ imp ] Implémentation du tri de l'état du ​​tracker.
- TKT/2014/00239 [fix] Correction des données saisies.
- [l10n] Mise à jour des traductions en français.
- TKT/2014/00239 Ajout d'une configuration manquante.

purchase_tracker 1.8.4

- Tell the invoice to fill move object reference with the tracker if the invoice is linked to a tracker.
- Remplir la facture avec le référence d'objet du tracker si la facture est liée à un tracker .
- Envoyer date d'échéance à la facture générée.

purchase_tracker 1.8.1.5

- Correction de règles de sécurité.

purchase_tracker 1.8.1.4

- Vérification que la séquence est extraite de la bonne compagnie.

purchase_tracker 1.8.1.3

- Ajout d'un contexte pour avoir la séquence.

purchase_tracker 1.8.1.2

- TKT/2014/00458 [fix] Ajout d'un nouveau champ de configuration  pour les serveurs entrants.

purchase_tracker 1.8.1.1

- Le tracker de la société est utilisé pour récupérer les propriétés.

purchase_tracker 1.8.3

- Ajout d'une vue de recherche par défaut pour les filtres.
- AJout d'un filtre sur le statut du tracker pour les factures.
- Ajout d'un filtre sur l'id du tracker pour les factures.
- Amélioration du regroupement par Etat: Nous ignorons désormais les bons de commande approuvés. 

purchase_tracker 1.8.2 

- Ajout d'un regroupement et d'un filtre sur le champ partner_id.

purchase_tracker 1.8.1

- Sécurité: Ajout de règles pour le multi-compagnies.

purchase_tracker 1.8

 ∅
 
 purchase_tracker 1.7.9
 
- Mise à jour des traductions en français.
- TKT/2014/00230 [fix] Ajout d'un domaine (not total) sur les bons de commande. 
