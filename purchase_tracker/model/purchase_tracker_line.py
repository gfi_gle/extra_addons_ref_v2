"""defines the purchase_tracker_line
"""

from openerp.osv import osv, fields
import openerp.addons.decimal_precision as dp


class purchase_tracker_line(osv.Model):
    """Reference a purchase.order.line
    and the quantity that was delivered.
    """
    _name = "purchase_tracker.tracker.line"
    _description = "Purchase Tracker Line"

    _columns = {
        'tracker_id': fields.many2one(
            'purchase_tracker.tracker',
            'Tracker',
            ondelete='cascade',
            required=True),
        'po_line_id': fields.many2one(
            'purchase.order.line',
            'Original Line'),
        'product_id': fields.many2one(
            'product.product',
            'Product',
            required=True,
            ondelete='set null'),
        'product_qty': fields.float(
            'Quantity delivered',
            digits_compute=dp.get_precision('Product Unit of Measure'),
            required=True),
        'price_unit': fields.float(
            'Unit Price',
            required=True,
            digits_compute=dp.get_precision('Product Price')),
        'prod_name': fields.related(
            'product_id',
            'name', string="Product name",
            type="char", store=False, readonly=True,
        ),
        'description': fields.char('Description'),
    }
