# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 XCG Consulting (www.xcg-consulting.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import fields, osv


class fetchmail_server(osv.Model):
    '''Add ability to force the company (used with multi company
    trackers creation)
    '''

    _name = 'fetchmail.server'
    _inherit = 'fetchmail.server'

    _columns = {
        'force_company_id': fields.many2one(
            'res.company', 'Force Company',
            help="Force Company to this value in context",
        ),
    }

    def fetch_mail(self, cr, uid, ids, context={}):
        '''Add the force_company_id to context if present'''
        if context is None:
            context = {}
        for server in self.browse(cr, uid, ids, context=context):
            new_context = context.copy()
            if server.force_company_id:
                new_context['force_company'] = server.force_company_id.id
            super(fetchmail_server, self).fetch_mail(
                cr, uid, [server.id], new_context)
