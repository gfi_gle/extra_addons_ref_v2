import ast
from openerp import api
from openerp import fields
from openerp import models


class PurchaseTracker(models.Model):
    """Customizations to purchase trackers, done Odoo 8 style:

    - Avoid a regular Odoo many2one field when showing the generated invoice,
    as form views opened from it are not always the right ones (in particular,
    for supplier refunds, it opens a client invoice view).
    """

    _inherit = 'purchase_tracker.tracker'

    @api.one
    def _get_invoice_label(self):
        """Forward the result of a "name_get" call on the invoice."""
        if self.invoice_id:
            name_list = self.invoice_id.name_get()
            if name_list:
                self.invoice_label = name_list[0][1]
                return
        self.invoice_label = u''

    # Utility field, only shown in purchase tracker forms.
    invoice_label = fields.Char(
        compute=_get_invoice_label,
        string='Generated invoice',
        help='Invoice generated from this tracker',
    )

    @api.multi
    def open_supplier_invoice(self):
        """Display the invoice / refund linked to this purchase tracker.
        Respect the specific actions defined for each invoice / refund kind.
        """

        self.ensure_one()

        invoice = self.invoice_id
        if not invoice:
            return

        action_ref = (
            'action_invoice_tree4' if self.invoice_id.type == 'in_refund'
            else 'action_invoice_tree2'
        )
        action = self.env.ref('account.%s' % action_ref).read()[0]

        context = ast.literal_eval(action['context'])
        context.update(self.env.context)

        action.update({
            'context': context,
            'res_id': invoice.id,
            'view_mode': 'form',
            'views': [(False, 'form')],
        })

        return action
