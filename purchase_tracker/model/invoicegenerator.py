# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 XCG Consulting (www.xcg-consulting.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
"""Defines helper class used to generate tracker based invoice.
"""
import datetime

from openerp import pooler, SUPERUSER_ID
from openerp.osv import osv
from openerp.tools.translate import _


class BaseInvoiceBuilder(object):
    """Base class used to generate invoice
    based on a purchase tracker
    """

    def __init__(self, cr, uid, context, tracker):
        self.cr = cr
        self.uid = uid
        self.tracker = tracker
        self.context = context
        self.pool = pooler.get_pool(cr.dbname)

    def _get_invoice_jrnl_id(self):
        """Get the id of the invoice journal
        """
        journal_prop = self.tracker.property_purchase_invoice_journal
        if not journal_prop:
            raise osv.except_osv(
                _(u"Error"),
                _(u"\"property_purchase_invoice_journal\" property missing.")
            )
        return journal_prop.id

    def _get_period(self):
        """Returns the id of the invoicable period.
        """
        purchase_date = self._get_purchase_date()
        month = ('%s' % purchase_date.month).zfill(2)
        period_code = '%s/%s' % (month,
                                 purchase_date.year)
        period_osv = self.pool.get('account.period')
        period_id = period_osv.search(
            self.cr, self.uid,
            [('code', '=', period_code), ],
            limit=1)[0]
        return period_id

    def _get_partner(self):
        """Returns the partner of the tracker
        """
        return self.tracker.partner_id

    def _get_account_bycode(self, code):
        """Returns an account given its code
        """
        acc_obj = self.pool.get('account.account')
        account = acc_obj.browse(
            self.cr, self.uid, acc_obj.search(
                self.cr, self.uid,
                [('code', '=', code)],
                context=self.context)[0],
            context=self.context)
        return account

    def _prepare_invoice(self):
        """ Builds the dict containing the values for the invoice
        """

        partner = self._get_partner()

        invoice_vals = {
            'name': '',
            'origin': '',
            'account_id': partner.property_account_payable.id,
            'partner_id': partner.id,
            'comment': '',
            'fiscal_position': partner.property_account_position.id,
            'date_invoice': self.tracker.invoice_date,
            'user_id': self.uid,
            'journal_id': self._get_invoice_jrnl_id(),
            'type': self.tracker.invoice_type,
            'supplier_invoice_number': self.tracker.invoice_reference,
            'date_due': self.tracker.due_date,
        }

        # Use the tracker's company when it is defined; otherwise, let the
        # default company getter work its magic.
        company = self.tracker.company_id
        if company:
            invoice_vals['company_id'] = company.id

        return invoice_vals

    def _create_invoice(self):
        vals = self._prepare_invoice()
        invoice_osv = self.pool.get('account.invoice')
        invoice_id = invoice_osv.create(
            self.cr,
            SUPERUSER_ID,
            vals,
            self.context)
        return invoice_osv.browse(
            self.cr,
            self.uid,
            invoice_id,
            self.context)

    def _create_lines(self):
        invoice_line_obj = self.pool.get('account.invoice.line')
        for line_data in self._get_lines_data():
            invoice_line_obj.create(
                self.cr,
                SUPERUSER_ID,
                line_data,
                self.context)

    def generate(self):
        """Creates the invoice and returns it.
        """
        self.invoice = self._create_invoice()
        self._create_lines()
        return self.invoice


class WithPOInvoiceBuilder(BaseInvoiceBuilder):
    """Build an invoice using a purchase order
    """

    def _prepare_invoice(self):
        """ Builds the dict containing the values for the invoice
        """
        po_br = self.tracker.purchase_order_id
        invoice_vals = super(WithPOInvoiceBuilder, self)._prepare_invoice()
        invoice_vals['origin'] = po_br.name
        # only copy the payment term if the disable option is not set
        if not self.tracker.company_id.tracker_disable_payment_term:
            invoice_vals['payment_term'] = po_br.payment_term_id.id
        if self.tracker.due_date:
            invoice_vals['date_due'] = self.tracker.due_date
        return invoice_vals

    def _get_lines_data(self):
        if self.tracker.po_tracker_type == 'quantity':
            return self._get_po_and_tracker_line_data()
        else:
            return self. _get_po_lines_data()

    def _get_po_and_tracker_line_data(self):
        lines = self._get_po_lines_data()
        res = list()
        trackerline_obj = self.pool.get('purchase_tracker.tracker.line')
        for line in lines:
            prod_id = line['product_id']
            tracker_lines = trackerline_obj.search(
                self.cr, self.uid,
                [('tracker_id', '=', self.tracker.id),
                 ('product_id', '=', prod_id)],
            )
            if tracker_lines:
                tracker_line = trackerline_obj.browse(
                    self.cr, self.uid,
                    tracker_lines[0])
                line['quantity'] = tracker_line.product_qty
                res.append(line)
        return res

    def _get_po_lines_data(self):
        lines = list()
        for order_line in self.tracker.purchase_order_id.order_line:
            lines.append(self._prepare_invoice_line(
                order_line))
        return lines

    def _get_tax(self, order_line):
        if order_line.taxes_id:
            return [(6, 0, [tax.id for tax in order_line.taxes_id])]
        return []

    def _prepare_invoice_line(self, order_line):
        """ Builds the dict containing the values for the invoice line
        """
        res = {
            'name': order_line.name,
            'origin': '',
            'invoice_id': self.invoice.id,
            'product_id': order_line.product_id.id,
            'account_id': order_line.product_id.property_account_expense.id,
            'price_unit': 0,
            'quantity': 1,
            'currency_id': self.tracker.invoice_currency_id.id,
            'invoice_line_tax_id': self._get_tax(order_line),
        }
        res.update(self.pool['analytic.structure'].extract_values(
            self.cr, self.uid, order_line, 'purchase_order_line',
            dest_model='account_invoice_line'
        ))
        return res


class NoPOInvoiceBuilder(BaseInvoiceBuilder):
    """Builds an invoice without a PO.
    """

    def _get_lines_data(self):
        """
        If no po is attached, the invoice contains no line at all
        """

        return {}


def invoicebuilder_factory(
        cr, uid, context, tracker):
    """Return the appropriate DetalLineBuilder for
    the given tracker.
    """
    if tracker.has_purchase_order:
        return WithPOInvoiceBuilder(cr, uid, context, tracker)
    return NoPOInvoiceBuilder(cr, uid, context, tracker)
