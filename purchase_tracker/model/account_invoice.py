# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 XCG Consulting (www.xcg-consulting.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv
from openerp.tools.translate import _

from .purchase_tracker import purchase_tracker


class account_invoice(osv.Model):

    TRACKER_SELECTION = [
        ('waiting', 'Waiting for approval'),
        ('pay', 'Good to pay'),
        ('cancel', 'Cancel'),
    ]

    _name = "account.invoice"
    _inherit = "account.invoice"

    def _get_object_reference(self, invoice):
        """ Override account_invoice fct so the reference
        in the move is the tracker
        """

        if invoice.tracker_id:
            return 'purchase_tracker.tracker,%s' % invoice.tracker_id.id
        return super(account_invoice, self)._get_object_reference(invoice)

    def _get_tracker_data(self, cr, uid, ids, field_names, arg, context):
        result = dict()
        tracker_osv = self.pool['purchase_tracker.tracker']
        for eyed in ids:
            tracker_ids = tracker_osv.search(
                cr, uid,
                [('invoice_id', '=', eyed)],
                context=context)
            nb_tracker = len(tracker_ids)
            assert nb_tracker < 2, ("Only one tracker",
                                    " should reference an invoice")
            # Make sure tracker_data has the field names asked
            if not isinstance(field_names, list):
                field_names = [field_names]
            tracker_data = {fn: None for fn in field_names}
            if nb_tracker == 1:
                tracker = tracker_osv.browse(cr, uid, tracker_ids[0], context)
                tracker_data.update({
                    'tracker_amount': tracker.invoice_amount,
                    'tracker_amount_untaxed': tracker.invoice_amount_untaxed,
                    'tracker_amount_vat': tracker.invoice_amount_vat,
                    'tracker_id': tracker.id,
                    'tracker_internal_sequence': tracker.internal_sequence,
                    'tracker_invoice_reference': tracker.invoice_reference,
                    'tracker_po_emitor': (
                        tracker.po_employee if tracker.has_purchase_order
                        else False
                    ),
                    'tracker_status': tracker.state,
                })
            result[eyed] = tracker_data
        return result

    def _search_tracker_id(
        self, cr, uid, model_again, field_name, criterion, context
    ):
        tracker_domain = map(
            lambda x: ('internal_sequence', x[1], x[2]), criterion)

        # search purchase_tracker_tracker with this domain
        tracker_obj = self.pool['purchase_tracker.tracker']
        tracker_ids = tracker_obj.search(
            cr, uid, tracker_domain, context=context
        )
        trackers = tracker_obj.browse(cr, uid, tracker_ids, context=context)
        # list invoices linked to these trackers
        invoice_ids = list({
            tracker.invoice_id.id
            for tracker in trackers
        })

        return [('id', 'in', invoice_ids)]

    def _search_tracker_status(
        self, cr, uid, model_again, field_name, criterion, context
    ):
        tracker_domain = map(lambda x: ('state', x[1], x[2]), criterion)

        # search purchase_tracker.tracker with this domain
        tracker_obj = self.pool['purchase_tracker.tracker']
        tracker_status = tracker_obj.search(
            cr, uid, tracker_domain, context=context
        )
        trackers = tracker_obj.browse(cr, uid, tracker_status, context=context)
        invoice_ids = list({
            tracker.invoice_id.id
            for tracker in trackers
        })

        return [('id', 'in', invoice_ids)]

    def _get_invoice_for_tracker(self, cr, uid, ids, context=None):
        '''Get the invoice associated with the tracker whose ids are provided
        '''
        result = dict()
        for tracker_br in self.pool['purchase_tracker.tracker'].browse(
                cr, uid, ids, context=context):
            if tracker_br.invoice_id:
                result[tracker_br.invoice_id.id] = True
        # Also update the invoice impacted by tracker_br.invoice_id changes
        invoice_ids = self.pool['account.invoice'].search(
            cr, uid, [('tracker_id', 'in', [ids])], context=context)
        for key in result.keys():
            if key not in invoice_ids:
                invoice_ids.append(key)
        return invoice_ids

    _columns = {
        "tracker_id": fields.function(
            _get_tracker_data,
            fnct_search=_search_tracker_id,
            method=True,
            string="Tracker",
            type='many2one',
            obj='purchase_tracker.tracker',
            store={
                'purchase_tracker.tracker': (
                    _get_invoice_for_tracker,
                    ['id', 'invoice_id'],
                    10),
            },
            multi='trackerdata'),

        'tracker_internal_sequence': fields.function(
            _get_tracker_data,
            method=True,
            string='Tracker',
            type='char',
            size=128,
            store={
                'purchase_tracker.tracker': (
                    _get_invoice_for_tracker,
                    ['internal_sequence', 'invoice_id'],
                    10
                ),
            },
            multi='trackerdata',
        ),

        "tracker_amount": fields.function(
            _get_tracker_data,
            method=True,
            string='Tracker Amount',
            type='float',
            store=False,
            multi='trackerdata'),
        "tracker_amount_untaxed": fields.function(
            _get_tracker_data,
            method=True,
            string='Tracker Untaxed Amount',
            type='float',
            store=False,
            multi='trackerdata'),
        "tracker_amount_vat": fields.function(
            _get_tracker_data,
            method=True,
            string='Tracker VAT Amount',
            type='float',
            store=False,
            multi='trackerdata'),
        "tracker_status": fields.function(
            _get_tracker_data,
            fnct_search=_search_tracker_status,
            method=True,
            string='Tracking Status',
            type='selection',
            store={
                'purchase_tracker.tracker': (
                    _get_invoice_for_tracker,
                    ['invoice_id', 'state'],
                    10),
            },
            selection=purchase_tracker.STATE_SELECTION,
            multi='trackerdata'),
        "tracker_invoice_reference": fields.function(
            _get_tracker_data,
            method=True,
            string='Invoice number',
            type='char',
            store={
                'purchase_tracker.tracker': (
                    _get_invoice_for_tracker,
                    ['invoice_id', 'invoice_reference'],
                    10),
            },
            multi='trackerdata'),
        "tracker_po_emitor": fields.function(
            _get_tracker_data,
            method=True,
            string='PO emitor',
            type='char',
            store={
                'purchase_tracker.tracker': (
                    _get_invoice_for_tracker,
                    ['invoice_id', 'has_purchase_order', 'po_employee'],
                    10),
            },
            multi='trackerdata'),
    }

    def can_open(self, cr, uid, ids, context=None):
        """An  invoice with a tracker non approved
        is not openable.
        """
        for invoice in self.pool['account.invoice'].browse(
            cr, uid, ids, context=context
        ):
            tracker = getattr(invoice, 'tracker_id', None)
            if tracker:
                openable = (tracker.state == 'approved')
                if not openable:
                    raise osv.except_osv(
                        _("Error"),
                        _("The tracker must be approved before you "
                          "open the invoice"))
        return True
