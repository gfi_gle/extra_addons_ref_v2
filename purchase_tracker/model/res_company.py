# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2014 XCG
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv


class company(osv.Model):
    _inherit = 'res.company'
    _columns = {
        'tracker_disable_payment_term': fields.boolean(
            "Disable payment term on invoice from tracker",
        ),
        'tracker_allow_employee_validation_for_po_overprice': fields.boolean(
            "Allow employees to validate their tracker that go above the "
            "purchase order amount",
        ),
    }
    _defaults = {
        'tracker_disable_payment_term': False,
        'tracker_allow_employee_validation_for_po_overprice': True,
    }
