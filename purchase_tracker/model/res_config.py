# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#                  2014 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import logging

from openerp.osv import fields, osv

_logger = logging.getLogger(__name__)


class purchase_tracker_configuration(osv.TransientModel):
    _name = 'purchase_tracker.config.settings'
    _inherit = 'res.config.settings'

    # TODO add the system parameter here (see __openerp__ for details)
    _columns = {
        'tracker_disable_payment_term': fields.boolean(
            "Disable payment term on invoice from tracker",
        ),
        'allow_employee_validation_for_po_overprice': fields.boolean(
            "Allow employees to validate their tracker that go below the "
            "purchase order amount",
        ),
    }

    def get_default_tracker_disable_payment_term(
        self, cr, uid, fields, context=None
    ):
        res = {}
        user = self.pool['res.users'].browse(cr, uid, uid, context)
        res['tracker_disable_payment_term'] = \
            user.company_id.tracker_disable_payment_term
        res['allow_employee_validation_for_po_overprice'] = \
            user.company_id.tracker_allow_employee_validation_for_po_overprice
        return res

    def set_tracker_disable_payment_term(self, cr, uid, ids, context=None):
        """Set this on the company of the user
        """
        user = self.pool['res.users'].browse(cr, uid, uid, context)
        company_id = user.company_id.id
        company_osv = self.pool['res.company']
        config_brl = self.browse(cr, uid, ids, context)
        for config_br in config_brl:
            val = {
                'tracker_disable_payment_term':
                    config_br.tracker_disable_payment_term,
                'tracker_allow_employee_validation_for_po_overprice':
                    config_br.allow_employee_validation_for_po_overprice,
            }
            company_osv.write(cr, uid, company_id, val, context=context)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
