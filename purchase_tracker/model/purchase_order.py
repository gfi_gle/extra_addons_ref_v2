# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013 XCG Consulting (www.xcg-consulting.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import json
from lxml import etree
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _

import openerp.addons.decimal_precision as dp


ENUM_STATUS = [
    ('draft', 'Draft'),
    ('open', 'Open'),
    ('partial', 'Partially invoiced'),
    ('total', 'Totally invoiced'),
    ('close', 'Closed manually'),
]

ENUM_TRACKER_TYPE = [
    ('invoicing', 'Track by invoices only'),
    ('quantity', 'Track deliveries'),
]


class purchase_order(osv.Model):

    _name = "purchase.order"
    _inherit = "purchase.order"

    def _get_tracker(self, cr, uid, ids, context=None):
        result = set()
        for tracker in self.browse(
                cr, uid, ids, context=context):
            result.add(tracker.purchase_order_id.id)
        return list(result)

    def _get_invoice(self, cr, uid, ids, context=None):
        result = set()
        for invoice in self.browse(
            cr, uid, ids, context=context
        ):
            if invoice.tracker_id:
                result.add(invoice.tracker_id.purchase_order_id.id)
        return list(result)

    def _get_invoice_line(self, cr, uid, ids, context=None):
        result = set()
        for line in self.browse(cr, uid, ids, context=context):
            if line.invoice_id.tracker_id:
                result.add(line.invoice_id.tracker_id.purchase_order_id.id)
        return list(result)

    def _get_invoice_tax(self, cr, uid, ids, context=None):
        result = set()
        for tax in self.browse(cr, uid, ids, context=context):
            if tax.invoice_id.tracker_id:
                result.add(tax.invoice_id.tracker_id.purchase_order_id.id)
        return list(result)

    def _amount_invoiced(self, cr, uid, ids, name, arg, context=None):
        res = dict()
        for purchase in self.browse(cr, uid, ids, context=context):
            tot = 0.0
            for tracker in purchase.approved_tracker_ids:
                invoiced = 0
                if not tracker.delivery_only and tracker.invoice_id:
                    invoice = tracker.invoice_id
                    if invoice.state not in ('draft', 'cancel'):
                        invoiced = invoice.amount_untaxed
                    if tracker.invoice_type == 'in_invoice':
                        tot += invoiced
                    elif tracker.invoice_type == 'in_refund':
                        tot -= invoiced
                    else:
                        raise osv.except_osv(_("Error!"),
                                             _("Unknown tracker type"))
            res[purchase.id] = tot
        return res

    def _amount_tracked(self, cr, uid, ids, name, arg, context=None):
        """Total amount of trackers related to a purchase
        """
        res = dict()
        for purchase in self.browse(cr, uid, ids, context=context):
            tot = 0.0
            for tracker in purchase.approved_tracker_ids:
                tot = tot + (
                    tracker.invoice_amount_untaxed
                    if tracker.invoice_type == 'in_invoice'
                    else -tracker.invoice_amount_untaxed)
            res[purchase.id] = tot
        return res

    def _amount_balance(self, cr, uid, ids, name, arg, context=None):
        res = dict()
        for purchase in self.browse(cr, uid, ids, context=context):
            tot = purchase.amount_untaxed - purchase.amount_invoiced
            res[purchase.id] = tot
        return res

    def _amount_total(self, cr, uid, ids, name, arg, context=None):
        res = dict()
        for purchase in self.browse(cr, uid, ids, context=context):
            res[purchase.id] = purchase.amount_untaxed
        return res

    def _approved_trackers(self, cr, uid, ids, name, arg, context=None):
        result = {eyed: list()
                  for eyed in ids}
        cr.execute(
            'SELECT purchase_tracker_tracker.id, '
            'purchase_tracker_tracker.purchase_order_id '
            'FROM purchase_tracker_tracker '
            'WHERE purchase_tracker_tracker'
            '.purchase_order_id IN %s '
            'AND purchase_tracker_tracker.state = \'approved\'',
            (tuple(ids),))
        r = cr.fetchall()
        for x in r:
            result[x[1]].append(x[0])
        return result

    _columns = {
        "tracker_ids": fields.one2many(
            'purchase_tracker.tracker', 'purchase_order_id', 'Trackers',
            readonly=True
        ),
        "approved_tracker_ids": fields.function(
            _approved_trackers,
            string='Approved trackers',
            store=False,
            method=True,
            type="one2many",
            relation='purchase_tracker.tracker',
        ),
        # "approved_tracker_ids": fields.related(
        #    'tracker_ids', string="Approved trackers",
        #    store=False, readonly=True, type="one2many",
        #    relation='purchase_tracker.tracker',
        #    domain=[('state', '=', 'approved')]
        #
        # ),
        "amount_total_clone": fields.function(
            _amount_total,
            digits_compute=dp.get_precision('Account'),
            string='Total amount',
            store=False
        ),
        "amount_invoiced": fields.function(
            _amount_invoiced,
            digits_compute=dp.get_precision('Account'),
            string='Invoiced amount',
            # This store is complex as we want to watch over functionnal field
            # 'amount_untaxed'. So we need to implement the same store that the
            # one defined in account_invoice.py
            store={
                'purchase_tracker.tracker': (
                    _get_tracker,
                    ['delivery_only', 'invoice_id', 'invoice_type', 'state'],
                    10
                ),
                'account.invoice': (
                    _get_invoice,
                    ['invoice_line', 'state'],
                    10
                ),
                'account.invoice.tax': (
                    _get_invoice_tax,
                    None,
                    10
                ),
                'account.invoice.line': (
                    _get_invoice_line,
                    [
                        'price_unit', 'invoice_line_tax_id', 'quantity',
                        'discount', 'invoice_id',
                    ],
                    20
                ),
            },
        ),
        "amount_tracked": fields.function(
            _amount_tracked,
            digits_compute=dp.get_precision('Account'),
            string='Tracked amount',
            store=False
        ),
        "amount_balance": fields.function(
            _amount_balance,
            digits_compute=dp.get_precision('Account'),
            string='Net engagement',
            # TODO store the value ?
            store=False,
        ),
        "tracker_type": fields.selection(
            ENUM_TRACKER_TYPE,
            string="Tracker type",
            required=True,
            select=True,
        ),
        "status": fields.selection(
            ENUM_STATUS,
            string="Order Status",
            required=True,
            select=True,
            readonly=True,
            track_visibility='onchange',
        ),
        'order_line_readonly': fields.related(
            'order_line', string="Order lines",
            store=False, readonly=True, type="one2many",
            relation='purchase.order.line',
        )
    }

    _defaults = {
        "tracker_type": lambda *a: 'invoicing',
        "status": lambda *a: 'draft',
    }

    def fields_view_get(self, cr, uid, view_id=None, view_type='form',
                        context=None, toolbar=False, submenu=False):
        """
        Override the fields_view_get to display tracking status
        only on approved po tree
        """
        if context is None:
            context = {}
        res = super(purchase_order,
                    self).fields_view_get(cr, uid, view_id=view_id,
                                          view_type=view_type,
                                          context=context,
                                          toolbar=toolbar,
                                          submenu=submenu)

        po = context.get('approved_po', False)

        if 'fields' in res:
            doc = etree.XML(
                res['arch']
            )
            line_fields = res['fields']

            for field in ('state', 'status'):
                if field in line_fields:
                    match = doc.xpath("//field[@name='{}']".format(field))[0]
                    modifiers = json.loads(match.get('modifiers', '{}'))
                    # status is shown if po, state is not shown if po
                    modifiers['tree_invisible'] = (
                        po if field == 'state' else not po)
                    match.set('modifiers', json.dumps(modifiers))

            res['arch'] = etree.tostring(doc)

        return res

    def create(self, cr, uid, vals, context=None):
        vals['status'] = 'draft'
        return super(purchase_order, self).create(
            cr, uid, vals, context=context)

    def action_cancel(self, cr, uid, ids, context=None):
        """Override to disallow canceling when there are trackers."""

        for order in self.browse(cr, uid, ids, context=context):
            for tracker in order.tracker_ids:
                if tracker.invoice_id:
                    raise osv.except_osv(
                        _(u"Error"),
                        _(
                            u"Purchase orders with trackers that have "
                            u"generated invoice lines cannot be cancelled."
                        )
                    )

        return super(purchase_order, self).action_cancel(
            cr, uid, ids, context=context
        )

    # override purchase workflow activity to change po tracking status
    def wkf_confirm_order(self, cr, uid, ids, context=None):
        res = super(purchase_order, self).wkf_confirm_order(
            cr, uid, ids, context
        )
        if res:
            return self.write(
                cr, uid, ids,
                {'status': 'open'},
                context=context)
        return res

    def _get_procurements_ids(self, cr, uid, context):
        employee_osv = self.pool.get("hr.employee")
        procurement_ids = employee_osv.find_employees_by_category(
            cr, uid, 'Acheteur', 'Bon de commande', context, True)
        return procurement_ids

    def close(self, cr, uid, ids, context=None):
        user_osv = self.pool.get("res.users")
        user = user_osv.browse(cr, uid, uid, context=context)
        if user.employee_id in self._get_procurements_ids(cr, uid, context):
            return self.write(
                cr, uid, ids,
                {'status': 'close'},
                context=context)
        else:
            raise osv.except_osv(
                _("Error"),
                _("Only member of the procurement team"
                  " can close a purchase order"))


class purchase_order_line(osv.Model):

    _name = "purchase.order.line"
    _inherit = "purchase.order.line"

    def _get_quantity_data(self, cr, uid, ids, field_names, arg, context):
        result = {eyed: dict(qty_tracked=0, qty_undelivered=0)
                  for eyed in ids}
        cr.execute(
            'SELECT purchase_order_line.id, '
            # tracked quantity on all trackers
            'SUM(purchase_tracker_tracker_line.product_qty), '
            # remaining quantity (no aggregate on po line)
            'purchase_order_line.product_qty - '
            'SUM(purchase_tracker_tracker_line.product_qty), '
            # original po line qty (no aggregate)
            'purchase_order_line.product_qty '
            'FROM purchase_tracker_tracker_line '
            'LEFT JOIN purchase_order_line ON purchase_tracker_tracker_line'
            '.po_line_id = purchase_order_line.id '
            'LEFT JOIN purchase_tracker_tracker ON '
            'purchase_tracker_tracker.id '
            '= purchase_tracker_tracker_line.tracker_id '
            'WHERE  purchase_order_line.id IN %s '
            'AND purchase_tracker_tracker.state = \'approved\' '
            'GROUP BY purchase_order_line.id',
            (tuple(ids),))
        r = cr.fetchall()
        for x in r:
            # make sure returned data contains a positive float, not None
            result[x[0]] = {
                'qty_tracked': x[1] or 0.0,
                'qty_undelivered': (
                    x[2] > 0 and x[2] or 0.0 if x[2] is not None else x[3]
                ),
            }
        return result

    _columns = {
        "qty_tracked": fields.function(
            _get_quantity_data,
            string='Quantity received',
            store=False,
            method=True,
            multi='quantitydata',
        ),
        "qty_undelivered": fields.function(
            _get_quantity_data,
            string='Quantity unreceived',
            store=False,
            method=True,
            multi='quantitydata',
        ),
    }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
