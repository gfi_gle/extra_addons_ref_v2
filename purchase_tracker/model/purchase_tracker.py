# -*- coding: utf-8 -*-

import datetime
import logging
from openerp import exceptions
from openerp import SUPERUSER_ID
import openerp.osv.orm
from openerp.osv import fields
from openerp.osv import osv
from openerp.tools.translate import _


import openerp.addons.decimal_precision as dp

from .invoicegenerator import invoicebuilder_factory
from .purchase_order import ENUM_STATUS, ENUM_TRACKER_TYPE


_logger = logging.getLogger(__name__)


class purchase_tracker(osv.Model):
    """An invoice tracker is an object that will reference a paper invoice
    coming to the accounting department but not yet keyed into the accounting
    system
    """

    _name = 'purchase_tracker.tracker'
    _description = 'Invoice Tracker'
    _inherit = ['mail.thread']
    _rec_name = "internal_sequence"

    STATE_SELECTION = [
        ('draft', "Draft"),
        ('validation', "Validation"),
        ('second_validation', "Second Validation"),
        ('validation_rejected', "Validation Rejected"),
        ('suspended', "Suspended"),
        ('dispute', "Dispute"),
        ('approved', "Approved"),
    ]

    PO_CHANGE_STATUS = [
        ('partial', 'Partially invoiced'),
        ('total', 'Totally invoiced'),
    ]

    _track = {
        'state': {
            'purchase_tracker.mt_tracker_validation': (
                lambda self, cr, uid, obj, ctx=None: (
                    obj['state'] == 'validation'
                )
            ),
            'purchase_tracker.mt_tracker_suspended': (
                lambda self, cr, uid, obj, ctx=None: (
                    obj['state'] == 'suspended'
                )
            ),
            'purchase_tracker.mt_tracker_dispute': (
                lambda self, cr, uid, obj, ctx=None: (
                    obj['state'] == 'dispute'
                )
            ),
            'purchase_tracker.mt_tracker_approved': (
                lambda self, cr, uid, obj, ctx=None: (
                    obj['state'] == 'approved'
                )
            ),
        },
    }

    def _invoice_amount(self, cr, uid, ids, name, arg, context=None):
        res = dict()
        for tracker in self.browse(cr, uid, ids, context=context):
            tot = tracker.invoice_amount_untaxed + tracker.invoice_amount_vat
            res[tracker.id] = tot
        return res

    _columns = dict(
        email_from=fields.char('Email from', size=255),
        email_cc=fields.char('Email CC', size=255),

        internal_sequence=fields.char(
            'Tracker', size=128,
            select=True, required=True),

        description=fields.char('Description', size=512),

        partner_id=fields.many2one(
            'res.partner', 'Partner',
            ondelete='set null', select=True,
            track_visibility='always'),

        company_id=fields.many2one(
            'res.company', 'Company',
            change_default=True, readonly=True,
            states={'draft': [('readonly', False)]}),

        invoice_type=fields.selection(
            [
                ('in_invoice', 'Supplier Invoice'),
                ('in_refund', 'Supplier Refund'),
            ],
            'Invoice type', select=True,
            track_visibility='always'),
        invoice_reference=fields.char('Invoice Reference', size=255),
        invoice_date=fields.date('Invoice Date'),
        due_date=fields.date('Due Date', required=False),
        invoice_amount=fields.function(
            _invoice_amount,
            digits_compute=dp.get_precision('Account'),
            string='Invoice Amount',
            help="The amount as seen on the invoice, including taxes",
            # FIXME
            store=True,
        ),
        invoice_amount_untaxed=fields.float(
            'Invoice Amount Untaxed',
            digits_compute=dp.get_precision('Account'),
            help="The amount as seen on the invoice, without taxes",
        ),
        invoice_amount_vat=fields.float(
            'Invoice VAT Amount',
            digits_compute=dp.get_precision('Account'),
            help="The amount of taxes as seen on the invoice",
        ),
        invoice_currency_id=fields.many2one(
            'res.currency', 'Invoice Currency',
            ondelete='set null', select=True),
        invoice_id=fields.many2one(
            'account.invoice',
            'Generated invoice',
            help="Invoice generated from this tracker",
            unique=True,
            ondelete='set null',  # To forbid Tracker deletion
            # if the invoice gets deleted
            # http://www.postgresql.org/docs/8.2/static/ddl-constraints.html
            track_visibility='onchange',
        ),
        delivery_date=fields.date('Delivery Date'),
        reception_date=fields.date('Reception Date', required=True),
        note=fields.text('Note'),
        state=fields.selection(
            STATE_SELECTION,
            # This is voluntarily not just "state" so as not to get messed up
            # by other translations...
            'Tracker State',
            readonly=True,
            select=True,
            track_visibility='onchange',
        ),
        date_open=fields.date(
            'Date Open',
            readonly=1,
            select=True,
            help="Date on which tracker has been open",
        ),
        date_validation=fields.date(
            'Date Validation',
            readonly=1,
            select=True,
            help="Date on which tracker has been passed for validation",
        ),
        date_suspended=fields.date(
            'Date Suspended',
            readonly=1,
            select=True,
            help="Date on which tracker has been suspended",
        ),
        date_dispute=fields.date(
            'Date Dispute',
            readonly=1,
            select=True,
            help="Date on which tracker has been put in dispute",
        ),
        date_approve=fields.date(
            'Date Approve',
            readonly=1,
            select=True,
            help="Date on which tracker was approved",
        ),
        has_purchase_order=fields.boolean('Has purchase order'),
        delivery_only=fields.boolean('Delivery only'),
        po_change_status=fields.selection(
            PO_CHANGE_STATUS, 'Change PO status',
            readonly=False, select=True,
        ),
        # fields used only if the user does not want to associate this invoice
        # to a quotation
        employee_id=fields.many2one(
            'hr.employee', 'Emitor',
            ondelete='set null', select=True,
        ),
        responsible_id=fields.many2one(
            'hr.employee', 'Validator',
            ondelete='set null', select=True,
        ),
        department_id=fields.many2one(
            'hr.department', 'Department',
            select=True,
            help='The department responsible to manage this invoice',
        ),
        # related quotation fields that will automatically contain values if we
        # link this tracker to a purchase order
        purchase_order_id=fields.many2one(
            'purchase.order', 'Purchase Order',
            ondelete='set null', select=True,
        ),
        property_purchase_invoice_journal=fields.property(
            obj='account.journal',
            type='many2one',
            string="Default invoice journal"
        ),
        pt_line_ids=fields.one2many(
            'purchase_tracker.tracker.line',
            'tracker_id', 'Tracker items'),
        po_employee=fields.related(
            'purchase_order_id',
            'employee_id', 'name',
            string="Emitor",
            type='char', readonly=True,
        ),
        po_responsible=fields.related(
            'purchase_order_id', 'department_id', 'manager_id',
            'name', string="Validator",
            type='char', readonly=True,
        ),
        po_department=fields.related(
            'purchase_order_id', 'department_id',
            'name', string="Department",
            type='char', readonly=True,
        ),
        po_amount_total=fields.related(
            'purchase_order_id', 'amount_total',
            type='float', string="P.O. Amount", readonly=True,
        ),
        po_tracker_type=fields.related(
            'purchase_order_id', 'tracker_type',
            type='selection', string="Tracker Type", readonly=True,
            selection=ENUM_TRACKER_TYPE,
        ),
        po_description=fields.related(
            'purchase_order_id', 'description',
            type='char', string="Description", readonly=True,
        ),
        po_status=fields.related(
            'purchase_order_id', 'status', string="Purchase Order Status",
            type='selection', readonly=True,
            selection=ENUM_STATUS,
        ),
    )

    def _get_default_currency(self, cr, uid, ctx):
        """
        inspired from product.pricelist._get_currency
        """
        comp = self.pool.get('res.users').browse(cr, uid, uid).company_id
        if not comp:
            comp_id = self.pool.get('res.company').search(cr, uid, [])[0]
            comp = self.pool.get('res.company').browse(cr, uid, comp_id)
        return comp.currency_id.id

    def _get_default_rd(self, cr, uid, context=None):
        """Get the default reception date
        """
        return datetime.datetime.now().date().strftime("%Y-%m-%d")

    _defaults = {
        'internal_sequence': '/',
        'state': 'draft',
        'invoice_currency_id': _get_default_currency,
        'company_id': lambda self, cr, uid, context:
        self.pool.get('res.company')._company_default_get(
            cr, uid, 'purchase_tracker.tracker', context=context),
        'has_purchase_order': False,
        'reception_date': _get_default_rd,
        'delivery_only': False,
    }

    _sql_constraints = [
        (
            'unique_sequence',
            'unique(internal_sequence)',
            'the internal sequence must be unique'
        ),
        (
            'unique_invoice',
            'unique(invoice_id)',
            'the invoice must be unique'
        )
    ]

    def create(self, cr, uid, vals, context=None):

        if not context:
            context = {}

        # Use force company if provided
        if 'force_company' in context:
            vals['company_id'] = context['force_company']

        if vals.get('internal_sequence', '/') == '/':

            # Ensure the sequence is fetched from the right company.
            comp_context = context.copy()
            company_id = vals.get('company_id')
            if company_id:
                comp_context['force_company'] = company_id

            vals['internal_sequence'] = self.pool['ir.sequence'].get(
                cr, uid, 'purchase_tracker.tracker', context=comp_context
            )

        if vals.get('has_purchase_order', False):
            vals['partner_id'] = self._get_partner(cr, uid, None,
                                                   vals, context).id

        self._change_po_status(cr, uid, [], vals, context)

        return super(purchase_tracker, self).create(
            cr, uid, vals, context=context)

    def write(self, cr, uid, ids, vals, context=None):
        if 'purchase_order_id' in vals:
            self._remove_pt_lines(cr, uid, ids, vals, context)

            # No partner_id when no purchase_order_id is empty/null
            if vals['purchase_order_id']:
                vals['partner_id'] = self._get_partner(cr, uid, ids,
                                                       vals, context).id

        self._change_po_status(cr, uid, ids, vals, context)

        self._forbid_write_approved(cr, uid, ids, vals, context)

        return super(purchase_tracker, self).write(
            cr, uid, ids, vals,
            context=context)

    def _forbid_write_approved(self, cr, uid, ids, vals, context):
        """Checks that tracker are in a writable 'state'.
        Raise osv.exception if write is forbidden.
        """

        if not ids:
            return True

        # Special case for the message thread, which attempts to edit the
        # record after the "approved" state has been recorded.
        if vals and 'message_last_post' in vals:
            return True

        approved = self.search(
            cr, uid,
            [('id', 'in', ids), ('state', '=', 'approved')])
        if approved:
            raise osv.except_osv(
                _("Error"),
                _("Can't modify an approved tracker"))

    def _remove_pt_lines(self, cr, uid, ids, vals, context):
        line_ids = list()
        for pt in self.browse(cr, uid, ids, context):
            if pt.purchase_order_id.id != vals['purchase_order_id']:
                line_ids.extend([line.id for line in pt.pt_line_ids])
        pt_line_osv = self.pool.get('purchase_tracker.tracker.line')
        pt_line_osv.unlink(cr, uid, line_ids, context)

    def _change_po_status(self, cr, uid, ids, vals, context):
        status_change = vals.get('po_change_status')
        if not status_change:
            return

        order_osv = self.pool['purchase.order']

        order_id = vals.get('purchase_order_id')
        if order_id:
            order_ids = [order_id]
        else:
            order_ids = order_osv.search(
                cr, uid,
                [('tracker_ids', 'in', ids)],
                context=context
            )

        # safe to perform this as SUPERUSER
        order_osv.write(
            cr, SUPERUSER_ID, order_ids,
            dict(status=vals['po_change_status']),
            context)

    def _get_partner(self, cr, uid, ids, vals, context):
        po_osv = self.pool.get('purchase.order')
        po = po_osv.browse(cr, uid, vals['purchase_order_id'], context)
        return po.partner_id

    def unlink(self, cr, uid, ids, context=None):
        drafts = self.search(
            cr, uid,
            ['&', ('id', 'in', ids),
             '&', ('state', '!=', 'draft'), ('state', '!=', 'undefined')])
        if drafts:
            raise osv.except_osv(
                _("Error"),
                _("Can't delete a tracker if it's not in 'draft' state"))
        return super(purchase_tracker, self).unlink(
            cr, uid, ids, context=context)

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = dict()

        default.update(
            dict(internal_sequence=self.pool.get(
                'ir.sequence').get(cr, uid, 'purchase_tracker.tracker')
            )
        )

        return super(purchase_tracker, self).copy(
            cr, uid, id, default=default, context=context)

    def message_new(self, cr, uid, msg, custom_values=None, context=None):
        """Automatically calls when new email message arrives"""
        res_id = super(purchase_tracker, self).message_new(
            cr, uid, msg, custom_values=custom_values, context=context)

        subject = msg.get('subject') or _("No Subject")
        body = msg.get('body_text')

        msg_from = msg.get('from')
        # priority = msg.get('priority')
        vals = dict(
            name=subject,
            email_from=msg_from,
            email_cc=msg.get('cc'),
            description=body,
            state="draft",
        )

        self.write(cr, uid, [res_id], vals, context)
        return res_id

    def message_update(self, cr, uid, ids, msg, vals=None,
                       default_act='pending', context=None):
        """not implemented at all
        """
        return True

    def _get_related(self, baseobj, column_name):
        """ Introspect self._column['column_name'] to
        get the related value, given baseobj.
        """
        relation_tuple = self._columns[column_name]._arg
        result = baseobj
        for attr in relation_tuple[1:]:
            result = getattr(result, attr, '')
            if not result or isinstance(result, openerp.osv.orm.browse_null):
                result = ''
                break
        return result

    def _fetch_relations(self, baseobj, *column_names):
        """Fetch every related obj specified by its column name.
        """
        res = dict()
        for column_name in column_names:
            res[column_name] = self._get_related(baseobj, column_name)
        return res

    def _get_po_fields(self, cr, uid, ids,
                       purchase_order_id, context):
        """Fetch some related PO fields
        """
        value = dict(
            po_department='', po_responsible='',
            po_employee='', po_amount_total='',
            po_tracker_type='', po_status='',
        )
        if purchase_order_id:
            order_osv = self.pool.get('purchase.order')
            order = order_osv.browse(
                cr, uid, purchase_order_id, context=context)

            value.update(
                self._fetch_relations(
                    order,
                    'po_tracker_type',
                    'po_department',
                    'po_responsible',
                    'po_employee',
                    'po_amount_total',
                    'po_status')
            )
            value.update({"partner_id": order.partner_id.id})
        return value

    def _get_po_lines(self, po):
        lines = list()
        for po_line in po.order_line:
                lines.append((
                    0,
                    0,
                    {
                        'po_line_id': po_line.id,
                        'description': po_line.name,
                        'prod_name': po_line.product_id.name,
                        'product_id': po_line.product_id.id,
                        'product_qty': po_line.product_qty,
                        'price_unit': po_line.price_unit,
                    }
                ))
        return lines

    def _get_related_lines(
            self, cr, uid, ids,
            purchase_order_id, context):
        res = dict()
        pt = self.pool.get('purchase_tracker.tracker').browse(
            cr, uid, ids, context=context)
        po = self.pool.get('purchase.order').browse(
            cr, uid, purchase_order_id, context=context)
        if not pt or (pt[0].purchase_order_id.id != purchase_order_id):
            lines = self._get_po_lines(po)
            res['pt_line_ids'] = lines

        return res

    def onchange_purchase_order_id(self, cr, uid, ids,
                                   purchase_order_id, context=None):
        value = self._get_po_fields(
            cr, uid, ids,
            purchase_order_id, context)
        if purchase_order_id:
            value.update(self._get_related_lines(
                cr, uid, ids,
                purchase_order_id, context))
        res = dict(value=value)
        return res

    def on_change_has_purchase_order(
        self, cr, uid, ids, has_purchase_order, context=None
    ):
        """purchase_order_id need to be blanked when has_purchase_order is
        false. The purchase order also need to be blank.
        """
        if not has_purchase_order:
            return {'value': {
                'purchase_order_id': None,
                'po_description': '',
            }}
        return {}

    def onchange_partner_invoice_date(
            self, cr, uid, ids,
            invoice_date, partner_id,
            context=None):
        res = dict()
        if partner_id and invoice_date:
            partner = self.pool.get('res.partner').browse(
                cr, uid, partner_id, context=context)
            payment_term = partner.property_supplier_payment_term
            lines = payment_term.line_ids
            if lines:
                days = lines[0].days
                invoice_date = datetime.datetime.strptime(
                    invoice_date,
                    "%Y-%m-%d")
                due_date = invoice_date + datetime.timedelta(days=days)
                res['due_date'] = due_date.strftime("%Y-%m-%d")
        return dict(value=res)

    def onchange_amount(
            self, cr, uid, ids,
            invoice_amount_untaxed, invoice_amount_vat,
            context=None):
        invoice_amount_untaxed = invoice_amount_untaxed \
            if invoice_amount_untaxed else 0
        invoice_amount_vat = invoice_amount_vat \
            if invoice_amount_vat else 0
        invoice_amount = invoice_amount_untaxed + invoice_amount_vat
        return dict(value=dict(invoice_amount=invoice_amount))

    def action_invoice_create(self, cr, uid, tracker, context=None):
        """Generates a move for the given tracker and
        returns it.
        """
        invoice_gen = invoicebuilder_factory(cr, uid, context, tracker)
        return invoice_gen.generate()

    def _notify_state_changed(self, cr, uid, tracker):
        """Notify interested users of a state change.
        Does nothing if no interested user can be reached.
        """
        if tracker.has_purchase_order:
            self._notify_po_owner(cr, uid, tracker)
        else:
            self._notify_tracker_owner(cr, uid, tracker)

    def _notify_po_owner(self, cr, uid, tracker):
        user = tracker.purchase_order_id.employee_id.user_id
        if user:
            self.message_subscribe_users(cr, uid, [tracker.id],
                                         user_ids=[user.id])

    def _notify_tracker_owner(self, cr, uid, tracker):
        user = tracker.employee_id.user_id
        if user:
            self.message_subscribe_users(cr, uid, [tracker.id],
                                         user_ids=[user.id])

    def _check_expense_account(self, order_line):
        if not order_line.product_id.property_account_expense:
                        raise osv.except_osv(
                            _("Error"),
                            _("The product %s"
                              " needs to have an expense account" %
                                order_line.product_id.name))

    def _check_and_invoice(self, cr, uid, tracker, context):

        if not context:
            context = {}

        # Ensure the tracker's company is being used to fetch properties.
        comp_context = context.copy()
        if tracker.company_id:
            comp_context['force_company'] = tracker.company_id.id
            tracker = self.browse(
                cr, uid, [tracker.id], context=comp_context
            )[0]

        if tracker.has_purchase_order:
            for order_line in tracker.purchase_order_id.order_line:
                self._check_expense_account(order_line)

        if not tracker.invoice_type:
            raise exceptions.Warning(_(
                'Invoice creation from a purchase tracker: Invoice type '
                'undefined.'
            ))

        invoice = self.action_invoice_create(
            cr, uid, tracker, context=comp_context)
        return self.write(
            cr, uid, [tracker.id],
            dict(invoice_id=invoice.id),
            context=comp_context,
        )

    def wkf_functionnal_check(self, cr, uid, ids, context=None):
        for tracker in self.browse(cr, uid, ids):
            self._notify_state_changed(cr, uid, tracker)
            if not tracker.delivery_only:
                self._check_and_invoice(cr, uid, tracker, context)
        return True

    def wkf_validation(self, cr, uid, ids, context=None):
        for tracker in self.browse(cr, uid, ids):
            self._notify_state_changed(cr, uid, tracker)
        return self.write(
            cr, uid, ids,
            {
                'state': 'validation',
                'date_validation': fields.date.context_today(
                    self, cr, uid, context=context)
            },
            context=context,
        )

    def wkf_second_validation(self, cr, uid, ids, context=None):
        for tracker in self.browse(cr, uid, ids):
            self._notify_state_changed(cr, uid, tracker)
        return self.write(
            cr, uid, ids,
            {
                'state': 'second_validation',
                'date_validation': fields.date.context_today(
                    self, cr, uid, context=context)
            },
            context=context,
        )

    def _delete_invoice(self, cr, uid, ids, context):
        cr.execute(
            'SELECT account_invoice.id '
            'FROM purchase_tracker_tracker '
            'JOIN account_invoice ON account_invoice.id='
            'purchase_tracker_tracker.invoice_id '
            'WHERE purchase_tracker_tracker'
            '.id IN %s ',
            (tuple(ids),))
        result = cr.fetchall()
        invoice_ids = [x[0] for x in result]
        invoice_osv = self.pool.get('account.invoice')
        return invoice_osv.unlink(cr, SUPERUSER_ID, invoice_ids, context)

    def wkf_validation_rejected(self, cr, uid, ids, context=None):
        for tracker in self.browse(cr, uid, ids):
            self._notify_state_changed(cr, uid, tracker)
        self._delete_invoice(cr, uid, ids, context)
        return self.write(
            cr, uid, ids,
            {
                'state': 'validation_rejected',
            },
            context=context,
        )

    def wkf_suspended(self, cr, uid, ids, context=None):
        for tracker in self.browse(cr, uid, ids):
            self._notify_state_changed(cr, uid, tracker)

        return self.write(
            cr, uid, ids,
            {
                'state': 'suspended',
                'date_suspended': fields.date.context_today(
                    self, cr, uid, context=context)
            },
            context=context,
        )

    def wkf_dispute(self, cr, uid, ids, context=None):
        for tracker in self.browse(cr, uid, ids):
            self._notify_state_changed(cr, uid, tracker)

        return self.write(
            cr, uid, ids,
            {
                'state': 'dispute',
                'date_dispute': fields.date.context_today(
                    self, cr, uid, context=context)
            },
            context=context,
        )

    def wkf_approved(self, cr, uid, ids, context=None):
        self.write(
            cr, uid, ids,
            {
                'state': 'approved',
                'date_approve': fields.date.context_today(
                    self, cr, uid, context=context)
            },
            context=context,
        )
        return True

    def wkf_reset(self, cr, uid, ids, context=None):
        return self.write(
            cr, uid, ids,
            {
                'state': 'draft',
                'date_approve': None,
                'date_dispute': None,
                'date_open': None,
                'date_validation': None,
            },
            context=context,
        )

    def is_manager(self, cr, uid, ids, context=None):
        res = self.user_has_groups(
            cr, uid,
            'purchase_tracker.res_groups_purchase_tracker_managers',
            context=context
        )
        return res

    def _get_procurements_ids(self, cr, uid, context):
        employee_osv = self.pool.get("hr.employee")
        procurement_ids = employee_osv.find_employees_by_category(
            cr, uid, 'Acheteur', 'Bon de commande', context, True)
        return procurement_ids

    def is_validator(self, cr, uid, ids, context=None):
        """Return True if the user can validate these tracker
        """
        procurement_ids = self._get_procurements_ids(cr, uid, context)
        res = True
        user_osv = self.pool.get("res.users")
        user = user_osv.browse(cr, uid, uid, context=context)
        for tracker in self.browse(cr, uid, ids):
            res = res and \
                self._is_validator(cr, user, tracker, procurement_ids, context)
        return res

    def _is_validator(self, cr, user, tracker, procurement_ids, context):
        """Return True if the use can validate the given tracker
        """
        employee = user.employee_id
        if user.employee_id in procurement_ids:
            return True
        if tracker.has_purchase_order:
            po = tracker.purchase_order_id
            po_emitor = po.employee_id
            company = tracker.company_id
            # if this company does not allow employee validation for overprice
            # and is over po amount, only manager is validator
            if not company.tracker_allow_employee_validation_for_po_overprice:
                new_total = tracker.invoice_amount
                new_total += tracker.purchase_order_id.amount_tracked
                if new_total > tracker.po_amount_total:
                    print 'new case!'
                    return employee == po_emitor.manager_id
            return employee == po_emitor or employee == po_emitor.manager_id
        return False

    def has_second_validation(self, cr, uid, ids, context=None):
        """Indicate if the second validation functionality is activated.
        :return: True if it is activated.
        :rtype: boolean
        """
        config_osv = self.pool['ir.config_parameter']
        # If the value is not present, consider that it was not activated
        config_value = config_osv.get_param(
            cr, uid, 'purchase_tracker.activate_second_validation',
            '', context)
        ret = config_value == '1' or config_value.lower() == 'true'
        _logger.debug('Has second validation: %s' % ret)
        return ret

    def need_second_validator(self, cr, uid, ids, context=None):
        # Ideally this should send back a boolean for each ids.
        # Fortunately, workflow is done on one item at a time.
        for tracker_br in self.browse(cr, uid, ids, context):
            # test that this tracker is attached to a purchase order
            if tracker_br.has_purchase_order:
                invoice_amount = tracker_br.invoice_amount
                if tracker_br.invoice_type == 'in_invoice':
                    valued_amount = invoice_amount
                elif tracker_br.invoice_type == 'in_refund':
                    valued_amount = -invoice_amount
                if (
                    tracker_br.purchase_order_id.amount_total_clone
                    < tracker_br.purchase_order_id.amount_invoiced
                    + valued_amount
                ):
                    return True
#                 if (
#                     not tracker_br.ignore_tracker_amount and
#                     tracker_br.purchase_order_id.amount_total_clone
#                     < tracker_br.purchase_order_id.amount_tracked
#                     + valued_amount
#                 ):
#                     raise osv.except_osv((
#                         _("Error"),
#                         _("Tracked amount plus this amount are superior to"
#                             " purchase order total. Check that invoice where"
#                             " validated")))
        return False

    def is_second_validator(self, cr, uid, ids, context=None):
        for tracker_br in self.browse(cr, uid, ids, context):
            if (
                tracker_br.has_purchase_order
                and tracker_br.purchase_order_id.validator.id != uid
            ):
                return False
        return True
