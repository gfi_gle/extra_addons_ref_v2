# -*- coding: utf-8 -*-
##############################################################################
#
#    Account Report, for OpenERP
#    Copyright (C) 2015 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': u"Account Report",
    'version': '1.1',
    'author': u"XCG Consulting",
    'category': "Accounting",
    'description': u"""
Account Report
==============

    This module provides a new menu and basic wizards that can be overriden.

Odoo dependencies
-----------------

* base
* account
    """,
    'website': "https://odoo.consulting/",
    'depends': [
        'base',
        'account',
    ],
    'data': [
        'wizards/menu.xml',
    ],
    'demo': [],
    'css': [],
    'test': [],
    'installable': True,
    'active': True,
    'external_dependencies': {
    },
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
