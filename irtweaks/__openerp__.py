# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
{
    "name": "IRTweaks",
    "version": "2.0.1",
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    "category": '',
    "description": """Various tweaks in the inner guts of Openerp

      * OpenERP.com will not trace your activity anymore (nodbuuid)
      * Default values stored as "ir.values" are editable under their
      serialized representation (in order to maintain their type).
    """,
    'init_xml': [],
    "depends": [
        'base',
        'web',
    ],
    "data": [
        'data/res_users.xml',
        'views/ir_values.xml',
        "views/webclient_templates.xml",
    ],
    'qweb': [
        "static/src/xml/base.xml",
    ],
    'demo_xml': [],
    'test': [],
    'installable': True,
    'active': True,
    'auto_install': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
