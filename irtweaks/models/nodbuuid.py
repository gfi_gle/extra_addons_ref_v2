from openerp.osv import osv


class ir_config_parameter(osv.Model):

    _inherit = "ir.config_parameter"

    def get_param(self, cr, uid, key, default=False, context=None):
        if key == "database.uuid":
            return None

        else:
            return super(ir_config_parameter, self).get_param(
                cr, uid, key, default=default, context=context)
