import babel.dates  # Odoo req.
from openerp import api
from openerp import fields
from openerp import models


# The amount of months in a regular year, before months become free.
REGULAR_MONTH_COUNT = 12


class SubscriptionFrequency(models.Model):
    """Configure the publishing frequency of a subscription.
    """

    _name = 'subscription.frequency'
    _description = 'Subscription frequency'

    @api.depends('free_month_count', 'month_ids', 'month_ids.enabled',
                 'month_ids.index')
    @api.one
    def _get_month_counts(self):
        """Count enabled months.
        """

        paid_month_count = len(self.month_ids.filtered('enabled'))
        self.paid_month_count = paid_month_count
        self.total_month_count = paid_month_count + self.free_month_count

    active = fields.Boolean(
        string='Active',
        default=True,
        help=(
            'Whether this frequency table is active. Inactive ones are hidden '
            'by default.'
        ),
    )

    free_month_count = fields.Integer(
        string='Free month count',
        help='The total amount of months where publications are free.',
        default=0,
        required=True,
    )

    def _default_month_ids(self):
        """Fill a default month list, including both regular and free months.
        """

        # Have to fill month defaults; not computed here for some reason...
        return [(0, 0, {
            'enabled': True,
            'index': index,
            'name': _get_month_name(self, index),
        }) for index in xrange(1, 1 + REGULAR_MONTH_COUNT)]

    month_ids = fields.One2many(
        comodel_name='subscription.frequency.month',
        inverse_name='frequency_id',
        string='Availability per month',
        default=_default_month_ids,
        help='The monthly expected publications.',
    )

    @api.depends('month_ids', 'month_ids.enabled', 'month_ids.index')
    @api.one
    def _get_month_names(self):
        """Provide the names of enabled months.
        """

        self.month_names = u', '.join(
            self.month_ids.filtered('enabled').mapped('name')
        )

    month_names = fields.Char(
        compute=_get_month_names,
        string='Availability per month',
        help='The monthly expected publications.',
        store=True,
    )

    name = fields.Char(
        string='Name',
        size=256,
        help='The name of this frequency table.',
        required=True,
    )

    paid_month_count = fields.Integer(
        compute=_get_month_counts,
        string='Paid month count',
        help='The total amount of months where publications are not free.',
        store=True,
    )

    total_month_count = fields.Integer(
        compute=_get_month_counts,
        string='Total month count',
        help='The total amount of months with publications.',
        store=True,
    )


class SubscriptionFrequencyMonth(models.Model):
    """Configure the monthly frequency of a subscription.
    """

    _name = 'subscription.frequency.month'
    _description = 'Subscription frequency month'

    _order = 'index ASC'

    enabled = fields.Boolean(
        string='Enabled',
        default=True,
        help='Whether publications are expected this month.',
    )

    frequency_id = fields.Many2one(
        comodel_name='subscription.frequency',
        string='Frequency',
        ondelete='cascade',
        help='The frequency table this month setting is for.',
        required=True,
    )

    # Read-only in views so as to allow imports.
    index = fields.Integer(
        string='Index',
        help='The position of this month setting within the frequency table.',
        required=True,
    )

    @api.depends('index')
    @api.one
    def _get_name(self):
        """Set a proper month name based on the index.
        """

        self.name = _get_month_name(self, self.index)

    name = fields.Char(
        compute=_get_name,
        string='Month name',
        help='The localized name of this month; based on the index number.',
        store=True,
    )


def _get_month_name(record, index):
    """Provide a localized name of the specified month. This is a little
    wrapper around a babel method which returns a whole list.
    """

    return babel.dates.get_month_names(
        locale=record.env.context.get('lang', 'en_US'),
    )[index].title() if 1 <= index <= REGULAR_MONTH_COUNT else u'Invalid'
