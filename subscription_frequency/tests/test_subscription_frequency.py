import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0100_create_frequency_table(self):
        """Ensure a frequency table can be created, and that it contains
        expected months by default.
        """

        freqtable, = self.createAndTest(
            'subscription.frequency',
            [{'name': genUuid()}],
        )
        self.memory.freqtable = freqtable

        self.assertTrue(freqtable.active)
        self.assertEqual(freqtable.free_month_count, 0)
        self.assertEqual(freqtable.paid_month_count, 12)
        self.assertEqual(freqtable.total_month_count, 12)
        self.assertEqual(len(freqtable.month_ids), 12)

        # Ensure all month names have been filled.
        for freqmonth in freqtable.month_ids:
            self.assertTrue(freqmonth.name and freqmonth.name != u'Invalid')

    def test_0900_delete_frequency_table(self):
        """Ensure the frequency table created previously can be successfully deleted.
        """

        self.assertTrue(self.memory.freqtable)
        self.assertTrue(self.memory.freqtable.unlink())
