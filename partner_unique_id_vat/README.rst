Partner unique ID & VAT
=======================

Customizations relevant to partner unique ID management & VAT Odoo addons being
installed together.
