Export profiles
===============

Display export profiles, provided by the default Odoo export system, via
regular Odoo views to ease their tweaking / saving.

New menu commands:

- Settings > Technical > User interface > Export profiles.
