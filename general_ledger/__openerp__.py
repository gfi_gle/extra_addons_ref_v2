# -*- coding: utf-8 -*-
##############################################################################
#
#    General Ledger, for OpenERP
#    Copyright (C) 2015-2016 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': u"General Ledger",
    'version': '1.0-dev',
    'author': u"XCG Consulting",
    'category': "Accounting",
    'description': u"""
General Ledger
==============

This module allows generating general ledger reports thanks to
``report_runner.general_ledger``.


Configuration
-------------

- ``general_ledger.max_char_count``: The maximum amount of characters per cell.
  Column widths will be slightly adjusted then. An ellipsis (3 dots) will be
  appended to cut texts.
  Integer.
  0 = No limit.
  Defaults to 15.

- ``general_ledger.max_line_count``: The maximum amount of lines (with relevant
  content) that can be added to the whole report.
  Integer.
  0 = No limit.
  Defaults to 3000.
    """,
    'website': "https://odoo.consulting/",
    'depends': [
        'base',
        'account_report',
        'external_job',
        'queue',
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/job_definitions/general_ledger.xml',
        'security/security.xml',

        # Must be before the other data files.
        'data/queue.csv',

        'data/cron_tasks/full_gl_cron_task.xml',
        'data/cron_tasks/gl_queue_cron_tasks.xml',
        'data/ir_config_parameter.xml',

        'views/general_ledger_view.xml',
    ],
    'demo': [],
    'css': [],
    'test': [],
    'installable': True,
    'active': True,
    'external_dependencies': {
        'python': [
            'sqlalchemy',
#             'report_runner.general_ledger', Not detected correctly but needed
        ],
    },
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
