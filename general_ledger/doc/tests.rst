Tests
=====

General Ledger
--------------

.. automodule:: openerp.addons.general_ledger.tests.test_general_ledger
    :members:
    :undoc-members:
