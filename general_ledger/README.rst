.. _README:

General Ledger
==============

This module allows generating general ledger reports thanks to
``report_runner.general_ledger``.


Configuration
-------------

- ``general_ledger.max_char_count``: The maximum amount of characters per cell.
  Column widths will be slightly adjusted then. An ellipsis (3 dots) will be
  appended to cut texts.
  Integer.
  0 = No limit.
  Defaults to 15.

- ``general_ledger.max_line_count``: The maximum amount of lines (with relevant
  content) that can be added to the whole report.
  Integer.
  0 = No limit.
  Defaults to 3000.
