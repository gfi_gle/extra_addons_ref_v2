##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015-2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import base64
import codecs
from collections import defaultdict
import datetime
import json
import logging
import StringIO
import tempfile
import time

from babel.dates import format_date, format_datetime
from babel.numbers import format_decimal
from openerp import SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools import config
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from openerp.tools.translate import _
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.schema import MetaData, Table
from sqlalchemy.sql import select

from openerp.addons.general_ledger.util.unicode_csv import UnicodeWriter


log = logging.getLogger(__name__)


def decode_utf8(string):
    """str -> unicode for UTF-8 text."""
    if isinstance(string, str):
        return string.decode('utf-8')
    return string


def encode_utf8(string):
    return string.encode('utf8') if string else string


def _get_int_setting(obj, cr, uid, key, context):
    """Read an int setting from global parameters."""

    config_obj = obj.pool['ir.config_parameter']
    config_ids = config_obj.search(
        cr, uid, [('key', '=', key)], limit=1, context=context,
    )
    if not config_ids:
        return 0
    config = config_obj.browse(cr, uid, config_ids, context=context)[0]
    return int(config.value) or 0


class GeneralLedger(osv.Model):
    """This is used to store the result of the wizard.
    """

    _name = 'general_ledger'

    def name_get(self, cr, uid, ids, context=None):
        """Provide the name of this object, constructed from the company name
        and the fiscal year.
        """
        if not ids:
            return []
        if isinstance(ids, (long, int)):
            ids = [ids]

        ledgers = self.browse(cr, uid, ids, context=context)
        return [
            (ledger.id, _("General Ledger - %s - %s") % (
                ledger.company_id.name, ledger.fiscalyear_id.name))
            for ledger in ledgers
        ]

    def get_company_id(self, cr, uid, context=None):
        """Get the company ID of either the current user or that forced via the
        context.

        :rtype: Integer.
        """

        # Look for a "force_company" key in the context.
        if context is not None and isinstance(context, dict):
            forced_company_id = context.get('force_company')
            if forced_company_id:
                return forced_company_id

        user = self.pool['res.users'].browse(cr, uid, uid, context=context)

        return user.company_id.id

    def get_fiscalyear(self, cr, uid, context=None):

        company_id = self.get_company_id(cr, uid, context=context)

        now = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

        return self.pool['account.fiscalyear'].search(
            cr, uid,
            [
                ('company_id', '=', company_id),
                ('date_start', '<', now),
                ('date_stop', '>', now)
            ],
            context=context
        )[0]

    def get_fiscalyear_periods(self, cr, uid, context=None):

        fiscalyear_id = self.get_fiscalyear(cr, uid, context=context)

        fiscalyear = self.pool['account.fiscalyear'].browse(
            cr, uid, fiscalyear_id, context=context
        )

        period_ids = self.pool['account.period'].search(
            cr, uid,
            [('fiscalyear_id', '=', fiscalyear.id)],
            context=context
        )

        return self.pool['account.period'].browse(
            cr, uid, period_ids, context=context
        )

    def get_period_from(self, cr, uid, context=None):

        periods = self.get_fiscalyear_periods(cr, uid, context=context)

        point = min([r.date_start for r in periods])

        dates = [
            (r.id, r.date_start)
            for r in periods
            if r.date_start == point and r.date_stop != r.date_start
        ]

        return dates[0][0]

    def get_period_to(self, cr, uid, context=None):

        periods = self.get_fiscalyear_periods(cr, uid, context=context)

        point = max([r.date_start for r in periods])

        dates = [
            (r.id, r.date_start)
            for r in periods
            if r.date_start == point
        ]

        return dates[0][0]

    def _get_file(self, cr, uid, ids, field_names, arg, context=None):
        """Forward files stored as attachments, either on job logs or on ledger
        objects themselves.
        """

        if isinstance(ids, (long, int)):
            ids = [ids]

        attachment_obj = self.pool['ir.attachment']
        job_log_obj = self.pool['external_job.job_log']

        ret = {}

        # IDs of the ledger objects that keep attachments on themselves.
        gledgers_with_attachment = []

        gledgers = self.browse(cr, uid, ids, context=context)
        for gledger in gledgers:

            if gledger.output_format == 'pdf':
                # Defer to the job log when it holds attachments.
                job_log_id = gledger.job_log_id.id
                ret[gledger.id] = {}
                if 'file' in field_names:
                    ret[gledger.id]['file'] = job_log_obj._get_attachment(
                        cr, uid, [job_log_id], 'out_file', None,
                        context=context,
                    )[job_log_id]
                if 'filename' in field_names:
                    ret[gledger.id]['filename'] = job_log_obj._get_filename(
                        cr, uid, [job_log_id], 'filename', None,
                        context=context,
                    )[job_log_id]

            else:
                gledgers_with_attachment.append(gledger.id)

        if not gledgers_with_attachment:
            # No need to go any further.
            return ret

        attachment_ids = attachment_obj.search(cr, uid, [
            ('res_model', '=', self._name),
            ('res_id', 'in', ids),
        ], context=context)

        # Only read the fields we need.
        to_read = ['res_id']
        if 'file' in field_names:
            to_read.append('datas')
        if 'filename' in field_names:
            to_read.append('datas_fname')
        attachments = attachment_obj.read(
            cr, uid, attachment_ids, to_read, context=context,
        )

        # Cache attachment accessors per record.
        atts_by_gledger = {
            attachment['res_id']: att_index
            for att_index, attachment in enumerate(attachments)
        }

        for gl_id in ids:
            ret[gl_id] = {}
            att_index = atts_by_gledger.get(gl_id)
            if att_index is not None:
                attachment = attachments[att_index]
                if 'file' in field_names:
                    ret[gl_id]['file'] = attachment['datas']
                if 'filename' in field_names:
                    ret[gl_id]['filename'] = attachment['datas_fname']
            else:
                ret[gl_id] = {'file': False, 'filename': False}

        return ret

    def _get_max_line_count(self, cr, uid, ids, field_name, arg, context):
        """Forward the max line count setting.
        """

        max_line_count = _get_int_setting(
            self, cr, uid, 'general_ledger.max_line_count', context,
        )
        return {record_id: max_line_count for record_id in ids}

    def _lang_get(self, cr, uid, context=None):
        # copied from res.partner
        lang_pool = self.pool['res.lang']
        ids = lang_pool.search(cr, uid, [], context=context)
        res = lang_pool.read(cr, uid, ids, ['code', 'name'], context)
        return [(r['code'], r['name']) for r in res]

    _columns = {
        'file': fields.function(
            _get_file,
            multi='get_file',
            type='binary',
            string='File',
            help='The generated file.',
            readonly=True,
        ),
        'filename': fields.function(
            _get_file,
            multi='get_file',
            type='char',
            string='Output',
            size=512,
            help='The generated file.',
            readonly=True,
        ),
        'job_log_id': fields.many2one(
            'external_job.job_log',
            u"External job",
            readonly=True,
            ondelete='cascade',
        ),
        'max_line_count': fields.function(
            _get_max_line_count,
            method=True,
            type='integer',
            store=False,
            string='Max line count',
            help='The maximum amount of lines to be included in the report.',
        ),
        'mode': fields.selection(
            selection=[
                ('full', 'Full - Generated nightly'),
                ('limited', 'Limited - Generated ASAP'),
            ],
            string='Mode',
            help=(
                'Full: The report contains all lines that match filters, but '
                'is generated nightly. Limited: The amount of lines in the '
                'report is limited so as not to overload the server; the '
                'report generation will be put into a queue and run ASAP.'
            ),
            required=True,
        ),
        'output_format': fields.selection(
            selection=[
                ('pdf', 'PDF'),
                ('csv', 'CSV'),
                ('csv_totals', 'CSV with totals'),
            ],
            string='Output format',
            help='The format of the file to generate.',
            required=True,
        ),
        'fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            string=u"Fiscal Year",
            required=True,
        ),
        'select_all_partners': fields.boolean(
            string=u"Select all partners",
        ),
        'partner_id': fields.many2one(
            'res.partner',
            string=u"Select this partner",
        ),
        'select_all_accounts': fields.boolean(
            string=u"Select all accounts",
        ),
        'account_from': fields.many2one(
            'account.account',
            string=u"First Account",
            help=u"""
                Select all accounts in alphanumeric order in the range of the
                the first account code to the last account code.
            """,
        ),
        'account_to': fields.many2one(
            'account.account',
            string=u"Last Account",
            help=u"""
                Select all accounts in alphanumeric order in the range of the
                the first account code to the last account code.
            """,
        ),
        'period_from': fields.many2one(
            'account.period',
            string=u"Period From",
            required=True,
        ),
        'period_to': fields.many2one(
            'account.period',
            string=u"Period To",
            required=True,
        ),
        'include_draft_moves': fields.boolean(
            string=u"Include Draft Moves",
        ),
        'company_id': fields.many2one(
            'res.company',
            string=u"Company",
            readonly=True,
        ),
        'lang': fields.selection(
            _lang_get,
            'Language',
            required=True,
        ),
        'task_id': fields.many2one(
            'queue.task',
            string=u"Task",
            readonly=True,
        ),
        'state': fields.related(
            'task_id',
            'state',
            string=u"Task State",
            readonly=True,
            type='selection',
            selection=[
                ('todo', "To Do"),
                ('started', "Started"),
                ('error', "Error"),
                ('done', "Done"),
            ],
        ),
    }

    _defaults = {
        'max_line_count': lambda self, cr, uid, context: _get_int_setting(
            self, cr, uid, 'general_ledger.max_line_count', context,
        ),
        'mode': lambda *a: 'limited',
        'output_format': lambda *a: 'pdf',
        'select_all_accounts': True,
        'select_all_partners': True,
        'company_id': get_company_id,
        'fiscalyear_id': get_fiscalyear,
        'period_from': get_period_from,
        'period_to': get_period_to,
        'lang': lambda self, cr, uid, ctx: ctx.get('lang', 'en_US'),
    }

    def generate_full_gledgers(self, cr, uid, context=None):
        """Generate full general ledgers for every company, with no restriction
        on the line count.
        """

        log.info('Generating full general ledgers for every company...')

        if context is None:
            context = {}

        company_ids = self.pool['res.company'].search(
            cr, uid, [], context=context,
        )

        gledger_ids = []

        for company_id in company_ids:

            company_context = context.copy()
            company_context.update({
                'company_id': company_id,
                'force_company': company_id,
            })

            gledger_ids.append(self.create(
                cr, uid, {'mode': 'full'}, context=company_context,
            ))

        log.info('%d general ledger generators ready; running...',
                 len(gledger_ids))

        self.generate(cr, uid, gledger_ids, context=context)

        log.info('Done generating %d full general ledgers.', len(gledger_ids))

    def _get_period_ids(self, cr, uid, data, context):
        period_osv = self.pool['account.period']
        edge_periods = period_osv.read(
            cr, uid, [
                data.period_from.id,
                data.period_to.id
            ], ['date_start'], context=context
        )
        start_period_ids = self.pool['account.period'].search(
            cr, uid, [
                ('date_start', '<', edge_periods[0]['date_start']),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
                ('special', '=', False),
            ], context=context
        )
        # if start == end, the read gets us a single value
        end_period = (
            edge_periods[1]
            if len(edge_periods) > 1
            else edge_periods[0])
        end_period_ids = self.pool['account.period'].search(
            cr, uid, [
                ('date_start', '<=', end_period['date_start']),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
                ('special', '=', False),
            ], context=context
        )
        opening_period_id = period_osv.search(
            cr, uid, [
                ('special', '=', True),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
            ], limit=1, context=context
        )[0]
        return opening_period_id, start_period_ids, end_period_ids

    def _split_lines_by_account(self, lines):
        res = defaultdict(list)
        for line in lines:
            res[line['Account Code']].append(line)
        return res

    def generate(self, cr, uid, ids, context=None):
        """Generate one or more general ledgers.
        """
        if type(ids) in (int, long):
            ids = [ids]
        for gl_id in ids:
            self._generate(cr, uid, gl_id, context)

    def _generate(self, cr, uid, gl_id, context=None):
        """Generate one ledger.
        """

        def logThis(msg, *args):
            log.info(
                'General ledger generation - ID %s: %s' % (gl_id, msg), *args
            )

        logThis('Start.')

        data_obj = self.pool['ir.model.data']

        if context is None:
            context = {}

        now = datetime.datetime.now()

        select_all = _(u"All")

        # Get user filters
        data = self.browse(cr, uid, gl_id, context=context)
        user = self.pool['res.users'].browse(
            cr, uid, uid, context=context
        )
        company = data.company_id
        output_format = data.output_format

        def get_int_setting(key=''):
            return _get_int_setting(self, cr, uid, key, context)

        max_char_count = get_int_setting(key='general_ledger.max_char_count')
        max_line_count = (
            get_int_setting(key='general_ledger.max_line_count')
            if data.mode == 'limited' else 0  # No limit when automatic.
        )

        # The periods we fetch lines for may be wider than asked, in order to
        # compute initial balances.
        fiscalyear = data.fiscalyear_id
        fiscalyear_id = fiscalyear.id
        opening_period_id, start_period_ids, end_period_ids = \
            self._get_period_ids(
                cr, uid, data, context
            )

        partner_id = data.partner_id.id if data.partner_id else None

        range = {}
        for position in ['from', 'to']:
            if getattr(data, 'account_%s' % position):
                range[position] = getattr(data, 'account_%s' % position).code
            else:
                range[position] = False

        transaction_date = bool(self.pool['ir.model.fields'].search(
            cr, uid,
            [
                ('model', '=', 'account.move'),
                ('name', '=', 'transaction_date'),
            ],
            context=context
        ))

        gl_generator = GeneralLedgerGenerator(
            cr, company.id, fiscalyear_id, range['from'], range['to'],
            partner_id, opening_period_id, start_period_ids, end_period_ids,
            data.include_draft_moves, data.select_all_accounts,
            data.select_all_partners, max_line_count=max_line_count,
            transaction_date=transaction_date,
        )

        lines = list(gl_generator())

        max_line_count_exceeded = (
            max_line_count and len(lines) > max_line_count
        )

        # Dataset with 1 row per account, ready to be fed to report engines.
        data_per_account = []

        start_balance_period_ids = [opening_period_id] + start_period_ids
        move_period_ids = set(end_period_ids) - set(start_period_ids)
        end_balance_period_ids = [opening_period_id] + end_period_ids

        # Group lines by account.
        lines_by_account = self._split_lines_by_account(lines)

        order = lines_by_account.keys()
        order.sort()

        def format_amount(amount):
            return encode_utf8(format_decimal(
                amount, u'#,##0.00', locale=data.lang,
            ))

        # Init totals.
        total_debit = 0.0
        total_credit = 0.0
        final_total_debit = 0.0
        final_total_credit = 0.0

        # Gather the lines
        for account_code in order:
            lines = lines_by_account[account_code]
            lines.sort(key=lambda line: line['Move Date'])

            # Reset per-account aggregators.
            start_balance = 0.0
            end_balance = 0.0
            debit_per_account = 0.0
            credit_per_account = 0.0
            acc_entries_per_account = []

            for line in lines:
                period_id = line['Period ID']
                debit = line['Line Debit']
                credit = line['Line Credit']

                if period_id in start_balance_period_ids:
                    start_balance += debit - credit
                if period_id in end_balance_period_ids:
                    end_balance += debit - credit
                if period_id in move_period_ids:
                    debit_per_account += debit
                    credit_per_account += credit
                    account_move_line = {
                        'date': format_date(
                            datetime.datetime.strptime(
                                line['Move Date'], DEFAULT_SERVER_DATE_FORMAT
                            ),
                            'short',
                            locale=data.lang
                        ),
                        'period': line['Period Name'],
                        'entry': line['Move Reference'],
                        'journal': line['Journal Code'],
                        'partner': encode_utf8(
                            line['Partner Name']
                            if line['Partner Name'] is not None
                            else ""
                        ),
                        'description': encode_utf8(line['Line Reference']),
                        'debit': format_amount(debit),
                        'credit': format_amount(credit),
                    }

                    if transaction_date:
                        if line['Transaction Date']:
                            ptime = datetime.datetime.strptime(
                                    line['Transaction Date'],
                                    DEFAULT_SERVER_DATE_FORMAT
                                )
                            formatted_date = format_date(
                                ptime,
                                'short',
                                locale=data.lang
                            )
                        else:
                            formatted_date = False
                        account_move_line.update({
                            'transaction_date': formatted_date,
                        })
                    acc_entries_per_account.append(account_move_line)

            start_debit = start_balance if start_balance > 0 else 0.0
            start_credit = -start_balance if start_balance < 0 else 0.0

            final_debit = end_balance if end_balance > 0 else 0.0
            final_credit = -end_balance if end_balance < 0 else 0.0

            data_per_account.append({
                'account_code': account_code,
                'account_name': encode_utf8(line['Account Name']),
                'initial_debit': format_amount(start_debit),
                'initial_credit': format_amount(start_credit),
                'total_movements_debit': format_amount(debit_per_account),
                'total_movements_credit': format_amount(credit_per_account),
                'final_debit': format_amount(final_debit),
                'final_credit': format_amount(final_credit),
                'final_balance': format_amount(end_balance),
                'moves': acc_entries_per_account,
            })

            # Increment totals.
            total_debit += debit_per_account
            total_credit += credit_per_account
            final_total_debit += final_debit
            final_total_credit += final_credit

        total_amounts = {
            'credit': format_amount(total_credit),
            'debit': format_amount(total_debit),
            'final_credit': format_amount(final_total_credit),
            'final_debit': format_amount(final_total_debit),
        }

        # Done! Produce a CSV or a PDF file.

        if output_format == 'csv' or output_format == 'csv_totals':

            # First row (labels).
            csv_data = [[
                _('Account'), _('Period'),
            ] + (
                [_('Transaction Date'), _('Accounting Date')]
                if transaction_date else [_('Date')]
            ) + [
                _('Entry'), _('Journal'),
                _('Partner'), _('Description'), _('Debit'), _('Credit'),
                _('Company'), _('Fiscal year'),
            ]]

            for acc_data in data_per_account:

                account_fullname = u'%s - %s' % (
                    decode_utf8(acc_data['account_code']),
                    decode_utf8(acc_data['account_name']),
                )

                if output_format == 'csv_totals':
                    # Header.
                    csv_data += [[
                        account_fullname, u'', u'', u'', u'', u'',
                        _('Initial'),
                        decode_utf8(acc_data['initial_debit']),
                        decode_utf8(acc_data['initial_credit']),
                        decode_utf8(company.name),
                        decode_utf8(data.fiscalyear_id.name),
                    ]]

                # The accounting entries.
                csv_data += [
                    [
                        account_fullname,
                        decode_utf8(acc_entry['period']),
                    ] + (
                        [decode_utf8(acc_entry['transaction_date'])]
                        if transaction_date else []
                    ) + [
                        decode_utf8(acc_entry['date']),
                        decode_utf8(acc_entry['entry']),
                        decode_utf8(acc_entry['journal']),
                        decode_utf8(acc_entry['partner']),
                        decode_utf8(acc_entry['description']),
                        decode_utf8(acc_entry['debit']),
                        decode_utf8(acc_entry['credit']),
                        decode_utf8(company.name),
                        decode_utf8(data.fiscalyear_id.name),
                    ]
                    for acc_entry in acc_data['moves']
                ]

                if output_format == 'csv_totals':
                    # Footer.
                    csv_data += [[
                        u'', u'', u'', u'', u'', u'',
                        _('Total'),
                        decode_utf8(acc_data['total_movements_debit']),
                        decode_utf8(acc_data['total_movements_credit']),
                        decode_utf8(company.name),
                        decode_utf8(data.fiscalyear_id.name),
                    ]]
                    csv_data += [[
                        u'', u'', u'', u'', u'', u'',
                        _('Final'),
                        decode_utf8(acc_data['final_debit']),
                        decode_utf8(acc_data['final_credit']),
                        decode_utf8(company.name),
                        decode_utf8(data.fiscalyear_id.name),
                    ]]

            if output_format == 'csv_totals':
                # Global footer for totals.
                csv_data += [[
                    u'', u'', u'', u'', u'', u'',
                    _('General total'),
                    decode_utf8(total_amounts['debit']),
                    decode_utf8(total_amounts['credit']),
                    decode_utf8(company.name),
                    decode_utf8(data.fiscalyear_id.name),
                ]]
                csv_data += [[
                    u'', u'', u'', u'', u'', u'',
                    _('General final'),
                    decode_utf8(total_amounts['final_debit']),
                    decode_utf8(total_amounts['final_credit']),
                    decode_utf8(company.name),
                    decode_utf8(data.fiscalyear_id.name),
                ]]

            # Prepare the CSV data.
            csv_stream = StringIO.StringIO()
            csv_stream.write(codecs.BOM_UTF8)  # Make Excel happy...
            csv_writer = UnicodeWriter(
                csv_stream, delimiter=';', quotechar='"',
            )
            csv_writer.writerows(csv_data)
            csv_data = csv_stream.getvalue()
            csv_stream.close()

            # Create an attachment.
            filename = _('GeneralLedger-%s') % now.date().isoformat() + '.csv'
            self.pool['ir.attachment'].create(cr, SUPERUSER_ID, {
                'datas_fname': filename,
                'datas': base64.b64encode(csv_data),
                'name': filename,
                'res_id': gl_id,
                'res_model': self._name,
            }, context=context)

            # We're done here; the rest of this method is for PDF files.
            logThis('Done.')
            return

        # Produce a PDF file by preparing JSON data to be sent to an external
        # report_runner.general_ledger program.

        if data.select_all_accounts:
            option_str = select_all
        else:
            option_str = data.account_from.code + " - " + data.account_to.code

        json_output = total_amounts

        json_output['metadata'] = {
            'lang': data.lang,
            'created': format_datetime(
                now, 'short', locale=data.lang
            ),
            'creator': encode_utf8(user.name),
        }

        json_output['options'] = {
            'company': encode_utf8(company.name),
            'website': company.website,
            'fiscalyear': data.fiscalyear_id.name,
            'currency': company.currency_id.name,
            'period_from': data.period_from.name,
            'period_to': data.period_to.name,
            'account': option_str,
            'moves': select_all,
            'reconcile': select_all,
        }

        json_output['tables'] = data_per_account

        json_temp = tempfile.mkstemp(suffix='.json')

        with codecs.open(json_temp[1], 'wb', "utf-8") as json_file:
            json.dump(json_output, json_file, skipkeys=True, encoding="utf-8")

        ledger_job_def = data_obj.get_object(
            cr, uid,
            'general_ledger',
            'job_definition_general_ledger',
            context=context
        )

        logThis('Got data; calling report_runner...')

        job_log_id = ledger_job_def.run_job(
            job_args={
                'max_char_count': max_char_count,
                'max_line_count': max_line_count,
                'max_line_count_exceeded': (
                    '1' if max_line_count_exceeded else '0'
                ),
            },
            in_file_name=json_temp[1],
            context=context,
        )['res_id']

        logThis('Done calling report_runner.')

        general_ledger_values = {
            'job_log_id': job_log_id,
        }

        self.write(
            cr, uid, [gl_id], general_ledger_values, context=context,
        )

        logThis('Done.')

    def queue(self, cr, uid, ids, context=None):
        """Create queue tasks to generate the specified general ledgers.
        """

        for gledger in self.browse(cr, uid, ids, context=context):

            # The queue depends on the mode.
            queue_id = self.pool['ir.model.data'].get_object_reference(
                cr, uid, 'general_ledger',
                '%s_general_ledger_queue' % gledger.mode,
            )[1]

            # Create a queue task.
            task_id = self.pool['queue.task'].create(cr, uid, {
                'args_interpolation': "{'general_ledger_id': %s}" % gledger.id,
                'queue_id': queue_id,
            }, context=context)

            # Save a link to the new queue task.
            self.write(
                cr, uid, [gledger.id], {'task_id': task_id}, context=context,
            )

    def unlink(self, cr, uid, ids, context=None):
        """Unlink job log and task if any.
        """
        for gl in self.browse(cr, uid, ids, context):
            if gl.job_log_id:
                gl.job_log_id.unlink()
            if gl.task_id:
                gl.task_id.unlink()
        return super(GeneralLedger, self).unlink(cr, uid, ids, context)


class GeneralLedgerGenerator(object):

    def __init__(
        self, cr, company_id, fiscalyear_id, from_code, to_code, partner_id,
        opening_period_id, period_start_ids, period_end_ids,
        include_draft_moves, select_all_accounts, select_all_partners,
        max_line_count=0, transaction_date=False,
    ):
        """Initialize an instance of this class.

        :param max_line_count: Maximum amount of lines to retrieve; a ``LIMIT``
        parameter will be set onto the ``SELECT`` query. 0 = No limit.
        :type max_line_count: Integer.
        """

        # TODO Complete the above comment.

        self.cr = cr
        self.fiscalyear_id = fiscalyear_id
        self.account_from = from_code
        self.account_to = to_code
        self.partner_id = partner_id
        self.company_id = company_id
        self.period_start_ids = period_start_ids
        self.period_end_ids = period_end_ids
        self.opening_period_id = opening_period_id
        self.include_draft_moves = include_draft_moves
        self.select_all_accounts = select_all_accounts
        self.select_all_partners = select_all_partners
        self.transaction_date = transaction_date
        self.tables = {}
        self.query = None
        self.period_query = None
        self.query_fields = []
        self.session = self.__get_session_maker()()
        self.meta = MetaData(bind=self.session.bind)
        self.__init_model()
        self.__init_period_query()
        if not self.select_all_accounts:
            self.__init_account_code_query()
        self.__init_query_fields()
        self.__init_query()
        self.__init_where()
        self.__init_orderby()

        if max_line_count:
            # Limit to +1 so we can know when it has been exceeded.
            self.query = self.query.limit(max_line_count + 1)

        super(GeneralLedgerGenerator, self).__init__()

    def __get_session_maker(self):
        engine = create_engine(
            "postgres://%s:%s@%s:%d/%s" % (
                config.get('db_user'),
                config.get('db_password'),
                config.get('db_host'),
                config.get('db_port'),
                self.cr.dbname,
            ),
            # echo=True,
        )
        return sessionmaker(bind=engine)

    def __init_model(self):
        self.tables.update({
            'move_line': Table(
                'account_move_line',
                self.meta,
                autoload=True,
            ),
            'move': Table(
                'account_move',
                self.meta,
                autoload=True,
            ),
            'account': Table(
                'account_account',
                self.meta,
                autoload=True,
            ),
            'period': Table(
                'account_period',
                self.meta,
                autoload=True,
            ),
            'journal': Table(
                'account_journal',
                self.meta,
                autoload=True,
            ),
            'partner': Table(
                'res_partner',
                self.meta,
                autoload=True,
            ),
        })

    def __init_query_fields(self):
        self.query_fields.extend([
            self.tables['period'].c.id.label('Period ID'),
            self.tables['period'].c.name.label('Period Name'),
            self.tables['account'].c.code.label('Account Code'),
            self.tables['account'].c.name.label('Account Name'),
            self.tables['move_line'].c.debit.label('Line Debit'),
            self.tables['move_line'].c.credit.label('Line Credit'),
        ] + (
            [self.tables['move'].c.transaction_date.label('Transaction Date')]
            if self.transaction_date else []
        ) + [
            self.tables['move'].c.date.label('Move Date'),
            self.tables['move_line'].c.ref.label('Line Reference'),
            self.tables['move'].c.ref.label('Move Reference'),
            self.tables['journal'].c.code.label('Journal Code'),
            self.tables['partner'].c.name.label('Partner Name'),
        ])

    def __init_query(self):
        self.query = select(self.query_fields).select_from(
            self.tables['move_line'].outerjoin(
                self.tables['account'],
                self.tables['account'].c.id ==
                self.tables['move_line'].c.account_id,
            ).outerjoin(
                self.tables['move'],
                self.tables['move'].c.id ==
                self.tables['move_line'].c.move_id,
            ).outerjoin(
                self.tables['period'],
                self.tables['period'].c.id ==
                self.tables['move_line'].c.period_id,
            ).outerjoin(
                self.tables['partner'],
                self.tables['partner'].c.id ==
                self.tables['move_line'].c.partner_id,
            ).outerjoin(
                self.tables['journal'],
                self.tables['journal'].c.id ==
                self.tables['move_line'].c.journal_id,
            )
        )

    def __init_period_query(self):
        self.period_query = select(
            [self.tables['period'].c.id]
        ).select_from(
            self.tables['period']
        ).where(
            and_(
                self.tables['period'].c.fiscalyear_id == self.fiscalyear_id,
                self.tables['period'].c.company_id == self.company_id,
            ),
        )

    def __init_account_code_query(self):
        self.account_code_query = select(
            [self.tables['account'].c.id]
        ).select_from(
            self.tables['account']
        ).where(
            and_(
                self.tables['account'].c.company_id == self.company_id,
                self.tables['account'].c.code >= self.account_from,
                self.tables['account'].c.code <= self.account_to,
            ),
        )

    def __init_where(self):
        and_args = (
            self.tables['account'].c.company_id == self.company_id,
            self.tables['move_line'].c.period_id.in_(self.period_query),
            self.tables['period'].c.id.in_(
                self.period_end_ids + [self.opening_period_id]
            ),
        )

        if not self.select_all_accounts:
            and_args += (
                self.tables['account'].c.id.in_(self.account_code_query),
            )

        if not self.select_all_partners:
            and_args += (
                self.tables['move_line'].c.partner_id == self.partner_id,
            )

        # Check if we care about draft moves
        move_states = ('posted',)
        if self.include_draft_moves:
            move_states += ('draft',)

        and_args += (self.tables['move'].c.state.in_(move_states),)

        self.query = self.query.where(and_(*and_args))

    def __init_orderby(self):
        self.query = self.query.order_by(
            self.tables['move'].c.date,
            self.tables['account'].c.code,
        )

    def __call__(self):
        # Establish connection, execute query and yield resulting rows
        conn = self.session.bind.connect()
        for result in conn.execute(self.query):
            yield result
        # XXX an explicit close might be needed here
