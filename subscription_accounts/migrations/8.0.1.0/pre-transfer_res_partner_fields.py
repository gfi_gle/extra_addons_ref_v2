# encoding: utf-8
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import logging

__name__ = (
    "Transfer the partner fields which are the purpose of this module from "
    "this module to a new one, which is to be installed"
)

log = logging.getLogger(__name__)


def migrate(cr, installed_version):

    log.info(
        "Transfer the partner fields which are the purpose of this module "
        "from this module to a new one, which is to be installed."
    )

    suffixes = [
        'account_id',
        'payment_terms_id',
    ]

    names = [
        'operational_client',
        'operational_supplier',
        'referrer_fees',
        'asset_supplier',
        'author',
    ]

    fields = [
        'is_operational_client',
        'operational_client_credit_limit',
        'is_operational_supplier',
        'has_referrer_fees',
        'is_asset_supplier',
        'is_author',
        'partner_info_required',
    ]

    for suffix in suffixes:
        for name in names:
            fields.append('%s_%s' % (name, suffix))

    for field in fields:

        cr.execute("""
            UPDATE ir_model_data
            SET module = 'partner_account_gle'
            WHERE name = 'field_res_partner_%s'
        """ % field)

    config_items = [
        'operational_client_payment_terms_id',
        'operational_supplier_payment_terms_id',
        'referrer_fees_payment_terms_id',
    ]

    for item in config_items:

        result = 'field_account_config_settings_%s' % item
        config = 'field_subscription_config_settings_%s' % item

        cr.execute("""
            UPDATE ir_model_data
            SET module = 'partner_account_gle', name = '%s'
            WHERE name = '%s'
        """ % (result, config))

    cr.execute("""
        SELECT id, res_id
        FROM ir_model_data
        WHERE module = 'subscription_accounts' and model = 'ir.ui.view'
    """)

    views = cr.fetchall()

    view_ids = ", ".join(["%d" % tpl[1] for tpl in views])
    xml_ids = ", ".join(["%d" % tpl[0] for tpl in views])

    cr.execute("""
       DELETE FROM ir_ui_view
       WHERE id in (%s)
    """ % view_ids)

    cr.execute("""
       DELETE FROM ir_model_data
       WHERE id in (%s)
    """ % xml_ids)

    log.info('Done.')
