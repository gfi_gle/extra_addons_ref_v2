Flat chart of accounts
======================

This Odoo module contains enhancements for a better accounting experience.

Once installed, your chart of accounts will be flat. No more parent_left,
parent_right and parent_id.

This speeds up accounting a lot and is the only way to do accouting in Odoo
if you have big charts of accounts.

This module also disables various amount sums which need account parents. In
our experience, no accountant actually uses these sums; they tend to prefer
custom reports.
