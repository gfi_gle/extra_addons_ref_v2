Welcome to this addon's documentation!
======================================

.. include:: manifest

Contents:

.. toctree::
   :maxdepth: 2

   README


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

