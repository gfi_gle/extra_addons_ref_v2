8.0.3.0.2
=========

* Sort accounts by code since they have no ``parent_left`` field anymore.


8.0.3.0.1
=========

* Remove method overrides ``_parent_store`` handles.


8.0.3.0.0
=========

* Always disable amount sums (in effect, we ignore the change added in version
  2.0).

* Odoo 7 is no longer supported in this version.

* Hide amount sums from account views.


2.0
===

* Enable amount sums in some cases (used by some reports).


1.0
===

* Initial version. Disables account parents & related amount sum fields.
