##############################################################################
#
#    Flat chart of accounts, for Odoo
#    Copyright (C) 2013 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Flat chart of accounts',
    'description': '''
Flat chart of accounts
======================

This Odoo module contains enhancements for a better accounting experience.

Once installed, your chart of accounts will be flat. No more parent_left,
parent_right and parent_id.

This speeds up accounting a lot and is the only way to do accouting in Odoo
if you have big charts of accounts.

This module also disables various amount sums which need account parents. In
our experience, no accountant actually uses these sums; they tend to prefer
custom reports.
''',
    'version': '8.0.3.0.2',
    'category': 'Accounting',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'base',
        'account_accountant',
    ],

    'data': [
        'views/account.xml',
    ],

    'installable': True,
}
