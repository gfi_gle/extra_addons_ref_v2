import time

import openerp.exceptions
import openerp.fields
import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_load_default_accounting_accounts(self):
        """Ensure default accounts can be loaded up fine.
        """

        # Before we start, reset existing accounts.
        self.env.cr.execute('''
        UPDATE account_account SET parent_left = 0, parent_right = 0
        ''')

        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': time.strftime('%Y-01-01'),
            'date_stop': time.strftime('%Y-12-31'),
            'period': 'month',
        }).execute()

        top_normal_account = self.env['account.account'].search(
            [('type', '=', 'other')],
            limit=1,
        )
        self.assertTrue(top_normal_account)
        self.memory.top_normal_account = top_normal_account

    def test_0001_create_account(self):
        """Create an account and check fields we have disabled.
        """

        parent_account = self.memory.top_normal_account

        account, = self.createAndTest('account.account', [
            {
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': parent_account.id,
                'reconcile': True,
                'type': parent_account.type,
                'user_type': parent_account.user_type.id,
            },
        ])

        self.assertFalse(account.debit)
        self.assertFalse(account.credit)
        self.assertFalse(account.balance)

        self.assertFalse(account.parent_left)
        self.assertFalse(account.parent_right)

        self.assertFalse(parent_account.debit)
        self.assertFalse(parent_account.credit)
        self.assertFalse(parent_account.balance)

        self.assertFalse(parent_account.parent_left)
        self.assertFalse(parent_account.parent_right)
