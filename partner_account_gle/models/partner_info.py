import logging
from openerp import api
from openerp import fields
from openerp import models

from openerp.addons.partner_account_gle.models.partner import PARTNER_ACCOUNTS

# The following allows us a soft dependency on partner_unique_id_repository.
try:
    from openerp.addons.partner_unique_id_repository.models.partner_info \
        import PartnerInfo as puidrPartnerInfo
    in_puidrepo = True
except ImportError:
    in_puidrepo = False


log = logging.getLogger(__name__)


class PartnerInfo(models.Model):
    """Adapt unique ID records for GLE:
    - Add and sync accounting accounts. The fields added here are similar to
      those added to partners, except:
        * Payment terms are no longer properties.
        * Accounts are only codes, not links to "account.account".
    """

    _inherit = 'res.partner.info'

    # Group: Operational client.

    is_operational_client = fields.Boolean(
        string='Operational client',
        help=(
            'When enabled, flag this partner as being a client relevant to '
            'current operations; and enable client accounting set-up.'
        ),
    )

    operational_client_account = fields.Char(
        string='Operational client account',
        help='Accounting account generated for current client operations.',
    )

    operational_client_credit_limit = fields.Float(
        string='Operational client credit limit',
        help='Credit limit for current client operations.',
    )

    operational_client_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Operational client payment terms',
        ondelete='restrict',
        help='Payment terms for current client operations.',
    )

    # Group: Operational supplier.

    is_operational_supplier = fields.Boolean(
        string='Operational supplier',
        help=(
            'When enabled, flag this partner as being a supplier relevant to '
            'current operations; and enable supplier accounting set-up.'
        ),
    )

    operational_supplier_account = fields.Char(
        string='Operational supplier account',
        help='Accounting account generated for current supplier operations.',
    )

    operational_supplier_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Operational supplier payment terms',
        ondelete='restrict',
        help='Payment terms for current supplier operations.',
    )

    # Group: Third-party share fee client.

    is_third_party_share_fee_client = fields.Boolean(
        string='Third-party share fee client',
        help=(
            'When enabled, flag this partner as being a client with '
            'third-party share fees; and enable client accounting set-up.'
        ),
    )

    third_party_share_fee_client_account = fields.Char(
        string='Third-party share fee client account',
        help='Accounting account generated for third-party share fees.',
    )

    third_party_share_fee_client_credit_limit = fields.Float(
        string='Third-party share fee client credit limit',
        help='Credit limit for third-party share fees.',
    )

    third_party_share_fee_client_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Third-party share fee client payment terms',
        ondelete='restrict',
        help='Payment terms for third-party share fees.',
    )

    # Group: Third-party share supplier.

    is_third_party_share_supplier = fields.Boolean(
        string='Third-party share supplier',
        help=(
            'When enabled, flag this partner as being a supplier of '
            'third-party shares; and enable supplier accounting set-up.'
        ),
    )

    third_party_share_supplier_account = fields.Char(
        string='Third-party share supplier account',
        help='Accounting account generated for third-party supplies.',
    )

    third_party_share_supplier_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Third-party share supplier payment terms',
        ondelete='restrict',
        help='Payment terms for third-party supplies.',
    )

    # Group: Referrer fees.

    # Should be named "has_referrer_fees" but "is" is easier for dynamic names.
    is_referrer_fees = fields.Boolean(
        string='Referrer fees',
        help=(
            'When enabled, flag this partner as benefiting from referrer '
            'fees; and enable supplier accounting set-up.'
        ),
    )

    referrer_fees_account = fields.Char(
        string='Referrer fees account',
        help='Accounting account generated for referrer fees.',
    )

    referrer_fees_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Referrer fees payment terms',
        ondelete='restrict',
        help='Payment terms for referrer fees.',
    )

    # Group: Asset supplier.

    is_asset_supplier = fields.Boolean(
        string='Asset supplier',
        help=(
            'When enabled, flag this partner as being a supplier of assets; '
            'and enable supplier accounting set-up.'
        ),
    )

    asset_supplier_account = fields.Char(
        string='Asset supplier account',
        help='Accounting account generated for asset supplies.',
    )

    asset_supplier_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Asset supplier payment terms',
        ondelete='restrict',
        help='Payment terms for asset supplies.',
    )

    # Group: Author.

    is_author = fields.Boolean(
        string='Author',
        help=(
            'When enabled, flag this partner as being an author; and enable '
            'supplier accounting set-up.'
        ),
    )

    author_account = fields.Char(
        string='Author account',
        help='Accounting account generated for being an author.',
    )

    author_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Author payment terms',
        ondelete='restrict',
        help='Payment terms for being an author.',
    )

    # Group: Goods trade.

    # Should be named "has_goods_trade" but "is" is easier for dynamic names.
    is_goods_trade = fields.Boolean(
        string='Goods trade',
        help=(
            'When enabled, flag this partner as participating in goods trade; '
            'and enable both client and supplier accounting set-up.'
        ),
    )

    goods_trade_account = fields.Char(
        string='Goods trade account',
        help='Accounting account generated for goods trade.',
    )

    goods_trade_credit_limit = fields.Float(
        string='Goods trade credit limit',
        help='Credit limit for goods trade.',
    )

    goods_trade_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Goods trade payment terms',
        ondelete='restrict',
        help='Payment terms for goods trade.',
    )

    # Utility.

    @api.one
    def _get_show_third_party_accounts(self):
        config_record = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', 'partner_account_gle.show_third_party_accounts')],
            limit=1,
        )
        self.show_third_party_accounts = (
            config_record and config_record.value == '1'
        )

    show_third_party_accounts = fields.Boolean(
        compute=_get_show_third_party_accounts,
        string='Show third-party accounts',
    )

    @api.one
    def _get_show_goods_trade_accounts(self):
        config_record = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', 'partner_account_gle.show_goods_trade_accounts')],
            limit=1,
        )
        self.show_goods_trade_accounts = (
            config_record and config_record.value == '1'
        )

    show_goods_trade_accounts = fields.Boolean(
        compute=_get_show_goods_trade_accounts,
        string='Show goods trade accounts',
    )

    @api.multi
    def process_creation_request_hook(
        self, log_prefix, data, new_partner_info,
    ):
        """Hook to let other addons further process a partner information
        creation request sent by an external system.

        Override this method defined in the "partner_unique_id_repository"
        addon to receive accounts. There is no explicit dependency on that
        addon though.

        :type log_prefix: String.

        :param data: Dict validated by the "process_creation_request" method.

        :param new_partner_info: Whether a new unique ID record is being
        created (as opposed to an existing one simply being augmented).
        :type new_partner_info: Boolean.
        """

        self.ensure_one()

        # Build update values to avoid too many updates in the loop.
        update_values = {}

        def ensureStr(text):
            """Ensure the specified variable is read as a string."""
            return text or u''

        for account_data in data.get('accounts', []):

            # Ignore disabled accounts. We only create new data.
            if not account_data.get('enabled'):
                continue

            name = account_data.get('name')
            if not name:
                log.warning(log_prefix + 'Received an account with no name; '
                            'ignoring.')
                continue

            current_enabled = getattr(self, 'is_%s' % name)
            if current_enabled:
                log.info(log_prefix + '%s account already enabled; ignoring.',
                         name)
                continue
            update_values['is_%s' % name] = True

            # Set an account code when new. Follow account generation rules.
            current_account = getattr(self, '%s_account' % name)
            if not current_account:
                accgenrule = self.env['account.generation_rule'].search(
                    [('partner_condition_field_id.name', '=',
                      PARTNER_ACCOUNTS[name])],
                    limit=1,
                )
                if accgenrule:
                    # Imitate account.generation_rule::generate_account.
                    update_values['%s_account' % name] = (
                        ensureStr(accgenrule.code_prefix) +
                        ensureStr(self.code) +
                        ensureStr(accgenrule.code_suffix)
                    )
                else:
                    log.warning(log_prefix + 'Account gen rule for "%s" not '
                                'found.', name)

            # Set a credit limit when new.
            climit = account_data.get('credit_limit')
            if climit:
                current_climit = getattr(self, '%s_credit_limit' % name)
                if not current_climit:
                    update_values['%s_credit_limit' % name] = climit

            # Set payment terms when new. Find based on the day count.
            ptermdays = account_data.get('payment_terms')
            if ptermdays:
                current_pterms = getattr(self, '%s_payment_terms_id' % name)
                if not current_pterms:
                    pterms = self.env['account.payment.term'].search(
                        [('line_ids.days', '=', int(ptermdays))], limit=1,
                    )
                    if pterms:
                        update_values['%s_payment_terms_id' % name] = pterms.id
                    else:
                        log.warning(log_prefix + 'Payment terms not found: '
                                    'days = %d.', ptermdays)

        if update_values:
            self.write(update_values)

        # super-like call (soft dependency on partner_unique_id_repository).
        return puidr_process_creation_request_hook(
            self, log_prefix, data, new_partner_info,
        )

    @api.multi
    def process_update_hook(self, log_prefix, data):
        """Hook to let other addons further process a partner information
        update sent by an external partner repository.

        Override this method defined in the "partner_unique_id" addon to
        receive accounts.

        :type log_prefix: String.

        :param data: Dict validated by the "process_update" method.
        """

        self.ensure_one()

        # Build update values to avoid too many updates in the loop.
        update_values = {}

        for account_data in data.get('accounts', []):

            name = account_data.get('name')
            if not name:
                log.warning(log_prefix + 'Received an account with no name; '
                            'ignoring.')
                continue

            update_values.update({
                '%s_account' % name: account_data.get('account_name'),
                'is_%s' % name: account_data.get('enabled'),
            })

            # Credit limit. Note that not every account has one such field.
            if hasattr(self, '%s_credit_limit' % name):
                update_values['%s_credit_limit' % name] = (
                    account_data.get('credit_limit')
                )

            # Payment terms.
            ptermdays = account_data.get('payment_terms')
            if ptermdays:
                pterms = self.env['account.payment.term'].search(
                    [('line_ids.days', '=', int(ptermdays))], limit=1,
                )
                if pterms:
                    update_values['%s_payment_terms_id' % name] = pterms.id
                else:
                    log.warning(log_prefix + 'Payment terms not found: days = '
                                '%d.', ptermdays)

        if update_values:
            self.write(update_values)

        return super(PartnerInfo, self).process_update_hook(log_prefix, data)

    @api.multi
    def update_xbus_dict(self):
        """Return a dict representing the specified record update, ready to be
        sent to Xbus.

        Override this method defined in the "partner_unique_id_repository"
        addon to add accounts. There is no explicit dependency on that addon
        though.
        """

        self.ensure_one()

        # super-like call (soft dependency on partner_unique_id_repository).
        ret = puidr_update_xbus_dict(self)

        def paymentTermDays(name):
            """Get a day count from a payment term entry."""
            pterms = getattr(self, '%s_payment_terms_id' % name)
            if pterms:
                ptermlines = pterms.line_ids
                if ptermlines:
                    return ptermlines[0].days
            return False

        ret['accounts'] = [
            {
                'account_name': getattr(self, '%s_account' % name),
                'credit_limit': getattr(self, '%s_credit_limit' % name, False),
                'enabled': getattr(self, 'is_%s' % name),
                'name': name,
                'payment_terms': paymentTermDays(name),
            }
            for name in PARTNER_ACCOUNTS.iterkeys()
        ]

        return ret


if in_puidrepo:
    # Save previous methods then replace them in (soft dependency on
    # partner_unique_id_repository).

    puidr_process_creation_request_hook = \
        puidrPartnerInfo.process_creation_request_hook
    puidrPartnerInfo.process_creation_request_hook = \
        PartnerInfo.process_creation_request_hook

    puidr_update_xbus_dict = puidrPartnerInfo.update_xbus_dict
    puidrPartnerInfo.update_xbus_dict = PartnerInfo.update_xbus_dict
