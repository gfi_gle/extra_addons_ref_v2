from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models


# New accounting flags that require the partner to be a client.
CLIENT_FLAGS = [
    'is_operational_client',
    'is_third_party_share_fee_client',
    'has_goods_trade',
]

# New accounting flags that require the partner to be a supplier.
SUPPLIER_FLAGS = [
    'is_operational_supplier',
    'is_third_party_share_supplier',
    'has_referrer_fees',
    'is_asset_supplier',
    'is_author',
    'has_goods_trade',
]

# The accounts we add. name -> flag mapping.
PARTNER_ACCOUNTS = {
    'operational_client': 'is_operational_client',
    'third_party_share_fee_client': 'is_third_party_share_fee_client',
    'operational_supplier': 'is_operational_supplier',
    'third_party_share_supplier': 'is_third_party_share_supplier',
    'referrer_fees': 'has_referrer_fees',
    'asset_supplier': 'is_asset_supplier',
    'author': 'is_author',
    'goods_trade': 'has_goods_trade',
}


class Partner(models.Model):
    """Adapt partners to accounting management in GLE.
    """

    _inherit = 'res.partner'

    # ===
    # Fields in the "Accounting" tab.
    # ===

    # Group: Operational client.

    is_operational_client = fields.Boolean(
        string='Operational client',
        help=(
            'When enabled, flag this partner as being a client relevant to '
            'current operations; and enable client accounting set-up.'
        ),
    )

    operational_client_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Operational client account',
        domain=[('type', '=', 'receivable')],
        ondelete='restrict',
        help='Accounting account generated for current client operations.',
    )

    operational_client_credit_limit = fields.Float(
        string='Operational client credit limit',
        help='Credit limit for current client operations.',
    )

    operational_client_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Operational client payment terms',
        ondelete='restrict',
        help='Payment terms for current client operations.',
    )

    # Group: Operational supplier.

    is_operational_supplier = fields.Boolean(
        string='Operational supplier',
        help=(
            'When enabled, flag this partner as being a supplier relevant to '
            'current operations; and enable supplier accounting set-up.'
        ),
    )

    operational_supplier_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Operational supplier account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for current supplier operations.',
    )

    operational_supplier_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Operational supplier payment terms',
        ondelete='restrict',
        help='Payment terms for current supplier operations.',
    )

    # Group: Third-party share fee client.

    is_third_party_share_fee_client = fields.Boolean(
        string='Third-party share fee client',
        help=(
            'When enabled, flag this partner as being a client with '
            'third-party share fees; and enable client accounting set-up.'
        ),
    )

    third_party_share_fee_client_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Third-party share fee client account',
        domain=[('type', '=', 'receivable')],
        ondelete='restrict',
        help='Accounting account generated for third-party share fees.',
    )

    third_party_share_fee_client_credit_limit = fields.Float(
        string='Third-party share fee client credit limit',
        help='Credit limit for third-party share fees.',
    )

    third_party_share_fee_client_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Third-party share fee client payment terms',
        ondelete='restrict',
        help='Payment terms for third-party share fees.',
    )

    # Group: Third-party share supplier.

    is_third_party_share_supplier = fields.Boolean(
        string='Third-party share supplier',
        help=(
            'When enabled, flag this partner as being a supplier of '
            'third-party shares; and enable supplier accounting set-up.'
        ),
    )

    third_party_share_supplier_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Third-party share supplier account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for third-party supplies.',
    )

    third_party_share_supplier_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Third-party share supplier payment terms',
        ondelete='restrict',
        help='Payment terms for third-party supplies.',
    )

    # Group: Referrer fees.

    has_referrer_fees = fields.Boolean(
        string='Referrer fees',
        help=(
            'When enabled, flag this partner as benefiting from referrer '
            'fees; and enable supplier accounting set-up.'
        ),
    )

    referrer_fees_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Referrer fees account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for referrer fees.',
    )

    referrer_fees_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Referrer fees payment terms',
        ondelete='restrict',
        help='Payment terms for referrer fees.',
    )

    # Group: Asset supplier.

    is_asset_supplier = fields.Boolean(
        string='Asset supplier',
        help=(
            'When enabled, flag this partner as being a supplier of assets; '
            'and enable supplier accounting set-up.'
        ),
    )

    asset_supplier_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Asset supplier account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for asset supplies.',
    )

    asset_supplier_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Asset supplier payment terms',
        ondelete='restrict',
        help='Payment terms for asset supplies.',
    )

    # Group: Author.

    is_author = fields.Boolean(
        string='Author',
        help=(
            'When enabled, flag this partner as being an author; and enable '
            'supplier accounting set-up.'
        ),
    )

    author_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Author account',
        domain=[('type', '=', 'payable')],
        ondelete='restrict',
        help='Accounting account generated for being an author.',
    )

    author_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Author payment terms',
        ondelete='restrict',
        help='Payment terms for being an author.',
    )

    # Group: Goods trade.

    has_goods_trade = fields.Boolean(
        string='Goods trade',
        help=(
            'When enabled, flag this partner as participating in goods trade; '
            'and enable both client and supplier accounting set-up.'
        ),
    )

    goods_trade_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Goods trade account',
        domain=[('type', '=', 'other')],
        ondelete='restrict',
        help='Accounting account generated for goods trade.',
    )

    goods_trade_credit_limit = fields.Float(
        string='Goods trade credit limit',
        help='Credit limit for goods trade.',
    )

    goods_trade_payment_terms_id = fields.Many2one(
        company_dependent=True,  # May be filled into settings.
        comodel_name='account.payment.term',
        string='Goods trade payment terms',
        ondelete='restrict',
        help='Payment terms for goods trade.',
    )

    # Utility.

    @api.one
    def _get_show_third_party_accounts(self):
        config_record = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', 'partner_account_gle.show_third_party_accounts')],
            limit=1,
        )
        self.show_third_party_accounts = (
            config_record and config_record.value == '1'
        )

    # TODO This field is never actually computed; fix.
    show_third_party_accounts = fields.Boolean(
        compute=_get_show_third_party_accounts,
        string='Show third-party accounts',
    )

    @api.one
    def _get_show_goods_trade_accounts(self):
        config_record = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', 'partner_account_gle.show_goods_trade_accounts')],
            limit=1,
        )
        self.show_goods_trade_accounts = (
            config_record and config_record.value == '1'
        )

    # TODO This field is never actually computed; fix.
    show_goods_trade_accounts = fields.Boolean(
        compute=_get_show_goods_trade_accounts,
        string='Show goods trade accounts',
    )

    # ===
    # Fields in the "Unique identification" tab.
    # ===

    partner_info_required = fields.Boolean(
        string='Unique information required',
        help=(
            'When enabled, a unique information record must be defined for '
            'this partner.'
        ),
    )

    @api.constrains(
        'partner_info_entity_identifier',
        'partner_info_entity_type_id',
        'partner_info_required',
    )
    @api.one
    def _check_required_partner_info(self):
        """Ensure the condition set by the "Unique information required" field
        is satisfied.
        """

        if self.partner_info_required and (
            not self.partner_info_entity_identifier or
            not self.partner_info_entity_type_id
        ):
            raise exceptions.ValidationError(_(
                'Unique information is required for the "%s" partner.'
            ) % self.name)

    @api.constrains(*set(
        ['customer', 'supplier'] +
        CLIENT_FLAGS + SUPPLIER_FLAGS
    ))
    @api.one
    def _check_client_supplier_flags(self):
        """Ensure "Client" and "Supplier" flags are correctly set when some
        flags that require them are.
        """

        if (
            any(getattr(self, client_flag) for client_flag in CLIENT_FLAGS) and
            not self.customer
        ):
            raise exceptions.ValidationError(_(
                'The "%s" partner must be marked as a client.'
            ) % self.name)

        if (
            any(getattr(self, sup_flag) for sup_flag in SUPPLIER_FLAGS) and
            not self.supplier
        ):
            raise exceptions.ValidationError(_(
                'The "%s" partner must be marked as a supplier.'
            ) % self.name)

    @api.multi
    def write(self, vals):
        """Override to:
        - Save operational client / supplier accounts into the base ones.
        - Save operational client / supplier payment terms into the base ones.
        - Generate accounts when enabling an account-related setting after
          partner information has been generated.
        - Sync account activation with the external partner repository.
        """

        # Save operational client / supplier accounts into the base ones.
        account_id = vals.get('operational_client_account_id')
        if account_id:
            vals['property_account_receivable'] = account_id
        account_id = vals.get('operational_supplier_account_id')
        if account_id:
            vals['property_account_payable'] = account_id

        # Save operational client / supplier payment terms into the base ones.
        pterms_id = vals.get('operational_client_payment_terms_id')
        if pterms_id:
            vals['property_payment_term'] = pterms_id
        pterms_id = vals.get('operational_supplier_payment_terms_id')
        if pterms_id:
            vals['property_supplier_payment_term'] = pterms_id

        ret = super(Partner, self).write(vals)

        # Generate accounts when enabling an account-related setting after
        # partner information has been generated.
        if any(vals.get(flag) for flag in set(CLIENT_FLAGS + SUPPLIER_FLAGS)):
            self.generate_accounts()

            # Sync account activation with the external partner repository.
            for partner in self:
                if partner.partner_info_status in ('submitted', 'validated'):
                    partner.send_unique_id_req(partial_update=True)

        return ret

    @api.multi
    def link_to_unique_id_record(self, partner_info):
        """Link partners to the specifieid unique ID record and update related
        data.

        Override this method defined in the "partner_unique_id" addon to
        generate accounts based on rules.

        :type partner_info: Odoo "res.partner.info" record set.
        """

        ret = super(Partner, self).link_to_unique_id_record(partner_info)
        self.generate_accounts()
        return ret

    @api.multi
    def unique_id_req_xbus_dict(self, partial_update=False):
        """Return a dict representing a unique ID request, ready to be sent to
        Xbus.

        Override this method defined in the "partner_unique_id" addon to add
        accounts.

        :param partial_update: Whether a partial update should be signaled.
        :type partial_update: Boolean.
        """

        self.ensure_one()

        ret = super(Partner, self).unique_id_req_xbus_dict(
            partial_update=partial_update,
        )

        def paymentTermDays(name):
            """Get a day count from a payment term entry."""
            pterms = getattr(self, '%s_payment_terms_id' % name)
            if pterms:
                ptermlines = pterms.line_ids
                if ptermlines:
                    return ptermlines[0].days
            return False

        ret['accounts'] = [
            {
                'credit_limit': getattr(self, '%s_credit_limit' % name, False),
                'enabled': getattr(self, flag),
                'name': name,
                'payment_terms': paymentTermDays(name),
            }
            for name, flag in PARTNER_ACCOUNTS.iteritems()
        ]

        return ret

    @api.one
    def generate_accounts(self):
        """Generate accounts based on rules."""

        if self.partner_info_status != 'validated':
            # Partner information is needed to generate accounts.
            return

        account_gen_rules = self.env['account.generation_rule'].search([])

        for account_gen_rule in account_gen_rules:
            account_gen_rule.generate_account(
                self, self.partner_info_id.code,
                self.partner_info_official_name,
            )

    @api.onchange(*set(CLIENT_FLAGS + SUPPLIER_FLAGS))
    def on_change_accounting_flags(self):
        """Auto-enable some flags when enabling specific accounting ones.
        """

        if any(getattr(self, client_flag) for client_flag in CLIENT_FLAGS):
            self.customer = True
            self.partner_info_required = True

        if any(getattr(self, sup_flag) for sup_flag in SUPPLIER_FLAGS):
            self.supplier = True
            self.partner_info_required = True
