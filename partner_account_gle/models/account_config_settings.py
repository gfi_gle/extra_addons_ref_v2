from openerp import api
from openerp import fields
from openerp import models


class AccountingConfigSettings(models.TransientModel):
    """Accounting configuration view for partner settings.
    """

    _inherit = 'account.config.settings'

    @api.model
    def get_partner_payment_terms(self):
        """List the property fields, per model.
        Override to include yours.
        :rtype: Dictionary.
        """

        return {
            'res.partner': [
                'operational_client_payment_terms_id',
                'operational_supplier_payment_terms_id',
                'third_party_share_fee_client_payment_terms_id',
                'third_party_share_supplier_payment_terms_id',
                'referrer_fees_payment_terms_id',
                'asset_supplier_payment_terms_id',
                'author_payment_terms_id',
                'goods_trade_payment_terms_id',
            ],
        }

    # ===
    # Payment terms, ordered the same as in the "res.partner" model.
    # ===

    operational_client_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Operational client payment terms',
        help='Payment terms for current client operations.',
    )

    operational_supplier_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Operational supplier payment terms',
        help='Payment terms for current supplier operations.',
    )

    third_party_share_fee_client_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Third-party share fee client payment terms',
        help='Payment terms for third-party share fees.',
    )

    third_party_share_supplier_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Third-party share supplier payment terms',
        help='Payment terms for third-party supplies.',
    )

    referrer_fees_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Referrer fees payment terms',
        help='Payment terms for referrer fees.',
    )

    asset_supplier_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Asset supplier payment terms',
        help='Payment terms for asset supplies.',
    )

    author_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Author payment terms',
        help='Payment terms for being an author.',
    )

    goods_trade_payment_terms_id = fields.Many2one(
        comodel_name='account.payment.term',
        string='Goods trade payment terms',
        help='Payment terms for goods trade.',
    )

    # ===

    @api.model
    def _get_property(self, model, field_name):
        """Get the specified property.

        :return: Odoo "ir.property" record set.
        """

        return self.env['ir.property'].search([
            ('res_id', '=', False),  # Only global ones.
            ('fields_id.model_id.model', '=', model),
            ('fields_id.name', '=', field_name),
        ], limit=1)

    @api.model
    def _read_property_id(self, model, field_name):
        """Get the record the specified property points to, if it is defined.

        :return: Record ID or False.
        """

        prop = self._get_property(model, field_name)
        if not prop.value_reference:
            return False
        model_name, record_id_str = prop.value_reference.split(',')
        return int(record_id_str)

    @api.model
    def get_default_partner_payment_terms(self, fields):
        """Read property fields. This function is called when the form is
        shown.
        """

        return {
            field: self._read_property_id(model, field)
            for model, field_list in (
                self.get_partner_payment_terms().iteritems()
            )
            for field in field_list
        }

    @api.multi
    def set_partner_payment_terms(self):
        """Update property fields. This function is called when saving the
        form.
        """

        for model, field_list in self.get_partner_payment_terms().iteritems():
            for field in field_list:

                value = getattr(self, field)

                # Find out whether a property already exists.
                prop = self._get_property(model, field)

                if value:
                    # Create or update the property.
                    value = '%s,%s' % (value._name, value.id)
                    if prop:
                        prop.value_reference = value
                    else:
                        prop.create({
                            'fields_id': self.env['ir.model.fields'].search([
                                ('model_id.model', '=', model),
                                ('name', '=', field),
                            ], limit=1).id,
                            'name': 'default_%s_%s' % (model, field),
                            'type': 'many2one',
                            'value_reference': value,
                        })

                elif prop:
                    # Delete the property.
                    prop.unlink()
