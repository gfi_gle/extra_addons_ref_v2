import logging
import openerp.exceptions
import openerp.models
import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


log = logging.getLogger(__name__)


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_add_xbus_emitter(self):
        """Add a fake Xbus emitter so messages can be set up without failure.
        """

        self.createAndTest('xbus.emitter', [{
            'front_url': genUuid(), 'login': genUuid(), 'password': genUuid(),
        }])

    def test_0001_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partner_info_entity_types = self.createAndTest(
            'res.partner.info.entity_type',
            [
                {
                    'identifier_char_range': 'digit',
                    'identifier_label': 'company',
                    'name': 'company',
                    'technical_name': 'company',
                    'identifier_min_chars': 13,
                    'identifier_max_chars': 15,
                },
            ],
        )

        self.memory.partners = self.createAndTest(
            'res.partner',
            [
                {
                    'name': genUuid(),
                },
                {
                    'name': genUuid(),
                },
                {
                    'name': genUuid(),
                },
                {
                    'name': genUuid(),
                },
            ],
        )

    def test_0010_check_required_partner_info(self):
        """Ensure the condition set by the "Unique information required" field
        is satisfied.
        """

        partner = self.memory.partners[0]

        with self.assertRaises(openerp.exceptions.ValidationError):
            partner.partner_info_required = True

    def test_0011_check_client_supplier_flags(self):
        """Ensure "Client" and "Supplier" flags are correctly set when some
        flags that require them are.
        """

        partner = self.memory.partners[1]

        def empty_flags():
            partner.write({
                'customer': False,
                'supplier': False,
                'is_operational_client': False,
                'is_operational_supplier': False,
            })

        empty_flags()
        with self.assertRaises(openerp.exceptions.ValidationError):
            partner.is_operational_client = True

        empty_flags()
        with self.assertRaises(openerp.exceptions.ValidationError):
            partner.is_operational_supplier = True

        # Ensure it works otherwise.
        empty_flags()
        partner.write({'customer': True, 'is_operational_client': True})
        empty_flags()
        partner.write({'supplier': True, 'is_operational_supplier': True})

    def test_0100_create_account_types(self):
        """Create account types, that will be used in other tests.
        """

        self.memory.test_account_type, = self.createAndTest(
            'account.account.type',
            [
                {
                    'code': 'test',
                    'name': 'test',
                },
            ],
        )

    def test_0110_create_account_generation_rules(self):
        """Create account generation rules, that will be used in other tests.
        """

        self.memory.account_gen_rule_referrer_fees, = self.createAndTest(
            'account.generation_rule',
            [
                {
                    'account_type_id': self.memory.test_account_type.id,
                    'code_prefix': 'REF',
                    'partner_account_field_id': self.browse_ref(
                        'partner_account_gle.'
                        'field_res_partner_referrer_fees_account_id'
                    ).id,
                    'partner_condition_field_id': self.browse_ref(
                        'partner_account_gle.'
                        'field_res_partner_has_referrer_fees'
                    ).id,
                },
            ],
        )

    def test_0120_account_generation_rule_generate_account(self):
        """Tests around the "generate_account" method of account generation
        rules; inspired by those done in the "partner_account_gen" addon but
        with partner fields added by this addon.
        """

        account_gen_rule = self.memory.account_gen_rule_referrer_fees

        account_code = 'testcode'
        account_name = 'testname'

        partner = self.memory.partners[0]

        # First, a test run without enabling referrer fees.
        account = account_gen_rule.generate_account(
            partner, account_code, account_name,
        )
        self.assertIsNone(account)

        # Now set referrer fees and launch generation again.
        partner.write({
            'has_referrer_fees': True,
            'supplier': True,
        })

        account = account_gen_rule.generate_account(
            partner, account_code, account_name,
        )

        # Ensure the account has been created as expected.
        self.assertIsInstance(account, openerp.models.BaseModel)
        self.assertEqual(account.code, 'REFtestcode')
        self.assertEqual(account.name, 'testname')

        # Ensure a link to the account has been saved on the partner.
        self.assertEqual(partner.referrer_fees_account_id.id, account.id)

        # Try again with another partner and ensure the same account is
        # re-used.
        other_partner = self.memory.partners[1]
        other_partner.write({
            'has_referrer_fees': True,
            'supplier': True,
        })
        other_account = account_gen_rule.generate_account(
            other_partner, account_code, account_name,
        )
        self.assertEqual(other_account.id, account.id)
        self.assertEqual(other_partner.referrer_fees_account_id.id, account.id)

    def test_0200_account_gen_when_unique_id_validated(self):
        """Ensure accounts are generated when a unique ID is received.
        We don't test the whole stack; that is done in tests of the
        "partner_unique_id" Odoo addon.
        """

        if self._inPartnerRepo():
            log.warning('Test disabled in the partner repository.')
            return

        # Set the minimum needed onto the partner.
        partner = self.memory.partners[2]
        partner.write({
            'has_referrer_fees': True,
            'supplier': True,
            'partner_info_entity_type_id': (
                self.memory.partner_info_entity_types[0].id
            ),
            'partner_info_entity_identifier': '123456789012345',
            'partner_info_official_name': genUuid(),
            'partner_info_status': 'validated',
        })

        # Generate accounts. This is called when linking to a unique ID.
        partner.generate_accounts()

        # Ensure it's all good!
        account = partner.referrer_fees_account_id
        self.assertEqual(
            account.code,
            self.memory.account_gen_rule_referrer_fees.code_prefix,
        )
        self.assertEqual(account.name, partner.partner_info_official_name)

        # Clean up.
        partner.write({
            'has_referrer_fees': False,
            'referrer_fees_account_id': False,
        })
        account.unlink()
        self.assertFalse(partner.referrer_fees_account_id)

    def test_0210_account_gen_when_enabling_after_unique_id_validated(self):
        """Ensure accounts are generated when enabling an accounting flag after
        a unique ID has been received.
        """

        if self._inPartnerRepo():
            log.warning('Test disabled in the partner repository.')
            return

        partner = self.memory.partners[3]

        # Set up our partner the way it would be after a unique ID link.
        partner.write({
            'partner_info_entity_type_id': (
                self.memory.partner_info_entity_types[0].id
            ),
            'partner_info_entity_identifier': '123456780002345',
            'partner_info_official_name': genUuid(),
            'partner_info_status': 'validated',
        })

        # Ensure it has no account so far.
        self.assertFalse(partner.referrer_fees_account_id)

        # Ask for one.
        partner.write({
            'has_referrer_fees': True,
            'supplier': True,
        })

        # Ensure an account has been generated.
        account = partner.referrer_fees_account_id
        self.assertEqual(
            account.code,
            self.memory.account_gen_rule_referrer_fees.code_prefix,
        )
        self.assertEqual(account.name, partner.partner_info_official_name)

        # Clean up.
        partner.write({
            'has_referrer_fees': False,
            'referrer_fees_account_id': False,
        })
        account.unlink()
        self.assertFalse(partner.referrer_fees_account_id)

    def _inPartnerRepo(self):
        """When this addon is installed in the partner repository (next to
        partner_unique_id_repository, various tests no longer work. Use this
        to selectively disable those that don't, so that basic ones can still
        keep running.

        :rtype: Boolean.
        """

        return self.env['ir.module.module'].search(
            [('name', '=', 'partner_unique_id_repository')], limit=1,
        ).state == 'installed'
