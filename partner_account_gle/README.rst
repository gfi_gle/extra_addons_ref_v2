Partner Accounting Customizations for GLE
=========================================

Adds accounting accounts and the associated management settings in Odoo
partners.

* Dependencies:

    - account_streamline
    - partner_unique_id
    - partner_account_gen
