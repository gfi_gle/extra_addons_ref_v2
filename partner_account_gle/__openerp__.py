##############################################################################
#
#    Partner Accounting Customizations for GLE, for Odoo
#    Copyright (C) 2017 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Partner Accounting Customizations for GLE',
    'description': '''
Partner Accounting Customizations for GLE
=========================================

Adds accounting accounts and the associated management settings in Odoo
partners.
''',
    'version': '8.0.1.0',
    'category': 'Accounting',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'account_streamline',
        'partner_unique_id',
        'partner_account_gen',
    ],

    'data': [
        'data/ir.config_parameter.xml',

        'views/account_config_settings.xml',
        'views/partner.xml',
        'views/partner_info.xml',
        'views/partner_no_formatting.xml',
    ],

    'installable': True,
}
