Tests
=====

Partner Accounting Customizations for GLE
-----------------------------------------

.. automodule:: openerp.addons.account_lep.tests.test_account_lep
    :members:
    :undoc-members:

