import time
import base64
import subprocess

import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid

from openerp import exceptions


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_account(self):
        """Create accounting account for use in further tests.
        """

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': time.strftime('%Y-01-01'),
            'date_stop': time.strftime('%Y-12-31'),
            'period': 'month',
        }).execute()

        parent_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )
        self.assertTrue(parent_account)

        self.memory.account, = self.createAndTest(
            'account.account',
            [{
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': parent_account.id,
                'reconcile': True,
                'type': parent_account.type,
                'user_type': parent_account.user_type.id,
            }],
        )

    def test_0001_create_payment_methods(self):
        """Create payment methods and store them for further use.
        """

        # Find accounting journals.
        payment_journal = self.env['account.journal'].search(
            [('type', '=', 'bank')], limit=1,
        )
        sales_journal = self.env['account.journal'].search(
            [('type', '=', 'sale')], limit=1,
        )
        self.assertTrue(payment_journal)
        self.assertTrue(sales_journal)

        self.memory.payment_methods = self.createAndTest(
            'crm.payment.mode',
            [
                {
                    'bank_tracked': True,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                    'sales_invoices_bank_payment_write_off_account_id': (
                        self.memory.account.id
                    ),
                    'sales_invoices_bank_payment_journal_id': (
                        payment_journal.id
                    ),
                },
                {
                    'bank_tracked': False,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                },
            ],
        )

        self.env['ir.property'].create({
            'fields_id': self.env.ref(
                'sale_invoice_bank_payment.'
                'field_sale_invoice_bank_tracker_'
                'bank_tracker_payment_method_id'
            ).id,
            'name': 'bank_tracker_payment_method_id',
            'type': 'many2one',
            'value_reference': 'crm.payment.mode,%d' % (
                self.memory.payment_methods[0].id
            ),
        })

    def test_0002_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partners = self.createAndTest(
            'res.partner',
            [
                {
                    'customer': True,
                    'name': genUuid(),
                },
            ],
        )

    def test_0003_create_products(self):
        """Create products for use in further tests.
        """

        # Create in 2 steps to avoid field propagation issues between products
        # and their templates.
        product_templates = self.createAndTest(
            'product.template',
            [
                {
                    'name': genUuid(),
                    'type': 'service',
                },
            ],
        )
        self.memory.products = self.createAndTest(
            'product.product',
            [
                {'product_tmpl_id': product_template.id}
                for product_template in product_templates
            ],
        )

    def test_0010_create_sales_orders(self):
        """Create sales orders and store them for further use.
        """

        def buildSalesOrder():
            return {
                'order_policy': 'prepaid',  # To generate an invoice.
                'partner_id': self.memory.partners[0].id,
                'payment_method_id': self.memory.payment_methods[0].id,
            }

        # Create a sales order.
        self.memory.sales_orders = self.createAndTest(
            'sale.order',
            [
                buildSalesOrder()
                for i in range(3)
            ]
        )

        for order, price in zip(
            self.memory.sales_orders, [277.20, 220.0, 200.0]
        ):
            # Add lines into the sales order.
            self.createAndTest(
                'sale.order.line',
                [
                    {
                        'name': genUuid(),
                        'order_id': order.id,
                        'price_unit': price,
                        'product_uom_qty': 1.0,
                        'product_id': self.memory.products[0].id,
                    },
                ],
            )

        self.memory.invoices = []

        for sales_order in self.memory.sales_orders:

            # Create the invoice.
            self.assertEqual(sales_order.state, 'draft')
            sales_order.signal_workflow('order_confirm')
            self.assertEqual(sales_order.state, 'progress')

            # Validate the invoice.
            invoice = sales_order.invoice_ids
            self.assertTrue(invoice)
            self.assertEqual(invoice.state, 'draft')
            invoice.signal_workflow('invoice_open')
            self.assertEqual(invoice.state, 'open')

            self.memory.invoices.append(invoice)

    def test_0020_launch_import_wizard(self):
        """Launch the AFB120 file import wizard to generate bank trackers.
        """

        for i in range(1, 5):
            setattr(
                self.memory, 'f%s' % i,
                open(
                    'data/unit_tests/sale_invoice_bank_tracker_afb120/'
                    'result_test%s.txt' % i,
                    'r'
                )
            )

        results = [
            {
                'date': '2016-01-17',
                'import_amount': 277.20,
                'description': self.memory.f1.read(),
            },
            {
                'date': '2017-01-31',
                'import_amount': 158.40,
                'description': self.memory.f2.read(),
            },
            {
                'date': '2016-12-10',
                'import_amount': 135.40,
                'description': self.memory.f3.read(),
            },
            {
                'date': '2016-02-29',
                'import_amount': 113.52,
                'description': self.memory.f4.read(),
            },
        ]

        f = open(
            'data/unit_tests/sale_invoice_bank_tracker_afb120/'
            'test_afb120_20170222 153000.txt',
            'r'
        )
        data = base64.b64encode(f.read())

        self.memory.wizard, = self.createAndTest(
            'sale.invoice_bank_tracker.import',
            [
                {
                    'file': data,
                    'type': 'afb120',
                },
            ],
        )

        tracker_ids = \
            self.memory.wizard.run_import_from_dialog()['domain'][0][2]

        self.memory.trackers = self.env['sale.invoice_bank_tracker'].browse(
            tracker_ids
        )

        i = 0
        for tracker, result in zip(self.memory.trackers, results):
            i += 1

            self.assertEqual(tracker.name, '0000000 VIR TEST%s' % i)
            self.assertTrue(tracker.import_generated)
            for field in result.keys():
                self.assertEqual(getattr(tracker, field), result[field])

    def test_0021_check_control_on_imported_amount_gap(self):

        tracker = self.memory.trackers[0]

        tracker.write(
            {
                'invoice_type': 'out_invoice',
                'name': genUuid(),
                'tracker_line_ids': [
                    (0, 0, {
                        'invoice_id': self.memory.invoices[0].id,
                        'amount_retained': 240.0,
                        # Normal amount to retain: 277.20
                    })
                ],
            },
        )

        self.assertEqual(tracker.state, 'draft')

        with self.assertRaises(exceptions.ValidationError):
            tracker.signal_workflow('validate')

    def test_0030_pass_control_on_imported_amount_gap(self):

        tracker = self.memory.trackers[0]
        tracker.tracker_line_ids[0].amount_retained = 277.20

        self.assertEqual(tracker.import_amount_gap, 0)
        self.assertEqual(tracker.state, 'draft')
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'validated')

    def test_0031_check_control_on_imported_amount_gap(self):

        tracker = self.memory.trackers[1]

        tracker.write(
            {
                'invoice_type': 'out_invoice',
                'name': genUuid(),
                'tracker_line_ids': [
                    (0, 0, {
                        'invoice_id': self.memory.invoices[1].id,
                        'amount_retained': 160.00,
                        # Normal amount to retain: 277.20
                    })
                ],
            },
        )

        self.assertEqual(tracker.state, 'draft')

        with self.assertRaises(exceptions.ValidationError):
            tracker.signal_workflow('validate')

    def test_0301_pass_control_on_imported_amount_gap(self):

        tracker = self.memory.trackers[1]
        tracker.tracker_line_ids[0].amount_retained = 158.40

        self.assertEqual(tracker.import_amount_gap, 0)
        self.assertEqual(tracker.state, 'draft')
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'amount_gap_validation')

        tracker.tracker_line_ids.unlink()
        tracker.tracker_line_ids = [(0, 0, {
            'invoice_id': self.memory.invoices[2].id,
            'amount_retained': 160.00,
        })]

        with self.assertRaises(exceptions.ValidationError):
            tracker.signal_workflow('validate')

        tracker.tracker_line_ids[0].amount_retained = 158.40
        self.assertEqual(tracker.import_amount_gap, 0)
        tracker.signal_workflow('validate')
        self.assertEqual(tracker.state, 'validated')
