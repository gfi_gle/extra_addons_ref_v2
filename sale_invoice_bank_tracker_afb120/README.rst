Sales Invoice Bank Tracker Import from AFB120 files
===================================================

* Category: Accounting

* Quick summary:
Create bank payments to reconcile against invoices based on the import of a 
AFB120 file.

* Version:
8.0.1.0

* Dependencies:
sale_invoice_bank_payment

* Repo owner or admin: XCG Consulting
