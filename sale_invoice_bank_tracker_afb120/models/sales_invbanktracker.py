from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class SaleInvoiceBankTracker(models.Model):
    """Override the bank tracker class, in the case of tracker generation based
    on the import of a AFB120 file.
    """

    _inherit = 'sale.invoice_bank_tracker'

    @api.depends(
        'invoice_total_amount', 'tracker_line_ids.amount_retained',
        'import_amount'
    )
    @api.one
    def _get_total_amounts(self):

        """Override to compute the imported amount gap."""

        super(SaleInvoiceBankTracker, self)._get_total_amounts()

        for record in self:

            record.import_amount_gap = abs(
                record.import_amount - record.amount
            )

    import_generated = fields.Boolean(
        string='Created on file import',
        readonly=True,
    )

    import_amount = fields.Float(
        string='Imported tracker amount',
        digits=dp.get_precision('Account'),  # Like tracker amount.
        help=(
            'Declared amount for this invoices bank tracker, on AFB120 file '
            'import. It must be equal to the tracker amount.'
        ),
        readonly=True,
    )

    import_amount_gap = fields.Float(
        compute=_get_total_amounts,
        string='Imported amount gap',
        digits=dp.get_precision('Account'),  # Like invoices amounts.
        help=(
            'Delta between the amount tracked by this invoices bank tracker '
            'and its theoretical amount declared on AFB120 file import.'
        ),
        store=True,
        readonly=True,
    )

    @api.multi
    def has_correct_import_amount_gap(self):
        """Find out whether the imported theoretical amount is not equal to the
        tracker amount on validation.
        """

        if self.import_generated and self.import_amount_gap != 0:

            raise exceptions.ValidationError(_(
                    'The tracker amount of %12.2f must be equal to the '
                    'theoretical amount of %12.2f, which has been declared '
                    'on file import for this tracker.'
                ) % (self.amount, self.import_amount))

        return True
