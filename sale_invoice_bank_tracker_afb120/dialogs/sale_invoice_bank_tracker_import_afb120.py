# -*- coding: utf-8 -*-

import base64
from datetime import datetime

from openerp import models, fields, api, exceptions
from openerp.tools.translate import _


class SaleInvoiceBankTrackerImportAFB120(models.TransientModel):
    _name = 'sale.invoice_bank_tracker.import'

    file = fields.Binary(
        u"Import File"
    )

    type = fields.Selection(
        [
            ('afb120', u"AFB120 / CFONB"),
        ],
        string=u"File Format",
        required=True,
        default='afb120',
    )

    @api.model
    def _parse_afb120_amount(self, amount_str, nb_of_dec):
        """Taken from the cfonb lib"""

        # translate the last char and set the sign
        credit_trans = {
            'A': '1', 'B': '2', 'C': '3', 'D': '4', 'E': '5',
            'F': '6', 'G': '7', 'H': '8', 'I': '9', '{': '0'
        }
        debit_trans = {
            'J': '1', 'K': '2', 'L': '3', 'M': '4', 'N': '5',
            'O': '6', 'P': '7', 'Q': '8', 'R': '9', '}': '0'
        }
        credit_str = credit_trans.get(amount_str[-1])
        debit_str = debit_trans.get(amount_str[-1])
        if credit_str is not None:
            last_digit, is_debit = credit_str, False
        elif debit_str is not None:
            last_digit, is_debit = debit_str, True
        else:
            raise exceptions.Warning(u"Invalid amount in AFB120 file.")

        if nb_of_dec > 0:
            amount_str = '{}.{}{}'.format(
                amount_str[:-nb_of_dec], amount_str[-nb_of_dec:-1], last_digit
            )
        else:
            amount_str = '{}{}'.format(amount_str[:-1], last_digit)
        amount_num = float(amount_str) * (-1 if is_debit else 1)
        return amount_num

    @api.model
    def _check_afb120(self, data_file):
        return data_file.lstrip().startswith('01')

    @api.model
    def _parse_afb120(self, data_str):
        """ Import a file in French CFONB format"""
        cfonb = self._check_afb120(data_str)
        if not cfonb:
            raise exceptions.Warning(_(u"Not a valid AFB120 file."))

        afb_lines = data_str.splitlines()
        if not afb_lines:
            raise exceptions.Warning(_(u"The file is empty."))

        trackers = self.env['sale.invoice_bank_tracker']
        bank_code = guichet_code = account_number = currency_code = False
        decimals = start_balance = False
        start_balance = end_balance = start_date_str = end_date_str = False
        vals_line = False

        for i, line in enumerate(afb_lines, 1):
            if not line:
                continue
            if len(line) != 120:
                raise Warning(
                    _(
                        u"Line {} is {} caracters long. All lines of a AFB120 "
                        u"bank statement file must be 120 caracters long."
                    ).format(i, len(line))
                )

            line_bank_code = line[2:7]
            line_guichet_code = line[11:16]
            line_account_number = line[21:32]
            line_currency_code = line[16:19]
            rec_type = line[0:2]
            decimals = int(line[19:20])
            date_cfonb_str = line[34:40]
            date_str = False
            if date_cfonb_str != '      ':
                date_dt = datetime.strptime(date_cfonb_str, '%d%m%y')
                date_str = fields.Date.to_string(date_dt)

            if i == 1:
                bank_code = line_bank_code
                guichet_code = line_guichet_code
                currency_code = line_currency_code
                account_number = line_account_number
                if rec_type != '01':
                    raise Warning(
                        _("The 2 first letters of the first line are '%s'. "
                            "A CFONB file should start with '01'")
                        % rec_type)
                start_balance = self._parse_afb120_amount(
                    line[90:104], decimals)
                start_date_str = date_str

            elif (
                bank_code != line_bank_code or
                guichet_code != line_guichet_code or
                currency_code != line_currency_code or
                account_number != line_account_number
            ):
                raise Warning(
                    _('Only single-account files and single-currency '
                        'files are supported for the moment. It is not '
                        'the case starting from line %d.') % i)

            if rec_type == '05':
                # This is a continuation line ; update the current transaction
                if not vals_line:
                    raise exceptions.Warning(
                        _(u"Line {}: not a valid continuation line").format(i)
                    )
                vals_line['description'] += '\n{}: {}'.format(
                    line[45:48], line[48:119].strip()
                )
                continue

            elif vals_line:

                # Only import bank transfers.
                tracker_descr = vals_line.get('description')
                if tracker_descr.startswith('VIR'):

                    # Save the previous transaction as a bank tracker.
                    trackers += trackers.create(vals_line)

                # Reset the data.
                vals_line = False

            if rec_type == '07':
                end_date_str = date_str
                end_balance = self._parse_afb120_amount(line[90:104], decimals)
            elif rec_type == '04':
                amount = self._parse_afb120_amount(line[90:104], decimals)
                ref = line[81:88].strip()  # This is not unique
                name = line[48:79].strip()
                vals_line = {
                    'name':  u"{} {}".format(ref, name),
                    'date': date_str,
                    'description': name,
                    'import_generated': True,
                    'import_amount': amount,
                }

        return trackers

    @api.multi
    def parse_file(self):
        """Parse the file and generate sales bank trackers. This method is here
        so it can be called from the outside.

        :return: The generated sales bank trackers.
        """

        self.ensure_one()

        data_str = base64.b64decode(self.file)
        if self.type == 'afb120':
            return self._parse_afb120(data_str)
        else:
            raise exceptions.Warning(
                _(u"Parser for {} files is not implemented".format(self.type))
            )

    @api.multi
    def run_import_from_dialog(self):
        """Parse the file and generate sales bank trackers. This method is
        meant to be called from the import dialog box.

        :return: Odoo action to display the imported bank trackers.
        """

        trackers = self.parse_file()

        return {
            'context': self.env.context,
            'domain': [('id', 'in', trackers.ids)],
            'name': _('Imported bank trackers'),
            'res_model': 'sale.invoice_bank_tracker',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }
