# -*- coding: utf-8 -*-
##############################################################################
#
#    Sales invoice bank tracker AFB120 Import for Odoo
#    Copyright (C) 2017 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Sales Invoice Bank Tracker AFB120 Import',
    'description': '''
Sales Invoice Bank Tracker Import from AFB120 files
===================================================

Create bank payments to reconcile against invoices based on the import of a
AFB120 file.

''',
    'version': '8.0.1.0',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'sale_invoice_bank_payment',
    ],

    'data': [
        'dialogs/sale_invoice_bank_tracker_import_afb120.xml',
        'views/sales_invbanktracker.xml',
        'workflows/sales_invbanktracker.xml',
    ],

    'installable': True,
}
