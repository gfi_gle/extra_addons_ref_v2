Models
======

.. graphviz:: models.gv

Account File
------------

.. automodule:: openerp.addons.account_file.models.account_file
    :members:
    :undoc-members:

Flow
----

.. automodule:: openerp.addons.account_file.models.account_file_flow
    :members:
    :undoc-members:

Document
--------

.. automodule:: openerp.addons.account_file.models.account_file_document
    :members:
    :undoc-members:

