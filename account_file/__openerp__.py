##############################################################################
#
#    Accounting files for Odoo
#    Copyright (C) 2015 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Accounting files',
    'description': '''
Accounting files
================

Define a base for accounting files, meant to group accounting entries together,
generated based on a generic event and with overridable parameters.

This Odoo addon is meant to serve as a base for more specific Odoo addons that
may need the concept of accounting files.
''',
    'version': '0.1',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'account_move_reversal',
        'account_streamline',
        'analytic_structure',
    ],

    'data': [
        'security/ir.model.access.csv',

        'menu.xml',

        'views/account_file.xml',

        'workflows/account_file.xml',
        'workflows/account_file_flow.xml',
    ],

    'installable': True,
}
