Accounting files
================

Define a base for accounting files, meant to group accounting entries together,
generated based on a generic event and with overridable parameters.

This Odoo addon is meant to serve as a base for more specific Odoo addons that
may need the concept of accounting files.
