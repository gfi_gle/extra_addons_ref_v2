from openerp import _
from openerp import api
from openerp import fields
from openerp import models

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


class AccountingFile(models.Model):
    """Accounting files are meant to group accounting entries together,
    generated based on a generic event and with overridable parameters.
    """

    __metaclass__ = MetaAnalytic

    _name = 'account.file'

    _analytic = True

    # Each accounting file corresponds to an analytic code.
    _dimension = True

    _order = 'id DESC'

    @api.depends('document_ids')
    @api.one
    def _get_document_count(self):
        self.document_count = self.env['account.file.document'].search(
            [('account_file_id', '=', self.id)], count=True,
        )

    document_count = fields.Integer(
        compute=_get_document_count,
        string='Accounting document count',
        help=(
            'Accounting documents generated in relation to this accounting '
            'file.'
        ),
        store=True,
    )

    document_ids = fields.One2many(
        comodel_name='account.file.document',
        inverse_name='account_file_id',
        string='Generated documents',
        readonly=True,
        help=(
            'Accounting documents generated in relation to this accounting '
            'file.'
        ),
    )

    flow_ids = fields.One2many(
        comodel_name='account.file.flow',
        inverse_name='account_file_id',
        string='Generated flows',
        help='Accounting flows generated in relation to this accounting file.',
    )

    name = fields.Char(
        string='Name',
        required=True,
        help='Name of this accounting file.',
    )

    state = fields.Selection(
        selection=[
            ('draft', 'Draft'),
            ('awaiting_reversal', 'Awaiting reversal'),
            ('reversed', 'Reversed'),
        ],
        string='State',
        help=(
            'State of this accounting file; updated when accounting documents '
            'are reversed.'
        ),
        readonly=True,
        track_visibility='onchange',
        default='draft',
    )

    @api.one
    def check_flow_workflows(self):
        """Check states of accounting flows in the specified accounting file,
        so as to automatically switch the state of the accounting file.
        """

        if all(self.flow_ids.mapped(lambda flow: flow.state == 'reversed')):
            self.signal_workflow('reversed')

    @api.multi
    def open_accounting_entries(self):
        """Display accounting entries related to this accounting file.
        """

        self.ensure_one()

        return {
            'context': self.env.context,
            'domain': [
                ('id', 'in', self.document_ids.mapped('account_move_id.id')),
            ],
            'name': _('Accounting entries'),
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.one
    def wkf_draft(self):
        """Switch the state to "draft".
        """

        if self.state != 'draft':  # It's the default.
            self.state = 'draft'

    @api.one
    def wkf_awaiting_reversal(self):
        """- Switch the state to "awaiting_reversal".
        - Signal accounting flows they are now also awaiting reversal.
        """

        self.state = 'awaiting_reversal'

        self.flow_ids.signal_workflow('awaiting_reversal')

    @api.one
    def wkf_reversed(self):
        """Switch the state to "reversed".
        """

        self.state = 'reversed'
