from openerp import api
from openerp import fields
from openerp import models


class AccountingFlow(models.Model):
    """Accounting flow generated in relation to an accounting file.
    Keep track of an accounting document and allow reversing it.
    """

    _name = 'account.file.flow'

    account_file_id = fields.Many2one(
        comodel_name='account.file',
        string='Accounting file',
        ondelete='cascade',
        required=True,
        help=(
            'Accounting file this accounting flow has been generated in '
            'relation to.'
        ),
    )

    document_ids = fields.One2many(
        comodel_name='account.file.document',
        inverse_name='flow_id',
        string='Generated documents',
        readonly=True,
        help=(
            'Accounting documents generated in relation to this accounting '
            'flow.'
        ),
    )

    name = fields.Char(
        string='Name',
        required=True,
        help='Name of this accounting flow.',
    )

    state = fields.Selection(
        selection=[
            ('draft', 'Draft'),
            ('awaiting_reversal', 'Awaiting reversal'),
            ('reversed', 'Reversed'),
        ],
        string='State',
        help=(
            'State of this accounting file flow; updated when accounting '
            'documents are reversed.'
        ),
        readonly=True,
        track_visibility='onchange',
        default='draft',
    )

    reversal_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Reversal accounting journal',
        help=(
            'The accounting journal used to reverse this accounting flow; '
            'when unspecified, the reversed accounting document will have the '
            'same accounting journal as its base accounting document.'
        ),
    )

    @api.one
    def reverse_documents(self, reconcile=True):
        """Reverse the accounting documents.

        :param reconcile: Whether source and reversed accounting entries are to
        be reconciled.
        """

        reversal_journal = self.reversal_journal_id

        for doc in self.document_ids:

            # Find out whether the reversed accounting document is being put
            # into a separate accounting journal. When that is the case, ask
            # for custom name & ref for the reversed accounting document.
            journal = reversal_journal or doc.journal_id
            custom_journal = journal.id != doc.journal_id.id

            # In Odoo 7 style as account_move_reversal is in Odoo 7 style.
            self.pool['account.move'].reverse_move(
                self.env.cr, self.env.user.id, move_id=doc.account_move_id.id,
                journal_id=journal.id, date=fields.Datetime.now(),
                doc_name=('/' if custom_journal else None),
                doc_ref=(doc.ref if custom_journal else None),
                reconcile=reconcile, context=self.env.context,
            )

    @api.one
    def wkf_draft(self):
        """Switch the state to "draft".
        """

        if self.state != 'draft':  # It's the default.
            self.state = 'draft'

    @api.one
    def wkf_awaiting_reversal(self):
        """Switch the state to "awaiting_reversal".
        """

        self.state = 'awaiting_reversal'

    @api.one
    def wkf_reversed(self):
        """- Reverse the accounting documents.
        - Switch the state to "reversed".
        - Signal the linked accounting file about this.
        """

        self.reverse_documents(reconcile=True)

        self.state = 'reversed'

        self.account_file_id.check_flow_workflows()
