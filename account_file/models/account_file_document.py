from openerp import api
from openerp import fields
from openerp import models


class AccountingDocument(models.Model):
    """Accounting document generated in relation to an accounting file.

    Inherit by delegation from Odoo accounting documents; this is a separate
    entity so one can add more information here.
    """

    _name = 'account.file.document'

    account_file_id = fields.Many2one(
        related='flow_id.account_file_id',
        comodel_name='account.file',
        string='Accounting file',
        readonly=True,
        store=True,
        help=(
            'Accounting file this accounting document has been generated in '
            'relation to.'
        ),
    )

    flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Accounting flow',
        ondelete='cascade',
        required=True,
        help=(
            'Accounting flow this accounting document has been generated in '
            'relation to.'
        ),
    )

    # Inherit by delegation from "account.move".
    account_move_id = fields.Many2one(
        comodel_name='account.move',
        string='Accounting entry',
        ondelete='cascade',
        delegate=True,
        readonly=True,
        required=True,
    )

    @api.multi
    def unlink(self):
        """Override to delete the accounting entry when deleting this object.
        """

        accounting_entries = self.mapped('account_move_id')
        ret = super(AccountingDocument, self).unlink()
        accounting_entries.unlink()
        return ret
