import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_files(self):
        """Create accounting files and store them for further use.
        """

        self.memory.accounting_files = self.createAndTest(
            'account.file',
            [
                {
                    'name': genUuid(),
                },
            ],
        )

    def test_0100_create_accounting_flows(self):
        """Create accounting flows using previously created accounting files
        and store them for further use.
        """

        self.memory.accounting_flows = self.createAndTest(
            'account.file.flow',
            [
                {
                    'account_file_id': accounting_file.id,
                    'name': genUuid(),
                }
                for accounting_file in self.memory.accounting_files
            ],
        )

    def test_0200_accounting_file_and_flow_workflows(self):
        """Tests around accounting file and flow workflows.
        """

        aflow = self.memory.accounting_flows[0]
        afile = aflow.account_file_id

        self.assertEqual(aflow.state, 'draft')
        self.assertEqual(afile.state, 'draft')

        afile.signal_workflow('awaiting_reversal')

        self.assertEqual(aflow.state, 'awaiting_reversal')
        self.assertEqual(afile.state, 'awaiting_reversal')

        aflow.signal_workflow('reversed')

        self.assertEqual(aflow.state, 'reversed')
        self.assertEqual(afile.state, 'reversed')
