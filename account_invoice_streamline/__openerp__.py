# -*- coding: utf-8 -*-
##############################################################################
#
#    Invoice improvements, for Odoo
#    Copyright (C) 2013, 2016 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": 'Invoice improvements',
    "version": "8.0.2.1",
    "author": "XCG Consulting",
    "category": 'Accounting',
    "description": '''
Invoice improvements
====================

Improve Odoo accounting invoices.

- Handle analytics.

- Live udpate due dates based on invoice dates & payment terms.

- Invoice reversal.

- Remove default invoice reports.

- Add the tax refresh button into the header.

- Invoice reference unicity control on demand.
In the setting parameters, configure
'account_invoice_streamline.invoice_reference_unicity' to 0, to deactivate it,
and to 1, to activate it.

''',
    'website': 'http://odoo.consulting/',
    'init_xml': [],
    "depends": [
        'base',
        'account',
        'account_streamline',
        'analytic_structure',
        'account_move_reversal',
    ],
    "data": [
        'data/ir.config_parameter.xml',
        'data/invoice_reports.xml',
        'views/account_invoice.xml',
        'workflow/account_invoice.xml',
    ],
    'test': [],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
