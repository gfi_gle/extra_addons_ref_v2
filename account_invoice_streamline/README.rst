Invoice improvements
====================

Improve Odoo accounting invoices.

- Handle analytics.

- Live udpate due dates based on invoice dates & payment terms.

- Invoice reversal.

- Remove default invoice reports.

- Add the tax refresh button into the header.
