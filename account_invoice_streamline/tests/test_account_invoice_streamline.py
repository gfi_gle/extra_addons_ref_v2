import openerp.tests
import time

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_account(self):
        """Create accounting account for use in further tests.
        """

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': time.strftime('%Y-01-01'),
            'date_stop': time.strftime('%Y-12-31'),
            'period': 'month',
        }).execute()

        parent_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )
        self.assertTrue(parent_account)

        self.memory.account, = self.createAndTest(
            'account.account',
            [{
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': parent_account.id,
                'reconcile': True,
                'type': parent_account.type,
                'user_type': parent_account.user_type.id,
            }],
        )

    def test_0001_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partner, = self.createAndTest(
            'res.partner',
            [{
                'name': genUuid(),
            }],
        )

    def test_0002_create_products(self):
        """Create products for use in further tests.
        """

        def createProduct(values):
            """Create a product template then a product variant."""
            ptemplate, = self.createAndTest('product.template', [values])
            product, = self.createAndTest(
                'product.product', [{'product_tmpl_id': ptemplate.id}],
            )
            return product

        self.memory.product = createProduct({
            'name': genUuid(),
            'property_account_expense': self.memory.account.id,
            'property_account_income': self.memory.account.id,
            'type': 'service',
        })

    def test_0100_validate_client_invoice(self):
        """Validate a regular client invoice.
        """

        invoice = self._make_invoice('out_invoice')

        # Validate the invoice.
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

    def test_0101_validate_supplier_invoice(self):
        """Validate a regular supplier invoice.
        """

        invoice = self._make_invoice('in_invoice')

        # Validate the invoice.
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

    def _make_invoice(self, invoice_type):
        """Create an accounting invoice and return it.

        :rtype: Odoo "account.invoice" record set.
        """

        # Create the invoice first and add lines afterwards.

        invoice, = self.createAndTest(
            'account.invoice',
            [{
                'account_id': self.memory.account.id,
                'partner_id': self.memory.partner.id,
                'type': invoice_type,
            }],
        )

        self.createAndTest(
            'account.invoice.line',
            [
                {
                    'invoice_id': invoice.id,
                    'name': genUuid(),
                    'price_unit': 42.0,
                    'product_id': self.memory.product.id,
                },
            ],
        )

        return invoice
