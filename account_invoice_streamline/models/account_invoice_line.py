from openerp import api
from openerp import models

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


class InvoiceLine(models.Model):
    """Add analytics into invoice lines.
    """

    __metaclass__ = MetaAnalytic

    _inherit = 'account.invoice.line'

    _analytic = 'account_invoice_line'

    def move_line_get_item(self, cr, uid, line, context=None):
        """Override this function to include analytic fields in generated
        move-line entries.
        """
        res = super(InvoiceLine, self).move_line_get_item(
            cr, uid, line, context=context,
        )
        res.update(self.pool['analytic.structure'].extract_values(
            cr, uid, line, 'account_move_line', context=context,
        ))
        return res

    @api.multi
    def product_id_change(
        self, product, uom_id, qty=0, name='', type='out_invoice',
        partner_id=False, fposition_id=False, price_unit=False,
        currency_id=False, company_id=None,
    ):
        """Override to:
        - Propagate product analytics.
        """

        ret = super(InvoiceLine, self).product_id_change(
            product, uom_id, qty=qty, name=name, type=type,
            partner_id=partner_id, fposition_id=fposition_id,
            price_unit=price_unit, currency_id=currency_id,
            company_id=company_id,
        )

        # We need a product. This is not an actual modern change handler but
        # only half-way there (@api.multi); in particular, the "product"
        # argument is actually an ID.
        product_id = product
        if not product_id:
            return ret
        product = self.env['product.product'].browse(product_id)
        if not product:
            return ret

        if 'value' not in ret:
            ret['value'] = {}

        # Let the "extract_values" method provide analytic field updates.
        ret['value'].update(self.env['analytic.structure'].extract_values(
            product.product_tmpl_id, 'product_template',
            dest_model='account_invoice_line',
        ))

        return ret
