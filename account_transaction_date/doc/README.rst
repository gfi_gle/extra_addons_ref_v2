Accounting Transaction Date
===========================

* Category: Accounting

* Quick summary:

Renames the accounting document date as being the accounting date, which must correspond to the validation date of the accounting document.
Adds a transaction date to accounting documents. It plays the former role of the accounting date. It is the invoicing date of accounting document
generators, as invoices or trackers. If it is not filled at accounting document validation, it is the accounting date.

* Version:
8.0.1.0

* Dependencies:
  account_streamline
  purchase_tracker

* Repo owner or admin: XCG Consulting
