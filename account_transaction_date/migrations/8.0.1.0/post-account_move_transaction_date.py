import logging


log = logging.getLogger(__name__)


def migrate(cr, installed_version):
    """At installation of the module, sets the transaction date of pre-existing
    accounting documents, as being the accounting date.
    """

    log.info('Start.')

    cr.execute("""
    UPDATE account_move
    SET transaction_date = date
    WHERE transaction_date is null
    """)

    log.info('Done.')
