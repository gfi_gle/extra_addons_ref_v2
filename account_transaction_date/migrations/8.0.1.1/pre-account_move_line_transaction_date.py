import logging


log = logging.getLogger(__name__)


def migrate(cr, installed_version):
    """At update of the module, drops the transaction date column of accounting
    document lines, to force the update of this field in these objects.
    """

    if not installed_version:
        return

    log.info('Start.')

    cr.execute("""
    ALTER TABLE account_move_line DROP COLUMN transaction_date;
    """)

    log.info('Done.')
