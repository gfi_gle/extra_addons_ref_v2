from openerp import api
from openerp import models


class AccountInvoice(models.Model):

    _inherit = 'account.invoice'

    @api.one
    def get_move_vals(self, vals):
        """Override of the Account Invoice Streamline helper, for the date of
        transaction..
        """

        vals.update({'transaction_date': self.date_invoice})

        return vals
