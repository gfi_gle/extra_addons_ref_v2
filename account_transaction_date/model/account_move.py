from datetime import date
from openerp import _
from openerp import api
from openerp import fields
from openerp import models


class AccountMove(models.Model):

    _inherit = 'account.move'

    transaction_date = fields.Date(
        string='Date of transaction',
        states={'posted': [('readonly', True)]},
        help=(
            'It is the invoicing date of accounting document generators, '
            'as invoices or trackers. If it is not filled at accounting '
            'document validation, it is the accounting date.'
        ),
    )

    date = fields.Date(
        string='Accounting date',
        readonly=True,
        help='The validation date of the accounting document.',
    )

    @api.multi
    def register_accounting_date(self):
        """TODO Document this.
        """

        # TODO Document what this does.

        today = date.today()

        posted_moved_ids = {}

        for move in self:

            company_id = move.company_id.id

            if company_id not in posted_moved_ids.keys():
                posted_moved_ids[company_id] = move
            else:
                posted_moved_ids[company_id] |= move

        if posted_moved_ids:

            for company_id, moves in posted_moved_ids.items():

                period = self.env['account.period'].search([
                    ('company_id', '=', company_id),
                    ('date_start', '<=', today),
                    ('date_stop', '>=', today),
                ])

                # TODO Rename these variables.
                moves1 = moves.filtered(lambda r: not r.transaction_date)
                moves2 = moves - moves1

                if moves1:
                    moves1.write({
                        'date': today,
                        'period_id': period.id,
                        'transaction_date': today,
                    })

                if moves2:
                    moves2.write({
                        'date': today,
                        'period_id': period.id,
                    })

        return True

    @api.multi
    def post(self):
        """Override the post method to register the accounting date, as being
        the date of the validation of the accounting document.
        """

        self.register_accounting_date()

        return super(AccountMove, self).post()

    @api.model
    def fields_get(
        self, allfields=None, write_access=True,
    ):
        """Override to translate the overridden fields.
        """

        RE_TRANSLATED_FIELDS = {
            'transaction_date': {
                'string': _('Date of transaction'),
                'help': _(
                    'It is the invoicing date of accounting document '
                    'generators, as invoices or trackers. If it is not filled '
                    'at accounting document validation, it is the accounting '
                    'date.'
                )
            },
            'date': {
                'string': _('Accounting date'),
                'help': _('The validation date of the accounting document.'),
            },
        }

        ret = \
            super(AccountMove, self.with_context(self.env.context)).fields_get(
                allfields=allfields, write_access=write_access,
            )

        for field_name, field_update in RE_TRANSLATED_FIELDS.iteritems():
            field_info = ret.get(field_name)
            if field_info:
                field_info.update(field_update)

        return ret
