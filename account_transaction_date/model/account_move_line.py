from openerp import fields
from openerp import models


class AccountMoveLine(models.Model):

    _inherit = 'account.move.line'

    transaction_date = fields.Date(
        related='move_id.transaction_date',
        string='Date of transaction',
        select=True,
        store=True,
        readonly=True,
    )
