# -*- coding: utf-8 -*-
##############################################################################
#
##############################################################################
{
    "name": "Accounting transaction date",
    "version": "8.0.1.1",
    "author": "XCG Consulting",
    "category": 'Accounting',
    "description": """
Accounting transaction date
===========================

Renames the accounting document date as being the accounting date, which must
correspond to the validation date of the accounting document.
Adds a transaction date to accounting documents. It plays the former role of
the accounting date. It is the invoicing date of accounting document
generators, as invoices or trackers. If it is not filled at accounting document
validation, it is the accounting date.


Installation
------------

- Install this addon as usual.
    """,
    'website': 'http://www.openerp-experts.com',
    'init_xml': [],
    "depends": [
        'account_invoice_streamline',
        'account_streamline',
        'purchase_tracker',
    ],
    "data": [
        'views/account_move.xml',
        'views/account_move_line.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
