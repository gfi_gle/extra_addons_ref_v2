from datetime import datetime, date, timedelta

import openerp.tests
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


# Additional fiscal years to create beyond the current one.
ADDITIONAL_YEARS = 0
ADDITIONAL_PREV_YEARS = 1

PRICE = 100
QUANTITY = 1


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_load_basic_accounting(self):
        """Load basic accounting stuff first.
        """

        current_year = date.today().year

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': '%s-01-01' % current_year,
            'date_stop': '%s-12-31' % current_year,
            'period': 'month',
        }).execute()

        # Prepare more fiscal years than the default ones, as some of our tests
        # span several fiscal years.
        for year_inc in xrange(-ADDITIONAL_PREV_YEARS, 1 + ADDITIONAL_YEARS):
            if year_inc == 0:
                continue
            year = current_year + year_inc
            self.env['account.fiscalyear'].create({
                'code': '%s' % year,
                'date_start': '%s-01-01' % year,
                'date_stop': '%s-12-31' % year,
                'name': '%s' % year,
            }).create_period()

    def test_0001_create_employee_and_partner(self):
        """Create employee and oartner for use in further tests.
        """

        self.memory.department = self.env.ref('hr.dep_management')

        self.memory.employee = self.env.user.employee_id
        self.memory.employee.department_id = self.memory.department.id

        self.memory.department.manager_id = \
            self.memory.employee.id

        self.memory.operational_department, = self.createAndTest(
            'hr.operational_department',
            [
                {
                    'employee_id': self.memory.employee.id,
                    'department_id': self.memory.department.id,
                }
            ],
        )

        self.memory.partner, = self.createAndTest(
            'res.partner',
            [
                {
                    'name': genUuid(),
                    'is_company': True,
                    'supplier': True,
                }
            ]
        )

    def test_0002_create_validation_groups(self):
        """Create validation groups for use in further tests.
        """

        self.memory.vgroups = self.createAndTest(
            'hr.vgroups',
            [{'name': genUuid()} for i in range(2)]
        )

        for vgroup in self.memory.vgroups:
            vgroup.write(
                {'members': [(4, self.env.uid)]}
            )

        for level in range(2):
            self.memory.employee.write({
                'validation_group_%d' % (level + 1): (
                    self.memory.vgroups[level].id
                ),
            })

    def test_0003_create_account_and_product(self):
        """Create accounting data and product for use in further tests.
        """

        # Account.

        top_normal_account = self.env['account.account'].search(
            [('type', '=', 'other')],
            limit=1,
        )

        top_receivable_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )

        (
            self.memory.normal_account,
            self.memory.receivable_account,
        ) = self.createAndTest(
            'account.account',
            [
                {
                    'code': genUuid(max_chars=4),
                    'name': genUuid(max_chars=8),
                    'parent_id': top_normal_account.id,
                    'reconcile': True,
                    'type': top_normal_account.type,
                    'user_type': top_normal_account.user_type.id,
                },
                {
                    'code': genUuid(max_chars=4),
                    'name': genUuid(max_chars=8),
                    'parent_id': top_receivable_account.id,
                    'reconcile': True,
                    'type': top_receivable_account.type,
                    'user_type': top_receivable_account.user_type.id,
                },
            ]
        )

        # Product.

        self.memory.product = self.createProduct({
            'name': genUuid(),
            'list_price': PRICE,
            'property_account_expense': self.memory.normal_account.id,
            'property_account_income': self.memory.normal_account.id,
            'type': 'service',
        })

        # Journals.

        journal = self.env['account.journal'].search(
            [('type', '=', 'purchase')], limit=1,
        )

        sales_journal = self.env['account.journal'].search(
            [('type', '=', 'sale')], limit=1,
        )

        self.memory.payment_journal = self.env['account.journal'].search(
            [('type', '=', 'bank')], limit=1,
        )

        self.memory.journal = sales_journal

        (
            self.env.ref('purchase_tracker.property_purchase_invoice_journal')
            .value_reference
        ) = 'account.journal,%d' % journal.id

    def test_0100_create_purchase_order_and_tracker(self):
        """Create a purchase order and a purchase tracker.
        """

        self.memory.purchase_order, = self._makePurchaseOrders()

        self.memory.transaction_date = self.shifted_date(-90)

        self.memory.purchase_trackers = self.createAndTest(
            'purchase_tracker.tracker',
            [
                {
                    'invoice_date': self.memory.transaction_date,
                    'partner_id': (
                        self.memory.purchase_order.partner_id.id
                    ),
                    'reception_date': self._today(),
                    'invoice_reference': genUuid(),
                    'invoice_type': 'in_invoice',
                    'state': 'draft',
                    'has_purchase_order': True,
                    'purchase_order_id': (
                        self.memory.purchase_order.id
                    ),
                    'invoice_amount_untaxed': PRICE,
                    'description': genUuid(),
                    'invoice_currency_id': 1,
                }
            ]
        )

        for tracker in self.memory.purchase_trackers:

            self.assertEqual(tracker.state, 'draft')
            self.assertEqual(tracker.purchase_order_id.status, 'open')

            tracker.signal_workflow('submit')
            self.assertNotEqual(tracker.state, 'draft')

    def test_0101_supplier_invoice_transaction_and_accounting_dates(self):
        """Approve a purchase tracker and validate the associated accounting
        document.
        Check the transaction and accounting dates.
        """

        for tracker in self.memory.purchase_trackers:

            # Validate the purchase tracker. Force the last step "by hand"; an
            # invoice will still have been generated (beyond "draft").
            tracker.state = 'approved'
            self.assertEqual(tracker.state, 'approved')

            invoice = tracker.invoice_id
            self.assertEqual(invoice.state, 'draft')
            invoice.signal_workflow('invoice_open')
            self.assertEqual(invoice.state, 'open')

            move = invoice.move_id
            self.assertEqual(move.state, 'posted')

            self.assertEqual(
                move.object_reference, tracker
            )

            self.assertEqual(
                move.transaction_date, self.memory.transaction_date
            )
            self.assertEqual(
                move.date, self._today()
            )

            # Ensure the date is within the period.
            self.assertGreaterEqual(
                move.date, move.period_id.date_start,
            )
            self.assertLessEqual(
                move.date, move.period_id.date_stop,
            )

    def test_0200_create_sales_order(self):
        """Create a sales_order and its invoice.
        """

        self.memory.sales_order, = self.createAndTest(
            'sale.order',
            [{
                'partner_id': self.memory.partner.id,
            }],
        )
        self._fillSalesOrderLines(self.memory.sales_order)

        self.assertEqual(self.memory.sales_order.state, 'draft')

        self.memory.sales_order.action_button_confirm()
        self.assertEqual(self.memory.sales_order.state, 'manual')

        self.memory.sales_order.manual_invoice()

        self.memory.invoice = self.memory.sales_order.invoice_ids[0]
        self.assertEqual(self.memory.invoice.state, 'draft')

    def test_0201_customer_invoice_transaction_and_accounting_dates(self):
        """Validate a customer invoice and validate the associated accounting
        document.
        Check the transaction and accounting dates.
        """

        invoice = self.memory.invoice
        invoice.date_invoice = self.memory.transaction_date

        # Validate the invoice.
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

        # Pay the invoice.
        invoice.pay_and_reconcile(
            PRICE, self.memory.receivable_account.id,
            self.env['account.period'].find().id,
            self.memory.payment_journal.id, None, None, None,
        )
        self.assertEqual(invoice.state, 'paid')

        move = invoice.move_id
        self.assertEqual(move.state, 'posted')

        self.assertEqual(
            move.object_reference, invoice
        )

        self.assertEqual(
            move.transaction_date, self.memory.transaction_date
        )
        self.assertEqual(
            move.date, self._today()
        )

        # Ensure the date is within the period.
        self.assertGreaterEqual(
            move.date, move.period_id.date_start,
        )
        self.assertLessEqual(
            move.date, move.period_id.date_stop,
        )

    def test_0300_create_and_validate_accounting_document(self):
        """Create and validate an accounting document.
        Check the transaction and accounting dates.
        """

        self.memory.move, = self.createAndTest(
            'account.move',
            [
                {
                    'journal_id': self.memory.journal.id,
                }
            ]
        )

        move = self.memory.move

        self.createAndTest(
            'account.move.line',
            [
                {
                    'account_id': self.memory.normal_account.id,
                    'credit': PRICE,
                    'date_maturity': self._today(),
                    'debit': 0.0,
                    'move_id': self.memory.move.id,
                    'name': genUuid(),
                },
                {
                    'account_id': self.memory.receivable_account.id,
                    'credit': 0.0,
                    'date_maturity': self._today(),
                    'debit': PRICE,
                    'move_id': self.memory.move.id,
                    'name': genUuid(),
                    'partner_id': self.memory.partner.id,
                },
            ]
        )

        self.assertEqual(move.state, 'draft')
        move.post()
        self.assertEqual(move.state, 'posted')

        self.assertEqual(
            move.transaction_date, move.date
        )
        self.assertEqual(
            move.date, self._today()
        )

    def shifted_date(self, days):
        return datetime.strftime(
            date.today() + timedelta(days=days),
            DEFAULT_SERVER_DATE_FORMAT
        )

    def _today(self):
        return datetime.strftime(date.today(), DEFAULT_SERVER_DATE_FORMAT)

    def _makePurchaseOrders(self):
        """Create a purchase order.

        :return: The purchase order.
        :rtype: Odoo "purchase" record set.
        """

        purchase_orders = self.createAndTest(
            'purchase.order',
            [
                {
                    'pricelist_id': 2,
                    'tracker_type': 'invoicing',
                    'employee_id': self.memory.employee.id,
                    'status': 'draft',
                    'description': genUuid(),
                    'partner_id': self.memory.partner.id,
                    'state': 'draft',
                    'operational_department_id': (
                        self.memory.operational_department.id
                    ),
                }
            ]
        )

        for purchase_order in purchase_orders:

            self._fillPurchaseOrderLines(purchase_order)

            self.assertEqual(purchase_order.department_id.id, 1)
            self.assertEqual(purchase_order.state, 'draft')

            purchase_order.signal_workflow('purchase_confirm')
            self.assertEqual(purchase_order.state, 'confirmed')

            purchase_order.action_purchase_approve()
            self.assertEqual(purchase_order.state, 'approved')

        return purchase_orders

    def _fillPurchaseOrderLines(self, purchases_order):
        """Add lines into the specified purchase order.
        """

        self.createAndTest(
            'purchase.order.line',
            [
                {
                    'name': genUuid(),
                    'order_id': purchases_order.id,
                    'price_unit': PRICE,
                    'product_qty': QUANTITY,
                    'date_planned': self._today(),
                    'product_id': (
                        self.memory.product.id
                    ),
                }
            ],
        )

    def createProduct(self, values):
        """Create a product template then a product variant."""
        ptemplate, = self.createAndTest('product.template', [values])
        product, = self.createAndTest(
            'product.product', [{'product_tmpl_id': ptemplate.id}],
        )
        return product

    def _fillSalesOrderLines(self, sales_order):
        """Add lines into the specified sales order.
        """

        self.createAndTest(
            'sale.order.line',
            [
                {
                    'name': genUuid(),
                    'order_id': sales_order.id,
                    'price_unit': PRICE,
                    'product_id': self.memory.product.id,
                },
            ],
        )
