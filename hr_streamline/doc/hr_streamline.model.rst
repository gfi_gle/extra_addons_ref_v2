openerp.addons.hr_streamline.model package
==========================================

Submodules
----------

openerp.addons.hr_streamline.model.hr_department module
-------------------------------------------------------

.. automodule:: openerp.addons.hr_streamline.model.hr_department
    :members:
    :undoc-members:
    :show-inheritance:

openerp.addons.hr_streamline.model.hr_employee module
-----------------------------------------------------

.. automodule:: openerp.addons.hr_streamline.model.hr_employee
    :members:
    :undoc-members:
    :show-inheritance:

openerp.addons.hr_streamline.model.hr_operational_department module
-------------------------------------------------------------------

.. automodule:: openerp.addons.hr_streamline.model.hr_operational_department
    :members:
    :undoc-members:
    :show-inheritance:

openerp.addons.hr_streamline.model.res_users module
---------------------------------------------------

.. automodule:: openerp.addons.hr_streamline.model.res_users
    :members:
    :undoc-members:
    :show-inheritance:

openerp.addons.hr_streamline.model.util module
----------------------------------------------

.. automodule:: openerp.addons.hr_streamline.model.util
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: openerp.addons.hr_streamline.model
    :members:
    :undoc-members:
    :show-inheritance:
