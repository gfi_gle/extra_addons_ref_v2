hr_streamline package
=====================

Subpackages
-----------

.. toctree::

    hr_streamline.model

Module contents
---------------

.. automodule:: openerp.addons.hr_streamline
    :members:
    :undoc-members:
    :show-inheritance:
