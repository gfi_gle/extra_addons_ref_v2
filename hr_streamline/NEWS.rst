=======
Changes
=======

.. _8.0.1.12:

8.0.1.12
--------

* Fixed security rule that was blocking the creation of some calendar, by not allowing the user to read the attachment of the mail of the sent invitation.

.. _8.0.1.11:

8.0.1.11
--------

* Merge with 8.0.1.7 branch

.. _1.6:

1.6
---

- [demo] added operational department

