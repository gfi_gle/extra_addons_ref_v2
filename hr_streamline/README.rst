Human Resources Streamline
--------------------------

Changes to the hr module.

.. todo:: document

- Disallow copies thanks to ``base_no_copy``.

Attachment
----------

Only allow hr officers to see the attachment linked to an employee.

