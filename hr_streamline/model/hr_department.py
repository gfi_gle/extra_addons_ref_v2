from openerp import models

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


class HrDepartment(models.Model):
    """- Add analytics into departments.
    - Disallow copies thanks to ``base_no_copy``.
    """

    __metaclass__ = MetaAnalytic

    _analytic = 'hr.department'

    _inherit = ['hr.department', 'base.no_copy_mixin']
    _name = 'hr.department'
