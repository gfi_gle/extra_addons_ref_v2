# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013-2014 XCG Consulting (www.xcg-consulting.fr)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv
from openerp.osv import fields
import openerp.addons.decimal_precision as dp
from openerp.addons.account.account import account_account
from openerp.tools import config

from sqlalchemy import create_engine, and_, case
from sqlalchemy.dialects import postgresql
from sqlalchemy.schema import MetaData, Table
from sqlalchemy.orm import aliased
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select
from sqlalchemy.sql.functions import sum


class account_account(osv.Model):
    _inherit = 'account.account'

    _columns = {

        'has_centralized_balance': fields.boolean(
            string=u"Centralized balance",
            help=(
                u'This option is used in trial balance computation. '
                u'It is to be set to true, on accounts for which all the '
                u'trial balance lines, relative to their child accounts, are '
                u'to be grouped at the level of the parent.'
            ),
            select=True,
        ),

    }


class account_compute_balance(osv.Model):
    _name = 'account.compute.balance'

    def balance(
        self, cr, company_id, fiscalyear_id,
        opening_period_id, start_period_ids, end_period_ids,
        include_draft_moves, centralized_groups='disabled',
        groups_account_ids=None, context=None,
    ):
        """TODO Document.

        :param centralized_groups: One of:
        - "disabled" (the default, for back-compat).
        - "enabled".
        - "only_grouped".
        :type centralized_groups: String.

        :param groups_account_ids: Parent accounts to filter on (optional).
        :type groups_account_ids: List.
        """

        return TrialBalanceGenerator(
            cr, company_id, fiscalyear_id,
            opening_period_id, start_period_ids, end_period_ids,
            include_draft_moves, centralized_groups=centralized_groups,
            groups_account_ids=groups_account_ids,
        )


class TrialBalanceGenerator(object):
    """TODO Document.
    """

    def __init__(
        self, cr, company_id, fiscalyear_id,
        opening_period_id, period_start_ids, period_end_ids,
        include_draft_moves, centralized_groups='disabled',
        groups_account_ids=None,
    ):
        """TODO Document.

        :param centralized_groups: One of:
        - "disabled" (the default, for back-compat).
        - "enabled".
        - "only_grouped".
        :type centralized_groups: String.

        :param groups_account_ids: Parent accounts to filter on (optional).
        :type groups_account_ids: List.
        """

        super(TrialBalanceGenerator, self).__init__()

        self.cr = cr
        self.fiscalyear_id = fiscalyear_id
        self.company_id = company_id
        self.period_start_ids = period_start_ids
        self.period_end_ids = period_end_ids
        self.opening_period_id = opening_period_id
        self.include_draft_moves = include_draft_moves
        self.tables = {}
        self.period_query = None
        self.session = self.__get_session_maker()()
        self.meta = MetaData(bind=self.session.bind)
        self.__init_model()
        self.__init_period_query()

        # Build the query. We may need a union of 2 queries depending on
        # settings.

        if centralized_groups == 'disabled':
            self.query = self.__make_query(centralized='all')

        elif centralized_groups == 'enabled':
            self.query = self.__make_query(centralized='only_grouped')
            self.query = self.query.union(
                self.__make_query(centralized='only_not_grouped'),
            )

        elif centralized_groups == 'only_grouped':
            self.query = self.__make_query(
                centralized='details',
                groups_account_ids=groups_account_ids,
            )

        else:
            raise ValueError('Invalid "centralized_groups" setting.')

    def __get_session_maker(self):
        engine = create_engine(
            "postgresql+psycopg2://%s:%s@%s:%d/%s" % (
                config.get('db_user'),
                config.get('db_password'),
                config.get('db_host'),
                config.get('db_port'),
                self.cr.dbname,
            )
        )
        return sessionmaker(bind=engine)

    def __init_model(self):
        self.tables.update({
            'move_line': Table(
                'account_move_line',
                self.meta,
                autoload=True,
            ),
            'move': Table(
                'account_move',
                self.meta,
                autoload=True,
            ),
            'account': Table(
                'account_account',
                self.meta,
                autoload=True,
            ),
            'period': Table(
                'account_period',
                self.meta,
                autoload=True,
            ),
        })

    def __init_period_query(self):
        self.period_query = select(
            [self.tables['period'].c.id]
        ).select_from(
            self.tables['period']
        ).where(
            and_(
                self.tables['period'].c.fiscalyear_id == self.fiscalyear_id,
                self.tables['period'].c.company_id == self.company_id,
            ),
        )

    def __make_query(self, centralized, groups_account_ids=None):
        """Produce a query based on the specified "centralized" setting.

        :param centralized: One of:
        - "details".
        - "only_grouped".
        - "only_not_grouped".
        - "all".
        :type centralized: String.

        :param groups_account_ids: Parent accounts to filter on (optional).
        :type groups_account_ids: List.
        """

        # Prepare an alias as we JOIN twice on the account table.
        account = self.tables['account']
        parent_account = aliased(self.tables['account'])

        # The fields we SELECT.
        selected_fields = []

        if centralized == 'only_grouped':
            selected_fields += [
                parent_account.c.code.label('Account Code'),
                parent_account.c.name.label('Account Name'),
            ]
        else:
            selected_fields += [
                account.c.code.label('Account Code'),
                account.c.name.label('Account Name'),
            ]

        selected_fields += [
            sum(
                case(
                    [(
                        self.tables['period'].c.id.in_(
                            [self.opening_period_id] +
                            self.period_start_ids
                        ),
                        self.tables['move_line'].c.debit -
                        self.tables['move_line'].c.credit
                    )],
                    else_=0.0,
                )
            ).label('Start Balance'),
            sum(
                case(
                    [(
                        self.tables['period'].c.id.in_(
                            # We keep the intersection between the two lists
                            set(self.period_end_ids) -
                            set(self.period_start_ids)
                        ),
                        self.tables['move_line'].c.debit
                    )],
                    else_=0.0,
                )
            ).label('Move Debit'),
            sum(
                case(
                    [(
                        self.tables['period'].c.id.in_(
                            # We keep the intersection between the two lists
                            set(self.period_end_ids) -
                            set(self.period_start_ids)
                        ),
                        self.tables['move_line'].c.credit
                    )],
                    else_=0.0,
                )
            ).label('Move Credit'),
            sum(
                case(
                    [(
                        self.tables['period'].c.id.in_(
                            self.period_end_ids + [self.opening_period_id]
                        ),
                        self.tables['move_line'].c.debit -
                        self.tables['move_line'].c.credit
                    )],
                    else_=0.0,
                )
            ).label('End Balance'),
        ]

        if centralized in ['all', 'details']:
            selected_fields += [
                parent_account.c.code.label('Parent Account Code'),
                parent_account.c.name.label('Parent Account Name'),
                parent_account.c.has_centralized_balance,
            ]

        # WHERE AND clauses.
        and_args = []

        if centralized == 'only_grouped':
            and_args.append(parent_account.c
                            .has_centralized_balance == True)  # noqa

        elif centralized == 'only_not_grouped':
            and_args.append(parent_account.c
                            .has_centralized_balance.isnot(True))

        elif centralized == 'details':
            and_args.append(parent_account.c
                            .has_centralized_balance == True)  # noqa

            # Optionally filter parent accounts.
            if groups_account_ids:
                and_args.append(parent_account.c.id.in_(groups_account_ids))

        and_args += [
            account.c.company_id == self.company_id,
            account.c.type != 'view',
            self.tables['move_line'].c.period_id.in_(self.period_query),
            self.tables['period'].c.id.in_(
                self.period_end_ids + [self.opening_period_id]
            ),
        ]

        # Check if we care about draft moves
        move_states = ['posted']
        if self.include_draft_moves:
            move_states += ['draft']
        and_args += [self.tables['move'].c.state.in_(move_states)]

        # GROUP BY arguments.
        if centralized == 'only_grouped':
            group_by_args = [parent_account.c.code, parent_account.c.name]
        elif centralized in ['all', 'details']:
            group_by_args = [
                account.c.code, account.c.name, parent_account.c.code,
                parent_account.c.name,
                parent_account.c.has_centralized_balance,
            ]
        else:
            group_by_args = [account.c.code, account.c.name]

        # All set! Build a query and return it.
        return select(selected_fields).select_from(
            self.tables['move_line'].outerjoin(
                account,
                account.c.id == self.tables['move_line'].c.account_id
            ).outerjoin(
                parent_account,
                parent_account.c.id == account.c.parent_id,
            ).outerjoin(
                self.tables['move'],
                self.tables['move'].c.id == self.tables['move_line'].c.move_id
            ).outerjoin(
                self.tables['period'],
                self.tables['period'].c.id ==
                self.tables['move_line'].c.period_id
            )
        ).where(and_(*and_args)).group_by(*group_by_args)

    def __call__(self):
        query_str = str(self.query.compile(
            dialect=postgresql.dialect(),
            compile_kwargs={'literal_binds': True}
        ))
        self.cr.execute(query_str)
        for result in self.cr.fetchall():
            yield result
