import codecs
import StringIO
from datetime import datetime
import base64

from openerp.osv import orm, fields
import openerp.addons.decimal_precision as dp
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp import api
from openerp import _

from openerp.addons.general_ledger.util.unicode_csv import UnicodeWriter


CENTRALIZED_GROUPS = [
    ('disabled', _('Disabled')),
    ('enabled', _('Enabled')),
    ('only_grouped', _('Subsidiary Trial Balance')),
]

BALANCE_OPTIONS = [
    ('standard', _('Standard')),
    ('open', _('Open')),
]


def decode_utf8(string):
    """str -> unicode for UTF-8 text."""
    if isinstance(string, str):
        return string.decode('utf-8')
    return string


class TrialBalance(orm.Model):
    _name = 'trial_balance'

    def _get_total(self, cr, uid, ids, name, arg, context=None):
        query = """
            SELECT
                balance.id,
                SUM(line.debit),
                SUM(line.credit),
                SUM(line.start_balance),
                SUM(line.end_balance),
                SUM(line.start_balance_debit),
                SUM(line.start_balance_credit),
                SUM(line.end_balance_debit),
                SUM(line.end_balance_credit)
            FROM
                trial_balance_line AS line
            LEFT JOIN
                trial_balance AS balance
            ON
                balance.id = line.trial_balance_id
            WHERE
                balance.id IN %s
            GROUP BY
                balance.id
        """
        cr.execute(query, (tuple(ids),))
        results = cr.fetchall()
        res = {
            result[0]: {
                'total_debit': result[1],
                'total_credit': result[2],
                'total_start_balance': result[3],
                'total_end_balance': result[4],
                'total_start_balance_debit': result[5],
                'total_start_balance_credit': result[6],
                'total_end_balance_debit': result[7],
                'total_end_balance_credit': result[8],
            }
            for result in results
        }
        return res

    _columns = {
        'create_date': fields.datetime(string=u"Create Date"),
        'company_id': fields.many2one(
            'res.company',
            string=u"Company",
        ),
        'fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            string=u"Fiscal Year",
        ),
        'period_from': fields.many2one(
            'account.period',
            string=u"Period From",
        ),
        'period_to': fields.many2one(
            'account.period',
            string=u"Period To",
        ),
        'line_ids': fields.one2many(
            'trial_balance.line',
            'trial_balance_id',
            string="Lines",
        ),
        'total_debit': fields.function(
            _get_total,
            string=u"Total Debit",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'total_credit': fields.function(
            _get_total,
            string=u"Total Credit",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'total_start_balance': fields.function(
            _get_total,
            string=u"Start Balance",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'total_start_balance_credit': fields.function(
            _get_total,
            string=u"Start Balance (credit)",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'total_start_balance_debit': fields.function(
            _get_total,
            string=u"Start Balance (debit)",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'total_end_balance': fields.function(
            _get_total,
            string=u"End Balance",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'total_end_balance_credit': fields.function(
            _get_total,
            string=u"End Balance (credit)",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'total_end_balance_debit': fields.function(
            _get_total,
            string=u"End Balance (debit)",
            digits_compute=dp.get_precision('Account'),
            multi='total',
            store=True,
        ),
        'include_draft_moves': fields.boolean(
            string=u"Include Draft Moves",
        ),
        'centralized_groups': fields.selection(
            CENTRALIZED_GROUPS,
            string=u"Group balance lines",
            help=u"""If this option is enabled, balance lines will be grouped
            at the level of accounts for which the centralized balance option
            is set. If the Subsidiary Trial  Balance option is set, only
            centralized lines will be visible.
            """,
        ),
        'balance_option': fields.selection(
            BALANCE_OPTIONS,
            string=u"Type of balance",
            help=u"""An open balance will exclude all end balance amounts,
            that are of null value. A standard balance will include all types
            of amounts.
            """,
        ),
        'description': fields.text(
            string=u"Parent accounts",
        ),
        'output_format': fields.selection(
            selection=[
                ('pdf', 'PDF'),
                ('csv', 'CSV'),
            ],
            string='Output format',
            help='The format of the file to generate.',
        ),
        'document_id': fields.many2one(
            'ir.attachment',
            string='The generated file',
        ),
        'file': fields.related(
            'document_id',
            'datas',
            type='binary',
            string='File',
            help='The generated file.',
            readonly=True,
        ),
        'filename': fields.related(
            'document_id',
            'name',
            type='char',
            string='Output',
            size=512,
            help='The generated file.',
            readonly=True,
        ),
    }

    @api.one
    def generate_csv(self, vals):

        now = datetime.strptime(
            self.create_date, DEFAULT_SERVER_DATETIME_FORMAT
        )

        # Balance header information.

        associations = [
            ('fiscalyear', 'account.fiscalyear', 'fiscalyear_id'),
            ('company', 'res.company', 'company_id'),
            ('period_from', 'account.period', 'period_from'),
            ('period_to', 'account.period', 'period_to'),
        ]

        tpl_lists = [
            ('centralized_groups', CENTRALIZED_GROUPS),
            ('balance_option', BALANCE_OPTIONS),
        ]

        names = {
            name: decode_utf8(
                self.env[model].browse(
                    vals[field]
                ).name
            )
            for name, model, field in associations
        }

        names.update({
            name: [
                tpl[1]
                for tpl in tpl_list
                if tpl[0] == vals[name]
            ][0]
            for name, tpl_list in tpl_lists
        })

        # Suffix information.
        suffix = [
            names['period_from'],
            names['period_to'],
            decode_utf8(
                _('True') if vals['include_draft_moves'] else _('False')
            ),
            decode_utf8(_(names['centralized_groups'])),
            decode_utf8(_(names['balance_option'])),
            names['company'],
            names['fiscalyear'],
            self.create_date,
        ]

        # First row (labels).

        csv_data = [[
            _('Account Code'), _('Account Name'),
            _('Start Balance (debit)'), _('Start Balance (credit)'),
            _('Start Balance'), _('Debit'), _('Credit'),
            _('End Balance (debit)'), _('End Balance (credit)'),
            _('End Balance'),
            _('Period From'), _('Period To'),
            _('Include Draft Moves'), _('Group balance lines'),
            _('Type of balance'),
            _('Company'), _('Fiscal year'), _('Create Date'),
        ]]

        # Body of the balance.
        csv_data += [
            [
                decode_utf8(line[2]['account_code']),
                decode_utf8(line[2]['account_name']),
                decode_utf8(line[2]['start_balance_debit']),
                decode_utf8(line[2]['start_balance_credit']),
                decode_utf8(line[2]['start_balance']),
                decode_utf8(line[2]['debit']),
                decode_utf8(line[2]['credit']),
                decode_utf8(line[2]['end_balance_debit']),
                decode_utf8(line[2]['end_balance_credit']),
                decode_utf8(line[2]['end_balance']),
            ] + suffix
            for line in vals['line_ids']
        ]

        # Footer.

        dictionary = self._get_total('', '')[self.id]

        csv_data += [
            [
                '',
                '',
                decode_utf8(dictionary['total_start_balance_debit']),
                decode_utf8(dictionary['total_start_balance_credit']),
                decode_utf8(dictionary['total_start_balance']),
                decode_utf8(dictionary['total_debit']),
                decode_utf8(dictionary['total_credit']),
                decode_utf8(dictionary['total_end_balance_debit']),
                decode_utf8(dictionary['total_end_balance_credit']),
                decode_utf8(dictionary['total_end_balance']),
            ] + suffix
        ]

        # Prepare the CSV data.
        csv_stream = StringIO.StringIO()
        csv_stream.write(codecs.BOM_UTF8)  # Make Excel happy...
        csv_writer = UnicodeWriter(
            csv_stream, delimiter=';', quotechar='"',
        )
        csv_writer.writerows(csv_data)
        csv_data = csv_stream.getvalue()
        csv_stream.close()

        # Create an attachment.
        filename = _('TrialBalance-%s') % now.date().isoformat() + '.csv'
        document = self.sudo().env['ir.attachment'].\
            with_context(self.env.context).create({
                'datas_fname': filename,
                'datas': base64.b64encode(csv_data),
                'name': filename,
                'res_id': self.id,
                'res_model': self._name,
            }
        )

        self.document_id = document.id

        return True

    @api.one
    def generate_pdf(self):

        now = datetime.strptime(
            self.create_date, DEFAULT_SERVER_DATETIME_FORMAT
        )

        action_report = self.env.ref('trial_balance.trial_balance_py3o')

        datas = action_report.render_report(
            [self.id], 'trial.balance.py3o', {'report_type': u'py3o'}
        )[0]

        filename = _('TrialBalance-%s') % now.date().isoformat() + '.pdf'

        document = self.sudo().env['ir.attachment'].\
            with_context(self.env.context).create({
                'datas_fname': filename,
                'datas': base64.b64encode(datas),
                'name': filename,
                'res_id': self.id,
                'res_model': self._name,
            }
        )

        self.document_id = document.id

        return True


class TrialBalanceLine(orm.Model):
    _name = 'trial_balance.line'

    _order = 'account_code'

    _columns = {
        'trial_balance_id': fields.many2one(
            'trial_balance',
            string=u"Trial Balance",
        ),
        'account_name': fields.char(string=u"Account Name"),
        'account_code': fields.char(string=u"Account Code"),
        'reference': fields.char(string=u"Reference"),
        'debit': fields.float(
            string=u"Debit",
            digits_compute=dp.get_precision('Account'),
        ),
        'credit': fields.float(
            string=u"Credit",
            digits_compute=dp.get_precision('Account'),
        ),
        'start_balance': fields.float(
            string=u"Start Balance",
            digits_compute=dp.get_precision('Account'),
        ),
        'end_balance': fields.float(
            string=u"End Balance",
            digits_compute=dp.get_precision('Account'),
        ),
        'start_balance_credit': fields.float(
            string=u"Start Balance (credit)",
            digits_compute=dp.get_precision('Account'),
        ),
        'start_balance_debit': fields.float(
            string=u"Start Balance (debit)",
            digits_compute=dp.get_precision('Account'),
        ),
        'end_balance_credit': fields.float(
            string=u"End Balance (credit)",
            digits_compute=dp.get_precision('Account'),
        ),
        'end_balance_debit': fields.float(
            string=u"End Balance (debit)",
            digits_compute=dp.get_precision('Account'),
        ),
    }
