# -*- coding: utf-8 -*-
##############################################################################
#
#    Trial Balance, for OpenERP
#    Copyright (C) 2015 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import orm, fields
from openerp.tools.translate import _

CENTRALIZED_GROUPS = [
        ('disabled', 'Disabled'),
        ('enabled', 'Enabled'),
        ('only_grouped', 'Subsidiary Trial Balance'),
]

BALANCE_OPTIONS = [
        ('standard', 'Standard'),
        ('open', 'Open')
]


class TrialBalanceWizard(orm.TransientModel):
    _name = 'wizard.trial_balance'

    def onchange_centralized_groups(
        self, cr, uid, ids, centralized_groups, context=None
    ):

        res = False

        if centralized_groups in ['only_grouped']:
            user = self.pool['res.users'].browse(
                cr, uid, uid, context=context
            )
            company = user.company_id
            res = {
                'domain': {
                    'groups_account_ids': [
                        ('company_id', '=', company.id),
                        ('has_centralized_balance', '=', True),
                    ]
                }
            }
        return res

    _columns = {
        'fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            string=u"Fiscal Year",
        ),
        'period_from': fields.many2one(
            'account.period',
            string=u"Period From",
        ),
        'period_to': fields.many2one(
            'account.period',
            string=u"Period To",
        ),
        'include_draft_moves': fields.boolean(
            string=u"Include Draft Moves",
        ),
        'centralized_groups': fields.selection(
            CENTRALIZED_GROUPS,
            string=u"Group balance lines",
            help=u"""If this option is enabled, balance lines will be grouped
            at the level of accounts for which the centralized balance option
            is set. If the Subsidiary Trial  Balance option is set, only
            centralized lines will be visible.
            """,
        ),
        'balance_option': fields.selection(
            BALANCE_OPTIONS,
            string=u"Type of balance",
            help=u"""An open balance will exclude all end balance amounts,
            that are of null value. A standard balance will include all types
            of amounts.
            """,
        ),
        'groups_account_ids': fields.many2many(
            'account.account',
            string=u"Grouped accounts",
        ),
        'output_format': fields.selection(
            selection=[
                ('pdf', 'PDF'),
                ('csv', 'CSV'),
            ],
            string='Output format',
            help='The format of the file to generate.',
            required=True,
        ),
    }

    _defaults = {
        'centralized_groups': 'enabled',
    }

    def _get_period_ids(self, cr, uid, data, context):
        period_osv = self.pool['account.period']
        edge_periods = period_osv.read(
            cr, uid, [
                data.period_from.id,
                data.period_to.id
            ], ['date_start'], context=context
        )
        start_period_ids = self.pool['account.period'].search(
            cr, uid, [
                ('date_start', '<', edge_periods[0]['date_start']),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
                ('special', '=', False),
            ], context=context
        )
        end_period_ids = self.pool['account.period'].search(
            cr, uid, [
                ('date_start', '<=', edge_periods[1]['date_start']),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
                ('special', '=', False),
            ], context=context
        )
        opening_period_id = period_osv.search(
            cr, uid, [
                ('special', '=', True),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
            ], limit=1, context=context
        )[0]
        return opening_period_id, start_period_ids, end_period_ids

    def generate(self, cr, uid, ids, context=None):
        if context is None:
            context = {}

        # Get user filters
        data = self.browse(cr, uid, ids, context=context)[0]

        company_id = self.pool['res.users'].read(
            cr, uid, uid, ['company_id'], context=context
        )['company_id'][0]
        fiscalyear_id = data.fiscalyear_id.id
        opening_period_id, start_period_ids, end_period_ids = \
            self._get_period_ids(
                cr, uid, data, context
            )

        if data.centralized_groups == 'enabled':
            option = 'disabled'
        else:
            option = data.centralized_groups

        balance_generator = self.pool['account.compute.balance'].balance(
            cr, company_id, fiscalyear_id,
            opening_period_id, start_period_ids, end_period_ids,
            data.include_draft_moves, option,
            [account.id for account in data.groups_account_ids]
        )

        description = ""

        if data.groups_account_ids:
            for account in data.groups_account_ids:
                description += account.code + " " + account.name + ", "

            description = description[0:len(description) - 2]

        preliminary_results = [
            {
                'account_code': r[0],
                'account_name': r[1],
                'start_balance': r[2],
                'debit': r[3],
                'credit': r[4],
                'end_balance': r[5],
                'parent_account_code': r[6],
                'parent_account_name': r[7],
                'centralize': r[8],
            }
            for r in balance_generator()
            if data.balance_option != 'open' or abs(r[5]) != 0
        ]

        final_results = {}
        start = False

        for r in preliminary_results:
            if not r['centralize'] or data.centralized_groups != 'enabled':
                final_results[r['account_code']] = r
                dictionary = final_results[r['account_code']]

                for label in [
                    'start_balance_credit', 'start_balance_debit',
                    'end_balance_credit', 'end_balance_debit'
                ]:
                    dictionary[label] = 0
            else:
                if r['parent_account_code'] not in final_results.keys():
                    final_results[r['parent_account_code']] = r

                    for label in [
                        'start_balance_credit', 'start_balance_debit',
                        'end_balance_credit', 'end_balance_debit'
                    ]:
                        final_results[r['parent_account_code']][label] = 0
                    start = True

                dictionary = final_results[r['parent_account_code']]

                if not start:
                    for label in [
                        'start_balance', 'debit', 'credit', 'end_balance'
                    ]:
                        dictionary[label] += r[label]

                start = False

                dictionary['account_code'] = r['parent_account_code']
                dictionary['account_name'] = r['parent_account_name']

            if r['start_balance'] <= 0:
                dictionary['start_balance_credit'] += -r['start_balance']
                dictionary['start_balance_debit'] += 0
            else:
                dictionary['start_balance_credit'] += 0
                dictionary['start_balance_debit'] += r['start_balance']

            if r['end_balance'] <= 0:
                dictionary['end_balance_credit'] += -r['end_balance']
                dictionary['end_balance_debit'] += 0
            else:
                dictionary['end_balance_credit'] += 0
                dictionary['end_balance_debit'] += r['end_balance']

        order_list = final_results.keys()
        order_list.sort()

        final_lines = []

        for code in order_list:
            dictionary = dict(
                (key, final_results[code][key])
                for key in final_results[code].keys()
            )
            dictionary.update({

            })
            final_lines.append((0, 0, dictionary))

        vals = {
            'company_id': company_id,
            'fiscalyear_id': fiscalyear_id,
            'period_from': data.period_from.id,
            'period_to': data.period_to.id,
            'include_draft_moves': data.include_draft_moves,
            'output_format': data.output_format,
            'centralized_groups': data.centralized_groups,
            'balance_option': data.balance_option,
            'description': description,
            'line_ids': final_lines,
        }

        # Create our trial_balance object
        res_id = self.pool['trial_balance'].create(
            cr, uid, vals, context=context
        )

        if data.output_format == 'csv':
            self.pool['trial_balance'].generate_csv(
                cr, uid, res_id, vals, context=context
            )
        elif data.output_format == 'pdf':
            self.pool['trial_balance'].generate_pdf(
                cr, uid, res_id, context=context
            )

        # Return an action pointing the newly created object
        return {
            'type': 'ir.actions.act_window',
            'name': _('Trial Balance'),
            'res_model': 'trial_balance',
            'res_id': res_id,
            'view_mode': 'form',
            'view_type': 'form',
        }
