.. include:: manifest

Contents:

.. toctree::
   :maxdepth: 2

   README
   CHANGES
   models
   tests
   TODO


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

