.. _README:

Partner unique ID management
============================

Link partners with partner information records uniquely identifying them in an
external partner repository.


Installation
------------

- Install this addon as usual.

- Odoo system parameters: Configure the
  ``partner_unique_id.partner_repo_env_name`` setting to match the technical
  name of an environment created in the partner repository.


Dependencies
------------

- account_iban (account_streamline, base_iban_streamline)

- account_invoice_streamline

- analytic_structure

- partner_firstname

- sale_streamline (product_streamline)

- xbus_emitter
