##############################################################################
#
#    Partner unique ID management for Odoo
#    Copyright (C) 2015 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Partner unique ID management',
    'description': '''
Partner unique ID management
============================

Link partners with partner information records uniquely identifying them in an
external partner repository.


Installation
------------

- Install this addon as usual.

- Odoo system parameters: Configure the
  ``partner_unique_id.partner_repo_env_name`` setting to match the technical
  name of an environment created in the partner repository.


Dependencies
------------

- account_iban (account_streamline, base_iban_streamline)

- account_invoice_streamline

- analytic_structure

- partner_firstname

- sale_streamline (product_streamline)

- xbus_emitter
''',
    'version': '8.0.2.0',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'base',
        'account_iban',
        'account_invoice_streamline',
        'analytic_structure',
        'partner_firstname',
        'sale_streamline',
        'xbus_emitter',
    ],

    'data': [
        'security/ir.model.access.csv',

        'data/ir.config_parameter.xml',

        'menu.xml',

        'views/partner.xml',
        'views/partner_info.xml',
        'views/partner_info_entity_type.xml',
    ],

    'installable': True,
}
