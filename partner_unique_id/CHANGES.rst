.. _CHANGES:

=========
Changelog
=========


8.0.2.0 - UNRELEASED
====================

* Remove change watchers.

* Assume an always-up-to-date partner information record list.

* Partners get linked to existing partner information records live (no sync).

* Sync is now on-demand for partners that are not in the external repository.

* Local vs social addresses.

* Bring in partner view changes from accounting addons that used to be in the
  "partner_unique_id_accounting" addon.

* Send a bank account document to the partner repository and receive bank
  accounts from it.

* Sync a "comments" multi-line text field.

* Sales orders / invoices: Warnings based on the unique ID status of partners.

* Sync analytics.

* Identifiers may be generated out of sequences.

* Partner official names are kept as is when already set once.


8.0.1.1 - 2017-09-28
====================

* Partner information entity type char ranges: Add "Free text" in addition to
  the existing "Alpha-numerical" & "Numerical".


8.0.1.0 - 2016-06-06
====================

* Initial version. Create partner information records and sync them with an
  external repository.
