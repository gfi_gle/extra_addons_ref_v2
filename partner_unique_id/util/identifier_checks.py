# -*- coding: utf-8 -*-

"""Identifier checks.
"""

import vatnumber  # Odoo req.


def _check_luhn(string):
    """Luhn test to check control keys.

    Taken from the "l10n_fr_siret" OCA module (res.partner:: _check_luhn).

    Credits:
        http://rosettacode.org/wiki/Luhn_test_of_credit_card_numbers#Python
    """

    r = [int(ch) for ch in string][::-1]
    return (
        sum(r[0::2]) +
        sum(
            sum(divmod(d * 2, 10))
            for d in r[1::2])
        ) % 10 == 0


def check_siret(siret):
    """Check the specified SIRET number.

    Taken from the "l10n_fr_siret" OCA module (res.partner:: _check_siret).
    """

    # NIC : numéro interne de classement, cinq derniers chiffres du SIRET.
    # SIREN : neuf premiers chiffres du SIRET.

    nic = siret[9:]
    siren = siret[:9]

    # Check the NIC type and length
    if not nic.decode('utf-8').isdecimal() or len(nic) != 5:
        return False

    # Check the SIREN type, length and key
    if not siren.decode('utf-8').isdecimal() or len(siren) != 9:
        return False

    if not _check_luhn(siren):
        return False

    # Check the NIC key (you need both SIREN and NIC to check it)
    if not _check_luhn(siret):
        return False

    return True


def check_ssn(fullnir):
    """Implementation of the modulo 97 algo to validate NIR keys.

    This is a specific implementation that copes with French NIR to
    manage 2A and 2B departments.

    Given a full nir this function will return True or False.
    True means your NIR is considered valid.
    False means your NIR is considered invalid.
    """

    # make sure the given NIR is 13 char long

    if not len(fullnir) == 15:
        return False

    # remove the key from the full NIR
    nir, key = fullnir[:13], fullnir[13:]

    try:
        intkey = int(key)
    except ValueError:
        return False

    # first of all replace 2A and 2B by their respective values
    nir = nir.replace('2A', '19')
    nir = nir.replace('2a', '19')
    nir = nir.replace('2B', '18')
    nir = nir.replace('2b', '18')
    try:
        intnir = int(nir)
    except ValueError:
        return False

    # calculate the modulo 97 key
    verifk = 97 - (intnir % 97)

    # compare it to the one given
    return verifk == intkey


def check_eu_vat(vat):
    """Check the specified VAT number using  the "vatnumber" library (it's an
    Odoo req).
    """

    return vatnumber.check_vat(vat)
