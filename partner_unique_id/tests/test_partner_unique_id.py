import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


# The prefix of the sequence "pietype_on_seq" will be linked to.
SEQ_PREFIX = genUuid(max_chars=4)


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_add_xbus_emitter(self):
        """Add a fake Xbus emitter so messages can be set up without failure.
        """

        self.createAndTest('xbus.emitter', [{
            'front_url': genUuid(), 'login': genUuid(), 'password': genUuid(),
        }])

    def test_0100_create_partner_info_entity_types(self):
        """Create partner information entity types with various rules.
        """

        # Create a sequence "pietype_on_seq" will be linked to.
        seq, = self.createAndTest(
            'ir.sequence',
            [{'name': genUuid(), 'prefix': SEQ_PREFIX}],
        )

        (
            self.memory.pietype_alnum_5,
            self.memory.pietype_digit_5,
            self.memory.pietype_free_text_5,
            self.memory.pietype_siret,
            self.memory.pietype_ssn,
            self.memory.pietype_eu_vat,
            self.memory.pietype_on_seq,
        ) = self.createAndTest(
            'res.partner.info.entity_type',
            [
                {
                    'identifier_char_range': 'alnum',
                    'identifier_label': 'TESTLABEL',  # Checked
                    'identifier_max_chars': 5,
                    'identifier_min_chars': 5,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                },
                {
                    'identifier_char_range': 'digit',
                    'identifier_label': genUuid(),
                    'identifier_max_chars': 5,
                    'identifier_min_chars': 5,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                    'identifier_type': False,
                },
                {
                    'identifier_char_range': 'free_text',
                    'identifier_label': genUuid(),
                    'identifier_max_chars': 5,
                    'identifier_min_chars': 5,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                },
                {
                    'identifier_char_range': 'digit',
                    'identifier_label': genUuid(),
                    'identifier_max_chars': 14,
                    'identifier_min_chars': 14,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                    'identifier_type': 'siret',
                },
                {
                    'identifier_char_range': 'digit',
                    'identifier_label': genUuid(),
                    'identifier_max_chars': 15,
                    'identifier_min_chars': 15,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                    'identifier_type': 'ssn',
                },
                {
                    'identifier_char_range': 'alnum',
                    'identifier_label': genUuid(),
                    'identifier_max_chars': 13,
                    'identifier_min_chars': 13,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                    'identifier_type': 'eu_vat',
                },
                {
                    'identifier_char_range': 'alnum',
                    'identifier_label': genUuid(),
                    'identifier_max_chars': 8,
                    'identifier_min_chars': 4,
                    'name': genUuid(),
                    'technical_name': genUuid(),
                    'identifier_sequence_id': seq.id,
                },
            ],
        )

    def test_0110_piet_match_identifier(self):
        """Tests around the "match_identifier" method of partner information
        entity types.
        """

        # alnum
        pietype = self.memory.pietype_alnum_5
        self.assertTrue(pietype.match_identifier('abcde'))
        self.assertTrue(pietype.match_identifier('12345'))
        self.assertFalse(pietype.match_identifier('ab/ d'))

        # digit
        pietype = self.memory.pietype_digit_5
        self.assertFalse(pietype.match_identifier('abcde'))
        self.assertTrue(pietype.match_identifier('12345'))
        self.assertFalse(pietype.match_identifier('ab/ d'))

        # free_text
        pietype = self.memory.pietype_free_text_5
        self.assertTrue(pietype.match_identifier('abcde'))
        self.assertTrue(pietype.match_identifier('12345'))
        self.assertTrue(pietype.match_identifier('ab/ d'))

        # character length
        self.assertFalse(self.memory.pietype_alnum_5.match_identifier('abcd'))

        # Company registration number (SIRET).
        pietype = self.memory.pietype_siret
        self.assertFalse(pietype.match_identifier('40483304550022'))
        self.assertTrue(pietype.match_identifier('40483304800022'))

        # Social security number.
        pietype = self.memory.pietype_ssn
        self.assertFalse(pietype.match_identifier('178092201204513'))
        self.assertTrue(pietype.match_identifier('255081416802538'))

        # EU VAT.
        pietype = self.memory.pietype_eu_vat
        self.assertFalse(pietype.match_identifier('FR25404833045'))
        self.assertTrue(pietype.match_identifier('FR83404833048'))

    def test_0200_create_partner_info_records(self):
        """Create partner information records by calling the "process_update"
        method (as that is the only way to create them).
        """

        self.env['res.partner.info'].process_update({
            'code': genUuid(max_chars=16),
            'entity_identifier': '00000',
            'entity_type': self.memory.pietype_alnum_5.technical_name,
            'official_name': '0-official-name',
            'street1': '0-street1',
            'bank_accounts': [
                {
                    'iban': 'DE44 5001 0517 5407 3249 31',
                    'iban_owner': '0-iban-owner',
                    'bank_code': 'AAAAZZ11',
                    'bank_name': '0-bank',
                },
                {
                    'iban': 'GR16 0110 1250 0000 0001 2300 695',
                    'iban_owner': '1-iban-owner',
                    'bank_code': 'AAAAZZ11',
                    'bank_name': '0-bank',
                },
            ]
        })
        self.env['res.partner.info'].process_update({
            'code': genUuid(max_chars=16),
            'entity_identifier': '11111',
            'entity_type': self.memory.pietype_alnum_5.technical_name,
            'official_name': '1-official-name',
            'street1': '1-street1',
        })

        # Store for further use.
        self.memory.pirecords = [
            self.env['res.partner.info'].search(
                [('entity_identifier', '=', entity_identifier)],
            )
            for entity_identifier in ('00000', '11111')
        ]
        self.assertEqual(len(self.memory.pirecords), 2)

        # Check creations.
        self.assertEqual(self.memory.pirecords[0].official_name,
                         '0-official-name')
        self.assertEqual(self.memory.pirecords[0].street1, '0-street1')
        self.assertEqual(self.memory.pirecords[1].official_name,
                         '1-official-name')
        self.assertEqual(self.memory.pirecords[1].street1, '1-street1')

    def test_0300_partner_validation(self):
        """Ensure a partner gets linked to a partner information record when
        setting its entity identifier to an existing one.
        """

        partner = self._makePartner()

        # Link a record via its identifier.
        partner.write({
            'partner_info_entity_identifier': '00000',
            'partner_info_entity_type_id': self.memory.pietype_alnum_5.id,
        })

        # Ensure a unique ID record has been linked and social information has
        # been propagated.
        self.assertEqual(partner.partner_info_id.id,
                         self.memory.pirecords[0].id)
        self.assertEqual(partner.partner_info_identifier_label, 'TESTLABEL')
        self.assertEqual(partner.partner_info_status, 'validated')
        self.assertEqual(partner.partner_info_official_name, '0-official-name')
        self.assertEqual(partner.partner_info_street1, '0-street1')
        self.assertEqual(partner.street, '0-street1')

        # Check bank accounts.
        bank_accounts = partner.bank_ids
        self.assertEqual(len(bank_accounts), 2)
        self.assertEqual(len(bank_accounts.mapped('bank')), 1)  # Same bank.
        self.assertEqual(set(bank_accounts.mapped('bank_bic')), {'AAAAZZ11'})
        self.assertEqual(set(bank_accounts.mapped('bank_name')), {'0-bank'})
        self.assertEqual(set(bank_accounts.mapped('iban')), {
            'DE44 5001 0517 5407 3249 31', 'GR16 0110 1250 0000 0001 2300 695',
        })
        self.assertEqual(set(bank_accounts.mapped('owner_name')),
                         {'0-iban-owner', '1-iban-owner'})
        self.assertEqual(set(bank_accounts.mapped('street')), {'0-street1'})

    def test_0310_partner_activation_1_step(self):
        """Fill all activation fields at once and ensure a partner gets
        activated.
        """

        partner = self._makePartner()
        partner.write({
            'street': '12345-street',
            'city': '12345-city',
            'country_id': self.env['res.country'].search([], limit=1).id,
        })

        # The partner should have been activated. No unique ID record though.
        self.assertFalse(partner.partner_info_id)
        self.assertEqual(partner.partner_info_status, 'activated')

        # Empty a field and ensure the partner is back to inactive.
        partner.street = False
        self.assertFalse(partner.partner_info_id)
        self.assertFalse(partner.partner_info_status)

    def test_0311_partner_activation_2_steps(self):
        """Fill partner activation fields in 2 steps and ensure the partner
        gets activated.
        """

        partner = self._makePartner()
        partner.write({
            'street': '12345-street',
        })
        self.assertFalse(partner.partner_info_status)
        partner.write({
            'city': '12345-city',
            'country_id': self.env['res.country'].search([], limit=1).id,
        })
        self.assertFalse(partner.partner_info_id)
        self.assertEqual(partner.partner_info_status, 'activated')

    def test_0312_partner_submission(self):
        """Fill fields required to submit the partner and check its submission
        process.
        """

        # Fill fields required to submit the partner.
        partner = self._makePartner()
        partner.write({
            'partner_info_entity_identifier': '12345',  # unknown ID
            'partner_info_entity_type_id': self.memory.pietype_alnum_5.id,
            'partner_info_official_name': '12345-official-name',
            'street': '12345-street',
            'city': '12345-city',
            'country_id': self.env['res.country'].search([], limit=1).id,
        })

        # The partner can now be submitted. No unique ID record though.
        self.assertEqual(partner.partner_info_status, 'activated')
        partner.submit_to_group()
        self.assertEqual(partner.partner_info_status, 'submitted')
        self.assertFalse(partner.partner_info_id)

        # Check the unique ID request (that would be sent when the partner is
        # submitted for validation to the external repository).
        unique_id_req = partner.unique_id_req_xbus_dict()
        self.assertEqual(unique_id_req[u'entity_identifier'], u'12345')
        self.assertEqual(unique_id_req[u'entity_type'],
                         self.memory.pietype_alnum_5.technical_name)
        self.assertEqual(unique_id_req[u'env'], u'NOT CONFIGURED')  # default
        self.assertEqual(unique_id_req[u'official_name'],
                         u'12345-official-name')
        self.assertEqual(unique_id_req[u'local_name'], partner.name)
        self.assertEqual(unique_id_req[u'street1'], u'12345-street')
        self.assertEqual(unique_id_req[u'street2'], u'')

        # Empty a field and ensure the unique ID status does not change.
        partner.street = False
        self.assertFalse(partner.partner_info_id)
        self.assertEqual(partner.partner_info_status, 'submitted')

    def test_0313_partner_submission_incomplete(self):
        """Ensure the check behind incomplete submission fields works as
        expected.
        """

        # No unique ID field.
        partner = self._makePartner()
        partner.write({
            'street': '12345-street',
            'city': '12345-city',
            'country_id': self.env['res.country'].search([], limit=1).id,
        })
        with self.assertRaises(openerp.exceptions.Warning):
            partner.submit_to_group()

        # Fill some but no all unique ID fields.
        partner = self._makePartner()
        partner.write({
            'partner_info_entity_identifier': '12345',  # unknown ID
            'partner_info_entity_type_id': self.memory.pietype_alnum_5.id,
            'street': '12345-street',
            'city': '12345-city',
            'country_id': self.env['res.country'].search([], limit=1).id,
        })
        with self.assertRaises(openerp.exceptions.Warning):
            partner.submit_to_group()

        # Control: Regular submission.
        partner = self._makePartner()
        partner.write({
            'partner_info_entity_identifier': '12345',  # unknown ID
            'partner_info_entity_type_id': self.memory.pietype_alnum_5.id,
            'partner_info_official_name': '12345-official-name',
            'street': '12345-street',
            'city': '12345-city',
            'country_id': self.env['res.country'].search([], limit=1).id,
        })
        partner.submit_to_group()
        self.assertEqual(partner.partner_info_status, 'submitted')

    def test_0320_partner_on_pietype_with_sequence(self):
        """Check the sequence linked to one of our test pietypes.
        """

        partner = self._makePartner()
        partner.partner_info_entity_type_id = self.memory.pietype_on_seq
        self.assertTrue(partner.partner_info_entity_identifier
                        .startswith(SEQ_PREFIX))

    def test_0400_partner_info_process_update_no_partner(self):
        """Call the "process_update" method on a partner information record
        that is not linked to any partner.
        """

        pirecord = self.memory.pirecords[1]
        self.assertFalse(pirecord.partner_ids)

        # Initial state.
        self.assertEqual(pirecord.official_name, '1-official-name')
        self.assertEqual(pirecord.street1, '1-street1')

        self.env['res.partner.info'].process_update({
            'code': genUuid(max_chars=16),
            'entity_identifier': pirecord.entity_identifier,
            'entity_type': pirecord.entity_type_id.technical_name,
            'official_name': '1-official-name-new',
            'street1': '1-street1-new',
        })

        # New state.
        self.assertEqual(pirecord.official_name, '1-official-name-new')
        self.assertEqual(pirecord.street1, '1-street1-new')

    def test_0401_partner_info_process_update_with_partners(self):
        """Call the "process_update" method on a partner information record
        that is linked to partners.
        """

        pirecord = self.memory.pirecords[0]
        partners = pirecord.partner_ids
        self.assertTrue(partners)

        # Initial state.
        self.assertEqual(pirecord.official_name, '0-official-name')
        self.assertEqual(pirecord.street1, '0-street1')
        for partner in partners:
            self.assertNotEqual(partner.partner_info_official_name,
                                '0-official-name-new')
            self.assertNotEqual(partner.partner_info_street1, '0-street1-new')
            self.assertNotEqual(partner.street, '0-street1-new')

        self.env['res.partner.info'].process_update({
            'code': genUuid(max_chars=16),
            'entity_identifier': pirecord.entity_identifier,
            'entity_type': pirecord.entity_type_id.technical_name,
            'official_name': '0-official-name-new',
            'street1': '0-street1-new',
        })

        # New state. The initial official name of the partner is kept.
        self.assertEqual(pirecord.official_name, '0-official-name-new')
        self.assertEqual(pirecord.street1, '0-street1-new')
        for partner in partners:
            self.assertNotEqual(partner.partner_info_official_name,
                                '0-official-name-new')
            self.assertEqual(partner.partner_info_street1, '0-street1-new')
            self.assertNotEqual(partner.street, '0-street1-new')  # No change.

    def _makePartner(self):
        """Build a partner and check defaults.

        :return: The new partner.
        :rtype: Odoo "res.partner" record set.
        """

        partner, = self.createAndTest('res.partner', [{'name': genUuid()}])

        # Check defaults: No unique ID record, no social information.
        self.assertFalse(partner.partner_info_id)
        self.assertFalse(partner.partner_info_status)
        self.assertFalse(partner.partner_info_official_name)
        self.assertFalse(partner.partner_info_street1)
        self.assertFalse(partner.street)

        return partner
