import logging


log = logging.getLogger(__name__)


def migrate(cr, installed_version):
    """Bank accounts stored in unique ID records have been moved to a separate
    model; migrate there then delete the previous columns.
    """

    log.info('Start.')

    # Create missing banks. Ignore bank names as we didn't have any before.
    cr.execute('''
    INSERT INTO res_bank (
        create_uid, create_date, write_uid, write_date,
        bic, name, active
    )
    SELECT
        1, now(), 1, now(),
        bank_code, bank_code, TRUE
    FROM res_partner_info
    LEFT JOIN res_bank ON bic = bank_code
    WHERE bank_code IS NOT NULL AND bic IS NULL
    GROUP BY bank_code;
    ''')

    cr.execute('''
    INSERT INTO res_partner_info_bank_account (
        partner_info_id,
        create_uid, create_date, write_uid, write_date,
        iban, iban_owner, bank_id, bank_code, bank_name,
        iban_creation_date, iban_creator, iban_update_date, iban_updater
    )
    SELECT
        pinfo.id,
        pinfo.create_uid, pinfo.create_date, pinfo.write_uid, pinfo.write_date,
        iban, iban_owner, bank.id, bank.bic, bank.name,
        iban_creation_date, creatorp.name, iban_update_date, updaterp.name
    FROM res_partner_info AS pinfo
    LEFT JOIN res_bank AS bank ON bank.bic = pinfo.bank_code
    LEFT JOIN res_users AS creatoru ON creatoru.id = pinfo.iban_creator_id
    LEFT JOIN res_partner AS creatorp ON creatorp.id = creatoru.partner_id
    LEFT JOIN res_users AS updateru ON updateru.id = pinfo.iban_updater_id
    LEFT JOIN res_partner AS updaterp ON updaterp.id = updateru.partner_id
    WHERE iban IS NOT NULL;
    ''')

    for col_to_delete in (
        'iban', 'iban_owner', 'bank_code', 'bank_name', 'iban_creation_date',
        'iban_creator_id', 'iban_update_date', 'iban_updater_id',
    ):
        cr.execute('''
        ALTER TABLE res_partner_info
        DROP COLUMN %s;
        ''' % col_to_delete)

    log.info('Done.')
