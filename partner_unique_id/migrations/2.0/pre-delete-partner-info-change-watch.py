import logging


log = logging.getLogger(__name__)


def migrate(cr, installed_version):
    """No longer watch for changes of the "res.partner.info" model to sync them
    with the external partner repository.
    Syncs are now on-demand.
    """

    if not installed_version:
        return

    log.info('Start.')

    cr.execute('''
    SELECT cwmi.id, cwmi.workflow_id
    FROM change_watcher_model_info AS cwmi
    JOIN ir_model AS model ON model.id = cwmi.model_id
    WHERE model.model = 'res.partner.info'
    ''')

    cwmis = cr.fetchall()  # [(id, workflow_id)]
    if not cwmis:
        log.info('No change watch to delete.')

    cwmi_ids = [cwmi[0] for cwmi in cwmis if cwmi[0]]
    log.info('%d change watches to delete: %s', len(cwmi_ids), cwmi_ids)

    wkf_ids = [cwmi[1] for cwmi in cwmis if cwmi[1]]
    log.info('%d workflows to delete: %s', len(wkf_ids), wkf_ids)

    if wkf_ids:
        cr.execute('''
        DELETE FROM wkf WHERE id IN (%s);
        ''' % ', '.join(str(wkf_id) for wkf_id in wkf_ids))

    if cwmi_ids:
        cr.execute('''
        DELETE FROM change_watcher_model_info WHERE id IN (%s);
        ''' % ', '.join(str(cwmi_id) for cwmi_id in cwmi_ids))

    log.info('Done.')
