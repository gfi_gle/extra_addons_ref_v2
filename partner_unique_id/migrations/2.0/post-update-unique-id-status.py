import logging


log = logging.getLogger(__name__)


def migrate(cr, installed_version):
    """Fill the unique ID status of partners that already had unique ID
    information, then delete the previous column.
    """

    log.info('Start.')

    cr.execute('''
    UPDATE res_partner
    SET partner_info_status = 'validated'
    WHERE partner_info_state = 'generated'
    ''')

    cr.execute('''
    ALTER TABLE res_partner
    DROP COLUMN partner_info_state;
    ''')

    log.info('Done.')
