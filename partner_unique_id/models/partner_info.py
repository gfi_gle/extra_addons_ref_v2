import logging
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


log = logging.getLogger(__name__)


class PartnerInfo(models.Model):
    """Information uniquely identifying a partner in an external partner
    repository.

    Fields are only read-only view-side (not model-side) to allow imports.
    """

    __metaclass__ = MetaAnalytic

    _name = 'res.partner.info'
    _description = 'Partner unique information'

    _analytic = True

    _order = 'code ASC'

    _rec_name = 'label'

    code = fields.Char(
        string='Code',
        size=16,
        help='Unique code of the partner in the external partner repository.',
        required=True,
    )

    comments = fields.Text(
        string='Comments',
        help=(
            'Comments about the unique ID. Synchronized with the external '
            'partner repository.'
        ),
    )

    entity_identifier = fields.Char(
        string='Entity identifier',
        size=16,
        help=(
            'The entity identifier is influenced by the entity type. The '
            '(type, identifier) couple is unique in the external partner '
            'repository.'
        ),
        required=True,
    )

    entity_type_id = fields.Many2one(
        comodel_name='res.partner.info.entity_type',
        string='Entity type',
        ondelete='restrict',
        help=(
            'The entity type defines the meaning of the entity identifier. '
            'The (type, identifier) couple is unique in the external partner '
            'repository.'
        ),
        required=True,
    )

    @api.depends('code', 'entity_type_id.label_prefix')
    @api.one
    def _get_label(self):
        """- Handle empty codes.
        - Concatenate the label prefix and the code.
        """

        label = self.code

        # Apply the prefix when there is one.
        label_prefix = self.entity_type_id.label_prefix
        if label_prefix:
            label = label_prefix + label

        self.label = label

    label = fields.Char(
        compute=_get_label,
        string='Label',
        help=(
            'Label based on the unique code of the partner in the external '
            'partner repository, and a prefix set on the entity type.'
        ),
        store=True,
    )

    official_name = fields.Char(
        string='Official name',
        size=256,
        help=(
            'Official name of this entity; synchronized from the external '
            'partner repository. No rules are attached to this field.'
        ),
        required=True,
    )

    partner_ids = fields.One2many(
        comodel_name='res.partner',
        inverse_name='partner_info_id',
        string='Partners',
        ondelete='cascade',
        help='The Odoo partners this information is for.',
    )

    # ===
    # Address.
    # ===

    # The following fields are similar to those in partners; they are defined
    # here so this object can stand on its own without requiring a partner
    # link.

    __address_kwargs = {
        'help': 'The address stored in the external partner repository.',
    }

    street1 = fields.Char(string='Street 1', **__address_kwargs)
    street2 = fields.Char(string='Street 2', **__address_kwargs)
    street3 = fields.Char(string='Street 3', **__address_kwargs)
    zip_code = fields.Char(string='Zip code', **__address_kwargs)
    city = fields.Char(string='City', **__address_kwargs)
    country_id = fields.Many2one(
        comodel_name='res.country', string='Country', **__address_kwargs
    )
    phone = fields.Char(string='Phone', **__address_kwargs)
    fax = fields.Char(string='Fax', **__address_kwargs)
    email = fields.Char(string='Email', **__address_kwargs)

    # ===
    # Bank accounts.
    # ===

    bank_account_ids = fields.One2many(
        comodel_name='res.partner.info.bank_account',
        inverse_name='partner_info_id',
        string='Bank accounts',
        help='Bank information stored in the external partner repository.',
    )

    # ===

    @api.model
    def process_update(self, data):
        """Process a partner information update sent by an external partner
        repository.

        :param data: Dict with the following attributes:

        - code: String (compulsory).
        - entity_identifier: String (compulsory).
        - entity_type: String (compulsory).
        - official_name: String (compulsory).

        - analytics: Dict (facultative).
        - comments: String (facultative).

        - street1: String (facultative).
        - street2: String (facultative).
        - street3: String (facultative).
        - zip_code: String (facultative).
        - city: String (facultative).
        - country_code: String (facultative).
        - phone: String (facultative).
        - fax: String (facultative).
        - email: String (facultative).

        - bank_accounts: List of dicts with the following attributes:
            - iban: String (compulsory).
            - iban_owner: String (compulsory).
            - bank_code: String (compulsory).
            - bank_name: String (facultative).
            - iban_creation_date: String (facultative).
            - iban_creator: String (facultative).
            - iban_update_date: String (facultative).
            - iban_updater: String (facultative).
        """

        LOG_PREFIX = 'Partner info > process_update: '

        # Ensure the data is valid according to the above comment.

        def invalid_data(reason):
            log.error(LOG_PREFIX + 'Invalid data: %s' % reason)
            raise exceptions.Warning(LOG_PREFIX + 'Invalid data: %s' % reason)

        if not isinstance(data, dict):
            invalid_data('Expected a dict.')

        code = data.get('code')
        entity_identifier = data.get('entity_identifier')
        entity_type_tech_name = data.get('entity_type')
        official_name = data.get('official_name')

        if (
            not code or not entity_identifier or not entity_type_tech_name or
            not official_name
        ):
            invalid_data('Missing compulsory parameters.')

        LOG_PREFIX += '%s, %s (official name: %s): ' % (
            entity_identifier, entity_type_tech_name, official_name,
        )
        log.info(LOG_PREFIX + 'Start.')

        # Find an entity type based on the specified technical name.
        entity_type = self.env['res.partner.info.entity_type'].search(
            [('technical_name', '=', entity_type_tech_name)], limit=1,
        )
        if not entity_type:
            invalid_data('Invalid entity type technical name.')

        # Find a country using the country code.
        country_code = data.get('country_code')
        country = self.env['res.country'].search(
            [('code', '=', country_code)], limit=1,
        ) if country_code else self.env['res.country']

        # Prepare bank accounts.
        bank_accounts = []

        for bank_data in data.get('bank_accounts', []):

            # Cache some data.
            iban = bank_data.get('iban')
            iban_owner = bank_data.get('iban_owner')
            bank_code = bank_data.get('bank_code')
            bank_name = bank_data.get('bank_name')

            if not iban or not iban_owner or not bank_code:
                log.warning('Bank account missing compulsory parameters.')
                continue

            # Find or create a bank based on its code.
            bank = self.env['res.bank'].search(
                [('bic', '=', bank_code)], limit=1,
            ) or self.env['res.bank'].create({
                'bic': bank_code,
                'name': bank_name or bank_code,
            })

            bank_accounts.append({
                'iban': iban,
                'iban_owner': iban_owner,
                'bank_id': bank.id,
                'bank_code': bank_code,
                'bank_name': bank_name or False,
                'iban_creation_date': (
                    bank_data.get('iban_creation_date') or False
                ),
                'iban_creator': bank_data.get('iban_creator') or False,
                'iban_update_date': (
                    bank_data.get('iban_update_date') or False
                ),
                'iban_updater': bank_data.get('iban_updater') or False,
            })

        partner_info_values = {
            'code': code,
            'entity_identifier': entity_identifier,
            'entity_type_id': entity_type.id,
            'official_name': official_name,

            'comments': data.get('comments') or False,

            # Address.
            'street1': data.get('street1') or False,
            'street2': data.get('street2') or False,
            'street3': data.get('street3') or False,
            'zip_code': data.get('zip_code') or False,
            'city': data.get('city') or False,
            'country_id': country.id,
            'phone': data.get('phone') or False,
            'fax': data.get('fax') or False,
            'email': data.get('email') or False,

            # Bank accounts.
            'bank_account_ids': [
                (0, 0, bank_account)  # 0: Create.
                for bank_account in bank_accounts
            ],
        }

        # Include analytics.
        astructs = {  # afield -> adim_id
            'a%s_id' % astruct.ordering: astruct.nd_id.id
            for astruct in (
                self.env['analytic.structure']
                .get_structures('res_partner_info')
            )
        }
        for afield, acode_name in data.get('analytics', {}).iteritems():
            adim_id = astructs.get(afield)
            if not adim_id:
                continue  # Ignore dimensions we don't have.
            # Either find or create an analytic code.
            acode = self.env['analytic.code'].search([
                ('name', '=', acode_name),
                ('nd_id', '=', adim_id),
            ], limit=1) or self.env['analytic.code'].create({
                'name': acode_name,
                'nd_id': adim_id,
            }) if acode_name else self.env['analytic.code']
            partner_info_values[afield] = acode.id

        # Either update an existing unique ID record or create a new one.
        partner_info = self.search([
            ('entity_identifier', '=', entity_identifier),
            ('entity_type_id', '=', entity_type.id),
        ], limit=1)
        if partner_info:
            partner_info.bank_account_ids = []  # Clean them up beforehand.
            partner_info.write(partner_info_values)
        else:
            partner_info = self.create(partner_info_values)

        # Hook to let other addons further process the message.
        partner_info.process_update_hook(LOG_PREFIX, data)

        log.info(LOG_PREFIX + 'Created / Updated the unique ID record %d; '
                 'updating partners...', partner_info.id)

        # Update local partners. They have no unique ID link yet so we have to
        # find them.
        self.env['res.partner'].search([
            ('partner_info_entity_identifier', '=', entity_identifier),
            ('partner_info_entity_type_id', '=', entity_type.id),
        ]).link_to_unique_id_record(partner_info)

        log.info(LOG_PREFIX + 'Done.')
        return True

    @api.multi
    def process_update_hook(self, log_prefix, data):
        """Hook to let other addons further process a partner information
        update sent by an external partner repository.

        :type log_prefix: String.

        :param data: Dict validated by the "process_update" method.
        """

        pass  # Empty on purpose.
