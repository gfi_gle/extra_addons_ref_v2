from openerp import _
from openerp import models


class Invoice(models.Model):
    """Warning message when the partner has not been validated.
    """

    _inherit = 'account.invoice'

    def onchange_partner_id(
        self, cr, uid, ids, type, partner_id, date_invoice=False,
        payment_term=False, partner_bank_id=False, company_id=False,
        context=None,
    ):
        """Override this (Odoo 7 style) change handler to add a warning message
        when the partner has not been validated.
        """

        ret = super(Invoice, self).onchange_partner_id(
            cr, uid, ids, type, partner_id, date_invoice=date_invoice,
            payment_term=payment_term, partner_bank_id=partner_bank_id,
            company_id=company_id, context=context,
        )

        if partner_id:
            pinfostatus = self.pool['res.partner'].browse(
                cr, uid, [partner_id], context=context,
            )[0].partner_info_status
            if not pinfostatus or pinfostatus != 'validated':
                # Clear out the partner.
                ret_value = ret.get('value', {})
                ret_value['partner_id'] = False

                # Append to the warning's title & message when there already
                # was one.
                warning = ret.get('warning')
                if warning:
                    warning_title = (warning.get('title') or u'') + u' & '
                    warning_message = (warning.get('message') or u'') + u'\n\n'
                else:
                    warning_title = u''
                    warning_message = u''
                warning_title += _('Inactive partner')
                warning_message += _(
                    'Partner unique ID status: The partner has not been '
                    'validated yet.'
                )

                ret.update({
                    'warning': {'title': warning_title,
                                'message': warning_message},
                    'value': ret_value,
                })

        return ret
