from openerp import fields
from openerp import models

from openerp.addons.partner_unique_id.util import identifier_checks


class PartnerInfoEntityType(models.Model):
    """Helper to define the meaning of the entity identifier of a partner
    information record; and to add configurable rules around it.
    """

    _name = 'res.partner.info.entity_type'
    _description = 'Partner unique info entity type'

    identifier_char_range = fields.Selection(
        selection=[
            # These are used with Python built-in string "isX()" methods.
            ('alnum', 'Alpha-numerical'),
            ('digit', 'Numerical'),

            ('free_text', 'Free text'),
        ],
        string='Identifier character range',
        help='Character range the entity identifier has to obey.',
        required=True,
    )

    identifier_label = fields.Char(
        string='Identifier label',
        help='Label to show the entity identifier as.',
        required=True,
    )

    identifier_max_chars = fields.Integer(
        string='Identifier max characters',
        help=(
            'Maximum amount of characters to input into the entity '
            'identifier.'
        ),
    )

    identifier_min_chars = fields.Integer(
        string='Identifier min characters',
        help=(
            'Minimum amount of characters to input into the entity '
            'identifier.'
        ),
    )

    identifier_sequence_id = fields.Many2one(
        comodel_name='ir.sequence',
        string='Identifier sequence',
        ondelete='restrict',
        help=(
            'When an identifier sequence is selected here, no identifier will '
            'have to be input by hand; it will be generated based on a '
            'sequence.'
        ),
    )

    identifier_type = fields.Selection(
        selection=[
            # Match functions exposed by the "util.identifer_checks" module.
            ('siret', 'SIRET'),
            ('ssn', 'Social security number'),
            ('eu_vat', 'EU VAT'),
        ],
        string='Identifier type',
        help=(
            'The type of the entity identifier; optional. May lead to '
            'additional checks of the identifier.'
        ),
    )

    label_prefix = fields.Char(
        string='Label prefix',
        help='Text prepended to the displayed label of codes.',
    )

    name = fields.Char(
        string='Name',
        required=True,
        help='Friendly name of this entity type.',
    )

    technical_name = fields.Char(
        string='Technical name',
        required=True,
        help='Technical name of this entity type; used when synchronizing.',
    )

    def match_identifier(self, identifier):
        """Find out whether the specified identifier matches rules set by this
        entity type.

        :rtype: bool.
        """

        self.ensure_one()

        # Check the char count.
        if not (
            self.identifier_min_chars <=
            len(identifier) <=
            self.identifier_max_chars
        ):
            return False

        # Check the char range.
        if (
            self.identifier_char_range != 'free_text' and
            not getattr(identifier, 'is%s' % self.identifier_char_range)()
        ):
            return False

        # Additional identifier checks, when specified.
        identifier_type = self.identifier_type
        if identifier_type:
            if not getattr(
                identifier_checks, 'check_%s' % identifier_type,
            )(identifier):
                return False

        # All good!
        return True
