import base64
from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models


# Name of the Xbus event type used to send unique ID requests.
PARTNER_INFO_REQ_EVENT_TYPE = u'partner_info_request'


class Partner(models.Model):
    """Link partners with partner information records.
    """

    _inherit = 'res.partner'

    @api.one
    def _get_partner_info_activation_message(self):
        """Build a message listing fields required for the partner to reach the
        "Activated" unique ID status.
        """

        incomplete_fields = self.get_incomplete_activation_fields()

        if not incomplete_fields:
            self.partner_info_activation_message = False
            return

        # Get field labels. Odoo 7 call to "fields_get" to avoid errors...
        field_labels = {
            field: field_info['string']
            for field, field_info in self.pool[self._name].fields_get(
                self.env.cr, self.env.user.id, incomplete_fields,
                context=self.env.context,
            ).iteritems()
        }

        self.partner_info_activation_message = _(
            'Fields required for this partner to reach the "Activated" '
            'status: %s.'
        ) % u', '.join(field_labels[field] for field in incomplete_fields)

    partner_info_activation_message = fields.Char(
        compute=_get_partner_info_activation_message,
        string='Activation message',
        help=(
            'Message listing fields required for the partner to reach the '
            '"Activated" unique ID status.'
        ),
    )

    partner_info_bank_account_doc = fields.Binary(
        string='Bank account document to send',
        help=(
            'Bank account document to send to the external partner repository '
            'when registering a new one.'
        ),
    )

    partner_info_bank_account_doc_filename = fields.Char(
        string='Bank account document to send - Filename',
        size=256,
    )

    partner_info_comments = fields.Text(
        string='Unique ID comments',
        help=(
            'Comments about the unique ID. Synchronized with the external '
            'partner repository.'
        ),
    )

    partner_info_id = fields.Many2one(
        comodel_name='res.partner.info',
        string='Unique identification',
        ondelete='set null',
        help=(
            'Information uniquely identifying this partner in an external '
            'partner repository.'
        ),
        track_visibility='onchange',
    )

    partner_info_entity_identifier = fields.Char(
        string='Entity identifier',
        size=16,
        help=(
            'The entity identifier is influenced by the entity type. The '
            '(type, identifier) couple is unique in the external partner '
            'repository.'
        ),
        track_visibility='onchange',
    )

    partner_info_entity_type_id = fields.Many2one(
        comodel_name='res.partner.info.entity_type',
        string='Entity type',
        ondelete='restrict',
        help=(
            'The entity type defines the meaning of the entity identifier. '
            'The (type, identifier) couple is unique in the external partner '
            'repository.'
        ),
        track_visibility='onchange',
    )

    @api.depends('partner_info_entity_type_id')
    @api.one
    def _get_partner_info_identifier_label(self):
        pietype = self.partner_info_entity_type_id
        self.partner_info_identifier_label = (
            pietype.identifier_label if pietype
            else _('Entity identifier')
        )

    # Field used to display a dynamic label next to the
    # "partner_info_entity_identifier" field.
    partner_info_identifier_label = fields.Char(
        compute=_get_partner_info_identifier_label,
        string='Identifier label',
        help=(
            'The entity identifier is influenced by the entity type. The '
            '(type, identifier) couple is unique in the external partner '
            'repository.'
        ),
        readonly=True,
    )

    partner_info_official_name = fields.Char(
        string='Official name',
        size=256,
        help=(
            'Official name of this entity; synchronized with the external '
            'partner repository. No rules are attached to this field.'
        ),
        track_visibility='onchange',
    )

    @api.depends('partner_info_entity_type_id')
    @api.one
    def _get_partner_info_on_sequence(self):
        self.partner_info_on_sequence = bool(
            self.partner_info_entity_type_id.identifier_sequence_id
        )

    partner_info_on_sequence = fields.Boolean(
        compute=_get_partner_info_on_sequence,
        string='Unique ID on sequence (Utility)',
    )

    partner_info_status = fields.Selection(
        selection=[
            ('activated', 'Activated'),
            ('submitted', 'Submitted to the group'),
            ('validated', 'Validated'),
        ],
        string='Unique ID status',
        help=(
            'Status of this partner with regards to its unique ID. The '
            '"validated" status may only be reached when an external partner '
            'repository says so.'
        ),
        readonly=True,
        track_visibility='onchange',
    )

    # ===
    # Fields to show the address stored in the external repository into the
    # partner form directly.
    # These fields are not auto-computed; they will instead be updated when the
    # partner information record is.
    # ===

    __address_kwargs = {
        'help': 'The address stored in the external partner repository.',
    }

    partner_info_street1 = fields.Char(string='Street 1', **__address_kwargs)
    partner_info_street2 = fields.Char(string='Street 2', **__address_kwargs)
    partner_info_street3 = fields.Char(string='Street 3', **__address_kwargs)
    partner_info_zip_code = fields.Char(string='Zip code', **__address_kwargs)
    partner_info_city = fields.Char(string='City', **__address_kwargs)
    partner_info_country_id = fields.Many2one(
        comodel_name='res.country', string='Country', **__address_kwargs
    )
    partner_info_phone = fields.Char(string='Phone', **__address_kwargs)
    partner_info_fax = fields.Char(string='Fax', **__address_kwargs)
    partner_info_email = fields.Char(string='Email', **__address_kwargs)

    # ===

    @api.constrains(
        'partner_info_entity_identifier',
        'partner_info_entity_type_id',
    )
    @api.one
    def _check_entity_identifier(self):
        """Ensure the entity identifier obeys rules set by the entity type.
        """

        entity_identifier = self.partner_info_entity_identifier
        entity_type = self.partner_info_entity_type_id

        if not entity_identifier or not entity_type:
            return  # Nothing to check.

        if not entity_type.match_identifier(entity_identifier):
            raise exceptions.ValidationError(_(
                'The entity identifier does not satisfy rules set by the '
                'entity type.'
            ))

    @api.model
    def create(self, vals):
        """Override to fill unique ID information when available, send a sync
        when necessary and mark partners as activated when they are.
        """

        self.generate_pinfo_identifier_sequence(vals)

        vals, partner_info = self.fill_unique_id_info(vals)

        partner = super(Partner, self).create(vals)

        partner.check_activation_status(vals)

        if partner_info:
            partner.link_to_unique_id_record(partner_info)
            partner.send_unique_id_req()

        return partner

    @api.multi
    def write(self, vals):
        """Override to fill unique ID information when available, send a sync
        when necessary and mark partners as activated when they are.
        """

        self.generate_pinfo_identifier_sequence(vals)

        vals, partner_info = self.fill_unique_id_info(vals)

        ret = super(Partner, self).write(vals)

        self.check_activation_status(vals)

        if partner_info:
            for partner in self:
                partner.link_to_unique_id_record(partner_info)
                partner.send_unique_id_req()

        return ret

    @api.one
    def check_activation_status(self, values):
        """When activation-related fields have been updated; find out whether
        partners can be marked / un-marked as "activated".
        :param values: Data used to create / update records.
        :type values: Dictionary.
        """

        if values.viewkeys() & self.get_activation_fields():
            incomplete = bool(self.get_incomplete_activation_fields())
            partner_info_status = self.partner_info_status
            if not incomplete and not partner_info_status:
                self.partner_info_status = 'activated'
            elif incomplete and partner_info_status == 'activated':
                self.partner_info_status = False

    @api.multi
    def fill_unique_id_info(self, values):
        """Fill unique ID information when available.
        :param values: Data used to create / update records.
        :type values: Dictionary.
        :return: Tuple with:
        - Data used to create / update records; potentially updated.
        - Unique ID record being linked with when found.
        :rtype: (dict, Odoo "res.partner.info" record set).
        """

        # This only runs when changing the unique ID.
        if (
            'partner_info_entity_identifier' not in values and
            'partner_info_entity_type_id' not in values
        ):
            return values, None

        def read_ex(key):
            """Read existing values when they are not being updated."""

            # Regular case: The value is being updated.
            value = values.get(key)
            if value is not None:
                return value

            if self:
                # Read the value from the (first) record.
                value = getattr(self[0], key)
                if isinstance(value, models.BaseModel):
                    value = value.id  # For m2o fields, return the ID.
                return value

        # The data we need to build a unique ID record.
        entity_identifier = read_ex('partner_info_entity_identifier')
        entity_type_id = read_ex('partner_info_entity_type_id')
        if not entity_identifier or not entity_type_id:
            return values, None

        # Find an existing unique ID record.
        partner_info = self.env['res.partner.info'].search([
            ('entity_identifier', '=', entity_identifier),
            ('entity_type_id', '=', entity_type_id),
        ], limit=1)
        if not partner_info:
            return values, None

        # Got one! Fill the local address from the social one. Other unique ID
        # information will be set later on through the
        # "link_to_unique_id_record" method.
        values.update({
            'street': partner_info.street1,
            'street2': partner_info.street2,
            'street3': partner_info.street3,
            'zip': partner_info.zip_code,
            'city': partner_info.city,
            'country_id': partner_info.country_id.id,
            'phone': partner_info.phone,
            'fax': partner_info.fax,
            'email': partner_info.email,
        })

        return values, partner_info

    @api.model
    def generate_pinfo_identifier_sequence(self, values):
        """Generate an identifier out of a sequence when one is set onto the
        entity type.
        :param values: Data used to create / update records. Mutable.
        :type values: Dictionary.
        """

        pietype_id = values.get('partner_info_entity_type_id')
        if not pietype_id:
            return

        pietype = self.env['res.partner.info.entity_type'].browse(pietype_id)
        if not pietype:
            return

        seq = pietype.identifier_sequence_id
        if not seq:
            return

        # Got a sequence! Generate a code.
        values['partner_info_entity_identifier'] = (
            api.guess(self.env['ir.sequence'].next_by_id)(seq.id)
        )

    def get_activation_fields(self):
        """List fields required for partners to reach the "Activated" unique ID
        status.
        :rtype: List.
        """

        return ['street', 'city', 'country_id']

    @api.multi
    def get_incomplete_activation_fields(self):
        """Among fields required for partners to reach the "Activated" unique
        ID status, list those not filled in yet for the specified partner.
        :rtype: List.
        """

        return self._get_incomplete_fields(self.get_activation_fields())

    @api.multi
    def get_incomplete_submission_fields(self):
        """Among fields required for partners to reach the "Submitted" unique
        ID status, list those not filled in yet for the specified partner.
        :rtype: List.
        """

        return self._get_incomplete_fields(self.get_submission_fields())

    def get_submission_fields(self):
        """List fields required for partners to reach the "Submitted" unique ID
        status.
        :rtype: List.
        """

        return [
            'partner_info_entity_type_id', 'partner_info_entity_identifier',
            'partner_info_official_name',
        ] + self.get_activation_fields()

    @api.model
    def get_validated_unique_id_values(self, partner_info):
        """Build values to update parthers with based on the specified
        validated unique ID record.

        :type partner_info: Odoo "res.partner.info" record set.
        """

        # Prepare a street that can be stored in 1 field (for bank accounts).
        street_parts = (
            partner_info.street1, partner_info.street2, partner_info.street3,
        )
        one_line_street = u' - '.join(
            street_part for street_part in street_parts if street_part
        )

        partner_values = {
            'partner_info_comments': partner_info.comments,
            'partner_info_id': partner_info.id,
            'partner_info_official_name': partner_info.official_name,
            'partner_info_status': 'validated',

            # Social address.
            'partner_info_street1': partner_info.street1,
            'partner_info_street2': partner_info.street2,
            'partner_info_street3': partner_info.street3,
            'partner_info_zip_code': partner_info.zip_code,
            'partner_info_city': partner_info.city,
            'partner_info_country_id': partner_info.country_id.id,
            'partner_info_phone': partner_info.phone,
            'partner_info_fax': partner_info.fax,
            'partner_info_email': partner_info.email,

            # Bank accounts.
            'bank_ids': [(0, 0, {  # 0: Create.
                'acc_number': bank_account.iban,
                'bank': bank_account.bank_id.id,
                'bank_bic': bank_account.bank_id.bic,
                'bank_name': bank_account.bank_id.name,
                'state': 'iban',

                # Bank owner.
                'owner_name': bank_account.iban_owner,
                'street': one_line_street,
                'zip': partner_info.zip_code,
                'city': partner_info.city,
                'country_id': partner_info.country_id.id,
            }) for bank_account in partner_info.bank_account_ids],
        }

        # Propagate analytics.
        partner_values.update(self.env['analytic.structure'].extract_values(
            partner_info, 'res_partner_info', dest_model='res_partner',
        ))

        return partner_values

    @api.multi
    def link_to_unique_id_record(self, partner_info):
        """Link partners to the specifieid unique ID record and update related
        data.

        :type partner_info: Odoo "res.partner.info" record set.
        """

        unique_id_values = self.get_validated_unique_id_values(partner_info)

        # When the partner already had an official name manually set from
        # within the local environment, do not override it.
        if len(self) == 1 and self.partner_info_official_name:
            unique_id_values.pop('partner_info_official_name')

        self.write(unique_id_values)

    @api.multi
    def send_unique_id_req(self, partial_update=False):
        """Send a unique ID request to the external partner repository.

        :param partial_update: Whether a partial update should be signaled.
        :type partial_update: Boolean.
        """

        self.ensure_one()

        # Use the most prioritized emitter profile.
        emitter = self.env['xbus.emitter'].search([], limit=1)
        if not emitter:
            raise exceptions.Warning(_('No Xbus emitter found.'))

        # Send 1 item within 1 Xbus message.
        emitter.send_items(
            PARTNER_INFO_REQ_EVENT_TYPE,
            self.unique_id_req_xbus_dict(partial_update=partial_update),
            delayed=True,
        )

    @api.multi
    def submit_to_group(self):
        """Submit the partner to the external repository and change the unique
        ID status.
        """

        incomplete_partners = self.filtered(
            lambda partner: partner.get_incomplete_submission_fields()
        )
        if incomplete_partners:
            needed_fields = self.get_submission_fields()
            # Get field labels. Odoo 7 call to "fields_get" to avoid errors...
            field_labels = {
                field: field_info['string']
                for field, field_info in self.pool[self._name].fields_get(
                    self.env.cr, self.env.user.id, needed_fields,
                    context=self.env.context,
                ).iteritems()
            }
            raise exceptions.Warning(
                _('Fields to be filled in to submit to the group:') + u'\n' +
                u', '.join(field_labels[field] for field in needed_fields) +
                u'.\n\n' +
                _('%d incomplete partners: %s.') % (
                    len(incomplete_partners),
                    u', '.join(incomplete_partners.mapped('name')),
                )
            )

        for partner in self:
            partner.send_unique_id_req()

        # Requests sent!
        self.write({'partner_info_status': 'submitted'})

        return True

    @api.multi
    def unique_id_req_xbus_dict(self, partial_update=False):
        """Return a dict representing a unique ID request, ready to be sent to
        Xbus.

        :param partial_update: Whether a partial update should be signaled.
        :type partial_update: Boolean.
        """

        self.ensure_one()

        env_name = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', 'partner_unique_id.partner_repo_env_name')], limit=1,
        ).value

        ret = {
            # Compulsory.
            u'entity_identifier': self.partner_info_entity_identifier,
            u'entity_type': self.partner_info_entity_type_id.technical_name,
            u'env': env_name,
            u'official_name': self.partner_info_official_name,
        }

        if partial_update:
            return ret

        bank_acc_doc = self.partner_info_bank_account_doc
        bank_acc_doc = base64.b64encode(bank_acc_doc) if bank_acc_doc else u''

        ret.update({
            # Facultative.
            u'bank_account_doc': bank_acc_doc,
            u'bank_account_doc_filename': (
                self.partner_info_bank_account_doc_filename or u''
            ),
            u'comments': self.partner_info_comments or u'',
            u'local_name': self.name or u'',

            # Address.
            u'street1': self.street or u'',
            u'street2': self.street2 or u'',
            u'street3': getattr(self, 'street3', None) or u'',
            u'zip_code': self.zip or u'',
            u'city': self.city or u'',
            u'country_code': self.country_id.code or u'',
            u'phone': self.phone or u'',
            u'fax': self.fax or u'',
            u'email': self.email or u'',
        })

        return ret

    @api.multi
    def _get_incomplete_fields(self, needed_fields):
        """List fields among the specified ones not filled in yet for the
        specified partner.
        :type needed_fields: List.
        :rtype: List.
        """

        self.ensure_one()

        # Prefer "read" over "browse" to be safer wrt the cache / perf.
        data = self.read(fields=needed_fields)[0]
        return [field for field in needed_fields if not data[field]]
