from openerp import fields
from openerp import models


class PartnerInfoBankAccount(models.Model):
    """Represent a bank account linked to a unique ID record, retrieved from an
    external partner repository.
    """

    _name = 'res.partner.info.bank_account'
    _description = 'Partner unique info bank account'

    partner_info_id = fields.Many2one(
        comodel_name='res.partner.info',
        string='Unique ID record',
        ondelete='cascade',
        help='The unique ID record this bank account is in.',
        readonly=True,
        required=True,
    )

    # The following fields are similar to those in bank accounts
    # (res.partner.bank model); they are defined here so this object can stand
    # on its own without requiring a partner link.

    __iban_kwargs = {
        'help': 'Bank information stored in the external partner repository.',
    }

    iban = fields.Char(string='IBAN', **__iban_kwargs)
    iban_owner = fields.Char(string='Bank account owner', **__iban_kwargs)
    bank_id = fields.Many2one(
        comodel_name='res.bank',
        ondelete='restrict',
        string='Bank',
        required=True,
        **__iban_kwargs
    )
    bank_code = fields.Char(string='Bank code', **__iban_kwargs)
    bank_name = fields.Char(string='Bank name', **__iban_kwargs)
    iban_creation_date = fields.Datetime(
        string='IBAN creation date', **__iban_kwargs
    )
    iban_creator = fields.Char(string='IBAN creator', **__iban_kwargs)
    iban_update_date = fields.Datetime(
        string='IBAN update date', **__iban_kwargs
    )
    iban_updater = fields.Char(string='IBAN updater', **__iban_kwargs)
