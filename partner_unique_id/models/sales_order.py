from openerp import _
from openerp import models


class SalesOrder(models.Model):
    """Warning message when the partner has not been activated.
    """

    _inherit = 'sale.order'

    def onchange_partner_id(self, cr, uid, ids, part, context=None):
        """Override this (Odoo 7 style) change handler to add a warning message
        when the partner has not been activated.
        """

        ret = super(SalesOrder, self).onchange_partner_id(
            cr, uid, ids, part, context=context,
        )

        if part:
            pinfostatus = self.pool['res.partner'].browse(
                cr, uid, [part], context=context,
            )[0].partner_info_status
            if not pinfostatus or pinfostatus not in (
                'activated', 'submitted', 'validated',
            ):
                # Clear out the partner.
                ret_value = ret.get('value', {})
                ret_value['partner_id'] = False

                # Append to the warning's title & message when there already
                # was one.
                warning = ret.get('warning')
                if warning:
                    warning_title = (warning.get('title') or u'') + u' & '
                    warning_message = (warning.get('message') or u'') + u'\n\n'
                else:
                    warning_title = u''
                    warning_message = u''
                warning_title += _('Inactive partner')
                warning_message += _(
                    'The partner unique ID status is inactive.'
                )

                ret.update({
                    'warning': {'title': warning_title,
                                'message': warning_message},
                    'value': ret_value,
                })

        return ret
