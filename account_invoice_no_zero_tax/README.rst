Accounting invoices - No zero tax
=================================

When accounting entries are generated from accounting invoices, don't include
tax accounting entries for which the amount is 0.
