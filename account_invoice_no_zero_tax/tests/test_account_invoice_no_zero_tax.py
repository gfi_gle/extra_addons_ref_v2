import time

import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_account(self):
        """Create accounting account for use in further tests.
        """

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': time.strftime('%Y-01-01'),
            'date_stop': time.strftime('%Y-12-31'),
            'period': 'month',
        }).execute()

        parent_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )
        self.assertTrue(parent_account)

        self.memory.account, = self.createAndTest(
            'account.account',
            [{
                'code': genUuid(max_chars=4),
                'name': genUuid(max_chars=8),
                'parent_id': parent_account.id,
                'reconcile': True,
                'type': parent_account.type,
                'user_type': parent_account.user_type.id,
            }],
        )

    def test_0001_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partner, = self.createAndTest(
            'res.partner',
            [{
                'name': genUuid(),
            }],
        )

    def test_0002_find_taxes(self):
        """Find taxes, for use in further tests.
        """

        tax_obj = self.env['account.tax']
        self.memory.sales_tax = tax_obj.search(
            [('amount', '>', 0.0), ('type_tax_use', '=', 'sale')], limit=1,
        )
        self.memory.purchase_tax = tax_obj.search(
            [('amount', '>', 0.0), ('type_tax_use', '=', 'purchase')], limit=1,
        )
        self.memory.sales_zero_tax = tax_obj.search(
            [('amount', '=', 0.0), ('type_tax_use', '=', 'sale')], limit=1,
        )
        self.memory.purchase_zero_tax = tax_obj.search(
            [('amount', '=', 0.0), ('type_tax_use', '=', 'purchase')], limit=1,
        )
        self.assertTrue(self.memory.sales_tax)
        self.assertTrue(self.memory.purchase_tax)
        self.assertTrue(self.memory.sales_zero_tax)
        self.assertTrue(self.memory.purchase_zero_tax)

    def test_0003_create_products(self):
        """Create products for use in further tests.
        """

        self.memory.product, = self.createAndTest(
            'product.product',
            [{
                'name': genUuid(),
                'property_account_expense': self.memory.account.id,
                'property_account_income': self.memory.account.id,
                'type': 'service',
            }],
        )

    def test_0010_create_invoices(self):
        """Create accounting invoices.
        """

        # Create invoices; lines will be added into them later on.
        (
            self.memory.invoice_with_tax,
            self.memory.invoice_without_tax,
            self.memory.invoice_with_zero_tax,
        ) = self.createAndTest(
            'account.invoice',
            [
                {
                    'account_id': self.memory.account.id,
                    'partner_id': self.memory.partner.id,
                    'type': 'out_invoice',
                },
                {
                    'account_id': self.memory.account.id,
                    'partner_id': self.memory.partner.id,
                    'type': 'out_invoice',
                },
                {
                    'account_id': self.memory.account.id,
                    'partner_id': self.memory.partner.id,
                    'type': 'out_invoice',
                },
            ],
        )

        def taxTester(test_instance, asked_value, recorded_value):
            test_instance.assertEqual(recorded_value.ids, [asked_value[0][1]])

        # Create invoice lines.
        self.createAndTest(
            'account.invoice.line',
            [
                {
                    'invoice_line_tax_id': [(4, self.memory.sales_tax.id)],
                    'invoice_id': self.memory.invoice_with_tax.id,
                    'name': genUuid(),
                    'price_unit': 42.0,
                    'product_id': self.memory.product.id,
                },
                {
                    'invoice_id': self.memory.invoice_without_tax.id,
                    'name': genUuid(),
                    'price_unit': 42.0,
                    'product_id': self.memory.product.id,
                },
                {
                    'invoice_line_tax_id': [
                        (4, self.memory.sales_zero_tax.id),
                    ],
                    'invoice_id': self.memory.invoice_with_zero_tax.id,
                    'name': genUuid(),
                    'price_unit': 42.0,
                    'product_id': self.memory.product.id,
                },
            ],
            custom_testers={
                'invoice_line_tax_id': taxTester,
            },
        )

    def test_0011_validate_invoice_with_tax(self):
        """Validate the accounting invoice with tax.
        """

        invoice = self.memory.invoice_with_tax

        # Validate the invoice.
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

        # Get the accounting document.
        acc_doc = invoice.move_id
        self.assertTrue(acc_doc)

        # Ensure there are 2 regular lines and 1 tax line.
        self.assertEqual(len(acc_doc.line_id), 3)

    def test_0012_validate_invoice_with_tax(self):
        """Validate the accounting invoice with tax.
        """

        invoice = self.memory.invoice_without_tax

        # Validate the invoice.
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

        # Get the accounting document.
        acc_doc = invoice.move_id
        self.assertTrue(acc_doc)

        # Ensure there are 2 regular lines.
        self.assertEqual(len(acc_doc.line_id), 2)

    def test_0013_validate_invoice_with_zero_tax(self):
        """Validate the accounting invoice with a tax with a zero amount.
        """

        invoice = self.memory.invoice_with_zero_tax

        # Validate the invoice.
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

        # Get the accounting document.
        acc_doc = invoice.move_id
        self.assertTrue(acc_doc)

        # Ensure there are 2 regular lines; the tax line should have been
        # omitted.
        self.assertEqual(len(acc_doc.line_id), 2)
