from openerp import api
from openerp import models


class AccountingInvoiceTax(models.Model):
    """Adapt accounting invoice tax helpers not to generate accounting entries
    with 0 amounts.
    """

    _inherit = 'account.invoice.tax'

    @api.multi
    def compute(self, invoice):
        """Override to take out "pre-entries" with a 0 amount.
        """

        ret = super(AccountingInvoiceTax, self).compute(invoice)

        if ret:
            ret = {
                key: value
                for key, value in ret.iteritems()
                if value and value.get('amount', 0.0) > 0.0
            }

        return ret
