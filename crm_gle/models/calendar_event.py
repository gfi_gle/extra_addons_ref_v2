# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from openerp import fields, models, api
from openerp import _


class CalendarEvent(models.Model):
    """Change the string of the field opportunity_id, and add a lead_type
    field.
    """
    _inherit = 'calendar.event'

    opportunity_id = fields.Many2one(
        string="Lead/Opportunity",
        domain=[],
    )

    opp_type = fields.Selection(
        [
            ('lead', _('Lead')),
            ('opportunity', _('Opportunity')),
        ],
        string="Opportunity Type",
    )
    """
    This field is meant to be a related field to
    opportunity_id.type, but this implementation
    will enable the group by on this field (which
    require the field to be stored), avoid
    an error on the creation of a calendar.event
    to occur, and allow the field to be exported
    in the translations.
    """

    @api.onchange('opportunity_id')
    def _onchange_opp_type(self):
        if self.opportunity_id:
            self.opp_type = self.opportunity_id.type
        else:
            self.opp_type = False

    @api.model
    def create(self, vals):
        vals.update(self._set_opp_type(vals))
        return super(CalendarEvent, self).create(vals)

    @api.multi
    def write(self, vals):
        for record in self:
            vals.update(record._set_opp_type(vals))
        return super(CalendarEvent, self).write(vals)

    def _set_opp_type(self, vals):
        """
        Inject opp_type in vals if it is
        declared as a readonly field.
        """
        if (
            "opportunity_id" in vals and
            "opp_type" not in vals
        ):
            if vals["opportunity_id"]:
                new_opportunity_id = self.env["crm.lead"].search([
                    ("id", "=", vals["opportunity_id"]),
                ])
                vals["opp_type"] = new_opportunity_id.type
            else:
                vals["opp_type"] = False
        return vals
