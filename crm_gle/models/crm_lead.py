from openerp import api
from openerp import fields
from openerp import models


class Lead(models.Model):
    """Add incentive and royalty management into authoring contracts."""

    _inherit = 'crm.lead'

    function_code_id = fields.Many2one(
        comodel_name='res.function_code',
        string='Function Code',
    )
    service_id = fields.Many2one(
        comodel_name='res.service',
        string='Service',
    )
    manpower_id = fields.Many2one(
        comodel_name='res.manpower',
        string='Manpower',
    )

    @api.multi
    def on_change_partner_id(self, partner_id):
        """Override method defined in crm to fill in new fields.

        Note: this called when the partner_id field is changed in the
        web interface.
        """
        values = super(Lead, self).on_change_partner_id(partner_id)

        partner_obj = self.env['res.partner']
        partner = partner_obj.browse(partner_id)

        extra_vals = {}

        if partner.function_code_id:
            extra_vals['function_code_id'] = partner.function_code_id.id

        if partner.service_id:
            extra_vals['service_id'] = partner.service_id.id

        if partner.manpower_id:
            extra_vals['manpower_id'] = partner.manpower_id.id

        vals = values['value'].copy()
        vals.update(extra_vals)
        values['value'] = vals

        return values

    def _lead_create_contact(
        self, cr, uid, lead, name, is_company, parent_id=False, context=None
    ):

        partner_id = super(Lead, self)._lead_create_contact(
            cr, uid, lead, name, is_company, parent_id=parent_id,
            context=context
        )

        partner = self.pool.get('res.partner').browse(
            cr, uid, partner_id, context=context
        )

        vals = {
            'function_code_id': (
                lead.function_code_id.id if not is_company else False
            ),
            'manpower_id': (
                lead.manpower_id.id if is_company else False
            ),
            'service_id': (
                lead.service_id.id if not is_company else False
            ),
        }

        if (
            'street3' in partner._columns.keys() and
            'street3' in lead._columns.keys()
        ):
            vals['street3'] = lead.street3

        partner.write(vals)

        return partner.id

    @api.multi
    def write(self, vals):

        if 'type' in vals:
            calendar_events = self.env['calendar.event'].search(
                [('opportunity_id', '=', self.id)]
            )

            for calendar_event in calendar_events:
                calendar_event.opp_type = vals['type']

        return super(Lead, self).write(vals)

