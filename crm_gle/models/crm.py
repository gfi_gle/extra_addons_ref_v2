# encoding: utf-8
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from openerp import api, fields, models, exceptions
from openerp import tools


class crm_lead(models.Model):
    _inherit = 'crm.lead'

    def _is_convert_into_client_displayed(self):
        if self.partner_id:
            self.is_convert_into_client_displayed = self.partner_id.customer
        else:
            self.is_convert_into_client_displayed = False

    contact_firstname = fields.Char(
        'Contact Firstname',
        size=64,
    )
    contact_lastname = fields.Char(
        'Contact Lastname',
        size=64,
    )
    service_id = fields.Many2one(
        'res.service',
        'Service',
    )
    typology_id = fields.Many2one(
        'res.typology',
        'Typology',
    )
    is_convert_into_client_displayed = fields.Boolean(
        'Is Customer',
        compute=_is_convert_into_client_displayed,
    )

    @api.onchange('contact_firstname', 'contact_lastname')
    def _onchange_firstname_lastname(self):
        if self.contact_lastname and self.contact_firstname:
            self.contact_name = self.contact_lastname + " " + self.contact_firstname

    def on_change_partner_id(self, cr, uid, ids, partner_id, context=None):
        value = super(crm_lead, self).on_change_partner_id(
            cr, uid, ids, partner_id, context=context)
        partner = self.pool.get('res.partner').browse(
            cr, uid, partner_id, context=context)
        value["value"]["typology_id"] = partner.typology_id if partner.typology_id else False
        value["value"]["contact_firstname"] = partner.firstname if partner.firstname else False
        value["value"]["contact_lastname"] = partner.lastname if partner.lastname else False
        value["value"]["service_id"] = partner.service_id.id if partner.service_id else False
        return value

    def convert_into_client(self, cr, uid, ids, action='create',
                            partner_id=False, context=None):
        l2p = self.pool.get("crm.lead2opportunity.partner")
        partner = self.pool.get('res.partner')
        for lead_id in self.browse(cr, uid, ids, context=context):
            self.check_fields(lead_id)
            if lead_id.contact_lastname and lead_id.contact_firstname:
                lead_id.contact_name = lead_id.contact_lastname + " " + lead_id.contact_firstname
            partner_id = l2p.create_partner(cr, uid, lead_id.id, context)
            partner_obj = partner.browse(cr, uid, partner_id, context=context)
            action = "convert_into_client"
            self.update_partner(
                cr, uid, partner, partner_obj, lead_id, action, context)
        return super(crm_lead, self).case_mark_won(cr, uid, ids, context)

    def case_mark_won(self, cr, uid, ids, context):
        partner = self.pool.get('res.partner')
        for lead_id in self.browse(cr, uid, ids, context=context):
            partner_obj = lead_id.partner_id
            action = "case_mark_won"
            self.update_partner(
                cr, uid, partner, partner_obj, lead_id, action, context)
        return super(crm_lead, self).case_mark_won(cr, uid, ids, context)

    def update_partner(self, cr, uid, partner, partner_obj,
                       lead_id, action, context=None):
        customer = True if action in ["case_mark_won", "convert_into_client"] else False
        if not partner_obj.is_company:
            vals = {
                'service_id': lead_id.service_id.id,
                'firstname': lead_id.contact_firstname,
                'lastname': lead_id.contact_lastname,
                'customer': customer,
            }
            partner.update_lead_data(cr, uid, partner_obj.id, vals, context)
            if partner_obj.parent_id:
                partner_obj = partner.browse(
                    cr, uid, partner_obj.parent_id.id, context=context)
        if partner_obj.is_company:
            vals = {
                'typology_id': lead_id.typology_id.id,
                'customer': customer,
                'street': lead_id.street,
                'street2': lead_id.street2,
                'street3': lead_id.street3,
                'zip': lead_id.zip,
                'city': lead_id.city,
                'state_id': lead_id.state_id.id,
                'country_id': lead_id.country_id.id,
                'phone': lead_id.phone,
                'mobile': lead_id.mobile,
                'fax': lead_id.fax,
                'email': lead_id.email_from,
                'comment': lead_id.description,
            }
            partner.update_lead_data(cr, uid, partner_obj.id, vals, context)

    def check_fields(self, lead_id):
        # Check common fields go here
        pass
