# encoding: utf-8
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from openerp import api, fields, models, exceptions


class crm_lead2opportunity_partner(models.TransientModel):
    _inherit = 'crm.lead2opportunity.partner'

    # TODO odoo8fy
    def create_partner(self, cr, uid, lead_id, context=None):
        if context is None:
            context = {}
        context["active_model"] = "crm.lead"
        return super(crm_lead2opportunity_partner, self)._create_partner(cr, uid, lead_id, "each_exist_or_create", None, context)

    # TODO odoo8fy
    def default_get(self, cr, uid, fields, context=None):
        res = super(
            crm_lead2opportunity_partner, self).default_get(
            cr, uid, fields, context=context
        )
        partner_id = res.get('partner_id')
        res.update({'action':  partner_id and 'exist' or 'nothing'})
        return res

    def _create_partner(self, cr, uid, lead_id, action, partner_id, context=None):
        partner_id = super(crm_lead2opportunity_partner, self)._create_partner(
            cr, uid, lead_id, action, partner_id, context=context)
        
        lead = self.pool.get("crm.lead").browse(cr, uid, [lead_id], context=context)
        partner = self.pool.get('res.partner')
        partner_obj = partner.browse(cr, uid, [partner_id], context=context)
        lead.update_partner(partner, partner_obj,
            lead, action, context=context)
        
        return partner_id
