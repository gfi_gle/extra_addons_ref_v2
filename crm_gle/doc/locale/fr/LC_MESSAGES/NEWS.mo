��          L               |      }   M   �      �   �   �      �  �  �     B  X   F     �  �   �     |   1.1 Defines common fields and functions to copy from lead/opportunity to partner. NEWS [fix] in the crm.lead view, categ_ids was present twice, once invisible, which caused problem when trying to save changes on the not invisible element. the invisible element is removed and problems get fixed. crm_gle 1.0 Project-Id-Version: CRM GLE 8.0.1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-03-18 11:42+0100
PO-Revision-Date: 2016-03-18 11:44+0200
Last-Translator: Vincent Hatakeyama <vincent.hatakeyama@xcg-consulting.fr>
Language-Team: XCG Consulting
Plural-Forms: nplurals=2; plural=(n > 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 1.3
 1.1 Définit les champs communs et les fonctions de copie de piste/opportunité vers client. Nouveautés [fix] in the crm.lead view, categ_ids was present twice, once invisible, which caused problem when trying to save changes on the not invisible element. the invisible element is removed and problems get fixed. crm_gle 1.0 