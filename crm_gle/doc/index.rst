.. CRM GLE documentation master file, created by
   sphinx-quickstart on Fri Mar 18 11:28:50 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: manifest

Contents:

.. toctree::
   :maxdepth: 2

   README
   NEWS

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

