import openerp
import datetime
import openerp.tests


class Singleton(type):
    """
    This is a neat singleton pattern. This was found in a comment on this page:
    http://www.garyrobinson.net/2004/03/python_singleto.html

    to use this, example :
    >>> class C(object):
    ...     __metaclass__ = Singleton
    ...     def __init__(self, foo):
    ...         self.foo = foo

    >>> C('bar').foo
    'bar'

    >>> C().foo
    'bar'

    and your class C is now a singleton, and it is safe to use
    the __init__ method as you usually do...
    """

    def __init__(cls, name, bases, dic):
        super(Singleton, cls).__init__(name, bases, dic)
        cls.instance = None

    def __call__(mcs, *args, **kw):
        if mcs.instance is None:
            mcs.instance = super(Singleton, mcs).__call__(*args, **kw)

        return mcs.instance


class TestMemory(object):
    __metaclass__ = Singleton


# SingleTransactionCase is to have one transaction rolledback at the end of all
# tests
# if you want one transaction per test use TransactionCase instead

@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class TestBase(openerp.tests.SingleTransactionCase):

    def setUp(self):
        super(TestBase, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_crmlead(self):
        crmlead_osv = self.env['crm.lead']
        values = {
            "name": "lead_01",
        }
        self.memory.lead_01 = crmlead_osv.create(values)
        self.assertEqual(self.memory.lead_01.name, "lead_01", "Name after creation should be lead_01")
