from openerp import _
from openerp import api
from openerp import fields
from openerp import models


class MonthlyTransferDlg(models.TransientModel):
    """Dialog box to generate accounting data monthly, based on breakdown
    tables linked to subscription files.
    """

    _name = 'subscription.monthly_transfer_dlg'

    breakdown_ids = fields.Many2many(
        comodel_name='subscription.breakdown',
        relation='subscription_monthly_transfer_dlg_breakdown_rel',
        column1='dlg_id',
        column2='breakdown_id',
        string='Breakdown entries',
        domain=[('period_id', '!=', False), ('state', '=', 'to_generate')],
        help='The breakdown entries to transfer.',
        required=True,
    )

    period_id = fields.Many2one(
        comodel_name='account.period',
        string='Accounting period',
        domain=[('special', '=', False), ('state', '=', 'draft')],
        ondelete='set null',
        default=lambda self: self.env['account.period'].find(),
        help=(
            'The accounting period to generate monthly transfers on. Only '
            'open periods may be selected here.'
        ),
        required=True,
    )

    @api.onchange('period_id')
    def _handle_period_id_change(self):
        """Update the breakdown entry list based on the selected accounting
        period.
        """

        self.breakdown_ids = self.env['subscription.breakdown'].search([
            ('period_id', '=', self.period_id.id),
            ('state', '=', 'to_generate'),
        ]) if self.period_id else False

    @api.multi
    def apply(self):
        """Generate accounting entries based on the specified breakdown
        entries.
        """

        self.ensure_one()

        # The subscription files, to show them later on.
        subsfiles = self.env['subscription.file']

        # Let subscription files handle the heavy work.
        for breakdown in self.breakdown_ids:
            subsfile = breakdown.subscription_file_id
            subsfiles |= subsfile
            subsfile.pl_transfer(breakdown)

        # Show the affected subscription files.
        return {
            'context': self.env.context,
            'domain': [('id', 'in', subsfiles.ids)],
            'name': _('Affected subscription files'),
            'res_model': 'subscription.file',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }
