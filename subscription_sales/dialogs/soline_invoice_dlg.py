from openerp import models


class SOLineInvoiceDlg(models.TransientModel):
    """Adapt sales order line invoice generation (when invoicing sales order
    lines 1 by 1) to subscription.
    """

    _inherit = 'sale.order.line.make.invoice'

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        """Override to:
        - Set the right accounting account.
        - Set the right payment terms.
        - Include subscription information into invoices generated from this
        sales order.
        """

        ret = super(SOLineInvoiceDlg, self)._prepare_invoice(
            cr, uid, order, lines, context=context,
        )

        if order.is_for_subscription:
            partner = order.partner_id

            ret.update({
                'account_id': partner.operational_client_account_id.id,
                'payment_term': partner.operational_client_payment_terms_id.id,
                'subscription_file_ids': [(4, order.subscription_file_id.id)],
                'subscription_type': 'sales',
            })

        return ret
