##############################################################################
#
#    Subscription sales for Odoo
#    Copyright (C) 2016 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Subscription sales',
    'description': '''
Subscription sales
==================

Handle subscription sales within Odoo.

- 3 new product types, along with some new product fields.

- Sales order workflow tweaks.

- Subscription file generation.

- Commercial agent fee purchase order.

- Purchase tracker tweaks.

- Balance / P&L transfers & breakdown plans.


Installation
------------

- If the Odoo database contains lots of sales order lines, pre-create computed
  columns by running the ``scripts/create_soline_computed_columns.sql`` script.

- Install this addon as usual.
''',
    'version': '0.1',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'account_file',
        'account_invoice_streamline',
        'account_streamline',
        'product_streamline',
        'purchase_streamline',
        'purchase_tracker',
        'queue',
        'sale_crm',
        'sale_streamline',
        'subscription_accounts',
        'subscription_base',
        'subscription_frequency',
    ],

    'data': [
        'security/ir.model.access.csv',

        'data/queues.xml',
        'data/sequences.xml',

        'dialogs/monthly_transfer_dlg.xml',

        'views/account_invoice.xml',
        'views/account_invoice_no_formatting.xml',
        'views/product_template.xml',
        'views/purchase_order.xml',
        'views/purchase_tracker.xml',
        'views/purchase_tracker_no_formatting.xml',
        'views/sales_order.xml',
        'views/subscription_breakdown.xml',
        'views/subscription_config_settings.xml',
        'views/subscription_file.xml',
        'views/subscription_turnover.xml',

        'workflows/account_file.xml',
        'workflows/account_file_flow.xml',
        'workflows/account_invoice.xml',
        'workflows/purchase_order.xml',
        'workflows/purchase_tracker.xml',
        'workflows/sales_order.xml',
    ],

    'installable': True,
}
