from collections import defaultdict
import datetime
from dateutil.relativedelta import relativedelta  # Odoo req.
import openerp.exceptions
import openerp.fields
import openerp.tests
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid

import random


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


# TODO Look into mocking dates
# <http://blog.xelnor.net/python-mocking-datetime/>.


# Amounts used in tests.
SUBS_FEE_RATE = 50.0  # %
LOW_FEE_RATE = 20.0  # %
SUBS_PRICE = 5000.0
QUANTITY = 3
QUANTITIES = [random.randint(1, 2) for i in range(3)]
QUANTITIES2 = [random.randint(4, 6) for i in range(3)]
LOW_PRICE = 100.0

# The amount of months in our default subscription frequency table.
MONTH_COUNT = 12

# The quantity delta and the appreciation amount we are going to simulate.
EXPECTED_QDELTA = 2.0
APPRECIATION_AMOUNT = EXPECTED_QDELTA * SUBS_PRICE

# Additional fiscal years to create beyond the current one.
ADDITIONAL_YEARS = 1


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_accounting_accounts(self):
        """Create accounting accounts for use in further tests.
        """

        self.memory.current_year = datetime.date.today().year

        # Load basic accounting stuff first.
        self.env['account.config.settings'].create({
            'chart_template_id': self.env.ref('account.conf_chart0').id,
            'date_start': '%s-01-01' % self.memory.current_year,
            'date_stop': '%s-12-31' % self.memory.current_year,
            'period': 'month',
        }).execute()

        # Prepare more fiscal years than the default ones, as some of our tests
        # span several fiscal years.
        for year_inc in xrange(1, 1 + ADDITIONAL_YEARS):
            year = self.memory.current_year + year_inc
            self.env['account.fiscalyear'].create({
                'code': '%s' % year,
                'date_start': '%s-01-01' % year,
                'date_stop': '%s-12-31' % year,
                'name': '%s' % year,
            }).create_period()

        top_normal_account = self.env['account.account'].search(
            [('type', '=', 'other')],
            limit=1,
        )
        self.assertTrue(top_normal_account)

        top_payable_account = self.env['account.account'].search(
            [('type', '=', 'payable')],
            limit=1,
        )
        self.assertTrue(top_payable_account)

        top_receivable_account = self.env['account.account'].search(
            [('type', '=', 'receivable')],
            limit=1,
        )
        self.assertTrue(top_receivable_account)

        (
            self.memory.normal_account,
            self.memory.payable_account,
            self.memory.receivable_account,
        ) = self.createAndTest(
            'account.account',
            [
                {
                    'code': genUuid(max_chars=4),
                    'name': genUuid(max_chars=8),
                    'parent_id': top_normal_account.id,
                    'reconcile': True,
                    'type': top_normal_account.type,
                    'user_type': top_normal_account.user_type.id,
                },
                {
                    'code': genUuid(max_chars=4),
                    'name': genUuid(max_chars=8),
                    'parent_id': top_payable_account.id,
                    'reconcile': True,
                    'type': top_payable_account.type,
                    'user_type': top_payable_account.user_type.id,
                },
                {
                    'code': genUuid(max_chars=4),
                    'name': genUuid(max_chars=8),
                    'parent_id': top_receivable_account.id,
                    'reconcile': True,
                    'type': top_receivable_account.type,
                    'user_type': top_receivable_account.user_type.id,
                },
            ],
        )

    def test_0001_create_frequency_tables(self):
        """Create frequency tables for user in further tests.
        """

        self.memory.freqtable, = self.createAndTest(
            'subscription.frequency',
            [{'name': genUuid()}],
        )

        # No further checks here; they are already done in tests of the
        # "subscription_frequency" addon.

    def test_0002_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        (
            self.memory.partner_buyer,
            self.memory.partner_com_agent,
        ) = self.createAndTest(
            'res.partner',
            [
                {  # partner_buyer
                    'customer': True,
                    'name': genUuid(),
                    'operational_client_account_id': (
                        self.memory.receivable_account.id
                    ),
                },
                {  # partner_com_agent
                    'has_referrer_fees': True,
                    'name': genUuid(),
                    'referrer_fees_account_id': self.memory.payable_account.id,
                    'supplier': True,
                },
            ],
        )

    def test_0003_create_products(self):
        """Check products with regards to subscription-related tweaks and save
        them for use in further tests.
        """

        def createProduct(values):
            """Create a product template then a product variant."""
            ptemplate, = self.createAndTest('product.template', [values])
            product, = self.createAndTest(
                'product.product', [{'product_tmpl_id': ptemplate.id}],
            )
            return product

        self.memory.subscription_product_theo_track = createProduct({
            'fee_rate': SUBS_FEE_RATE,
            'name': genUuid(),
            'property_account_expense': self.memory.normal_account.id,
            'property_account_income': self.memory.normal_account.id,
            'tracking_type': 'theoretical',
            'type': 'subscription',
        })
        self.memory.subscription_product_actu_track = createProduct({
            'fee_rate': SUBS_FEE_RATE,
            'name': genUuid(),
            'property_account_expense': self.memory.normal_account.id,
            'property_account_income': self.memory.normal_account.id,
            'tracking_type': 'actual',
            'type': 'subscription',
        })
        self.memory.subscription_product_actu_track2 = createProduct({
            'fee_rate': SUBS_FEE_RATE,
            'name': genUuid(),
            'property_account_expense': self.memory.normal_account.id,
            'property_account_income': self.memory.normal_account.id,
            'tracking_type': 'actual',
            'type': 'subscription',
        })
        self.memory.setup_product = createProduct({
            'fee_rate': LOW_FEE_RATE,
            'name': genUuid(),
            'property_account_expense': self.memory.normal_account.id,
            'property_account_income': self.memory.normal_account.id,
            'tracking_type': 'theoretical',
            'type': 'setup',
        })
        self.memory.maintenance_product = createProduct({
            'fee_rate': LOW_FEE_RATE,
            'name': genUuid(),
            'property_account_expense': self.memory.normal_account.id,
            'property_account_income': self.memory.normal_account.id,
            'tracking_type': 'theoretical',
            'type': 'maintenance',
        })
        self.memory.service_product = createProduct({
            'fee_rate': LOW_FEE_RATE,
            'name': genUuid(),
            'property_account_expense': self.memory.normal_account.id,
            'property_account_income': self.memory.normal_account.id,
            'type': 'service',
        })
        self.memory.products = (
            self.memory.subscription_product_theo_track,
            self.memory.subscription_product_actu_track,
            self.memory.subscription_product_actu_track2,
            self.memory.setup_product,
            self.memory.maintenance_product,
            self.memory.service_product,
        )

        # Ensure the "requires_breakdown" field has been computed as expected.
        self.assertTrue(self.memory.subscription_product_theo_track
                        .requires_breakdown)
        self.assertTrue(self.memory.subscription_product_actu_track
                        .requires_breakdown)
        self.assertFalse(self.memory.setup_product.requires_breakdown)
        self.assertTrue(self.memory.maintenance_product.requires_breakdown)
        self.assertFalse(self.memory.service_product.requires_breakdown)

    def test_0004_setup_admin_account(self):
        """Set the admin account up:
        - Add an operational department for purchase order generation.
        """

        # Find the employee that should have been created at database setup.
        employee = self.env['hr.employee'].search([
            ('user_id', '=', self.env.user.id),
        ], limit=1)

        department, = self.createAndTest(
            'hr.department',
            [{'name': genUuid()}],
        )
        self.createAndTest(
            'hr.operational_department',
            [{
                'department_id': department.id,
                'employee_id': employee.id,
            }],
        )

    def test_0005_setup_analytics(self):
        """Configure analytics of importance to this test suite.
        """

        # Clear out analytics beforehand (some demo data may add some).
        self.env['analytic.structure'].search([]).unlink()

        # An analytic dimension set on products.
        product_test_adim, = self.createAndTest(
            'analytic.dimension', [{'name': genUuid()}],
        )

        self.createAndTest(
            'analytic.structure',

            [  # account.file dimension.
                {
                    'model_name': model_name,
                    'nd_id': self.env.ref(
                        '.account_file_analytic_dimension_id'
                    ).id,
                    'ordering': '1',
                }
                for model_name in (
                    'account_invoice_line',
                    'account_move_line',
                    'purchase_order_line',
                )

            ] + [  # product_test_adim.
                {
                    'model_name': model_name,
                    'nd_id': product_test_adim.id,
                    'ordering': (
                        '1' if model_name == 'product_template' else '2'
                    ),
                }
                for model_name in (
                    'account_invoice_line',
                    'account_move_line',
                    'product_template',
                    'purchase_order_line',
                )
            ],
        )

        # Add a code into the product test dimension and set it onto products
        # created so far.
        self.memory.product_test_acode, = self.createAndTest(
            'analytic.code',
            [{'name': genUuid(), 'nd_id': product_test_adim.id}],
        )
        for product in self.memory.products:
            product.a1_id = self.memory.product_test_acode

    def test_0006_setup_properties(self):
        """Set properties up:
        - Subscription accounting accounts & journals.
        """

        # Find accounting journals.
        journals_per_type = {
            journal_type: self.env['account.journal'].search(
                [('type', '=', journal_type)], limit=1,
            )
            for journal_type in ('general', 'purchase', 'sale', 'sale_refund')
        }
        self.assertTrue(all(journals_per_type.values()))

        # Subscription accounting accounts.
        for field in (
            'com_agent_fee_adjustment_balance_transfer_debit_account_id',
            'com_agent_fee_adjustment_provision_credit_account_id',
            'com_agent_fee_adjustment_provision_debit_account_id',
            'com_agent_fee_balance_transfer_debit_account_id',
            'com_agent_fee_provision_credit_account_id',
            'com_agent_fee_provision_debit_account_id',
            'overbilled_revenue_provision_credit_account_id',
            'sales_balance_transfer_credit_account_id',
            'unbilled_revenue_provision_debit_account_id',
        ):
            self.env['ir.property'].create({
                'fields_id': self.env.ref(
                    'subscription_sales.field_subscription_file_%s' % field
                ).id,
                'name': field,
                'type': 'many2one',
                'value_reference': 'account.account,%d' % (
                    self.memory.normal_account.id
                ),
            })

        # Subscription accounting journals.
        for field, model_ref, journal_type in (
            ('com_agent_fee_adjustment_journal_id',
             'purchase_tracker_tracker', 'purchase'),
            ('com_agent_fee_journal_id',
             'purchase_tracker_tracker', 'purchase'),
            ('com_agent_fee_adjustment_balance_transfer_journal_id',
             'subscription_file', 'general'),
            ('com_agent_fee_adjustment_pl_transfer_journal_id',
             'subscription_file', 'general'),
            ('com_agent_fee_adjustment_provision_journal_id',
             'subscription_file', 'general'),
            ('com_agent_fee_adjustment_provision_reversal_journal_id',
             'subscription_file', 'general'),
            ('com_agent_fee_balance_transfer_journal_id',
             'subscription_file', 'general'),
            ('com_agent_fee_pl_transfer_journal_id',
             'subscription_file', 'general'),
            ('com_agent_fee_provision_journal_id',
             'subscription_file', 'general'),
            ('com_agent_fee_provision_reversal_journal_id',
             'subscription_file', 'general'),
            ('final_turnover_invoice_journal_id', 'subscription_file', 'sale'),
            ('final_turnover_refund_journal_id',
             'subscription_file', 'sale_refund'),
            ('overbilled_revenue_provision_journal_id',
             'subscription_file', 'general'),
            ('overbilled_revenue_provision_reversal_journal_id',
             'subscription_file', 'general'),
            ('sales_balance_transfer_journal_id',
             'subscription_file', 'general'),
            ('sales_pl_transfer_journal_id', 'subscription_file', 'general'),
            ('unbilled_revenue_provision_journal_id',
             'subscription_file', 'general'),
            ('unbilled_revenue_provision_reversal_journal_id',
             'subscription_file', 'general'),
        ):
            self.env['ir.property'].create({
                'fields_id': self.env.ref(
                    'subscription_sales.field_%s_%s' % (model_ref, field)
                ).id,
                'name': field,
                'type': 'many2one',
                'value_reference': (
                    'account.journal,%d' % journals_per_type[journal_type].id
                ),
            })

    def test_0100_run_subscription_order_process(self):
        """Run the subscription order processes for a regular sales order.
        """

        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        self._validateSubscriptionSalesOrder(sales_order)
        self._createSubscriptionFile(sales_order)
        self._submitComAgentFeesPurchaseTracker(sales_order)
        self._validateSubscriptionSalesInvoice(sales_order, False)
        self._runPLTransfer(sales_order, final=True)
        self._ensureSubscriptionFileReversed(sales_order)

    def test_0101_run_subscription_order_process_setup_product(self):
        """Run the subscription order process with specificities to check how
        invoices for setup products are handled.
        """

        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        self._validateSubscriptionSalesOrder(sales_order)
        self._createSubscriptionFile(sales_order)
        self._submitComAgentFeesPurchaseTracker(sales_order)

        # The following is similar to the "_validateSubscriptionSalesInvoice"
        # function, except it is only for the setup product.

        subsfile = sales_order.subscription_file_id

        prev_turnover_amount = subsfile.turnover_amount

        # The sales order line we have for a setup product.
        soline = sales_order.order_line.filtered(
            lambda soline: soline.product_id.type == 'setup'
        )
        self.assertTrue(soline)

        # Create an invoice for the setup sales order line.
        invoice = self._createPartialSalesInvoice(soline)

        # Mark the invoice as being an advance and ensure the "invoiced" flag
        # of the sales order line is correctly updated.
        self.assertTrue(soline.invoiced)
        invoice.subscription_setup_type = 'advance'
        self.assertFalse(soline.invoiced)

        # Validate the sales invoice.
        self._validateInvoice(invoice)

        # The amount of lines in the invoice.
        INVLINE_COUNT = 1
        self.assertEqual(len(invoice.invoice_line), INVLINE_COUNT)

        # Check invoice analytics (only credit accounting entries).
        self._checkAnalytics(invoice.move_id.line_id.filtered('credit'), {
            1: subsfile.account_file_id.analytic_id,  # Accounting file.
            2: self.memory.product_test_acode,  # Product test code.
        })

        # Check the subscription file.
        self.assertEqual(subsfile.turnover_amount, (
            prev_turnover_amount + LOW_PRICE
        ))

        # Check the detail table.
        sidetails = subsfile.sales_invoice_detail_ids
        self.assertEqual(len(sidetails), INVLINE_COUNT)

        # Check the balance transfer accounting document.
        accdoc = subsfile.sales_balance_transfer_flow_id.document_ids
        self.assertEqual(accdoc.amount, LOW_PRICE)
        self.assertEqual(accdoc.account_move_id.state, 'posted')

        # Ensure breakdown tables contain a pseudo-entry for the advance.
        for breakdowns in (
            subsfile.com_agent_fee_breakdown_ids,
            subsfile.turnover_breakdown_ids,
        ):
            self.assertEqual(len(breakdowns), 1)
            breakdown = breakdowns[0]
            self.assertFalse(breakdown.period_id)
            self.assertEqual(breakdown.state, 'to_generate')

        # Redo the same but with a balance invoice.
        self._validateInvoice(self._createPartialSalesInvoice(soline))

        # Check the subscription file.
        self.assertEqual(subsfile.turnover_amount, (
            prev_turnover_amount + LOW_PRICE * 2
        ))

        # Pseudo-breakdown entries should have been processed.
        for breakdowns in (
            subsfile.com_agent_fee_breakdown_ids,
            subsfile.turnover_breakdown_ids,
        ):
            self.assertEqual(len(breakdowns), 1)
            breakdown = breakdowns[0]
            self.assertFalse(breakdown.period_id)
            self.assertEqual(breakdown.state, 'generated')

        # The balance transfer flow should contain a reversal document.
        self.assertEqual(
            len(subsfile.sales_balance_transfer_flow_id.document_ids),
            2,
        )

        # Invoice the rest.
        for soline in sales_order.order_line.filtered(
            lambda soline: soline.product_id.type != 'setup'
        ):
            invoice = self._createPartialSalesInvoice(soline)
            self._fillSalesInvoiceBeginningPeriods(invoice)
            self._validateInvoice(invoice)

        # Continue the regular process.
        self._runPLTransfer(sales_order, final=True)
        self._ensureSubscriptionFileReversed(sales_order)

        # Ensure balance transfer accounting entries have been reconciled.
        transfer_flows = (
            subsfile.com_agent_fee_balance_transfer_flow_id |
            subsfile.sales_balance_transfer_flow_id
        )
        for acc_entry in transfer_flows.mapped('document_ids.line_id'):
            self.assertTrue(acc_entry.reconcile_id)

    def test_0190_run_subscription_order_process_canceled(self):
        """Start running the subscription order processes but abort by
        canceling the sales order.
        """

        # Cancel before validation.
        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        sales_order.signal_workflow('cancel')
        self.assertEqual(sales_order.state, 'cancel')
        self.assertFalse(sales_order.subscription_file_id)

        # Cancel after validation but before the subscription file has had time
        # to be created (race check as subscription file creation happens via a
        # queued task).
        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        self._validateSubscriptionSalesOrder(sales_order)
        self._cancelSalesOrder(sales_order)
        self.assertFalse(sales_order.subscription_file_id)
        self._runQueuedTask(sales_order.queue_task_ids[0])
        self.assertFalse(sales_order.invoice_ids)
        self.assertEqual(sales_order.state, 'cancel')
        self.assertFalse(sales_order.subscription_file_id)

        # Cancel after a subscription file creation. Ensure all flows of the
        # subscription file are canceled when the sales order is.
        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        self._validateSubscriptionSalesOrder(sales_order)
        self._createSubscriptionFile(sales_order)
        self._cancelSalesOrder(sales_order)
        self.assertEqual(sales_order.subscription_file_id.state, 'canceled')
        for flow in sales_order.subscription_file_id.flow_ids:
            self.assertEqual(flow.state, 'canceled')

        # Same as above but after a complete subscription order process. The
        # subscription file should remain reversed in this case.
        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        self._validateSubscriptionSalesOrder(sales_order)
        self._createSubscriptionFile(sales_order)
        self._submitComAgentFeesPurchaseTracker(sales_order)
        self._validateSubscriptionSalesInvoice(sales_order, False)
        self._runPLTransfer(sales_order, final=True)
        self._ensureSubscriptionFileReversed(sales_order)
        self._cancelSalesOrder(sales_order)
        self._ensureSubscriptionFileReversed(sales_order)

    def test_0200_run_pl_transfer_6months(self):
        """Run a P&L transfer on an accounting period 6 months from now and
        check the effect on breakdown entries.
        """

        self._cleanSlate()

        # Create subscription files, ready for P&L transfers.
        SUBSFILE_COUNT = 3
        subsfiles = self.env['subscription.file']
        for subsfile_counter in xrange(SUBSFILE_COUNT):
            sales_order = self._makeSalesOrder()
            self._fillSalesOrderLines(sales_order)
            self._validateSubscriptionSalesOrder(sales_order)
            self._createSubscriptionFile(sales_order)
            self._submitComAgentFeesPurchaseTracker(sales_order)
            self._validateSubscriptionSalesInvoice(sales_order, False)
            subsfiles |= sales_order.subscription_file_id

        PRODUCTS_WITH_BREAKDOWN = 4
        BREAKDOWN_KINDS_PER_SUBFILE = 2  # CAF & sales

        # All our checks are going to have to be multiplied by this factor.
        MULTIPLIER = (
            SUBSFILE_COUNT *
            BREAKDOWN_KINDS_PER_SUBFILE *
            PRODUCTS_WITH_BREAKDOWN
        )

        # The amount of breakdown entries expected to be canceled in favor of a
        # fix 6 months later, per product & per subscription file.
        EXPECTED_CANCELED = 6

        # Check breakdown states before we start.
        breakdown_counts_by_state = self._getBreakdownCountsByState()
        self.assertDictEqual(breakdown_counts_by_state, {
            'to_generate': 12 * MULTIPLIER,
        })

        # Find an accounting period 6 months from now.
        period = self.env['account.period'].find(
            dt=(datetime.date.today() + relativedelta(months=6)),
        )

        self._runPLTransfer(period=period)

        GENERATED_PL_TRANSFERS = 1

        # Check breakdown states now that the P&L transfer has run.
        breakdown_counts_by_state = self._getBreakdownCountsByState()
        self.assertDictEqual(breakdown_counts_by_state, {
            'to_generate': (
                (12 - GENERATED_PL_TRANSFERS - EXPECTED_CANCELED) * MULTIPLIER
            ),
            'generated': GENERATED_PL_TRANSFERS * MULTIPLIER,
            'canceled': EXPECTED_CANCELED * MULTIPLIER,
            'fix': 1 * MULTIPLIER,
        })

        # The subscription files should not have been reversed (breakdown
        # entries still remaining).
        for subsfile in subsfiles:
            self.assertEqual(subsfile.state, 'awaiting_reversal')

    def test_0201_run_pl_transfer_final(self):
        """Run the final P&L transfer and check the effect on breakdown
        entries and subscription files.
        Also ensure all related accounting entries have been reconciled.
        """

        # Re-use the subscription files set up in the previous test.
        subsfiles = self.env['subscription.file'].search([])
        SUBSFILE_COUNT = len(subsfiles)

        PRODUCTS_WITH_BREAKDOWN = 4
        BREAKDOWN_KINDS_PER_SUBFILE = 2  # CAF & sales

        # All our checks are going to have to be multiplied by this factor.
        MULTIPLIER = (
            SUBSFILE_COUNT *
            BREAKDOWN_KINDS_PER_SUBFILE *
            PRODUCTS_WITH_BREAKDOWN
        )

        # The amount of breakdown entries expected to be canceled in favor of a
        # fix, taking into account breakdown entries generated in previous
        # tests here; per product & per subscription file.
        EXPECTED_CANCELED = 4

        # Save the current breakdown counts before we start.
        prev_breakdown_counts_by_state = self._getBreakdownCountsByState()

        self._runPLTransfer(final=True)

        GENERATED_PL_TRANSFERS = 1

        # Check breakdown states now that the P&L transfer has run.
        breakdown_counts_by_state = self._getBreakdownCountsByState()
        self.assertDictEqual(breakdown_counts_by_state, {
            'generated': (
                prev_breakdown_counts_by_state['generated'] +
                GENERATED_PL_TRANSFERS * MULTIPLIER
            ),
            'canceled': (
                prev_breakdown_counts_by_state['canceled'] +
                EXPECTED_CANCELED * MULTIPLIER
            ),
            'fix': prev_breakdown_counts_by_state['fix'] + 1 * MULTIPLIER,
        })

        for subsfile in subsfiles:
            # The subscription files should have been reversed.
            self.assertEqual(subsfile.state, 'reversed')

            # Ensure all related accounting entries have been reconciled.
            transfer_flows = (
                subsfile.com_agent_fee_balance_transfer_flow_id |
                subsfile.sales_balance_transfer_flow_id
            )
            for acc_entry in transfer_flows.mapped('document_ids.line_id'):
                self.assertTrue(acc_entry.reconcile_id)

    def test_0300_sales_invoice_end_periods(self):
        """Attempt to create and validate a production order; check the risen
        exception on invoices when end periods are different.
        """

        # Create a subscription sales order and attempt to trigger an
        # exception.
        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        self._validateSubscriptionSalesOrder(sales_order)
        self._createSubscriptionFile(sales_order)
        self._submitComAgentFeesPurchaseTracker(sales_order)

        with self.assertRaises(openerp.exceptions.Warning):
            self._triggerEndPeriodsExceptionSubscriptionSalesInvoice(
                sales_order
            )

    def test_0400_validate_production_orders(self):
        """Create and validate production orders; check effects on subscription
        files.
        """

        # Create a regular subscription sales order.
        sales_order = self._makeSalesOrder()
        self._fillSalesOrderLines(sales_order)
        self._validateSubscriptionSalesOrder(sales_order)
        self._createSubscriptionFile(sales_order)
        self._submitComAgentFeesPurchaseTracker(sales_order)
        self._validateSubscriptionSalesInvoice(sales_order, False)
        self._runPLTransfer(sales_order, final=True)
        self._ensureSubscriptionFileReversed(sales_order)

        subsfile = sales_order.subscription_file_id

        # Regular production order, on the current month.
        self._validateProductionOrder(sales_order, quantities=False)

        # Do the same again and ensure we get blocked (already
        # appreciated).
        with self.assertRaises(openerp.exceptions.Warning):
            self._validateProductionOrder(sales_order, quantities=False)

        # Production orders for the other months.
        for month_delta in xrange(1, 12):
            self._validateProductionOrder(
                sales_order, quantities=False, month_delta=month_delta
            )

        # At this point the flow should have been reversed and we should
        # have a final turnover invoice.
        ftinvoice = subsfile.final_turnover_invref_id
        self.assertEqual(
            ftinvoice.amount_untaxed, APPRECIATION_AMOUNT * 12
        )
        self.assertEqual(
            subsfile.unbilled_revenue_provision_flow_state, 'reversed'
            )

    def test_0410_validate_production_orders_with_adjustments(self):
        """Create and validate production orders; check effects on subscription
        files.
        """

        for quantities in [QUANTITIES, QUANTITIES2]:

            # Create a regular subscription sales order.
            sales_order = self._makeSalesOrder()
            self._fillSalesOrderLines(sales_order)
            self._validateSubscriptionSalesOrder(sales_order)
            self._createSubscriptionFile(sales_order)
            self._submitComAgentFeesPurchaseTracker(sales_order)
            self._validateSubscriptionSalesInvoice(sales_order, quantities)
            self._runPLTransfer(sales_order, final=True)
            self._ensureSubscriptionFileReversed(sales_order)

            subsfile = sales_order.subscription_file_id

            # Regular production order, on the current month.
            self._validateProductionOrder(sales_order, quantities=quantities)

            # Do the same again and ensure we get blocked (already
            # appreciated).

            with self.assertRaises(openerp.exceptions.Warning):
                self._validateProductionOrder(
                    sales_order, quantities=quantities
                )

            # Production orders for the other months.
            for month_delta in xrange(1, 12):
                self._validateProductionOrder(
                    sales_order, quantities=quantities, month_delta=month_delta
                )

            # At this point the flow should have been reversed and we should
            # have a final turnover invoice.
            ftinvoice = subsfile.final_turnover_invref_id
            self.assertEqual(
                ftinvoice.amount_untaxed, APPRECIATION_AMOUNT * 12
            )
            self.assertEqual(
                subsfile.unbilled_revenue_provision_flow_state, 'reversed'
            )

    def test_0500_check_invoice_expected_amount(self):
        """Ensure the check preventing invoice validation when its total amount
        does not correspond to its expected amount works as expected.
        """

        account = self.memory.normal_account
        partner = self.memory.partner_com_agent
        product = self.memory.service_product

        def invLineTester(test_instance, asked_value, recorded_value):
            test_instance.assertEqual(len(recorded_value), len(asked_value))

        def makeInvoice(expected_amount, line_amount):
            invoice_values = {
                'account_id': account.id,
                'expected_total_amount': expected_amount,
                'invoice_line': [(0, 0, {
                    'name': genUuid(),
                    'price_unit': line_amount,
                    'product_id': product.id,
                })],
                'partner_id': partner.id,
                'type': 'in_invoice',
            }
            invoice, = self.createAndTest(
                'account.invoice', [invoice_values],
                custom_testers={'invoice_line': invLineTester},
            )
            return invoice

        # Control: Without an expected amount, validation works fine.
        invoice = makeInvoice(False, 42.0)
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

        # Control: With an expected amount, and a valid invoice, validation
        # works fine.
        invoice = makeInvoice(42.0, 42.0)
        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

        # With an expected amount and an invalid invoice, validation should be
        # blocked.
        invoice = makeInvoice(21.0, 42.0)
        self.assertEqual(invoice.state, 'draft')
        with self.assertRaises(openerp.exceptions.Warning):
            invoice.signal_workflow('invoice_open')

    def _cleanSlate(self):
        """Remove elements created so far to start from a clean slate.
        """

        self.env.cr.execute(
            'TRUNCATE sale_order CASCADE;'
            'TRUNCATE account_voucher CASCADE;'
            'TRUNCATE account_invoice CASCADE;'
            'TRUNCATE account_move CASCADE;'
            'TRUNCATE purchase_order CASCADE;'
            'TRUNCATE subscription_file CASCADE;'
            'TRUNCATE account_payment_batch CASCADE;'
        )
        self.env.invalidate_all()

    def _makeSalesOrder(self):
        """Create a subscription sales order.

        :return: The sales order.
        :rtype: Odoo "sale.order" record set.
        """

        sales_order, = self.createAndTest(
            'sale.order',
            [{
                'com_agent_partner_id': self.memory.partner_com_agent.id,
                'is_for_subscription': True,
                'partner_id': self.memory.partner_buyer.id,
            }],
        )

        # Specific creation date to ensure it is taken into account later on.
        self.env.cr.execute(
            'UPDATE sale_order SET create_date = %s WHERE id = %s',
            ('%s-01-01 01:00:00' % self.memory.current_year, sales_order.id),
        )
        sales_order.refresh()

        return sales_order

    def _fillSalesOrderLines(self, sales_order):
        """Add subscription lines into the specified sales order.
        """

        self.createAndTest(
            'sale.order.line',
            [
                {
                    'name': genUuid(),
                    'order_id': sales_order.id,
                    'price_unit': SUBS_PRICE,
                    'product_uom_qty': QUANTITY,
                    'product_id': (
                        self.memory.subscription_product_theo_track.id
                    ),
                    'subscription_frequency_id': self.memory.freqtable.id,
                }, {
                    'name': genUuid(),
                    'order_id': sales_order.id,
                    'price_unit': SUBS_PRICE,
                    'product_uom_qty': QUANTITY,
                    'product_id': (
                        self.memory.subscription_product_actu_track.id
                    ),
                    'subscription_frequency_id': self.memory.freqtable.id,
                },
                {
                    'name': genUuid(),
                    'order_id': sales_order.id,
                    'price_unit': SUBS_PRICE,
                    'product_uom_qty': QUANTITY,
                    'product_id': (
                        self.memory.subscription_product_actu_track2.id
                    ),
                    'subscription_frequency_id': self.memory.freqtable.id,
                },
                {
                    'name': genUuid(),
                    'order_id': sales_order.id,
                    'price_unit': LOW_PRICE,
                    'product_id': self.memory.setup_product.id,
                },
                {
                    'name': genUuid(),
                    'order_id': sales_order.id,
                    'price_unit': LOW_PRICE,
                    'product_id': self.memory.maintenance_product.id,
                },
                {
                    'name': genUuid(),
                    'order_id': sales_order.id,
                    'price_unit': LOW_PRICE,
                    'product_id': self.memory.service_product.id,
                },
            ],
        )

    def _validateSubscriptionSalesOrder(self, sales_order):
        """Validate the sales order; this should generate:
        - A subscription file creation task.
        """

        sales_order.action_button_confirm()

        self.assertEqual(len(sales_order.queue_task_ids), 1)

    def _createSubscriptionFile(self, sales_order):
        """Run the subscription file creation task; this should generate:
        - A subscription file.
        - A commercial agent fee purchase order.
        """

        # The amount of lines in the sales order.
        SOLINE_COUNT = 6
        self.assertEqual(len(sales_order.order_line), SOLINE_COUNT)

        # Fetch the queued task and run it.
        self._runQueuedTask(sales_order.queue_task_ids[0])

        # Check the sales order.
        self.assertEqual(sales_order.state, 'manual')

        # Check the subscription file.
        subsfile = sales_order.subscription_file_id
        self.assertEqual(subsfile.sales_order_id.id, sales_order.id)
        self.assertEqual(subsfile.turnover_amount, 0.0)  # No invoice yet.
        self.assertEqual(subsfile.state, 'awaiting_reversal')

        # Check detail tables.
        cafdetails = subsfile.com_agent_fee_detail_ids
        sodetails = subsfile.sales_order_detail_ids
        self.assertEqual(len(sodetails), SOLINE_COUNT)
        self.assertEqual(len(cafdetails), SOLINE_COUNT)

        # Check various amounts.
        CAF_AMOUNT = (
            MONTH_COUNT * SUBS_PRICE * SUBS_FEE_RATE * QUANTITY / 100.0 * 3 +
            LOW_PRICE * LOW_FEE_RATE / 100.0 * 3
        )
        self.assertEqual(subsfile.com_agent_fee_theoretical_amount, CAF_AMOUNT)
        self.assertEqual(sum(cafdetails.mapped('theoretical_amount')),
                         CAF_AMOUNT)
        self.assertEqual(subsfile.com_agent_fee_invoiced_amount, 0.0)
        self.assertEqual(subsfile.com_agent_fee_invoiced_delta, 0.0)
        self.assertEqual(subsfile.com_agent_fee_adjustment_invoiced_amount,
                         0.0)
        self.assertEqual(subsfile.com_agent_fee_adjustment_invoiced_delta, 0.0)

        # Check the provision accounting document.
        provision_accdoc = (
            subsfile.com_agent_fee_provision_flow_id.document_ids
        )
        self.assertEqual(provision_accdoc.account_move_id.state, 'posted')
        self.assertEqual(provision_accdoc.date,
                         '%s-01-01' % self.memory.current_year)
        self.assertTrue(provision_accdoc.period_id.name.endswith(
            '01/%s' % self.memory.current_year,
        ))

        # Check the commercial agent fee purchase order. It should have a total
        # amount equalling the fee amount, split across as many lines as there
        # are in the sales order.
        cafpo = sales_order.com_agent_fee_purchase_order_id
        self.assertEqual(cafpo.amount_untaxed, CAF_AMOUNT)
        self.assertEqual(len(cafpo.order_line), SOLINE_COUNT)
        self.assertEqual(cafpo.partner_id.id, self.memory.partner_com_agent.id)
        self.assertEqual(cafpo.state, 'approved')
        self.assertEqual(cafpo.subscription_file_id.id, subsfile.id)

        # We should have no invoice so far (invoice generation is manual and
        # comes later on in the process).
        self.assertEqual(len(sales_order.invoice_ids), 0)

    def _validateSubscriptionSalesInvoice(self, sales_order, quantities):
        """Generate and validate an accounting invoice for the specified sales
        order. Check:
        - The sales order.
        - The generated invoice.
        - The subscription file.
        - The commercial agent fee adjustment.
        """

        PRODUCTS_WITH_BREAKDOWN = 4
        PRODUCTS_WITH_TURNOVER_TRACKING = 2
        TOTAL_QUANTITY = sum(quantities) if quantities else QUANTITY * 3

        subsfile = sales_order.subscription_file_id

        prev_turnover_amount = subsfile.turnover_amount

        # Create and validate a sales invoice.
        sales_order.manual_invoice()
        self.assertEqual(len(sales_order.invoice_ids), 1)
        invoice = sales_order.invoice_ids[0]
        self._fillSalesInvoiceBeginningPeriods(invoice)
        if quantities:
            self._modifySalesInvoiceContent(invoice, quantities)
        self._validateInvoice(invoice)

        # The amount of lines in the invoice.
        INVLINE_COUNT = 6
        self.assertEqual(len(invoice.invoice_line), INVLINE_COUNT)

        # Check ending dates that should have been filled in the invoice.
        self.assertEqual(len(invoice.invoice_line.filtered(
            lambda invline: invline.end_period_id
        )), PRODUCTS_WITH_BREAKDOWN)

        # Check the sales order.
        self.assertEqual(sales_order.state, 'progress')

        # Check invoice analytics (only credit accounting entries).
        self._checkAnalytics(invoice.move_id.line_id.filtered('credit'), {
            1: subsfile.account_file_id.analytic_id,  # Accounting file.
            2: self.memory.product_test_acode,  # Product test code.
        })

        # Check the subscription file.
        self.assertEqual(subsfile.turnover_amount, (
            prev_turnover_amount + MONTH_COUNT * SUBS_PRICE *
            TOTAL_QUANTITY +
            LOW_PRICE * 3
        ))

        # Check the detail table.
        sidetails = subsfile.sales_invoice_detail_ids
        self.assertEqual(len(sidetails), INVLINE_COUNT)

        # Check the balance transfer accounting document.
        accdoc = subsfile.sales_balance_transfer_flow_id.document_ids
        # The amount should only concern setup & maintenance products.
        SBTRANSFER_AMOUNT = \
            MONTH_COUNT * SUBS_PRICE * TOTAL_QUANTITY + LOW_PRICE
        self.assertEqual(accdoc.amount, SBTRANSFER_AMOUNT)
        self.assertEqual(accdoc.account_move_id.state, 'posted')
        self.assertEqual(subsfile.sales_balance_transfer_provision_amount,
                         SBTRANSFER_AMOUNT)
        self.assertEqual(subsfile.sales_balance_transfer_reversal_amount, 0.0)
        self.assertEqual(subsfile.sales_balance_transfer_net_amount,
                         SBTRANSFER_AMOUNT)

        # Check breakdown tables.
        cafbreakdown = subsfile.com_agent_fee_breakdown_ids
        tobreakdown = subsfile.turnover_breakdown_ids
        self.assertEqual(len(cafbreakdown), 12 * PRODUCTS_WITH_BREAKDOWN)
        self.assertEqual(len(tobreakdown), 12 * PRODUCTS_WITH_BREAKDOWN)

        # Check turnover tracking details.
        totracking = subsfile.turnover_ids
        self.assertEqual(len(totracking), 12 * PRODUCTS_WITH_TURNOVER_TRACKING)

        # Check the commercial agent fee adjustment.
        if quantities:
            self._checkComAgentFeeAdjustment(sales_order, quantities)
            self._submitComAgentFeesAdjustmentPurchaseTracker(
                sales_order, quantities
            )

    def _checkComAgentFeeAdjustment(self, sales_order, quantities):
        """Check the commercial agent fee adjustment:
            - purchase order generation;
            - provision.
        """

        SUBSCRIPTIONS_COUNT = 3

        subsfile = sales_order.subscription_file_id
        invoice = sales_order.invoice_ids[0]
        invlines = invoice.invoice_line.filtered(
            lambda r: r.product_id.type == 'subscription'
        )

        # Purchase order generation.
        adjustment = subsfile.com_agent_fee_adjustment_purchase_order_id

        self.assertEqual(
            len(adjustment.order_line), SUBSCRIPTIONS_COUNT
        )
        subs_lines = adjustment.order_line
        for line, invl, qty in zip(subs_lines, invlines, quantities):
            self.assertEqual(line.product_id.id, invl.product_id.id)
            self.assertEqual(
                line.price_subtotal,
                (
                    (qty - QUANTITY) * MONTH_COUNT * SUBS_PRICE *
                    SUBS_FEE_RATE / 100.0
                )
            )

        self.assertEqual(
            adjustment.amount_total,
            (
                (sum(quantities) - 3 * QUANTITY) * MONTH_COUNT *
                SUBS_PRICE * SUBS_FEE_RATE / 100.0
            )
        )

    def _checkComAgentFeeAdjustmentProvision(self, sales_order, quantities):
        """Check the contents of the accounting documents created for
        commercial agent fee adjustments, in the case of balance transfer and
        provision generation and reversal.
        """

        SUBSCRIPTIONS_COUNT = 3

        subsfile = sales_order.subscription_file_id
        invoice = sales_order.invoice_ids[0]
        invlines = invoice.invoice_line.filtered(
            lambda r: r.product_id.type == 'subscription'
        ).sorted(key=lambda r: r.product_id.id)

        # Provision generation and reversal.
        flows = [
            (
                'balance_transfer', 1, 'Transfert au bilan',
                SUBSCRIPTIONS_COUNT * 2
            ),
            ('provision', 2, 'FNP', 2)]
        f = 0
        for flow, nb_moves, label, nb_lines in flows:

            f += 1

            moves = (
                getattr(
                    subsfile,
                    'com_agent_fee_adjustment_%s_flow_id' % flow
                ).
                document_ids
            )
            self.assertEqual(len(moves), nb_moves)

            for move in moves:
                movlines = move.line_id.sorted(key=lambda r: r.product_id.id)
                self.assertEqual(len(movlines), nb_lines)

                if f == 1:
                    for i, line in enumerate(movlines):

                        j = i / 2
                        invl = invlines[j]

                        debit = line.debit
                        credit = line.credit

                        self.assertEqual(
                            line.product_id.id, invl.product_id.id
                        )
                        self.assertEqual(
                            debit if i % 2 == 0 else credit,
                            (
                                abs(quantities[j] - QUANTITY) * MONTH_COUNT *
                                SUBS_PRICE * SUBS_FEE_RATE / 100.0
                            )
                        )
                        self.assertEqual(credit if i % 2 == 0 else debit, 0)
                else:
                    for i, line in enumerate(movlines):

                        credit = (
                            line.debit if quantities == QUANTITIES
                            else line.credit
                        )
                        debit = (
                            line.credit if quantities == QUANTITIES
                            else line.debit
                        )

                        self.assertEqual(
                            debit if i == 0 else credit,
                            (
                                abs(sum(quantities) - 3 * QUANTITY) *
                                MONTH_COUNT *
                                SUBS_PRICE * SUBS_FEE_RATE / 100.0
                            )
                        )
                        self.assertEqual(credit if i == 0 else debit, 0)

    def _triggerEndPeriodsExceptionSubscriptionSalesInvoice(self, sales_order):
        """Generate and attempts to validate an accounting invoice for the
        specified sales order. Check, if an exception is risen, when filling
        different end periods.
        """

        # Create and attempts to validate a sales invoice.
        sales_order.manual_invoice()
        self.assertEqual(len(sales_order.invoice_ids), 1)
        invoice = sales_order.invoice_ids[0]
        self._fillSalesInvoiceWrongBeginningPeriods(invoice)
        self._validateInvoice(invoice)

    def _submitComAgentFeesPurchaseTracker(self, sales_order):
        """Create a purchase tracker for commercial agent fees and validate its
        invoice.
        """

        subsfile = sales_order.subscription_file_id
        purchase_order = sales_order.com_agent_fee_purchase_order_id

        # Submit 1 tracker with the commercial agent fee amount.
        TRACKER_AMOUNT = (
            MONTH_COUNT * SUBS_PRICE * SUBS_FEE_RATE / 100.0 * 3 * QUANTITY +
            LOW_PRICE * LOW_FEE_RATE / 100.0 * 3
        )

        tracker = self._createPurchaseTracker(
            purchase_order, 'com_agent_fees', 'in_invoice', TRACKER_AMOUNT,
        )
        self._validatePurchaseTracker(purchase_order, tracker)

        # The invoice generated when the tracker got validated.
        invoice = tracker.invoice_id
        self.assertTrue(invoice)
        self.assertEqual(invoice.expected_total_amount, TRACKER_AMOUNT)

        # Check pre-validation effects on the subscription file.
        self.assertEqual(subsfile.com_agent_fee_invoiced_amount,
                         TRACKER_AMOUNT)
        self.assertEqual(subsfile.com_agent_fee_invoiced_delta, 0.0)
        prev_turnover_amount = subsfile.turnover_amount

        # Validate the invoice.
        self._validateInvoice(invoice)

        # Check post-validation effects on the subscription file.
        self.assertEqual(subsfile.com_agent_fee_invoice_id.id, invoice.id)
        self.assertEqual(subsfile.com_agent_fee_invoiced_amount,
                         TRACKER_AMOUNT)
        self.assertEqual(subsfile.com_agent_fee_invoiced_delta, 0.0)
        # The following is on purpose: No change of the turnover amount.
        self.assertEqual(subsfile.turnover_amount, prev_turnover_amount)

        # Check invoice analytics (only debit accounting entries).
        self._checkAnalytics(invoice.move_id.line_id.filtered('debit'), {
            1: subsfile.account_file_id.analytic_id,  # Accounting file.
            2: self.memory.product_test_acode,  # Product test code.
        })

        # Check effects on the subscription file flow.
        flow = subsfile.com_agent_fee_provision_flow_id
        self.assertEqual(flow.state, 'reversed')
        # The flow should contain 2 documents: provision, reversal.
        self.assertEqual(len(flow.document_ids), 2)

        # Check balance transfers.
        flow = subsfile.com_agent_fee_balance_transfer_flow_id
        self.assertEqual(flow.state, 'awaiting_reversal')
        # The flow should contain 1 documents: initial balance transfer.
        self.assertEqual(len(flow.document_ids), 1)

        # No breakdown yet as sales invoices come later on.
        self.assertFalse(subsfile.com_agent_fee_breakdown_ids)

        # Ensure the tracker has successfully registered onto the subscription
        # file.
        self.assertEqual(len(subsfile.purchase_tracker_ids), 1)
        self.assertEqual(subsfile.purchase_tracker_count, 1)

    def _submitComAgentFeesAdjustmentPurchaseTracker(
        self, sales_order, quantities
    ):
        """Create a purchase tracker for commercial agent fees adjustment and
        validate its invoice.
        """

        subsfile = sales_order.subscription_file_id
        purchase_order = subsfile.com_agent_fee_adjustment_purchase_order_id

        # Submit 1 tracker with the commercial agent fee amount.
        TRACKER_AMOUNT = abs(
            MONTH_COUNT * SUBS_PRICE * SUBS_FEE_RATE / 100.0 *
            (3 * QUANTITY - sum(quantities))
        )

        TRACKER_DELTA = 2 * TRACKER_AMOUNT if quantities == QUANTITIES else 0.0

        tracker = self._createPurchaseTracker(
            purchase_order, 'com_agent_fee_adjustments', 'in_invoice',
            TRACKER_AMOUNT,
        )
        self._validatePurchaseTracker(purchase_order, tracker)

        # The invoice generated when the tracker got validated.
        invoice = tracker.invoice_id
        self.assertTrue(invoice)
        self.assertEqual(invoice.expected_total_amount, TRACKER_AMOUNT)

        # Check pre-validation effects on the subscription file.
        self.assertEqual(
            subsfile.com_agent_fee_adjustment_invoiced_amount, TRACKER_AMOUNT
        )
        self.assertEqual(
            subsfile.com_agent_fee_adjustment_invoiced_delta,
            TRACKER_DELTA
        )

        # Validate the invoice.
        self._validateInvoice(invoice)

        # Check post-validation effects on the subscription file.
        self.assertEqual(
            subsfile.com_agent_fee_adjustment_invoice_id.id, invoice.id)
        self.assertEqual(subsfile.com_agent_fee_adjustment_invoiced_amount,
                         TRACKER_AMOUNT)
        self.assertEqual(
            subsfile.com_agent_fee_adjustment_invoiced_delta, TRACKER_DELTA
        )

        # Check invoice analytics (only debit accounting entries).
        self._checkAnalytics(invoice.move_id.line_id.filtered('debit'), {
            1: subsfile.account_file_id.analytic_id,  # Accounting file.
            2: self.memory.product_test_acode,  # Product test code.
        })

        # Check effects on the subscription file flow.
        flow = subsfile.com_agent_fee_adjustment_provision_flow_id
        self.assertEqual(flow.state, 'reversed')
        # The flow should contain 2 documents: provision, reversal.
        self.assertEqual(len(flow.document_ids), 2)
        # Check the accounting documents.
        self._checkComAgentFeeAdjustmentProvision(sales_order, quantities)

        # Check balance transfers.
        flow = subsfile.com_agent_fee_adjustment_balance_transfer_flow_id
        self.assertEqual(flow.state, 'awaiting_reversal')
        # The flow should contain 1 documents: initial balance transfer.
        self.assertEqual(len(flow.document_ids), 1)

        # No breakdown yet as sales invoices come later on.
        self.assertTrue(subsfile.com_agent_fee_adjustment_breakdown_ids)

        # Ensure the tracker has successfully registered onto the subscription
        # file.
        self.assertEqual(len(subsfile.purchase_tracker_ids), 2)
        self.assertEqual(subsfile.purchase_tracker_count, 2)

    def _runPLTransfer(
        self, sales_orders=None, period=None, final=False,
        expect_sales_flow=True, expect_caf_flow=True,
    ):
        """Run a P&L transfer for the current accounting period. Expect only
        the specified sales orders.

        :param sales_orders: The sales orders expected to be affected.
        Optional, in which case the effect on sales orders / subscription files
        will not be checked.
        :type sales_orders: Odoo "sale.oder" record set.

        :param period: A specific period to run the P&L transfer on. Optional,
        in which case the transfer will run on the current period.
        :type period: Odoo "account.period" record set.

        :param final: Run the "final" P&L transfer, 12 months from now.
        Overrides the "period" parameter.

        :param expect_sales_flow: Whether a sales flow is to be expected filled
        with at least 2 accounting documents on each subscription file.

        :param expect_caf_flow: Whether a commercial agent fee flow is to be
        expected filled with at least 2 accounting documents on each
        subscription file.
        """

        if final:
            # During these tests, simply assume the final P&L transfer is
            # scheduled 11 months from now.
            # Find an accounting period 6 months from now.
            period = self.env['account.period'].find(
                dt=(datetime.date.today() + relativedelta(months=11)),
            )

        # Simulate a P&L transfer dialog box,
        dlg_values = {}
        if period is not None:
            dlg_values['period_id'] = period.id

        dlg, = self.createAndTest('subscription.monthly_transfer_dlg',
                                  [dlg_values])

        # Imitate "_handle_period_id_change", which is not called here.
        dlg.breakdown_ids = self.env['subscription.breakdown'].search([
            ('period_id', '=',
             period.id if period is not None else dlg.period_id.id),
            ('state', '=', 'to_generate'),
        ])

        dlg_ret = dlg.apply()

        # Ensure the affected breakdown items have been updated.
        for breakdown in dlg.breakdown_ids:
            self.assertEqual(breakdown.state, 'generated')

        # When we know affected sales orders, also check subscription files.
        if sales_orders is None:
            return
        subsfiles = sales_orders.mapped('subscription_file_id')

        # The dialog box returns an action to display affected subscription
        # files; pick them from there.
        # 'domain': [('id', 'in', subsfiles.ids)],
        subsfile_ids = dlg_ret['domain'][0][2]
        self.assertSetEqual(set(subsfile_ids), set(subsfiles.ids))

        # Check generated accounting documents.
        for subsfile in subsfiles:
            # The flows should contain > 1 documents: initial balance transfer
            # & at least 1 P&L transfer.
            if expect_sales_flow:
                flow = subsfile.sales_balance_transfer_flow_id
                self.assertGreater(len(flow.document_ids), 1)
            if expect_caf_flow:
                flow = subsfile.com_agent_fee_balance_transfer_flow_id
                self.assertGreater(len(flow.document_ids), 1)

    def _validateProductionOrder(
        self, sales_order, quantities=False, month_delta=0
    ):
        """Create and validate a production order for the specified
        subscription sales order; check effects on the subscription file.

        :param month_delta: An amount of months, from to the current month. To
        simulate a production order in the future.
        :type month_delta: Integer.

        :type sales_order: Odoo "sale.order" record set.
        """

        subsfile = sales_order.subscription_file_id
        partner = sales_order.partner_id

        # Create a production order by calling the helper provided on the
        # subscription file.
        podefaults = subsfile.create_production_order()['context']
        self.assertEqual(podefaults['default_subscription_file_id'],
                         subsfile.id)
        self.assertEqual(podefaults['default_type'],
                         'subscription_production_order')
        polines = podefaults['default_order_line']  # [(0, 0, {...})]
        self.assertEqual(len(polines), 2)
        self.assertEqual(polines[0][2]['product_id'],
                         self.memory.subscription_product_actu_track.id)
        self.assertEqual(polines[0][2]['price_unit'], SUBS_PRICE)
        self.assertEqual(
            polines[0][2]['product_qty'],
            QUANTITY if not quantities else quantities[1]
        )

        # Apply our quantity delta.
        polines[0][2]['product_qty'] += EXPECTED_QDELTA

        def poLineTester(test_instance, asked_value, recorded_value):
            test_instance.assertEqual(len(recorded_value), len(asked_value))

        prod_order, = self.createAndTest(
            'purchase.order',
            [{
                'date_order': (
                    datetime.datetime.now() + relativedelta(months=month_delta)
                ).strftime(DEFAULT_SERVER_DATETIME_FORMAT),
                'order_line': polines,
                'partner_id': partner.id,
                'pricelist_id': partner.property_product_pricelist_purchase.id,
                'subscription_file_id': subsfile.id,
                'type': podefaults['default_type'],
            }],
            custom_testers={'order_line': poLineTester},
        )

        # Cache pre-validation data.
        prev_turnover_appreciation = subsfile.turnover_appreciation or 0.0

        # Validate the production order.
        self._validatePurchaseOrder(prod_order)

        # Check effects on the subscription file.
        self.assertTrue(subsfile.unbilled_revenue_provision_flow_id)
        self.assertFalse(subsfile.overbilled_revenue_provision_flow_id)
        self.assertEqual(subsfile.turnover_appreciation,
                         prev_turnover_appreciation + APPRECIATION_AMOUNT)

        # Check effects on the affected turnover entry.
        turnover_entry = subsfile.turnover_ids[month_delta]
        self.assertEqual(turnover_entry.quantity_delta, EXPECTED_QDELTA)
        self.assertEqual(turnover_entry.appreciation, APPRECIATION_AMOUNT)
        self.assertEqual(turnover_entry.state, 'appreciated')

    def _ensureSubscriptionFileReversed(self, sales_order):
        """Ensure the subscription file linked to the specified sales order has
        been fully reversed.
        """

        self.assertEqual(sales_order.subscription_file_id.state, 'reversed')

    def _cancelSalesOrder(self, sales_order):
        """Cancel the specified sales order, along with its client invoices.

        :type sales_order: Odoo "sale.order" record set.
        """

        invoices = sales_order.invoice_ids

        # When the "hr_timesheet_invoice" addon is installed, it adds a link to
        # invoice analytics that we need to clear out first.
        try:
            (
                self.env['account.analytic.line']
                .search([('invoice_id', 'in', invoices.ids)])
                .unlink()
            )
        except (KeyError, ValueError):
            pass  # Do nothing when no Odoo analytics.

        # First cancel invoices by going through a dialog box.
        dlg_action = invoices.wizard_invoice_cancel()
        if isinstance(dlg_action, dict):
            (
                self.env[dlg_action['res_model']]
                .with_context(dlg_action['context'])
                .create({}).create_reversals()
            )

        # Invoices have been canceled; now cancel the sales order.
        sales_order.action_cancel()
        self.assertEqual(sales_order.state, 'cancel')

    def _checkAnalytics(self, records, expected_analytics):
        """Ensure the specified records contain expected analytics.

        :param records: An Odoo record set; may contain multiple elements.

        :param expected_analytics: Dictionary mapping analytic ordering
        integers to expected Odoo "analytic.code" record sets.
        """

        for record in records:
            for ordering, acode in expected_analytics.iteritems():
                self.assertEqual(getattr(record, 'a%d_id' % ordering).id,
                                 acode.id)

    def _createPartialSalesInvoice(self, solines):
        """Create a sales invoice restricted to the specified sales order
        lines.

        :type solines: Odoo "sale.order.line" record set.
        :rtype: Odoo "account.invoice" record set.
        """

        invoice_ids = self.env['sale.order.line.make.invoice'].with_context({
            'active_ids': solines.ids,
            'active_model': 'sale.order.line',
            'open_invoices': True,
        }).make_invoices()['res_id']
        self.assertTrue(invoice_ids)
        return self.env['account.invoice'].browse(invoice_ids)

    def _createPurchaseTracker(
        self, purchase_order, subscription_type, invoice_type, invoice_amount,
    ):
        """Create a purchase tracker for the specified purchase order.
        """

        def spoTester(test_instance, asked_value, recorded_value):
            test_instance.assertEqual(recorded_value.id, asked_value[0][1])

        purchase_tracker, = self.createAndTest(
            'purchase_tracker.tracker',
            [{
                'invoice_date': openerp.fields.Date.today(),
                'invoice_type': invoice_type,
                'partner_id': purchase_order.partner_id.id,
                'reception_date': openerp.fields.Date.today(),
                'subscription_invoice_amount': invoice_amount,
                'subscription_purchase_order_ids': [(4, purchase_order.id)],
                'subscription_supplier_id': purchase_order.partner_id.id,
                'subscription_type': subscription_type,
            }],
            custom_testers={
                'subscription_purchase_order_ids': spoTester,
            },
        )

        return purchase_tracker

    def _fillSalesInvoiceBeginningPeriods(self, invoice):
        """Fill beginning periods of the specified sales invoice. Set them all
        to the current period.

        :type invoice: Odoo "account.invoice" record set.
        """

        current_period = self.env['account.period'].find()
        for invline in invoice.invoice_line:
            if invline.product_id.requires_breakdown:
                invline.begin_period_id = current_period

    def _fillSalesInvoiceWrongBeginningPeriods(self, invoice):
        """Fill wrong beginning periods for the specified sales invoice. Set
        them all to result on different end periods.

        :type invoice: Odoo "account.invoice" record set.
        """

        period = self.env['account.period'].find()

        current_day = datetime.date.today()

        dates = [
            current_day.replace(month=rnd)
            for rnd in random.sample([a + 1 for a in range(12)], 2)
        ]

        periods = [
            self.env['account.period'].search(
                [('date_start', '<', date)],
                limit=1, order='date_start DESC',
            ) for date in dates
        ]

        for i, invline in enumerate(invoice.invoice_line):
            if invline.product_id.requires_breakdown:

                invline.begin_period_id = periods[i] if i < 2 else period

    def _modifySalesInvoiceContent(self, invoice, quantities):
        """Modify the content of the specified sales invoice. Set them in order
        to make compute a commercial agent fee adjustment.

        :type invoice: Odoo "account.invoice" record set.
        """

        subscription_lines = invoice.invoice_line.filtered(
            lambda r: r.product_id.type == 'subscription'
        )

        for line, quantity in zip(subscription_lines, quantities):
            line.quantity = quantity

    def _getBreakdownCountsByState(self):
        """Provide breakdown counts, grouped by breakdown state.

        :rtype: Dictionary.
        """

        breakdowns = self.env['subscription.breakdown'].search([])
        breakdown_counts_by_state = defaultdict(int)
        for breakdown in breakdowns:
            breakdown_counts_by_state[breakdown.state] += 1
        return breakdown_counts_by_state

    def _runQueuedTask(self, queued_task):
        """Run the specified queued task.

        :type queued_task: Odoo "queue.task" record set.
        """

        self.assertEqual(queued_task.state, 'todo')
        queued_task.run()
        self.assertEqual(queued_task.state, 'done')

    def _validateInvoice(self, invoice):
        """Validate the specified invoice.
        """

        self.assertEqual(invoice.state, 'draft')
        invoice.signal_workflow('invoice_open')
        self.assertEqual(invoice.state, 'open')

    def _validatePurchaseOrder(self, purchase_order):
        """Validate the specified purchase order.
        """

        self.assertEqual(purchase_order.state, 'draft')
        purchase_order.signal_workflow('purchase_confirm')
        self.assertEqual(purchase_order.state, 'confirmed')
        purchase_order.action_purchase_approve()
        self.assertEqual(purchase_order.state, 'approved')

    def _validatePurchaseTracker(self, purchase_order, tracker):
        """Validate the specified purchase tracker (just submitting it is
        enough as it is for subscriptions).
        """

        self.assertEqual(tracker.state, 'draft')
        self.assertEqual(purchase_order.status, 'open')

        tracker.signal_workflow('submit')

        # Check the purchase tracker.
        self.assertEqual(tracker.state, 'approved')

        # Check the purchase order.
        self.assertEqual(purchase_order.status, 'total')

        # Check the invoice.
        invoice = tracker.invoice_id
        self.assertEqual(invoice.subscription_type, tracker.subscription_type)
        self.assertEqual(invoice.tracker_id.id, tracker.id)
