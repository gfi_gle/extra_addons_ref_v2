Subscription sales
==================

Handle subscription sales within Odoo.

- 3 new product types, along with some new product fields.

- Sales order workflow tweaks.

- Subscription file generation.

- Commercial agent fee purchase order.

- Purchase tracker tweaks.

- Balance / P&L transfers & breakdown plans.


Installation
------------

- If the Odoo database contains lots of sales order lines, pre-create computed
  columns by running the ``scripts/create_soline_computed_columns.sql`` script.

- Install this addon as usual.
