from openerp import api
from openerp import fields
from openerp import models


class SubscriptionConfigSettings(models.TransientModel):
    """Augment the configuration view for subscription settings.
    """

    _inherit = 'subscription.config.settings'

    @api.model
    def get_property_fields(self):
        """List the property fields, per model.
        Override to include yours.
        :rtype: Dictionary.
        """

        ret = super(SubscriptionConfigSettings, self).get_property_fields()

        ret.update({
            'purchase_tracker.tracker': [
                'com_agent_fee_adjustment_journal_id',
                'com_agent_fee_journal_id',
            ],
            'subscription.file': [
                'com_agent_fee_adjustment_balance_transfer_debit_account_id',
                'com_agent_fee_adjustment_balance_transfer_journal_id',
                'com_agent_fee_adjustment_pl_transfer_journal_id',
                'com_agent_fee_adjustment_provision_credit_account_id',
                'com_agent_fee_adjustment_provision_debit_account_id',
                'com_agent_fee_adjustment_provision_journal_id',
                'com_agent_fee_adjustment_provision_reversal_journal_id',
                'com_agent_fee_balance_transfer_debit_account_id',
                'com_agent_fee_balance_transfer_journal_id',
                'com_agent_fee_pl_transfer_journal_id',
                'com_agent_fee_provision_credit_account_id',
                'com_agent_fee_provision_debit_account_id',
                'com_agent_fee_provision_journal_id',
                'com_agent_fee_provision_reversal_journal_id',
                'final_turnover_invoice_journal_id',
                'final_turnover_refund_journal_id',
                'overbilled_revenue_provision_credit_account_id',
                'overbilled_revenue_provision_journal_id',
                'overbilled_revenue_provision_reversal_journal_id',
                'sales_balance_transfer_credit_account_id',
                'sales_balance_transfer_journal_id',
                'sales_pl_transfer_journal_id',
                'unbilled_revenue_provision_debit_account_id',
                'unbilled_revenue_provision_journal_id',
                'unbilled_revenue_provision_reversal_journal_id',
            ],
        })

        return ret

    # ===
    # Subscription settings in the "purchase_tracker.tracker" model.
    # ===

    com_agent_fee_adjustment_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee adjustment invoicing journal',
        help=(
            'Accounting journal to use when creating purchase tracker '
            'invoices for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee invoicing journal',
        help=(
            'Accounting journal to use when creating purchase tracker '
            'invoices for commercial agent fees.'
        ),
    )

    # ===
    # Subscription settings in the "subscription.file" model.
    # ===

    com_agent_fee_adjustment_balance_transfer_debit_account_id = (
        fields.Many2one(
            comodel_name='account.account',
            string=(
                'Commercial agent fee adjustment balance transfer debit '
                'account'
            ),
            help=(
                'Accounting account to use for debit entries in balance '
                'transfer accounting documents for the commercial agent fee '
                'adjustment.'
            ),
        )
    )

    com_agent_fee_adjustment_balance_transfer_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee adjustment balance transfer journal',
        help=(
            'Accounting journal to use when creating balance transfer '
            'accounting documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_pl_transfer_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee adjustment p&l transfer journal',
        help=(
            'Accounting journal to use when creating p&l transfer accounting '
            'documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_credit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Commercial agent fee adjustment provision credit account',
        help=(
            'Accounting account to use for credit entries in provision '
            'accounting documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_debit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Commercial agent fee adjustment provision debit account',
        help=(
            'Accounting account to use for debit entries in provision '
            'accounting documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee adjustment provision journal',
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_reversal_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee adjustment provision reversal journal',
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_balance_transfer_debit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Commercial agent fee balance transfer debit account',
        help=(
            'Accounting account to use for debit entries in balance transfer '
            'accounting documents for the commercial agent fee.'
        ),
    )

    com_agent_fee_balance_transfer_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee balance transfer journal',
        help=(
            'Accounting journal to use when creating balance transfer '
            'accounting documents for commercial agent fees.'
        ),
    )

    com_agent_fee_pl_transfer_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee p&l transfer journal',
        help=(
            'Accounting journal to use when creating p&l transfer accounting '
            'documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_credit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Commercial agent fee provision credit account',
        help=(
            'Accounting account to use for credit entries in provision '
            'accounting documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_debit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Commercial agent fee provision debit account',
        help=(
            'Accounting account to use for debit entries in provision '
            'accounting documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee provision journal',
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_reversal_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Commercial agent fee provision reversal journal',
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for commercial agent fees.'
        ),
    )

    final_turnover_invoice_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Final turnover invoice journal',
        help=(
            'Accounting journal to use when creating final turnover invoices.'
        ),
    )

    final_turnover_refund_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Final turnover refund journal',
        help='Accounting journal to use when creating final turnover refunds.',
    )

    overbilled_revenue_provision_credit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Overbilled revenue provision credit account',
        help=(
            'Accounting account to use for credit entries in provision '
            'accounting documents for overbilled revenue.'
        ),
    )

    overbilled_revenue_provision_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Overbilled revenue provision journal',
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for overbilled revenue.'
        ),
    )

    overbilled_revenue_provision_reversal_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Overbilled revenue provision reversal journal',
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for overbilled revenue.'
        ),
    )

    sales_balance_transfer_credit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Sales balance transfer credit account',
        help=(
            'Accounting account to use for credit entries in balance transfer '
            'accounting documents for sales.'
        ),
    )

    sales_balance_transfer_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Sales balance transfer journal',
        help=(
            'Accounting journal to use when creating balance transfer '
            'accounting documents for sales.'
        ),
    )

    sales_pl_transfer_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Sales p&l transfer journal',
        help=(
            'Accounting journal to use when creating p&l transfer accounting '
            'documents for sales.'
        ),
    )

    unbilled_revenue_provision_debit_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Unbilled revenue provision debit account',
        help=(
            'Accounting account to use for debit entries in provision '
            'accounting documents for unbilled revenue.'
        ),
    )

    unbilled_revenue_provision_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Unbilled revenue provision journal',
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for unbilled revenue.'
        ),
    )

    unbilled_revenue_provision_reversal_journal_id = fields.Many2one(
        comodel_name='account.journal',
        string='Unbilled revenue provision reversal journal',
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for unbilled revenue.'
        ),
    )

    # ===
