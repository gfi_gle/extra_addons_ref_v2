"""Detailed information about subscription file elements; these model
definitions help compose detail tables in subscription files.
"""

from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class SubscriptionFileComAgentFeeDetail(models.Model):
    """Detailed information about a commercial agent fee; to be used in a
    subscription file.

    Most fields are similar to those in the "product.product",
    "sale.order.line", "subscription.frequency" models.
    """

    _name = 'subscription.file.com_agent_fee_detail'
    _description = 'Subscription file commercial agent fee detail'

    fee_rate = fields.Float(
        string='Fee rate (%)',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help=(
            'The rate to use when computing fees from this product; a '
            'percentage (between 0 and 100).'
        ),
        readonly=True,
    )

    free_month_count = fields.Integer(
        string='Free month count',
        help='The total amount of months where publications are free.',
        readonly=True,
    )

    paid_month_count = fields.Integer(
        string='Paid month count',
        help='The total amount of months where publications are not free.',
        readonly=True,
    )

    price = fields.Float(
        string='Price',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help='The price of the sales order line.',
        readonly=True,
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product',
        ondelete='restrict',
        help='The product this detail is for.',
        readonly=True,
        required=True,
    )

    product_type = fields.Selection(
        selection=[
            ('service', 'Service'),
            ('subscription', 'Subscription'),
            ('setup', 'Setup'),
            ('maintenance', 'Maintenance'),
        ],
        string='Product type',
        help='The type of the product this detail is for.',
        readonly=True,
    )

    quantity = fields.Float(
        string='Quantity',
        digits=dp.get_precision('Product UoS'),  # Like sales amounts.
        help='The quantity of the sales order line.',
        readonly=True,
    )

    sales_order_line_id = fields.Many2one(
        comodel_name='sale.order.line',
        string='Sales order line',
        ondelete='restrict',
        help='The sales order line this detail is for.',
        readonly=True,
    )

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',
        help='The subscription file this detail is included into.',
        readonly=True,
        required=True,
    )

    subscription_frequency_id = fields.Many2one(
        comodel_name='subscription.frequency',
        string='Frequency table',
        ondelete='restrict',
        help='The subscription frequency table defined on sales.',
        readonly=True,
    )

    theoretical_amount = fields.Float(
        string='Theoretical amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        default=0.0,
        help=(
            'Theoretical amount of the commercial agent fee for this product.'
        ),
        readonly=True,
    )

    unit_price = fields.Float(
        string='Unit price',
        digits=dp.get_precision('Product Price'),  # Like sales amounts.
        help='The unit price of the sales order line.',
        readonly=True,
    )


class SubscriptionFileComAgentFeeAdjustmentDetail(models.Model):
    """Detailed information about a commercial agent fee adjustment; to be used
    in a subscription file.

    Same as theoretical com agent fee details, except fields are to be fetched
    from accounting invoices.
    """

    _name = 'subscription.file.com_agent_fee_adjustment_detail'
    _description = 'Subscription file commercial agent fee adjustment detail'

    _inherit = 'subscription.file.com_agent_fee_detail'

    actual_amount = fields.Float(
        string='Actual amount',
        digits=dp.get_precision('Account'),  # Like invoice amounts.
        default=0.0,
        help='Actual amount of the commercial agent fee for this product.',
        readonly=True,
    )

    adjustment_amount = fields.Float(
        string='Adjustment amount',
        digits=dp.get_precision('Account'),  # Like invoice amounts.
        default=0.0,
        help='Adjustment amount of the commercial agent fee for this product.',
        readonly=True,
    )


class SubscriptionFileSalesInvoiceDetail(models.Model):
    """Detailed information about a sales invoice; to be used in a subscription
    file.

    Most fields are similar to those in the "account.invoice.line",
    "product.product", "subscription.frequency" models.
    """

    _name = 'subscription.file.sales_invoice_detail'
    _description = 'Subscription file sales invoice detail'

    begin_period_id = fields.Many2one(
        comodel_name='account.period',
        string='Beginning period',
        ondelete='restrict',
        help='The accounting period this detail line starts from.',
        readonly=True,
    )

    end_period_id = fields.Many2one(
        comodel_name='account.period',
        string='Ending period',
        ondelete='restrict',
        help='The accounting period this detail line goes up to.',
        readonly=True,
    )

    discount = fields.Float(
        string='Discount (%)',
        digits=dp.get_precision('Discount'),  # Like invoice amounts.
        help='The discount applied to this invoice line.',
        readonly=True,
    )

    free_month_count = fields.Integer(
        string='Free month count',
        help='The total amount of months where publications are free.',
        readonly=True,
    )

    invoice_date = fields.Datetime(
        string='Invoice date',
        help='The creation date of the invoice this detail is for.',
        readonly=True,
    )

    invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Invoice',
        ondelete='restrict',
        help='The invoice this detail is for.',
        readonly=True,
    )

    invoice_line_id = fields.Many2one(
        comodel_name='account.invoice.line',
        string='Invoice line',
        ondelete='restrict',
        help='The invoice line this detail is for.',
        readonly=True,
    )

    # Same as account.invoice::subscription_setup_type.
    invoice_setup_type = fields.Selection(
        selection=[
            ('advance', 'Advance'),
            ('balance', 'Balance'),
        ],
        string='Invoice type',
        help=(
            'Type specific to sales of "Setup" products to distinguish '
            'advance invoices from final ones.'
        ),
        readonly=True,
    )

    paid_month_count = fields.Integer(
        string='Paid month count',
        help='The total amount of months where publications are not free.',
        readonly=True,
    )

    price = fields.Float(
        string='Total price',
        digits=dp.get_precision('Account'),  # Like invoice amounts.
        help='The price of the invoice line.',
        readonly=True,
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product',
        ondelete='restrict',
        help='The product this detail is for.',
        readonly=True,
        required=True,
    )

    product_type = fields.Selection(
        selection=[
            ('service', 'Service'),
            ('subscription', 'Subscription'),
            ('setup', 'Setup'),
            ('maintenance', 'Maintenance'),
        ],
        string='Product type',
        help='The type of the product this detail is for.',
        readonly=True,
    )

    quantity = fields.Float(
        string='Quantity',
        digits=dp.get_precision('Product Unit of Measure'),  # Like invoices.
        help='The quantity of the invoice line.',
        readonly=True,
    )

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',
        help='The subscription file this detail is included into.',
        readonly=True,
        required=True,
    )

    subscription_frequency_id = fields.Many2one(
        comodel_name='subscription.frequency',
        string='Frequency table',
        ondelete='restrict',
        help='The subscription frequency table defined on sales.',
        readonly=True,
    )

    tracking_type = fields.Selection(
        selection=[
            ('theoretical', 'Theoretical quantities'),
            ('actual', 'Actual quantities'),
        ],
        string='Tracking type',
        help=(
            'Whether the turnover is to be tracked based on actually '
            'delivered quantities (as opposed to ordered quantities).'
        ),
        readonly=True,
    )

    unit_price = fields.Float(
        string='Unit price',
        digits=dp.get_precision('Product Price'),  # Like invoice amounts.
        help='The unit price of the invoice line.',
        readonly=True,
    )


class SubscriptionFileSalesOrderDetail(models.Model):
    """Detailed information about a sales order; to be used in a subscription
    file.

    Most fields are similar to those in the "product.product",
    "sale.order.line", "subscription.frequency" models.
    """

    _name = 'subscription.file.sales_order_detail'
    _description = 'Subscription file sales order detail'

    discount = fields.Float(
        string='Discount (%)',
        digits=dp.get_precision('Discount'),  # Like sales amounts.
        help='The discount applied to this sales order line.',
        readonly=True,
    )

    free_month_count = fields.Integer(
        string='Free month count',
        help='The total amount of months where publications are free.',
        readonly=True,
    )

    paid_month_count = fields.Integer(
        string='Paid month count',
        help='The total amount of months where publications are not free.',
        readonly=True,
    )

    price = fields.Float(
        string='Price',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help='The price of the sales order line.',
        readonly=True,
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product',
        ondelete='restrict',
        help='The product this detail is for.',
        readonly=True,
        required=True,
    )

    product_type = fields.Selection(
        selection=[
            ('service', 'Service'),
            ('subscription', 'Subscription'),
            ('setup', 'Setup'),
            ('maintenance', 'Maintenance'),
        ],
        string='Product type',
        help='The type of the product this detail is for.',
        readonly=True,
    )

    quantity = fields.Float(
        string='Quantity',
        digits=dp.get_precision('Product UoS'),  # Like sales amounts.
        help='The quantity of the sales order line.',
        readonly=True,
    )

    sales_order_line_id = fields.Many2one(
        comodel_name='sale.order.line',
        string='Sales order line',
        ondelete='restrict',
        help='The sales order line this detail is for.',
        readonly=True,
        required=True,
    )

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',
        help='The subscription file this detail is included into.',
        readonly=True,
        required=True,
    )

    subscription_frequency_id = fields.Many2one(
        comodel_name='subscription.frequency',
        string='Frequency table',
        ondelete='restrict',
        help='The subscription frequency table defined on sales.',
        readonly=True,
    )

    tracking_type = fields.Selection(
        selection=[
            ('theoretical', 'Theoretical quantities'),
            ('actual', 'Actual quantities'),
        ],
        string='Tracking type',
        help=(
            'Whether the turnover is to be tracked based on actually '
            'delivered quantities (as opposed to ordered quantities).'
        ),
        readonly=True,
    )

    unit_price = fields.Float(
        string='Unit price',
        digits=dp.get_precision('Product Price'),  # Like sales amounts.
        help='The unit price of the sales order line.',
        readonly=True,
    )
