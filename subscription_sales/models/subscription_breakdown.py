from openerp import _
from openerp import api
from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class SubscriptionBreakdown(models.Model):
    """One line of a financial breakdown shown in subscription files.
    """

    _name = 'subscription.breakdown'
    _description = 'Subscription breakdown'

    _order = 'index ASC'

    amount = fields.Float(
        string='Amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help='The amount of this breakdown entry.',
        readonly=True,
        required=True,
    )

    kind = fields.Selection(
        selection=[
            ('com_agent_fee', 'Commercial agent fee'),
            ('com_agent_fee_adjustment', 'Commercial agent fee adjustment'),
            ('turnover', 'Turnover'),
        ],
        string='Kind',
        help='The type of this breakdown entry.',
        readonly=True,
        required=True,
    )

    index = fields.Integer(
        string='Index',
        help='Index, used for sorting.',
        readonly=True,
        required=True,
    )

    period_id = fields.Many2one(
        comodel_name='account.period',
        string='Accounting period',
        ondelete='restrict',
        help='The accounting period this breakdown entry is for.',
        readonly=True,
    )

    @api.depends('period_id', 'product_id')
    @api.one
    def _get_period_displayed(self):
        """Handle pseudo breakdown entries from advance invoices, which contain
        no period link.
        """

        self.period_displayed = (
            self.period_id.name if self.period_id
            else '[%s]' % _('Advance') if self.product_id.type == 'setup'
            else '[Invalid]'
        )

    period_displayed = fields.Char(
        compute=_get_period_displayed,
        string='Accounting period',
        help=(
            'The accounting period this breakdown entry is for, or '
            '"[Advance]" when this is a pseudo entry from an advance invoice.'
        ),
        store=True,
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product',
        ondelete='restrict',
        help='The product this breakdown entry is for.',
        readonly=True,
        required=True,
    )

    state = fields.Selection(
        selection=[
            ('to_generate', 'To generate'),
            ('generated', 'Generated'),
            ('canceled', 'Canceled'),
            ('fix', 'Fix'),
        ],
        string='State',
        help='The state of this breakdown entry.',
        readonly=True,
    )

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',
        help='The subscription file this breakdown entry is on.',
        readonly=True,
        required=True,
    )
