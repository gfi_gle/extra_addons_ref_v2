from collections import OrderedDict
from openerp import _
from openerp import api
from openerp import fields
from openerp import models


class AccountingFile(models.Model):
    """Add a "canceled" state to accounting files.
    """

    _inherit = 'account.file'

    # Override to add an element into the selection list.
    state = fields.Selection(
        selection_add=[('canceled', 'Canceled')],
    )

    def fields_get(
        self, cr, uid, allfields=None, context=None, write_access=True,
    ):
        """Override to:
        - Properly translate added selection items (Odoo bug).
        """

        ADDED_SEL_ITEMS = {
            'state': {
                'canceled': _('Canceled'),
            },
        }

        ret = super(AccountingFile, self).fields_get(
            cr, uid, allfields=allfields, context=context,
            write_access=write_access,
        )

        # ret: { field-name: { 'selection': sel-items } }

        for field_name, sel_items in ADDED_SEL_ITEMS.iteritems():

            field_info = ret.get(field_name)
            if field_info:

                sel_data = field_info.get('selection')
                if sel_data:

                    # sel_data is a list of tuples; convert to a dictionary to
                    # update it, then back to a list of tuples.
                    sel_data = OrderedDict(sel_data)
                    sel_data.update(sel_items)
                    field_info['selection'] = sel_data.items()

        return ret

    @api.one
    def check_flow_workflows(self):
        """Check states of accounting flows in the specified accounting file,
        so as to automatically switch the state of the accounting file.

        Override to handle "canceled" states.
        """

        if all(self.flow_ids.mapped(lambda flow: flow.state == 'canceled')):
            self.signal_workflow('canceled')

        elif all(self.flow_ids.mapped(
            lambda flow: flow.state in ('reversed', 'canceled')
        )):
            self.signal_workflow('reversed')

    @api.one
    def wkf_canceled(self):
        """Switch the state to "canceled".
        """

        self.state = 'canceled'
