from openerp import fields
from openerp import models


class PurchaseOrderLine(models.Model):
    """Adapt purchase orders lines to subscription sales.
    """

    _inherit = 'purchase.order.line'

    # This is a utility field used to propagate the information to invoices
    # created via purchase trackers. This field is never shown in the UI and
    # has no particular constraint around it.
    subscription_frequency_id = fields.Many2one(
        comodel_name='subscription.frequency',
        string='Frequency table',
        ondelete='restrict',
        help='The subscription frequency table to use for this line.',
    )
