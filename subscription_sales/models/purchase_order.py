from openerp import _
from openerp import api
from openerp import fields
from openerp import models


class PurchaseOrder(models.Model):
    """Adapt purchase orders to subscription sales.
    """

    _inherit = 'purchase.order'

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',  # On purpose, to let the purch order block dels.
        help='The subscription file linked to this purchase order.',
        readonly=True,
    )

    # Override to add purchase order types specific to subscriptions.
    # Types are similar to those added to purchase trackers and invoices.
    type = fields.Selection(
        selection_add=[
            ('subscription_com_agent_fees',
             'Subscription commercial agent fees'),
            ('subscription_com_agent_fee_adjustments',
             'Subscription commercial agent fee adjustments'),

            # Specific to purchase orders (not for trackers / invoices).
            ('subscription_production_order', 'Subscription production order'),
        ],
    )

    @api.model
    def create(self, vals):
        """Override to fill analytic codes when there is a subscription file.
        """

        purchase_order = super(PurchaseOrder, self).create(vals)

        if 'subscription_file_id' in vals:
            purchase_order._fill_subscription_analytics()

        return purchase_order

    @api.multi
    def write(self, vals):
        """Override to fill analytic codes when there is a subscription file.
        """

        ret = super(PurchaseOrder, self).write(vals)

        if 'subscription_file_id' in vals:
            self._fill_subscription_analytics()

        return ret

    def fields_get(
        self, cr, user, allfields=None, context=None, write_access=True,
    ):
        """Override to rename fields related to purchase tracking.
        """

        RENAMED_FIELDS = {
            'amount_balance': {'string': _('Amount excl tax')},
            'amount_total': {'string': _('Amount incl tax')},
            'amount_total_clone': {'string': _('Amount incl tax')},
        }

        ret = super(PurchaseOrder, self).fields_get(
            cr, user, allfields=allfields, context=context,
            write_access=write_access,
        )

        for field_name, field_update in RENAMED_FIELDS.iteritems():
            field_info = ret.get(field_name)
            if field_info:
                field_info.update(field_update)

        return ret

    @api.multi
    def wkf_approve_order(self):
        """Override to let subscription files know of the validation, when
        there are.
        """

        ret = super(PurchaseOrder, self).wkf_approve_order()

        for purchase_order in self:
            subsfile = purchase_order.subscription_file_id
            if subsfile:
                subsfile.signal_validated_purchase_order(purchase_order)

        return ret

    @api.one
    def _fill_subscription_analytics(self):
        """Include analytic codes related to subscription files when there are.
        """

        subsfile = self.subscription_file_id
        if not subsfile:
            return

        bound_analytics = subsfile.get_bound_analytics('purchase_order_line')
        if bound_analytics:
            self.order_line.write(bound_analytics)
