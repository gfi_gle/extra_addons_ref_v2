from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class SubscriptionTurnover(models.Model):
    """One line of a turnover tracking shown in subscription files.
    """

    _name = 'subscription.turnover'
    _description = 'Subscription turnover'

    _order = 'index ASC'

    actual_quantity = fields.Float(
        string='Actual quantity',
        digits=dp.get_precision('Product Unit of Measure'),  # Like invoices.
        help=(
            'The actual quantity of this turnover entry; from a production '
            'order.'
        ),
        readonly=True,
    )

    appreciation = fields.Float(
        string='Appreciation',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help=(
            'The appreciation of this turnover entry; corresponds to '
            'quantity-delta * unit-price.'
        ),
        readonly=True,
    )

    index = fields.Integer(
        string='Index',
        help='Index, used for sorting.',
        readonly=True,
        required=True,
    )

    period_id = fields.Many2one(
        comodel_name='account.period',
        string='Accounting period',
        ondelete='restrict',
        help='The accounting period this turnover entry is for.',
        readonly=True,
        required=True,
    )

    product_id = fields.Many2one(
        comodel_name='product.product',
        string='Product',
        ondelete='restrict',
        help='The product this turnover entry is for.',
        readonly=True,
        required=True,
    )

    quantity_delta = fields.Float(
        string='Quantity delta',
        digits=dp.get_precision('Product Unit of Measure'),  # Like invoices.
        help=(
            'The difference between the actual and the theoretical quantities '
            'of this turnover entry.'
        ),
        readonly=True,
    )

    state = fields.Selection(
        selection=[
            ('to_appreciate', 'To appreciate'),
            ('appreciated', 'Appreciated'),
        ],
        string='State',
        help='The state of this turnover entry.',
        readonly=True,
    )

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',
        help='The subscription file this turnover entry is on.',
        readonly=True,
        required=True,
    )

    theoretical_quantity = fields.Float(
        string='Theoretical quantity',
        digits=dp.get_precision('Product Unit of Measure'),  # Like invoices.
        help=(
            'The theoretical quantity of this turnover entry; from a '
            'subscription sales invoice.'
        ),
        readonly=True,
        required=True,
    )

    unit_price = fields.Float(
        string='Unit price',
        digits=dp.get_precision('Product Price'),  # Like invoices.
        help=(
            'The unit price of the product this turnover entry is for; from a '
            'subscription sales invoice.'
        ),
        readonly=True,
        required=True,
    )
