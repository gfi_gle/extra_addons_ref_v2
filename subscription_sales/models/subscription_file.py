import ast
import datetime
from collections import defaultdict
from dateutil.relativedelta import relativedelta  # Odoo req.
import logging
from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models
from openerp import tools
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT

import openerp.addons.decimal_precision as dp

from openerp.addons.account_file.util.accounting_doc_maker import (
    build_accounting_entry,
    create_accounting_document,
)

log = logging.getLogger(__name__)


class SubscriptionFile(models.Model):
    """Manage a subscription order and accounting documents generated for it.
    Handle reversal of these documents and so on.

    Inherit by delegation from accounting files (provided by the "account_file"
    Odoo addon).

    Each subscription file corresponds to an analytic code.
    """

    _name = 'subscription.file'
    _description = 'Subscription file'

    _order = 'id DESC'

    # Inherit by delegation from "account.file".
    account_file_id = fields.Many2one(
        comodel_name='account.file',
        string='Accounting file',
        ondelete='cascade',
        delegate=True,
        help=(
            'Accounting file generated in relation to this subscription file.'
        ),
        readonly=True,
        required=True,
    )

    channel_id = fields.Many2one(
        comodel_name='crm.tracking.medium',
        string='Channel',
        ondelete='set null',
        help='The channel set on the sales order.',
        readonly=True,
    )

    client_invoice_count = fields.Integer(
        string='Client invoice count',
        help='The client invoices linked to this subscription file.',
        readonly=True,
    )

    # The reversed n:n field exists in accounting invoices.
    client_invoice_ids = fields.Many2many(
        comodel_name='account.invoice',
        relation='invoice_subscription_file_rel',
        column1='subscription_file_id',
        column2='invoice_id',
        string='Client invoices',
        domain=[('type', '=', 'out_invoice')],
        help='The client invoices linked to this subscription file.',
        readonly=True,
    )

    client_refund_count = fields.Integer(
        string='Client refund count',
        help='The client refunds linked to this subscription file.',
        readonly=True,
    )

    # The reversed n:n field exists in accounting invoices.
    client_refund_ids = fields.Many2many(
        comodel_name='account.invoice',
        relation='invoice_subscription_file_rel',
        column1='subscription_file_id',
        column2='invoice_id',
        string='Client refunds',
        domain=[('type', '=', 'out_refund')],
        help='The client refunds linked to this subscription file.',
        readonly=True,
    )

    com_agent_fee_actual_amount = fields.Float(
        string='Actual commercial agent fee amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help='Actual amount of the commercial agent fee.',
        readonly=True,
    )

    com_agent_fee_adjustment_amount = fields.Float(
        string='Commercial agent fee adjustment amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help=(
            'Amount of the commercial agent fee adjustment, based on the '
            'theoretical and actual amounts.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_balance_transfer_debit_account_id = (
        fields.Many2one(
            company_dependent=True,
            comodel_name='account.account',
            string=(
                'Subscriptions: Commercial agent fee adjustment balance '
                'transfer debit account'
            ),
            help=(
                'Accounting account to use for debit entries in balance '
                'transfer accounting documents for the commercial agent fee '
                'adjustment.'
            ),
        )
    )

    com_agent_fee_adjustment_balance_transfer_flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Commercial agent fee adjustment balance transfer flow',
        ondelete='set null',
        help=(
            'Accounting file flow for the balance transfer of the commercial '
            'agent fee adjustment.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_balance_transfer_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string=(
            'Subscriptions: Commercial agent fee adjustment balance transfer '
            'journal'
        ),
        help=(
            'Accounting journal to use when creating balance transfer '
            'accounting documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_balance_transfer_net_amount = fields.Float(
        string='Commercial agent fee adjustment balance transfer net amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Difference between the provision amount and the reversal amount '
            'of the commercial agent fee adjustment balance transfer flow.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_balance_transfer_provision_amount = fields.Float(
        string=(
            'Commercial agent fee adjustment balance transfer provision amount'
        ),
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Sum of the provisions within the commercial agent fee adjustment '
            'balance transfer flow.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_balance_transfer_reversal_amount = fields.Float(
        string=(
            'Commercial agent fee adjustment balance transfer reversal amount'
        ),
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Sum of the reversals within the commercial agent fee adjustment '
            'balance transfer flow.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_breakdown_ids = fields.One2many(
        comodel_name='subscription.breakdown',
        inverse_name='subscription_file_id',
        string='Commercial agent fee adjustment breakdown',
        domain=[('kind', '=', 'com_agent_fee_adjustment')],
        help=(
            'Financial breakdown of the commercial agent fee adjustment, '
            'based on frequency tables.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_detail_ids = fields.One2many(
        comodel_name='subscription.file.com_agent_fee_adjustment_detail',
        inverse_name='subscription_file_id',
        string='Commercial agent fee adjustment details',
        help=(
            'Details of the commercial agent fee adjustment, split by '
            'product.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Commercial agent fee adjustment invoice',
        ondelete='set null',
        help=(
            'The accounting invoice for the commercial agent fee adjustment; '
            'comes from a purchase tracker.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_invoiced_amount = fields.Float(
        string='Commercial agent fee adjustment invoiced amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        default=0.0,
        help=(
            'Invoiced amount of the commercial agent fee adjustment, as '
            'invoiced via a purchase tracker.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_invoiced_delta = fields.Float(
        string='Commercial agent fee adjustment invoiced delta',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        default=0.0,
        help=(
            'Delta between invoiced and estimated amounts of the commercial '
            'agent fee adjustment.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_net_amount = fields.Float(
        string='Commercial agent fee adjustment net amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Difference between the provision amount and the reversal amount '
            'of the commercial agent fee adjustment flow.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_pl_transfer_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string=(
            'Subscriptions: Commercial agent fee adjustment p&l transfer '
            'journal'
        ),
        help=(
            'Accounting journal to use when creating p&l transfer accounting '
            'documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_amount = fields.Float(
        string='Commercial agent fee adjustment provision amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Sum of the provisions within the commercial agent fee adjustment '
            'flow.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_provision_credit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string=(
            'Subscriptions: Commercial agent fee adjustment provision credit '
            'account'
        ),
        help=(
            'Accounting account to use for credit entries in provision '
            'accounting documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_debit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string=(
            'Subscriptions: Commercial agent fee adjustment provision debit '
            'account'
        ),
        help=(
            'Accounting account to use for debit entries in provision '
            'accounting documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Commercial agent fee adjustment provision flow',
        ondelete='set null',
        help=(
            'Accounting file flow for the commercial agent fee adjustment '
            'provision, computed based on the theoretical commercial agent '
            'fee and the actual one.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_provision_flow_state = fields.Selection(
        related=('com_agent_fee_adjustment_provision_flow_id', 'state'),
        string='Commercial agent fee adjustment provision flow state',
        help=(
            'State of the accounting file flow for the commercial agent fee '
            'adjustment provision.'
        ),
        readonly=True,
        store=True,
    )

    com_agent_fee_adjustment_provision_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string=(
            'Subscriptions: Commercial agent fee adjustment provision journal'
        ),
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_provision_reversal_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string=(
            'Subscriptions: Commercial agent fee adjustment provision '
            'reversal journal'
        ),
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for commercial agent fee adjustments.'
        ),
    )

    com_agent_fee_adjustment_purchase_order_id = fields.Many2one(
        comodel_name='purchase.order',
        string='Commercial agent fee adjustment purchase order',
        ondelete='cascade',  # On purpose, to let purchases block dels.
        help=(
            'The purchase order generated from this subscription file to '
            'track commercial agent fee adjustment invoices.'
        ),
        readonly=True,
    )

    com_agent_fee_adjustment_reversal_amount = fields.Float(
        string='Commercial agent fee adjustment reversal amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Sum of the reversals within the commercial agent fee adjustment '
            'flow.'
        ),
        readonly=True,
    )

    com_agent_fee_balance_transfer_debit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string=(
            'Subscriptions: Commercial agent fee balance transfer debit '
            'account'
        ),
        help=(
            'Accounting account to use for debit entries in balance transfer '
            'accounting documents for the commercial agent fee.'
        ),
    )

    com_agent_fee_balance_transfer_flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Commercial agent fee balance transfer flow',
        ondelete='set null',
        help=(
            'Accounting file flow for the balance transfer of the commercial '
            'agent fee.'
        ),
        readonly=True,
    )

    com_agent_fee_balance_transfer_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Commercial agent fee balance transfer journal',
        help=(
            'Accounting journal to use when creating balance transfer '
            'accounting documents for commercial agent fees.'
        ),
    )

    com_agent_fee_balance_transfer_net_amount = fields.Float(
        string='Commercial agent fee balance transfer net amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Difference between the provision amount and the reversal amount '
            'of the commercial agent fee balance transfer flow.'
        ),
        readonly=True,
    )

    com_agent_fee_balance_transfer_provision_amount = fields.Float(
        string='Commercial agent fee balance transfer provision amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Sum of the provisions within the commercial agent fee balance '
            'transfer flow.'
        ),
        readonly=True,
    )

    com_agent_fee_balance_transfer_reversal_amount = fields.Float(
        string='Commercial agent fee balance transfer reversal amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Sum of the reversals within the commercial agent fee balance '
            'transfer flow.'
        ),
        readonly=True,
    )

    com_agent_fee_breakdown_ids = fields.One2many(
        comodel_name='subscription.breakdown',
        inverse_name='subscription_file_id',
        string='Commercial agent fee breakdown',
        domain=[('kind', '=', 'com_agent_fee')],
        help=(
            'Financial breakdown of the commercial agent fee, based on '
            'frequency tables.'
        ),
        readonly=True,
    )

    com_agent_fee_detail_ids = fields.One2many(
        comodel_name='subscription.file.com_agent_fee_detail',
        inverse_name='subscription_file_id',
        string='Commercial agent fee details',
        help='Details of the commercial agent fee, split by sales order line.',
        readonly=True,
    )

    com_agent_fee_invoice_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Commercial agent fee invoice',
        ondelete='set null',
        help=(
            'The accounting invoice for the commercial agent fee; comes from '
            'a purchase tracker.'
        ),
        readonly=True,
    )

    com_agent_fee_invoiced_amount = fields.Float(
        string='Commercial agent fee invoiced amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        default=0.0,
        help=(
            'Invoiced amount of the commercial agent fee, as invoiced via a '
            'purchase tracker.'
        ),
        readonly=True,
    )

    com_agent_fee_invoiced_delta = fields.Float(
        string='Commercial agent fee invoiced delta',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        default=0.0,
        help=(
            'Delta between invoiced and estimated amounts of the commercial '
            'agent fee.'
        ),
        readonly=True,
    )

    com_agent_fee_net_amount = fields.Float(
        string='Commercial agent fee net amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Difference between the provision amount and the reversal amount '
            'of the commercial agent fee flow.'
        ),
        readonly=True,
    )

    com_agent_fee_pl_transfer_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Commercial agent fee p&l transfer journal',
        help=(
            'Accounting journal to use when creating p&l transfer accounting '
            'documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_amount = fields.Float(
        string='Commercial agent fee provision amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the provisions within the commercial agent fee flow.',
        readonly=True,
    )

    com_agent_fee_provision_credit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string='Subscriptions: Commercial agent fee provision credit account',
        help=(
            'Accounting account to use for credit entries in provision '
            'accounting documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_debit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string='Subscriptions: Commercial agent fee provision debit account',
        help=(
            'Accounting account to use for debit entries in provision '
            'accounting documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Theoretical commercial agent fee provision flow',
        ondelete='set null',
        help=(
            'Accounting file flow for the theoretical commercial agent fee '
            'provision; filled in after the initial sales order.'
        ),
        readonly=True,
    )

    com_agent_fee_provision_flow_state = fields.Selection(
        related=('com_agent_fee_provision_flow_id', 'state'),
        string='Theoretical commercial agent fee provision flow state',
        help=(
            'State of the accounting file flow for the theoretical commercial '
            'agent fee provision.'
        ),
        readonly=True,
        store=True,
    )

    com_agent_fee_provision_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Commercial agent fee provision journal',
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for commercial agent fees.'
        ),
    )

    com_agent_fee_provision_reversal_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string=(
            'Subscriptions: Commercial agent fee provision reversal journal'
        ),
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for commercial agent fees.'
        ),
    )

    com_agent_fee_reversal_amount = fields.Float(
        string='Commercial agent fee reversal amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the reversals within the commercial agent fee flow.',
        readonly=True,
    )

    com_agent_fee_theoretical_amount = fields.Float(
        string='Theoretical commercial agent fee amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help='Theoretical amount of the commercial agent fee.',
        readonly=True,
    )

    com_agent_partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Commercial agent',
        ondelete='set null',
        help='The commercial agent behind this subscription sales order.',
        readonly=True,
    )

    @api.one
    def _get_documents_by_analytics(self):
        """Find accounting documents that contain accounting entries with
        analytics pointing to the current subscription file.
        """

        acc_file_analytics = (
            self.account_file_id.get_bound_analytics('account_move_line')
        )
        if acc_file_analytics:
            acc_file_a_key, acc_file_a_value = acc_file_analytics.items()[0]
            docs = self.env['account.move'].search([(
                'line_id.' + acc_file_a_key, '=', acc_file_a_value,
            )])
            self.document_count_by_analytics = len(docs)
            self.entries_count_by_analytics = len(docs.mapped('line_id'))
            self.documents_by_analytics_ids = docs
        else:
            self.document_count_by_analytics = 0
            self.entries_count_by_analytics = 0
            self.documents_by_analytics_ids = []

    document_count_by_analytics = fields.Integer(
        compute=_get_documents_by_analytics,
        string='Accounting document count (by analytics)',
        help=(
            'Accounting documents generated in relation to this accounting '
            'file; fetched based on analytics of their accounting entries.'
        ),
    )

    entries_count_by_analytics = fields.Integer(
        compute=_get_documents_by_analytics,
        string='Accounting entry count (by analytics)',
        help=(
            'Accounting entries generated in relation to this accounting file;'
            ' fetched based on their analytics.'
        ),
    )

    documents_by_analytics_ids = fields.Many2many(
        compute=_get_documents_by_analytics,
        comodel_name='account.move',
        relation='subscription_file_documents_by_analytics_rel',
        column1='subscription_file_id',
        column2='document_id',
        string='Accounting documents (by analytics)',
        help=(
            'Accounting documents generated in relation to this accounting '
            'file; fetched based on analytics of their accounting entries.'
        ),
    )

    end_period_id = fields.Many2one(
        comodel_name='account.period',
        string='Ending period',
        ondelete='restrict',
        help=(
            'The accounting period this subscription file goes up to; filled '
            'from sales invoices.'
        ),
        readonly=True,
    )

    final_turnover_invoice_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Final turnover invoice journal',
        help=(
            'Accounting journal to use when creating final turnover invoices.'
        ),
    )

    final_turnover_invref_id = fields.Many2one(
        comodel_name='account.invoice',
        string='Final turnover invoice / refund',
        ondelete='set null',
        help=(
            'The accounting invoice or refund generated generated after all '
            'turnover entries have been appreciated.'
        ),
        readonly=True,
    )

    final_turnover_refund_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Final turnover refund journal',
        help='Accounting journal to use when creating final turnover refunds.',
    )

    overbilled_revenue_net_amount = fields.Float(
        string='Overbilled revenue net amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Difference between the provision amount and the reversal amount '
            'of the overbilled revenue flow.'
        ),
        readonly=True,
    )

    overbilled_revenue_provision_amount = fields.Float(
        string='Overbilled revenue provision amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the provisions within the overbilled revenue flow.',
        readonly=True,
    )

    overbilled_revenue_provision_credit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string='Subscriptions: Overbilled revenue provision credit account',
        help=(
            'Accounting account to use for credit entries in provision '
            'accounting documents for overbilled revenue.'
        ),
    )

    overbilled_revenue_provision_flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Overbilled revenue provision flow',
        ondelete='set null',
        help=(
            'Accounting file flow for the overbilled revenue provision; '
            'filled in from production orders.'
        ),
        readonly=True,
    )

    overbilled_revenue_provision_flow_state = fields.Selection(
        related=('overbilled_revenue_provision_flow_id', 'state'),
        string='Overbilled revenue provision flow state',
        help=(
            'State of the accounting file flow for the overbilled revenue '
            'provision.'
        ),
        readonly=True,
        store=True,
    )

    overbilled_revenue_provision_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Overbilled revenue provision journal',
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for overbilled revenue.'
        ),
    )

    overbilled_revenue_provision_reversal_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Overbilled revenue provision reversal journal',
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for overbilled revenue.'
        ),
    )

    overbilled_revenue_reversal_amount = fields.Float(
        string='Overbilled revenue reversal amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the reversals within the overbilled revenue flow.',
        readonly=True,
    )

    @api.depends('purchase_order_ids')
    @api.one
    def _get_purchase_order_count(self):
        self.purchase_order_count = self.env['purchase.order'].search(
            [('subscription_file_id', '=', self.id)], count=True,
        )

    purchase_order_count = fields.Integer(
        compute=_get_purchase_order_count,
        string='Purchase order count',
        help='The purchase orders linked to this subscription file.',
        store=True,
    )

    purchase_order_ids = fields.One2many(
        comodel_name='purchase.order',
        inverse_name='subscription_file_id',
        string='Purchase orders',
        help='Purchase orders linked to this subscription file.',
        readonly=True,
    )

    purchase_tracker_count = fields.Integer(
        string='Purchase tracker count',
        help=(
            'The validated purchase trackers for purchase orders linked to '
            'this subscription file.'
        ),
        readonly=True,
    )

    purchase_tracker_attachment_ids = fields.Many2many(
        comodel_name='ir.attachment',
        relation='subscription_file_purchase_tracker_attachment_rel',
        column1='subscription_file_id',
        column2='attachment_id',
        string='Purchase tracker attachments',
        help=(
            'The documents attached to validated purchase trackers for '
            'purchase orders linked to this subscription file.'
        ),
        readonly=True,
    )

    purchase_tracker_ids = fields.Many2many(
        comodel_name='purchase_tracker.tracker',
        relation='subscription_file_purchase_tracker_rel',
        column1='subscription_file_id',
        column2='purchase_tracker_id',
        string='Purchase trackers',
        domain=[('state', '=', 'approved')],
        help=(
            'The validated purchase trackers for purchase orders linked to '
            'this subscription file.'
        ),
        readonly=True,
    )

    sales_balance_transfer_credit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string='Subscriptions: Sales balance transfer credit account',
        help=(
            'Accounting account to use for credit entries in balance transfer '
            'accounting documents for sales.'
        ),
    )

    sales_balance_transfer_flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Sales balance transfer flow',
        ondelete='set null',
        help=(
            'Accounting file flow for the balance transfer of sales invoices.'
        ),
        readonly=True,
    )

    sales_balance_transfer_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Sales balance transfer journal',
        help=(
            'Accounting journal to use when creating balance transfer '
            'accounting documents for sales.'
        ),
    )

    sales_balance_transfer_net_amount = fields.Float(
        string='Sales balance transfer net amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Difference between the provision amount and the reversal amount '
            'of the sales balance transfer flow.'
        ),
        readonly=True,
    )

    sales_balance_transfer_provision_amount = fields.Float(
        string='Sales balance transfer provision amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the provisions within the sales balance transfer flow.',
        readonly=True,
    )

    sales_balance_transfer_reversal_amount = fields.Float(
        string='Sales balance transfer reversal amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the reversals within the sales balance transfer flow.',
        readonly=True,
    )

    sales_pl_transfer_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Sales p&l transfer journal',
        help=(
            'Accounting journal to use when creating p&l transfer accounting '
            'documents for sales.'
        ),
    )

    sales_invoice_detail_ids = fields.One2many(
        comodel_name='subscription.file.sales_invoice_detail',
        inverse_name='subscription_file_id',
        string='Sales invoice details',
        help='Details from sales invoices, split by invoice line.',
        readonly=True,
    )

    sales_order_amount = fields.Float(
        string='Sales order amount',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help=(
            'Total amount (untaxed) of the sales order this subscription file '
            'comes from.'
        ),
        readonly=True,
    )

    @api.depends('sales_order_id')
    @api.one
    def _get_sales_order_count(self):
        # Always 1 for now.
        self.sales_order_count = 1

    sales_order_count = fields.Integer(
        compute=_get_sales_order_count,
        string='Sales order count',
        help='The sales orders linked to this subscription file.',
        store=True,
    )

    sales_order_creation_date = fields.Datetime(
        string='Sales order creation date',
        help=(
            'Creation date of the sales order this subscription file comes '
            'from.'
        ),
        readonly=True,
    )

    sales_order_detail_ids = fields.One2many(
        comodel_name='subscription.file.sales_order_detail',
        inverse_name='subscription_file_id',
        string='Sales order details',
        help='Details from the sales order, split by sales order line.',
        readonly=True,
    )

    sales_order_id = fields.Many2one(
        comodel_name='sale.order',
        string='Sales order',
        ondelete='restrict',
        help='Sales order this subscription file comes from.',
        readonly=True,
    )

    sales_order_partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Sales order client',
        help=(
            'Client linked to the sales order this subscription file comes '
            'from.'
        ),
        readonly=True,
        required=True,
    )

    sales_order_state = fields.Selection(
        related=('sales_order_id', 'state'),
        string='Sales order state',
        help='State of the sales order this subscription file comes from.',
        readonly=True,
        store=True,
    )

    sales_order_validation_date = fields.Date(
        string='Sales order validation date',
        help=(
            'Validation date of the sales order this subscription file comes '
            'from.'
        ),
        readonly=True,
    )

    supplier_invoice_count = fields.Integer(
        string='Supplier invoice count',
        help='The supplier invoices linked to this subscription file.',
        readonly=True,
    )

    # The reversed n:n field exists in accounting invoices.
    supplier_invoice_ids = fields.Many2many(
        comodel_name='account.invoice',
        relation='invoice_subscription_file_rel',
        column1='subscription_file_id',
        column2='invoice_id',
        string='Client invoices',
        domain=[('type', '=', 'in_invoice')],
        help='The supplier invoices linked to this subscription file.',
        readonly=True,
    )

    supplier_refund_count = fields.Integer(
        string='Supplier refund count',
        help='The supplier refunds linked to this subscription file.',
        readonly=True,
    )

    # The reversed n:n field exists in accounting invoices.
    supplier_refund_ids = fields.Many2many(
        comodel_name='account.invoice',
        relation='invoice_subscription_file_rel',
        column1='subscription_file_id',
        column2='invoice_id',
        string='Supplier refunds',
        domain=[('type', '=', 'in_refund')],
        help='The supplier refunds linked to this subscription file.',
        readonly=True,
    )

    turnover_amount = fields.Float(
        string='Turnover amount',
        digits=dp.get_precision('Account'),  # Like invoice amounts.
        help=(
            'Total amount (untaxed) of the sales invoices and refunds linked '
            'to the sales order this subscription file comes from.'
        ),
        readonly=True,
    )

    turnover_appreciation = fields.Float(
        string='Turnover appreciation',
        digits=dp.get_precision('Account'),  # Like turnover amounts.
        help='Total appreciation amount; from turnover entries.',
        readonly=True,
    )

    turnover_breakdown_ids = fields.One2many(
        comodel_name='subscription.breakdown',
        inverse_name='subscription_file_id',
        string='Turnover breakdown',
        domain=[('kind', '=', 'turnover')],
        help=(
            'Financial breakdown of the turnover, based on frequency tables.'
        ),
        readonly=True,
    )

    turnover_ids = fields.One2many(
        comodel_name='subscription.turnover',
        inverse_name='subscription_file_id',
        string='Turnover tracking',
        help=(
            'Turnover tracking, based on frequency tables. Initiated from the '
            'sales invoice. Updated when production orders are validated.'
        ),
        readonly=True,
    )

    unbilled_revenue_net_amount = fields.Float(
        string='Unbilled revenue net amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help=(
            'Difference between the provision amount and the reversal amount '
            'of the unbilled revenue flow.'
        ),
        readonly=True,
    )

    unbilled_revenue_provision_amount = fields.Float(
        string='Unbilled revenue provision amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the provisions within the unbilled revenue flow.',
        readonly=True,
    )

    unbilled_revenue_provision_debit_account_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.account',
        string='Subscriptions: Unbilled revenue provision debit account',
        help=(
            'Accounting account to use for debit entries in provision '
            'accounting documents for unbilled revenue.'
        ),
    )

    unbilled_revenue_provision_flow_id = fields.Many2one(
        comodel_name='account.file.flow',
        string='Unbilled revenue provision flow',
        ondelete='set null',
        help=(
            'Accounting file flow for the unbilled revenue provision; filled '
            'in from production orders.'
        ),
        readonly=True,
    )

    unbilled_revenue_provision_flow_state = fields.Selection(
        related=('unbilled_revenue_provision_flow_id', 'state'),
        string='Unbilled revenue provision flow state',
        help=(
            'State of the accounting file flow for the unbilled revenue '
            'provision.'
        ),
        readonly=True,
        store=True,
    )

    unbilled_revenue_provision_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Unbilled revenue provision journal',
        help=(
            'Accounting journal to use when creating provision accounting '
            'documents for unbilled revenue.'
        ),
    )

    unbilled_revenue_provision_reversal_journal_id = fields.Many2one(
        company_dependent=True,
        comodel_name='account.journal',
        string='Subscriptions: Unbilled revenue provision reversal journal',
        help=(
            'Accounting journal to use when reversing provision accounting '
            'documents for unbilled revenue.'
        ),
    )

    unbilled_revenue_reversal_amount = fields.Float(
        string='Unbilled revenue reversal amount',
        digits=dp.get_precision('Account'),  # Like accounting amounts.
        help='Sum of the reversals within the unbilled revenue flow.',
        readonly=True,
    )

    def fields_get(
        self, cr, uid, allfields=None, context=None, write_access=True,
    ):
        """Override to:
        - Include analytics from the accounting file.
        """

        ret = super(SubscriptionFile, self).fields_get(
            cr, uid, allfields=allfields, context=context,
            write_access=write_access,
        )

        self.pool['analytic.structure'].analytic_fields_get(
            cr, uid, 'account_file', ret, context=context,
        )

        return ret

    def fields_view_get(
        self, cr, uid, view_id=None, view_type='form', context=None,
        toolbar=False, submenu=False,
    ):
        """Override to include analytics from the accounting file."""

        ret = super(SubscriptionFile, self).fields_view_get(
            cr, uid, view_id=view_id, view_type=view_type, context=context,
            toolbar=toolbar, submenu=submenu,
        )

        self.pool['analytic.structure'].analytic_fields_view_get(
            cr, uid, 'account_file', ret, context=context,
        )

        return ret

    @api.multi
    def unlink(self):
        """Override to delete the accounting file when deleting this object.
        """

        accounting_files = self.mapped('account_file_id')
        ret = super(SubscriptionFile, self).unlink()
        accounting_files.unlink()
        return ret

    @api.multi
    def billed_revenue_provision(self, turnover, podate):
        """Create a provision for unbilled or overbilled revenue.

        :param podate: The date to set on accounting entries; should be within
        the accounting period of the specified turnover entry. In the regular
        Odoo date format.
        :type podate: String.

        :param turnover: The turnover entry to generate a provision about.
        :type turnover: Odoo "subscription.turnover" record set.
        """

        self.ensure_one()

        amount = turnover.appreciation
        if not amount:
            log.warn('%s: Turnover without amount.', self.name)
            return

        # Whether this is for unbilled or overbilled revenue. For overbilled
        # revenue provisions, debit & credit entries are inverted.
        unbilled = amount > 0.0
        amount = abs(amount)

        # Some settings that depend on the amount sign, so the rest of the code
        # can stay shared.
        if unbilled:
            acc_journal_field = 'unbilled_revenue_provision_journal_id'
            field_prefix = 'unbilled_revenue'
            flow_field = 'unbilled_revenue_provision_flow_id'
            label = _('Unbilled revenue')
            product_account_field = 'property_account_income'
            provision_account_field = (
                'unbilled_revenue_provision_debit_account_id'
            )
            rev_acc_journal_field = (
                'unbilled_revenue_provision_reversal_journal_id'
            )
            short_label = _('UnbilledRevProv')
        else:
            acc_journal_field = 'overbilled_revenue_provision_journal_id'
            field_prefix = 'overbilled_revenue'
            flow_field = 'overbilled_revenue_provision_flow_id'
            label = _('Overbilled revenue')
            product_account_field = 'property_account_expense'
            provision_account_field = (
                'overbilled_revenue_provision_credit_account_id'
            )
            rev_acc_journal_field = (
                'overbilled_revenue_provision_reversal_journal_id'
            )
            short_label = _('OverbilledRevProv')

        log.info('%s: Creating the %s provision.', self.name, label)

        # The partner to assign the provision to.
        partner = self.sales_order_partner_id

        # Take accounting journals from settings.
        acc_journal = getattr(self, acc_journal_field)
        rev_acc_journal = getattr(self, rev_acc_journal_field)
        if not acc_journal:
            raise exceptions.Warning(_(
                'No %s provision journal set up.'
            ) % label)
        if not rev_acc_journal:
            raise exceptions.Warning(_(
                'No %s provision reversal journal set up.'
            ) % label)

        # Take accounting accounts from settings.
        provision_account = getattr(self, provision_account_field)
        product_account = getattr(turnover.product_id, product_account_field)
        if not provision_account:
            raise exceptions.Warning(_(
                'No %s provision account set up.'
            ) % label)
        if not product_account:
            raise exceptions.Warning(_(
                '%s: No account set up on the %s product.'
            ) % (label, turnover.product_id.name))

        provision_line = build_accounting_entry(
            account_file=self.account_file_id,
            account=provision_account,
            amount=amount,
            debit_or_credit=unbilled,
            name='%s%s' % (short_label, self.name),
            partner=partner,
        )

        product_line = build_accounting_entry(
            account_file=self.account_file_id,
            account=product_account,
            amount=amount,
            debit_or_credit=not unbilled,
            name='%s%s' % (short_label, self.name),
            partner=partner,
        )

        # The accounting lines to be created within the accounting document.
        accounting_lines = [provision_line, product_line]

        # Prepare the accounting document date and period.
        accdoc_date = podate
        accdoc_period = turnover.period_id

        # Re-use an existing accounting flow or create one when necessary.
        flow = getattr(self, flow_field)
        if not flow:
            setattr(self, flow_field, self.env['account.file.flow'].create({
                'account_file_id': self.account_file_id.id,
                'name': label,
            }))
            flow = getattr(self, flow_field)
            # Early-set for reversal; will be reversed later on.
            flow.signal_workflow('awaiting_reversal')

        # Create an accounting document within that flow.
        create_accounting_document(
            flow=flow,
            name=_('%s provision - %s') % (label, self.name),
            ref='%s%s' % (acc_journal.code, self.name),
            accounting_entries=accounting_lines,
            journal=acc_journal,
            reversal_journal=rev_acc_journal,
            to_reverse=True,
            date=accdoc_date,
            period_id=accdoc_period.id,
        )

        # Adjust total counters on the subscription file.
        self.turnover_appreciation += turnover.appreciation
        self._update_flow_amounts(field_prefix)

        log.info('%s: Created the %s provision.', self.name, label)

    @api.multi
    def breakdown(self, invoice, sales_invoice=None):
        """Compute a breakdown plan, out of a specific invoice.
        For sales invoices, also compute a turnover tracking plan.

        :type invoice: Odoo "account.invoice" record set.

        :param sales_invoice: The sales invoice behind this breakdown request
        (the breakdown itself will be based on the specified invoice).
        :type sales_invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()

        # Convert from invoice kinds to breakdown kinds.
        kind = {
            'com_agent_fees': 'com_agent_fee',
            'com_agent_fee_adjustments': 'com_agent_fee_adjustment',
            'sales': 'turnover',
        }.get(invoice.subscription_type or '')
        if not kind:
            raise exceptions.Warning('Invalid invoice %d kind.' % invoice.id)

        log.info('%s: Creating the %s breakdown.', self.name, kind)

        # When a sales invoice is specified, limit the breakdown to products in
        # that sales invoice (by default, the breakdown applies to all products
        # found in the specified invoice).
        if sales_invoice:
            sales_invoice.ensure_one()
            sales_products = sales_invoice.invoice_line.mapped('product_id')

        breakdown_values = []
        if kind == 'turnover':
            turnover_values = []

        # Prepare amount rounding helpers.

        rounding_precision = self.pool['decimal.precision'].precision_get(
            self.env.cr, self.env.user.id, 'Account',
        )

        def roundFloat(value):
            return tools.float_round(
                value, precision_digits=rounding_precision,
            )

        # A counter to make sure items are sorted as expected. Start at the
        # bottom when appending to an existing list.
        breakdown_index = self.env['subscription.breakdown'].search(
            [('subscription_file_id', '=', self.id)],
            limit=1, order='index DESC',
        ).index or 0

        # Go through invoice lines and break them down, based on settings
        # defined per product.

        for invline in invoice.invoice_line:
            amount_per_line = invline.price_subtotal
            product = invline.product_id

            # Skip when a limiting to a specific set of products.
            if sales_invoice and product.id not in sales_products.ids:
                continue

            # Special case: When this is an advance invoice for a setup
            # product, register a single breakdown entry that has no period.
            if (
                product.type == 'setup' and
                (sales_invoice or invoice).subscription_setup_type == 'advance'
            ):
                breakdown_values.append({
                    'amount': amount_per_line,
                    'kind': kind,
                    'index': breakdown_index,
                    'product_id': product.id,
                    'state': 'to_generate',
                })
                breakdown_index += 1
                continue

            # Ensure the product requires a breakdown.
            if not product.requires_breakdown:
                continue
            if product.type not in ('subscription', 'maintenance'):
                continue

            # Cache whether we want to track the turnover for this product.
            turnover_tracking = (
                kind == 'turnover' and
                product.tracking_type == 'actual' and
                product.type == 'subscription'
            )

            # Get sales invoice details (filled when a sales invoice got
            # validated).
            sidetail = (
                self.env['subscription.file.sales_invoice_detail']
                .search([
                    ('product_id', '=', product.id),
                    ('subscription_file_id', '=', self.id),
                ], limit=1)
            )
            if not sidetail:
                raise exceptions.Warning(_(
                    '"%s" subscription file: No sales invoice line found for '
                    'the "%s" product.'
                ) % (self.name, product.name))

            # Get periods to produce breakdown entries for.
            periods = self.get_affected_periods(sidetail)

            # Count how many months to share the amount on.
            month_count = 0
            for period_iter in periods:
                if period_iter[1]:  # 1: "enabled" flag
                    month_count += 1
            if not month_count:
                raise exceptions.Warning('Error: month_count = 0')

            # Prepare amounts.
            # Don't choke on empty frequency tables, just in case.
            amount_per_month = (
                roundFloat(amount_per_line / month_count)
                if month_count else 0.0
            )
            # Take rounding errors into account for the last month.
            last_month_amount = roundFloat(
                amount_per_line - amount_per_month * (month_count - 1)
            )

            # Loop through the periods to build a value list.
            for period, enabled_month in periods:

                breakdown_values.append({
                    'amount': amount_per_month if enabled_month else 0.0,
                    'kind': kind,
                    'index': breakdown_index,
                    'period_id': period.id,
                    'product_id': product.id,
                    'state': 'to_generate' if enabled_month else False,
                })

                # Fill turnover tracking values when needed.
                if turnover_tracking:
                    turnover_values.append({
                        'index': breakdown_index,
                        'period_id': period.id,
                        'product_id': product.id,
                        'state': 'to_appreciate' if enabled_month else False,
                        'theoretical_quantity': (
                            invline.quantity if enabled_month else 0.0
                        ),
                        'unit_price': invline.price_unit,
                    })

                # Prepare for the next iteration.
                breakdown_index += 1

            # Set the last month amount now so we don't have problems with
            # disabled months.
            for breakdown_v in reversed(breakdown_values):
                if breakdown_v['state'] == 'to_generate':
                    breakdown_v['amount'] = last_month_amount
                    break

        if breakdown_values:
            # Ready! Save the breakdown plan.
            self.write({'%s_breakdown_ids' % kind: [
                (0, 0, breakdown_v)  # 0: Create.
                for breakdown_v in breakdown_values
            ]})

            # Also save the turnover tracking plan when we have one.
            if kind == 'turnover':
                self.write({'turnover_ids': [
                    (0, 0, turnover_v)  # 0: Create.
                    for turnover_v in turnover_values
                ]})

            log.info('%s: Created the %s breakdown.', self.name, kind)

        else:
            # No breakdown plan here.
            log.info('%s: No %s breakdown.', self.name, kind)

    @api.multi
    def com_agent_fee_adjustment(self, invoice):
        """Compute the actual commercial agent fee based on the specified
        client invoice, and the potential adjustment when there is one.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()

        log.info('%s: Computing the com agent fee adjustment.', self.name)

        # Build details by splitting by product.
        # Also compute total amounts in the process.

        com_agent_fee_actual_amount = 0.0
        com_agent_fee_adjustment_amount = 0.0
        com_agent_fee_adj_details = []

        for invline in invoice.invoice_line:

            product = invline.product_id
            if product.type != 'subscription':
                continue  # This is only for "subscription" products.

            amount_per_line = invline.price_subtotal
            subsfreq = invline.subscription_frequency_id

            fee_rate = product.fee_rate
            if not fee_rate:
                continue
            fee_amount_per_line = amount_per_line * fee_rate / 100.0
            com_agent_fee_actual_amount += fee_amount_per_line

            # Find the theoretical amount among theoretical CAF detail lines.
            theoretical_amount = (
                self.env['subscription.file.com_agent_fee_detail']
                .search([
                    ('product_id', '=', product.id),
                    ('subscription_file_id', '=', self.id),
                ], limit=1)
                .theoretical_amount
            )

            adjustment_per_line = fee_amount_per_line - theoretical_amount
            com_agent_fee_adjustment_amount += adjustment_per_line

            com_agent_fee_adj_details.append({
                'actual_amount': fee_amount_per_line,
                'adjustment_amount': adjustment_per_line,
                'fee_rate': fee_rate,
                'free_month_count': subsfreq.free_month_count,
                'paid_month_count': subsfreq.paid_month_count,
                'price': amount_per_line,
                'product_id': product.id,
                'product_type': product.type,
                'quantity': invline.quantity,
                'subscription_frequency_id': subsfreq.id,
                'theoretical_amount': theoretical_amount,
                'unit_price': invline.price_unit,
            })

        # Ready! Update the subscription file.
        self.write({
            'com_agent_fee_actual_amount': com_agent_fee_actual_amount,
            'com_agent_fee_adjustment_amount': com_agent_fee_adjustment_amount,
            'com_agent_fee_adjustment_detail_ids': [
                (0, 0, com_agent_fee_adj_detail)  # 0: Create.
                for com_agent_fee_adj_detail in com_agent_fee_adj_details
            ],
        })

        log.info('%s: Computed the com agent fee adjustment.', self.name)

    @api.multi
    def com_agent_fee_adjustment_purchase(self):
        """Create a purchase order meant to track commercial agent fee
        adjustment supplier invoices / refunds.

        Very similar to the commercial agent fee purchase order creation from
        sales orders.
        """

        self.ensure_one()

        # Do nothing when there is no adjustment.
        if not self.com_agent_fee_adjustment_provision_flow_id:
            return

        log.info('%s: Creating the com agent fee adj purchase.', self.name)

        partner = self.com_agent_partner_id
        if not partner:
            log.info('%s: No commercial agent.', self.name)
            return

        # Prepare some data.
        fiscal_position = partner.property_account_position
        payment_terms = partner.referrer_fees_payment_terms_id

        def addAnalytics(order_line, product):
            # Propagate sales order analytics.
            # Let the "extract_values" method provide analytic field updates.
            order_line.update(self.env['analytic.structure'].extract_values(
                self.sales_order_id, 'sale_order',
                dest_model='purchase_order_line',
            ))

            # Propagate product analytics.
            # Let the "extract_values" method provide analytic field updates.
            order_line.update(self.env['analytic.structure'].extract_values(
                product, 'product_template', dest_model='purchase_order_line',
            ))

        def mapTaxes(taxes):
            """Map taxes through the fiscal position and format to insert."""
            return [(6, 0, fiscal_position.map_tax(taxes).ids)]

        # Prepare the line list.
        order_lines = []

        # Defaults for all order lines.
        order_line_defaults = {
            'date_planned': fields.Date.today(),
        }

        # Fill the line list based on adjustment details that have been
        # previously computed.
        for detail in self.com_agent_fee_adjustment_detail_ids:
            order_line = order_line_defaults.copy()
            product = detail.product_id
            order_line.update({
                'name': product.name,
                'price_unit': detail.adjustment_amount,
                'product_id': product.id,
                'product_qty': 1.0,
                'product_uom': product.uom_po_id.id,
                'subscription_frequency_id': (
                    detail.subscription_frequency_id.id
                ),
            })
            if fiscal_position:
                order_line['taxes_id'] = mapTaxes(product.supplier_taxes_id)
            addAnalytics(order_line, product)
            order_lines.append(order_line)

        # All set! Create a purchase order.
        self.com_agent_fee_adjustment_purchase_order_id = (
            self.env['purchase.order']
            .with_context({'partner_id': partner.id})
            .create({
                'description': _('Commercial agent fee adjustment'),
                'fiscal_position': fiscal_position.id,
                'order_line': [(0, 0, line) for line in order_lines],
                'partner_id': partner.id,
                'payment_term_id': payment_terms.id,
                'subscription_file_id': self.id,
                'type': 'subscription_com_agent_fee_adjustments',
            })
        )

        log.info('%s: Created the com agent fee adj purchase.', self.name)

    @api.multi
    def com_agent_fee_balance_transfer(self, invoice):
        """Create an accounting document for the balance transfer of a
        commercial agent fee (theoretical or adjustment).

        :param invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()

        # Some settings that depend on fee kind, so the rest of the code can
        # stay shared.
        if invoice.subscription_type == 'com_agent_fees':
            acc_journal_field = 'com_agent_fee_balance_transfer_journal_id'
            debit_account_field = (
                'com_agent_fee_balance_transfer_debit_account_id'
            )
            field_prefix = 'com_agent_fee_balance_transfer'
            flow_field = 'com_agent_fee_balance_transfer_flow_id'
            label = _('Commercial agent fee')
            short_label = _('ComAgFeeBT')
        elif invoice.subscription_type == 'com_agent_fee_adjustments':
            acc_journal_field = (
                'com_agent_fee_adjustment_balance_transfer_journal_id'
            )
            debit_account_field = (
                'com_agent_fee_adjustment_balance_transfer_debit_account_id'
            )
            field_prefix = 'com_agent_fee_adjustment_balance_transfer'
            flow_field = 'com_agent_fee_adjustment_balance_transfer_flow_id'
            label = _('Commercial agent fee adjustment')
            short_label = _('ComAgFeeAdjBT')
        else:
            raise exceptions.Warning('Invalid invoice type.')

        log.info('%s: Creating the %s balance transfer.', self.name, label)

        # Take accounting journals from settings.
        acc_journal = getattr(self, acc_journal_field)
        if not acc_journal:
            raise exceptions.Warning(_(
                'No %s balance transfer journal set up.'
            ) % label)

        # Take accounting accounts from settings.
        debit_account = getattr(self, debit_account_field)
        if not debit_account:
            raise exceptions.Warning(_(
                'No %s balance transfer debit account set up.'
            ) % label)

        # Prepare analytics in order to filter accounting entries.
        acc_file_analytics = (
            self.account_file_id.get_bound_analytics('account_move_line')
        )
        if not acc_file_analytics:
            raise exceptions.Warning(_(
                'No accounting file analytics on "account_move_line".'
            ))
        acc_file_a_key, acc_file_a_value = acc_file_analytics.items()[0]

        # Gather product expense lines linked to this subscription file from
        # the supplier invoice / refund. Just filtering by subscription file
        # analytics is enough.
        product_entries = (
            invoice.move_id.line_id.filtered(lambda acc_entry: (
                getattr(acc_entry, acc_file_a_key).id == acc_file_a_value
            ))
        )

        # The accounting lines to be created within the accounting document.
        accounting_lines = []

        for product_entry in product_entries:

            # Handle both debit & credit as this could be a refund.
            amount = product_entry.debit or product_entry.credit
            partner = product_entry.partner_id
            product = product_entry.product_id

            credit_line = build_accounting_entry(
                account_file=self.account_file_id,
                account=product_entry.account_id,
                amount=amount,
                debit_or_credit=False,
                name='%s%s' % (short_label, self.name),
                partner=partner,
                product_id=product.id,
            )

            debit_line = build_accounting_entry(
                account_file=self.account_file_id,
                account=debit_account,
                amount=amount,
                debit_or_credit=True,
                name='%s%s' % (short_label, self.name),
                partner=partner,
                product_id=product.id,
            )

            accounting_lines += [credit_line, debit_line]

        if not accounting_lines:
            log.info('%s: Nothing to add to the %s balance transfer.',
                     self.name, label)
            return

        # Re-use an existing accounting flow or create one when necessary.
        flow = getattr(self, flow_field)
        if not flow:
            setattr(self, flow_field, self.env['account.file.flow'].create({
                'account_file_id': self.account_file_id.id,
                'name': _('%s balance transfer') % label,
            }))
            flow = getattr(self, flow_field)
            # The flow will be reversed via monthly transfers.
            flow.signal_workflow('awaiting_reversal')

        # Create an accounting document within that flow.
        # Trick used here: We want to know which accounting documents are
        # balance transfers (as opposed to p&l transfers, which are reversals);
        # but we do not want auomatic reversal. -> We therefore fill the
        # "reversal_journal_id" field to a dummy value to be able to
        # distinguish.
        create_accounting_document(
            flow=flow,
            name=_('%s balance transfer - %s') % (label, self.name),
            ref='%s%s' % (acc_journal.code, self.name),
            accounting_entries=accounting_lines,
            journal=acc_journal,
            reversal_journal=acc_journal,
            to_reverse=False,
        )

        self._update_flow_amounts(field_prefix)

        log.info('%s: Created the %s balance transfer.', self.name, label)

    @api.multi
    def com_agent_fee_provision(self, adjustment):
        """Create a commercial agent fee provision (theoretical or adjustment).

        :param adjustment: Whether an adjustment is being generated (as opposed
        to the theoretical fee).
        :type adjustment: Boolean.
        """

        self.ensure_one()

        # Some settings that depend on fee kind, so the rest of the code can
        # stay shared.
        if adjustment:
            acc_journal_field = 'com_agent_fee_adjustment_provision_journal_id'
            credit_account_field = (
                'com_agent_fee_adjustment_provision_credit_account_id'
            )
            debit_account_field = (
                'com_agent_fee_adjustment_provision_debit_account_id'
            )
            fee_amount_field = 'com_agent_fee_adjustment_amount'
            field_prefix = 'com_agent_fee_adjustment'
            flow_field = 'com_agent_fee_adjustment_provision_flow_id'
            label = _('Commercial agent fee adjustment')
            rev_acc_journal_field = (
                'com_agent_fee_adjustment_provision_reversal_journal_id'
            )
            short_label = _('ComAgFeeAdjProv')
        else:
            acc_journal_field = 'com_agent_fee_provision_journal_id'
            credit_account_field = 'com_agent_fee_provision_credit_account_id'
            debit_account_field = 'com_agent_fee_provision_debit_account_id'
            fee_amount_field = 'com_agent_fee_theoretical_amount'
            field_prefix = 'com_agent_fee'
            flow_field = 'com_agent_fee_provision_flow_id'
            label = _('Commercial agent fee')
            rev_acc_journal_field = (
                'com_agent_fee_provision_reversal_journal_id'
            )
            short_label = _('ComAgFeeProv')

        log.info('%s: Creating the %s provision.', self.name, label)

        # The partner to assign the provision to.
        partner = self.com_agent_partner_id

        # Ensure provisions have to be generated.
        if not partner:
            log.info('%s: No commercial agent.', self.name)
            return
        if not partner.has_referrer_fees:
            log.info('%s: The partner has no referrer fees.', self.name)
            return
        fee_amount = getattr(self, fee_amount_field)
        if not fee_amount:
            log.info('%s: No %s.', self.name, label)
            return

        # Take accounting journals from settings.
        acc_journal = getattr(self, acc_journal_field)
        rev_acc_journal = getattr(self, rev_acc_journal_field)
        if not acc_journal:
            raise exceptions.Warning(_(
                'No %s provision journal set up.'
            ) % label)
        if not rev_acc_journal:
            raise exceptions.Warning(_(
                'No %s provision reversal journal set up.'
            ) % label)

        # Take accounting accounts from settings.
        debit_account = getattr(self, debit_account_field)
        credit_account = getattr(self, credit_account_field)
        if not debit_account:
            raise exceptions.Warning(_(
                'No %s provision debit account set up.'
            ) % label)
        if not credit_account:
            raise exceptions.Warning(_(
                'No %s provision credit account set up.'
            ) % label)

        # When the amount is negative, invert credit & debit lines.
        invert_credit_debit = fee_amount < 0
        fee_amount = abs(fee_amount)

        credit_line = build_accounting_entry(
            account_file=self.account_file_id,
            account=credit_account,
            amount=fee_amount,
            debit_or_credit=invert_credit_debit,
            name='%s%s' % (short_label, self.name),
            partner=partner,
        )

        debit_line = build_accounting_entry(
            account_file=self.account_file_id,
            account=debit_account,
            amount=fee_amount,
            debit_or_credit=not invert_credit_debit,
            name='%s%s' % (short_label, self.name),
            partner=partner,
        )

        # The accounting lines to be created within the accounting document.
        accounting_lines = [credit_line, debit_line]

        # The accounting document date corresponds to the sales order creation
        # date. Convert datetime -> date. Fetch a corresponding period as well.
        accdoc_date = datetime.datetime.strptime(
            self.sales_order_creation_date, DEFAULT_SERVER_DATETIME_FORMAT,
        ).strftime(DEFAULT_SERVER_DATE_FORMAT)
        accdoc_period = self.env['account.period'].find(dt=accdoc_date)

        # Create an accounting flow.
        setattr(self, flow_field, self.env['account.file.flow'].create({
            'account_file_id': self.account_file_id.id,
            'name': label,
        }))

        # Create an accounting document within that flow.
        create_accounting_document(
            flow=getattr(self, flow_field),
            name=_('%s provision - %s') % (label, self.name),
            ref='%s%s' % (acc_journal.code, self.name),
            accounting_entries=accounting_lines,
            journal=acc_journal,
            reversal_journal=rev_acc_journal,
            to_reverse=True,
            date=accdoc_date,
            period_id=accdoc_period.id,
        )

        # The flow is to be reversed.
        getattr(self, flow_field).signal_workflow('awaiting_reversal')

        self._update_flow_amounts(field_prefix)

        log.info('%s: Created the %s provision.', self.name, label)

    @api.model
    def create_from_sales_order(self, sales_order):
        """- Create a subscription file from a sales order.
        - Fill sales order details.
        - Compute and fill theoretical commercial agent fee details.
        - Initiate the theoretical commercial agent fee provision flow.

        :type sales_order: Odoo "sale.order" record set.
        :rtype: Odoo "subscription.file" record set.
        """

        if not sales_order.is_for_subscription:
            raise exceptions.Warning(_(
                'The "%s" sales order is not marked as being for a '
                'subscription.'
            ) % sales_order.name)

        # Build details by splitting by sales order line.
        # Also compute the theoretical commercial agent fee in the process.

        sales_order_details = []
        com_agent_fee_details = []
        com_agent_fee_amount = 0.0

        for soline in sales_order.order_line:

            product = soline.product_id
            amount_per_line = soline.price_subtotal
            subsfreq = soline.subscription_frequency_id

            sales_order_details.append({
                'discount': soline.discount,
                'free_month_count': subsfreq.free_month_count,
                'paid_month_count': subsfreq.paid_month_count,
                'price': amount_per_line,
                'product_id': product.id,
                'product_type': product.type,
                'quantity': soline.product_uom_qty,
                'sales_order_line_id': soline.id,
                'subscription_frequency_id': subsfreq.id,
                'tracking_type': product.tracking_type,
                'unit_price': soline.price_unit,
            })

            fee_rate = product.fee_rate
            if not fee_rate:
                continue
            fee_amount_per_line = amount_per_line * fee_rate / 100.0
            com_agent_fee_amount += fee_amount_per_line

            com_agent_fee_details.append({
                'fee_rate': fee_rate,
                'free_month_count': subsfreq.free_month_count,
                'paid_month_count': subsfreq.paid_month_count,
                'price': amount_per_line,
                'product_id': product.id,
                'product_type': product.type,
                'quantity': soline.product_uom_qty,
                'sales_order_line_id': soline.id,
                'subscription_frequency_id': subsfreq.id,
                'theoretical_amount': fee_amount_per_line,
                'unit_price': soline.price_unit,
            })

        # Prepare the data to create the subscription file with.
        subsfile_values = {
            'channel_id': sales_order.medium_id.id,
            'com_agent_fee_detail_ids': [
                (0, 0, com_agent_fee_detail)  # 0: Create.
                for com_agent_fee_detail in com_agent_fee_details
            ],
            'com_agent_fee_theoretical_amount': com_agent_fee_amount,
            'com_agent_partner_id': sales_order.com_agent_partner_id.id,
            'name': self.env['ir.sequence'].next_by_code(self._name),
            'sales_order_amount': sales_order.amount_untaxed,
            'sales_order_creation_date': sales_order.create_date,
            'sales_order_detail_ids': [
                (0, 0, sales_order_detail)  # 0: Create.
                for sales_order_detail in sales_order_details
            ],
            'sales_order_id': sales_order.id,
            'sales_order_partner_id': sales_order.partner_id.id,
            'sales_order_validation_date': sales_order.date_confirm,
        }

        # Propagate sales order analytics.
        # Let the "extract_values" method provide analytic field updates.
        subsfile_values.update(self.env['analytic.structure'].extract_values(
            sales_order, 'sale_order', dest_model='account_file',
        ))

        subsfile = self.create(subsfile_values)

        # Initiate the theoretical commercial agent fee provision flow.
        subsfile.com_agent_fee_provision(adjustment=False)

        subsfile.account_file_id.signal_workflow('awaiting_reversal')

        return subsfile

    @api.multi
    def create_production_order(self):
        """Simply redirect to a purchase order form with some defaults set to
        permit the creation of a production order linked to this subscription
        file.
        """

        self.ensure_one()

        # The lines to include by default in the purchase order; built based on
        # turnover entries.
        polines = []

        # Guard against multiple passes on the same product.
        processed_product_ids = set()

        for turnover in self.turnover_ids:
            if not turnover.state:
                continue

            product = turnover.product_id

            # Guard against multiple passes on the same product.
            if product.id in processed_product_ids:
                continue
            processed_product_ids.add(product.id)

            poline_values = {
                'date_planned': fields.Date.today(),
                'name': product.name,
                'price_unit': turnover.unit_price,
                'product_id': product.id,
                'product_uom': product.uom_po_id.id,
                'product_qty': turnover.theoretical_quantity,
            }

            # Propagate subscription file analytics.
            subsfile_analytics = (
                self.get_bound_analytics('purchase_order_line')
            )
            if subsfile_analytics:
                poline_values.update(subsfile_analytics)

            # Propagate product analytics.
            # Let the "extract_values" method provide analytic field updates.
            poline_values.update(self.env['analytic.structure'].extract_values(
                product, 'product_template', dest_model='purchase_order_line',
            ))

            polines.append((0, 0, poline_values))  # 0: Create.

        context = self.env.context.copy()
        context.update({
            'default_description': _('%s - Production order') % self.name,
            'default_order_line': polines,
            'default_subscription_file_id': self.id,
            'default_type': 'subscription_production_order',
        })

        return {
            'context': context,
            'name': _('Production order creation'),
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
        }

    @api.multi
    def fill_sales_invoice_details(self, invoice):
        """- Build details from the sales invoice.
        - Compute the global ending period.
        - Compute / fill ending periods into each detail line.
        - Compute / fill ending periods into each invoice line.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()

        # Cache invoice data.
        invoice_data = {
            'invoice_date': invoice.create_date,
            'invoice_id': invoice.id,
            'invoice_setup_type': invoice.subscription_setup_type,
        }

        sales_invoice_details = []

        # Build details by splitting by invoice line. Ending periods will be
        # computed later on.

        for invline in invoice.invoice_line:

            begin_period = invline.begin_period_id
            product = invline.product_id
            subsfreq = invline.subscription_frequency_id

            if product.requires_breakdown and not begin_period:
                raise exceptions.Warning(_(
                    'A beginning period is required for the "%s" product.'
                ) % product.name)

            sidetails = invoice_data.copy()
            sidetails.update({
                'begin_period_id': begin_period.id,
                'discount': invline.discount,
                'free_month_count': subsfreq.free_month_count,
                'invoice_line_id': invline.id,
                'paid_month_count': subsfreq.paid_month_count,
                'price': invline.price_subtotal,
                'product_id': product.id,
                'product_type': product.type,
                'quantity': invline.quantity,
                'subscription_frequency_id': subsfreq.id,
                'tracking_type': product.tracking_type,
                'unit_price': invline.price_unit,
            })
            sales_invoice_details.append(sidetails)

        # Save the detail lines.
        self.write({'sales_invoice_detail_ids': [
            (0, 0, sales_invoice_detail)  # 0: Create.
            for sales_invoice_detail in sales_invoice_details
        ]})

        # Compute the global ending period. To avoid multiple passes on the
        # same detail line, results are cached into "end_periods_per_sidid".

        end_periods_per_sidid = {}

        global_end_period = self.end_period_id

        for sidetail in self.sales_invoice_detail_ids:

            # Only subscription products contribute to the global end period.
            if sidetail.product_id.type != 'subscription':
                continue

            periods = self.get_affected_periods(sidetail)
            if not periods:
                continue

            # Save the last period into the cache.
            # -1: Last element of the list returned by "get_affected_periods".
            # 0: First element of the (period, bool) tuple.
            end_period = periods[-1][0]
            end_periods_per_sidid[sidetail.id] = end_period

            # Keep the earliest end period as the global one.
            if (
                not global_end_period or
                end_period.date_start < global_end_period.date_start
            ):
                global_end_period = end_period

        if global_end_period:
            self.end_period_id = global_end_period

        # Now that we have sales invoice detail lines ready and a global ending
        # period, compute / fill ening periods into each detail line. Also save
        # them onto their original invoice lines.
        for sidetail in self.sales_invoice_detail_ids:

            # We might already have an end period in the cache.
            end_period = end_periods_per_sidid.get(sidetail.id)

            if not end_period:
                # Take the last period.
                periods = self.get_affected_periods(sidetail)
                if not periods:
                    continue
                # See above for details on the -1 & 0.
                end_period = periods[-1][0]

            # Save the end period onto the deatil line and the invoice line.
            sidetail.end_period_id = end_period.id
            sidetail.invoice_line_id.end_period_id = end_period.id

        # When we're done, do a final check to ensure all ending periods are
        # the same.
        same_period = None
        for sidetail in self.sales_invoice_detail_ids:
            end_period = sidetail.end_period_id
            if not end_period:
                continue
            if same_period:
                if end_period.id != same_period.id:

                    end_period_warning_message = \
                        self.generate_end_period_warning()

                    raise exceptions.Warning(_(
                        '"%s" subscription file: All ending periods must be '
                        'the same.\n'
                    ) % (self.name) + end_period_warning_message)
            else:
                same_period = end_period

    @api.multi
    def generate_end_period_warning(self):
        """Builds the body of the warning to be casted, if end periods are
        different within an invoice.

        :return: string: The body of the warning to be casted.
        """

        self.ensure_one()

        # The end periods of all the invoice lines emitted for a
        # subscription file must be the same. Prepare the message of the
        # warning to be casted, listing each invoice line and its end period.
        end_period_warning_message = _(
            '\n Product Frequency Quantity Price Begin period End period \n \n'
        )
        for sidetail in self.sales_invoice_detail_ids:

            product = sidetail.product_id.name_template
            price = sidetail.price
            quantity = sidetail.quantity
            subscription_frequency = sidetail.subscription_frequency_id.name
            begin_period = sidetail.begin_period_id.name
            end_period = sidetail.end_period_id.name

            end_period_warning_message += '%s %s %s %s %s %s \n' % (
                product, subscription_frequency, quantity, price,
                begin_period, end_period
            )

        return end_period_warning_message

    @api.multi
    def final_turnover_invref(self, log_prefix, partner):
        """Generate a final invoice / refund once all turnover entries have
        been appreciated.

        :param log_prefix: A prefix to prepend to log messages.
        :type log_prefix: String.

        :type partner: Odoo "res.partner" record set.
        """

        self.ensure_one()
        partner.ensure_one()

        total_amount = self.turnover_appreciation
        if not total_amount:
            log.info(log_prefix + (
                'The total appreciation is 0 -> no invoice / refund.'
            ))
            return

        # Some settings depend on the sign of the total amount, as we may be
        # generating an invoice or a refund.
        if total_amount > 0.0:
            invoice_type = 'out_invoice'
            journal = self.final_turnover_invoice_journal_id
            label = _('Final invoice')
            short_label = _('FinInv')
        else:
            invoice_type = 'out_refund'
            journal = self.final_turnover_refund_journal_id
            label = _('Final refund')
            short_label = _('FinRef')

        if not journal:
            raise exceptions.Warning(_('"%s" journal not defined.') % label)

        fiscal_position = partner.property_account_position
        payment_terms = partner.operational_client_payment_terms_id

        # Ensure we have accounting accounts.
        partner_account = partner.operational_client_account_id
        if not partner_account:
            raise exceptions.Warning(_(
                'The "%s" partner has operational client account.'
            ) % partner.name)

        # Cache some data, per product.
        # { product-id: [product, account, amount] }
        per_product_data = {}

        # Accessors to tuples stored in "per_product_data".
        PRODUCT, ACCOUNT, AMOUNT = range(3)

        for turnover in self.turnover_ids:
            if turnover.state != 'appreciated':
                continue

            product = turnover.product_id

            if product.id not in per_product_data:
                # Cache the accounting account set on the product.
                account = product.property_account_income
                if not account:
                    raise exceptions.Warning(_(
                        'The "%s" product has no income account.'
                    ) % product.name)
                per_product_data[product.id] = [product, account, 0.0]

            per_product_data[product.id][AMOUNT] += turnover.appreciation

        # Prepare invoice lines. 1 invoice line per product.
        invoice_lines = []

        # All set! Go through per-product data and add invoice lines.
        for product, account, amount in per_product_data.itervalues():
            if not amount:
                continue

            invline_values = {
                'account_id': account.id,
                'name': '%s%s' % (short_label, self.name),
                'price_unit': amount,
                'product_id': product.id,
                'subscription_file_id': self.id,
                'uos_id': product.uom_id.id,
            }

            # Apply the tax when neeeded. Taken from
            # account.invoice.line::product_id_change.
            if fiscal_position:
                invline_values['invoice_line_tax_id'] = [
                    (6, 0, fiscal_position.map_tax(
                        product.taxes_id or account.tax_ids,
                    ).ids),
                ]

            # Propagate product analytics.
            # Let the "extract_values" method provide analytic field updates.
            invline_values.update(
                self.env['analytic.structure'].extract_values(
                    product, 'product_template',
                    dest_model='account_invoice_line',
                )
            )

            invoice_lines.append(invline_values)

        invoice_values = {
            'account_id': partner_account.id,
            'comment': '%s - %s' % (label, self.name),
            'fiscal_position': fiscal_position.id,
            'invoice_line': [(0, 0, line) for line in invoice_lines],
            'journal_id': journal.id,
            'name': '%s%s' % (short_label, self.name),
            'subscription_file_ids': [(6, 0, [self.id])],
            'subscription_type': 'final_turnover_invref',
            'partner_id': partner.id,
            'payment_term': payment_terms.id,
            'type': invoice_type,
        }

        # Simulate a change event to grab an updated "date_due" value.
        invoice_values.update(
            self.env['account.invoice'].onchange_payment_term_date_invoice(
                payment_terms.id, None,
            ).get('value', {})
        )

        # Create and validate an accounting invoice.
        self.final_turnover_invref_id = (
            self.env['account.invoice'].create(invoice_values)
        )
        self.final_turnover_invref_id.signal_workflow('invoice_open')

    @api.multi
    def get_affected_periods(self, sidetail):
        """Compute a list of accounting periods relevant to the specified sales
        invoice line:
        - For subscription products, follow the frequency table.
        - For maintenance products, set consecutive months (12 max, unless a
        global ending period is set on the subscription file).
        - For other products, do nothing.

        :type sidetail: Odoo "subscription.file.sales_invoice_detail" record
        set.

        :return: List of tuples containing:
        - An Odoo "account.period" record set.
        - A boolean flag indicating whether the month is enabled.
        """

        self.ensure_one()
        sidetail.ensure_one()

        # The list of affected periods we are going to fill.
        affected_periods = []

        product = sidetail.product_id

        if product.type == 'subscription':
            # We rely on a frequency table here.
            subsfreq = sidetail.subscription_frequency_id
            if not subsfreq:
                raise exceptions.Warning(_(
                    'No frequency table defined for the "%s" product.'
                ) % product.name)

            month_count = subsfreq.total_month_count

            # The months to take into account in the breakdown.
            enabled_months = (
                subsfreq.month_ids.filtered('enabled').mapped('index')
            )

            # No global ending period here; respect the frequency table.
            end_period = None

        elif product.type == 'maintenance':
            # 12 months max, or until the global ending period set on the
            # subscription file.
            month_count = 12
            enabled_months = range(1, month_count + 1)
            end_period = self.end_period_id

        else:  # No-op.
            return affected_periods

        # Start from the beginning period set on the sales invoice line.
        date_obj = datetime.datetime.strptime(
            sidetail.begin_period_id.date_start, DEFAULT_SERVER_DATE_FORMAT,
        )

        # Loop through as many months as required to build a period list.
        added_months = 0
        while added_months < month_count:

            enabled_month = date_obj.month in enabled_months

            # Find an accounting period representing that month.
            # No need to check the result; the "find" method throws when no
            # period can be found.
            period = self.env['account.period'].find(
                dt=date_obj.strftime(DEFAULT_SERVER_DATE_FORMAT),
            )

            affected_periods.append((period, enabled_month))

            # Get out early when a global ending period is set.
            if end_period and period.id == end_period.id:
                break

            # Prepare for the next iteration.
            date_obj += relativedelta(months=1)
            if enabled_month:
                added_months += 1

        return affected_periods

    def get_bound_analytics(self, model):
        """Get analytic codes bound to elements linked to the specified
        subscription file, ready to be fed into the specified model, when the
        latter contains corresponding analytic dimensions.
        """

        self.ensure_one()

        ret = {}

        # Analytic code bound to the accounting file.
        acc_file_analytics = self.account_file_id.get_bound_analytics(model)
        if acc_file_analytics:
            ret.update(acc_file_analytics)

        return ret

    @api.one
    def increase_invoice_count(self, invoice):
        """When an invoice registers onto this subscription file, increase the
        relevent invoice counter.

        :type invoice: Odoo "account.invoice" record set.
        """

        count_field = {
            'out_invoice': 'client_invoice_count',
            'out_refund': 'client_refund_count',
            'in_invoice': 'supplier_invoice_count',
            'in_refund': 'supplier_refund_count',
        }[invoice.type]
        setattr(self, count_field, getattr(self, count_field) + 1)

    @api.multi
    def get_accounting_document_analytic_code(self):

        self.ensure_one()

        acc_file_analytics = (
            self.account_file_id.get_bound_analytics('account_move_line')
        )
        if not acc_file_analytics:
            raise exceptions.Warning(_(
                'No accounting document is related to this subscription file.'
            ))

        return acc_file_analytics.items()[0]

    @api.multi
    def open_accounting_documents(self):
        """Display accounting entries related to this accounting file. Don't do
        it as is done in the accounting file addon; prefer analytics to fetch
        accounting entries instead.
        """

        self.ensure_one()

        acc_file_a_key, acc_file_a_value = \
            self.get_accounting_document_analytic_code()

        return {
            'context': self.env.context,
            'domain': [('line_id.' + acc_file_a_key, '=', acc_file_a_value)],
            'name': _('Accounting documents'),
            'res_model': 'account.move',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.multi
    def open_accounting_entries(self):
        """Display accounting entries related to this accounting file.
        Doesn't do it as it is done in the accounting file addon; prefer
        analytics to fetch accounting entries instead.
        """

        self.ensure_one()

        acc_file_a_key, acc_file_a_value = \
            self.get_accounting_document_analytic_code()

        return {
            'context': self.env.context,
            'domain': [(
                'move_id.line_id.' + acc_file_a_key, '=', acc_file_a_value
            )],
            'name': _('Accounting entries'),
            'res_model': 'account.move.line',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.multi
    def open_breakdowns(self):
        """Display breakdown entries related to this subscription file.
        """

        self.ensure_one()

        return {
            'context': self.env.context,
            'domain': [('subscription_file_id', '=', self.id)],
            'name': _('%s - Breakdown') % self.name,
            'res_model': 'subscription.breakdown',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.multi
    def open_client_invoices(self):
        """Display client invoices related to this accounting file.
        """

        self.ensure_one()

        return self._open_invoices('action_invoice_tree1')

    @api.multi
    def open_client_refunds(self):
        """Display client refunds related to this accounting file.
        """

        self.ensure_one()

        return self._open_invoices('action_invoice_tree3')

    @api.multi
    def open_purchase_orders(self):
        """Display purchase orders related to this accounting file.
        """

        self.ensure_one()

        return {
            'context': self.env.context,
            'domain': [('subscription_file_id', '=', self.id)],
            'name': _('Purchase orders'),
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.multi
    def open_purchase_trackers(self):
        """Display purchase trackers related to this accounting file.
        """

        self.ensure_one()

        return {
            'context': self.env.context,
            'domain': [('id', 'in', self.purchase_tracker_ids.ids)],
            'name': _('Purchase trackers'),
            'res_model': 'purchase_tracker.tracker',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.multi
    def open_sales_orders(self):
        """Display sales orders related to this accounting file.
        """

        self.ensure_one()

        return {
            'context': self.env.context,
            # For now there is only 1 sales order.
            'domain': [('id', '=', self.sales_order_id.id)],
            'name': _('Sales orders'),
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.multi
    def open_supplier_invoices(self):
        """Display supplier invoices related to this accounting file.
        """

        self.ensure_one()

        return self._open_invoices('action_invoice_tree2')

    @api.multi
    def open_supplier_refunds(self):
        """Display supplier refunds related to this accounting file.
        """

        self.ensure_one()

        return self._open_invoices('action_invoice_tree4')

    @api.multi
    def _open_invoices(self, action_ref):
        """Display invoices / refunds related to this accounting file. Respect
        the specific actions defined for each kind.
        """

        action = self.env.ref('account.%s' % action_ref).read()[0]

        context = ast.literal_eval(action['context'])
        context.update(self.env.context)
        action['context'] = context

        domain = ast.literal_eval(action['domain'])
        domain.append(('subscription_file_ids.id', '=', self.id))
        action['domain'] = domain

        return action

    @api.multi
    def pl_transfer(self, breakdown):
        """Create accounting documents based on a breakdown entry, to be
        included within the relevant balance transfer flow (it is a "monthly
        reversal" of that intial balance transfer).

        :type breakdown: Odoo "subscription.breakdown" record set.
        """

        self.ensure_one()
        breakdown.ensure_one()

        # Cache some data.
        index = breakdown.index
        kind = breakdown.kind

        # First off, find out whether we have previous breakdowns with the same
        # properties that have not been generated and include them within a new
        # breakdown item.
        bdowns_to_fix = self.env['subscription.breakdown'].search([
            ('index', '<', index),
            ('kind', '=', kind),
            ('product_id', '=', breakdown.product_id.id),
            ('state', '=', 'to_generate'),
            ('subscription_file_id', '=', self.id),
        ])
        if bdowns_to_fix:
            this_name = '%s - %s - %s' % (
                self.name, breakdown.period_id.name, breakdown.kind,
            )
            log.info('%s: %d previous breakdown entries to fix.',
                     this_name, len(bdowns_to_fix))

            # Mark the previous breakdowns as canceled.
            bdowns_to_fix.write({'state': 'canceled'})

            # Increase the index of all breakdown entries coming after the
            # current one. Done via direct SQL to avoid 1 UPDATE per record.
            self.env.cr.execute(
                'UPDATE subscription_breakdown SET index = index + 1 WHERE '
                'index >= %s AND kind = %s AND subscription_file_id = %s',
                (index, kind, self.id,),
            )
            self.env.invalidate_all()

            # Create a new breakdown entry, inserted at the right position.
            new_breakdown = self.env['subscription.breakdown'].create({
                'amount': sum(bdowns_to_fix.mapped('amount')),
                'kind': kind,
                'index': index,
                'period_id': breakdown.period_id.id,
                'product_id': breakdown.product_id.id,
                'state': 'fix',
                'subscription_file_id': self.id,
            })

            # Generate accounting entries for the new breakdown entry.
            self.pl_transfer_impl(new_breakdown)

        # Generate accounting entries for the specified breakdown entry.
        self.pl_transfer_impl(breakdown)
        breakdown.state = 'generated'

        # Reconcile transfer entries when applicable.
        self.reconcile_balance_transfer(kind)

        self._update_flow_amounts('%s_balance_transfer' % {
            'com_agent_fee': 'com_agent_fee',
            'com_agent_fee_adjustment': 'com_agent_fee_adjustment',
            'turnover': 'sales',
        }[kind])

    @api.multi
    def pl_transfer_impl(self, breakdown):
        """Create an accounting document based on a breakdown entry, to be
        included within the relevant balance transfer flow (it is a "monthly
        reversal" of that intial balance transfer).

        :type breakdown: Odoo "subscription.breakdown" record set.
        """

        self.ensure_one()
        breakdown.ensure_one()

        # Cache some data from the breakdown entry.
        amount = breakdown.amount
        product = breakdown.product_id

        # Some settings that depend on the breakdown kind.
        settings_by_kind = {
            'com_agent_fee': {
                'flow_field': 'com_agent_fee_balance_transfer_flow_id',
                'journal_field': 'com_agent_fee_pl_transfer_journal_id',
                'label': _('Commercial agent fee'),
            },
            'com_agent_fee_adjustment': {
                'flow_field': (
                    'com_agent_fee_adjustment_balance_transfer_flow_id'
                ),
                'journal_field': (
                    'com_agent_fee_adjustment_pl_transfer_journal_id'
                ),
                'label': _('Commercial agent fee adjustment'),
            },
            'turnover': {
                'flow_field': 'sales_balance_transfer_flow_id',
                'journal_field': 'sales_pl_transfer_journal_id',
                'label': _('Turnover'),
            },
        }[breakdown.kind]

        this_name = '%s - %s - %s' % (
            self.name, breakdown.period_id.name, settings_by_kind['label'],
        )
        log.info('%s: Creating a p&l transfer.', this_name)

        # The flow to insert our new accounting document into.
        flow = getattr(self, settings_by_kind['flow_field'])
        if not flow:
            raise exceptions.Warning(_(
                'No %s balance transfer flow started on the "%s" subscription '
                'file.'
            ) % (settings_by_kind['label'], self.name))

        # Take accounting journals from settings.
        acc_journal = getattr(self, settings_by_kind['journal_field'])
        if not acc_journal:
            raise exceptions.Warning(_(
                'No %s p&l transfer journal set up.'
            ) % settings_by_kind['label'])

        # The accounting lines to be created within the accounting document.
        accounting_lines = []

        # Trick used here: We want to know which accounting documents are
        # balance transfers (as opposed to p&l transfers, which are reversals);
        # but we do not want auomatic reversal. -> We therefore fill the
        # "reversal_journal_id" field to a dummy value to be able to
        # distinguish.
        for acc_entry in (
            flow.document_ids.filtered('reversal_journal_id').mapped('line_id')
        ):
            # Filter by product.
            if acc_entry.product_id.id != product.id:
                continue

            # Build a reverse of the accounting entry.
            accounting_lines.append(build_accounting_entry(
                account_file=self.account_file_id,
                account=acc_entry.account_id,
                amount=amount,
                debit_or_credit=bool(acc_entry.credit),
                name='%s%s' % (acc_journal.code, self.name),
                partner=acc_entry.partner_id,
                product_id=product.id,
            ))

        # Create an accounting document within the balance transfer flow.
        create_accounting_document(
            flow=flow,
            name=_('P&L transfer - %s - %s - %s') % (
                settings_by_kind['label'], self.name, product.name,
            ),
            ref='%s%s' % (acc_journal.code, self.name),
            accounting_entries=accounting_lines,
            journal=acc_journal,
            to_reverse=False,
        )

        log.info('%s: Created a p&l transfer.', this_name)

    @api.multi
    def pl_transfer_setup_products(self, invoice):
        """Create P&L transfers for setup products, when there are.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()
        invoice.ensure_one()

        # This is only for balance invoices (not advances).
        if invoice.subscription_setup_type != 'balance':
            return

        # Find setup products in the invoice.
        products = (
            invoice.invoice_line.mapped('product_id')
            .filtered(lambda product: product.type == 'setup')
        )
        if not products:
            return

        # Find breakdown entries generated out of advances for these products.
        breakdowns = self.env['subscription.breakdown'].search([
            ('product_id', 'in', products.ids),
            ('state', '=', 'to_generate'),
            ('subscription_file_id', '=', self.id),
        ])
        if not breakdowns:
            return

        # Got breakdown entries; create P&L transfers.
        for breakdown in breakdowns:
            self.pl_transfer(breakdown)

    @api.multi
    def reconcile_all_balance_transfers(self):
        """Go through all available balance transfer flows to see which may be
        reconciled.
        """

        self.ensure_one()

        for kind in ('com_agent_fee', 'com_agent_fee_adjustment', 'turnover'):
            self.reconcile_balance_transfer(kind)

    @api.multi
    def reconcile_balance_transfer(self, kind):
        """Balance transfers are reversed via monthly increments, but may also
        be reversed at sales order validation (for setup products).

        When a balance transfer has been fully reversed, reconcile the initial
        balance transfer along with P&L transfers.

        This method ensures all sales products have been invoiced before going
        further.

        :param kind: A breakdown kind; see the "kind" field of
        "subscription.breakdown" for available values.
        """

        self.ensure_one()

        # Ensures all sales products have been invoiced before going further.
        sold_products = self.sales_order_detail_ids.mapped('product_id')
        invoiced_products = self.sales_invoice_detail_ids.mapped('product_id')
        if sold_products.ids != invoiced_products.ids:
            return

        # When this is the last breakdown entry, mark the flow as "reversed"
        # (which is true: reversals have happened via monthly increments) and
        # reconcile the initial balance transfer along with P&L transfers.
        if self.env['subscription.breakdown'].search_count([
            ('kind', '=', kind),
            ('state', '=', 'to_generate'),
            ('subscription_file_id', '=', self.id),
        ]):
            return  # Still some breakdown entries left to process.

        # Get the relevant flow based on the breakdown kind.
        flow = getattr(self, {
            'com_agent_fee': 'com_agent_fee_balance_transfer_flow_id',
            'com_agent_fee_adjustment': (
                'com_agent_fee_adjustment_balance_transfer_flow_id'
            ),
            'turnover': 'sales_balance_transfer_flow_id',
        }[kind])

        if flow.state == 'reversed':
            return  # Already reversed; no need to go any further.

        # Mark the flow as reversed.
        flow.signal_workflow('reversed')

        # Group entries to reconcile by accounting account.
        entries_by_account = defaultdict(lambda: self.env['account.move.line'])
        for entry in flow.document_ids.mapped('line_id'):
            entries_by_account[entry.account_id.id] |= entry

        # Reconcile each accounting entry set.
        for entry_group in entries_by_account.itervalues():
            if len(entry_group) <= 1:
                continue
            entry_group.reconcile()

    @api.model
    def register_invoice(self, invoice):
        """Register the specified invoice amongst relevant subscription files.

        :type invoice: Odoo "account.invoice" record set.
        """

        subsfiles = invoice.subscription_file_ids

        # Delegate to per-type methods when there are for further processing.
        per_type_method = getattr(
            subsfiles,
            'register_%s_invoice' % invoice.subscription_type,
            None,
        )
        if per_type_method:
            per_type_method(invoice)

        subsfiles.increase_invoice_count(invoice)

    @api.multi
    def register_com_agent_fees_invoice(self, invoice):
        """- Register the commercial agent fee invoice.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.write({'com_agent_fee_invoice_id': invoice.id})

    @api.multi
    def register_com_agent_fee_adjustments_invoice(self, invoice):
        """- Register the commercial agent fee adjustment invoice.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.write({'com_agent_fee_adjustment_invoice_id': invoice.id})

    @api.multi
    def register_purchase_tracker(self, tracker):
        """Register the specified purchase tracker among the list of validated
        purchase trackers of the subscription file. Also register attached
        documents.

        :type tracker: Odoo "purchase_tracker.tracker" record set.
        """

        self.ensure_one()

        # 4: Add a link.
        self.write({
            'purchase_tracker_attachment_ids': [
                (4, attachment.id)
                for attachment in self.env['ir.attachment'].search([
                    ('res_id', '=', tracker.id),
                    ('res_model', '=', 'purchase_tracker.tracker'),
                ])
            ],
            'purchase_tracker_count': self.purchase_tracker_count + 1,
            'purchase_tracker_ids': [(4, tracker.id)],
        })

    @api.multi
    def unregister_purchase_tracker(self, tracker):
        """Remove the specified purchase tracker from the list of validated
        purchase trackers of the subscription file. Also remove attached
        documents.

        :type tracker: Odoo "purchase_tracker.tracker" record set.
        """

        self.ensure_one()

        # 3: Cut the link.
        self.write({
            'purchase_tracker_attachment_ids': [
                (3, attachment.id)
                for attachment in self.env['ir.attachment'].search([
                    ('res_id', '=', tracker.id),
                    ('res_model', '=', 'purchase_tracker.tracker'),
                ])
            ],
            'purchase_tracker_count': self.purchase_tracker_count - 1,
            'purchase_tracker_ids': [(3, tracker.id)],
        })

    @api.multi
    def sales_balance_transfer(self, invoice):
        """Create an accounting document for the sales balance transfer.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()

        log.info('%s: Creating the sales balance transfer.', self.name)

        # Take accounting journals from settings.
        acc_journal = self.sales_balance_transfer_journal_id
        if not acc_journal:
            raise exceptions.Warning(_(
                'No sales balance transfer journal set up.'
            ))

        # Take accounting accounts from settings.
        credit_account = self.sales_balance_transfer_credit_account_id
        if not credit_account:
            raise exceptions.Warning(_(
                'No sales balance transfer credit account set up.'
            ))

        # Cache some data.
        is_advance = invoice.subscription_setup_type == 'advance'
        partner = invoice.partner_id

        # The accounting lines to be created within the accounting document.
        accounting_lines = []

        # Go through invoice lines to prepare accounting entries.
        for invoice_line in invoice.invoice_line:

            product = invoice_line.product_id

            # Ignore products that do not require a balance transfer.
            if not (
                product.requires_breakdown or
                (is_advance and product.type == 'setup')
            ):
                continue

            amount = invoice_line.price_subtotal

            credit_line = build_accounting_entry(
                account_file=self.account_file_id,
                account=credit_account,
                amount=amount,
                debit_or_credit=False,
                name=_('SalesBT%s') % self.name,
                partner=partner,
                product_id=product.id,
            )

            debit_line = build_accounting_entry(
                account_file=self.account_file_id,
                account=invoice_line.account_id,
                amount=amount,
                debit_or_credit=True,
                name=_('SalesBT%s') % self.name,
                partner=partner,
                product_id=product.id,
            )

            accounting_lines += [credit_line, debit_line]

        if not accounting_lines:
            log.info('%s: Nothing to add to the sales balance transfer.',
                     self.name)
            return

        # Re-use an existing accounting flow or create one when necessary.
        if not self.sales_balance_transfer_flow_id:
            self.sales_balance_transfer_flow_id = (
                self.env['account.file.flow'].create({
                    'account_file_id': self.account_file_id.id,
                    'name': _('Sales balance transfer'),
                })
            )
            # The flow will be reversed via monthly transfers.
            (
                self.sales_balance_transfer_flow_id
                .signal_workflow('awaiting_reversal')
            )

        # Create an accounting document within that flow.
        # Trick used here: We want to know which accounting documents are
        # balance transfers (as opposed to p&l transfers, which are reversals);
        # but we do not want auomatic reversal. -> We therefore fill the
        # "reversal_journal_id" field to a dummy value to be able to
        # distinguish.
        create_accounting_document(
            flow=self.sales_balance_transfer_flow_id,
            name=_('Sales balance transfer - %s') % self.name,
            ref='%s%s' % (acc_journal.code, self.name),
            accounting_entries=accounting_lines,
            journal=acc_journal,
            reversal_journal=acc_journal,
            to_reverse=False,
        )

        self._update_flow_amounts('sales_balance_transfer')

        log.info('%s: Created the sales balance transfer.', self.name)

    @api.multi
    def signal_updated_invoice(self, invoice):
        """- Update invoiced amounts we track (to provide a pseudo-live update
        in invoice forms).

        :type invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()

        # Update invoice amounts we track.
        invoice_amount_fields = {
            'com_agent_fees': (
                'com_agent_fee_theoretical_amount',
                'com_agent_fee_invoiced_amount',
                'com_agent_fee_invoiced_delta',
            ),
            'com_agent_fee_adjustments': (
                'com_agent_fee_adjustment_amount',
                'com_agent_fee_adjustment_invoiced_amount',
                'com_agent_fee_adjustment_invoiced_delta',
            ),
        }.get(invoice.subscription_type)
        if invoice_amount_fields:
            invoiced_amount = invoice.amount_untaxed
            if invoice.type in ('in_refund', 'out_refund'):
                invoiced_amount *= -1.0  # Refunds: negative amount.
            (
                estimated_amount_field,
                invoiced_amount_field,
                invoiced_delta_field,
            ) = invoice_amount_fields
            setattr(self, invoiced_amount_field, invoiced_amount)
            setattr(self, invoiced_delta_field, (
                getattr(self, invoiced_amount_field) -
                getattr(self, estimated_amount_field)
            ))

    @api.multi
    def signal_validated_invoice(self, invoice):
        """- Delegate to per-type methods when there are for further
        processing.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.ensure_one()

        # Delegate to per-type methods when there are for further processing.
        per_type_method = getattr(
            self,
            'signal_validated_%s_invoice' % invoice.subscription_type,
            None,
        )
        if per_type_method:
            per_type_method(invoice)

    @api.multi
    def signal_validated_sales_invoice(self, invoice):
        """Handle sales invoices:
        - Increase the turnover amount.
        - Fill sales invoice details.
        - Compute the actual commercial agent fee and the potential adjustment.
        - Create a balance transfer accounting document.
        - Prepare a breakdown plan from the commercial agent fee invoice,
        should one already be registered.
        - Prepare a breakdown plan from the sales invoice.
        - Prepare a turnover tracking plan (along with the sales invoice
        breakdown plan).
        - Create P&L transfers for setup products, when there are.
        - Go through all available balance transfer flows to see which may be
        reconciled.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.turnover_amount += abs(invoice.amount_untaxed)
        self.fill_sales_invoice_details(invoice)
        self.com_agent_fee_adjustment(invoice)
        self.com_agent_fee_provision(adjustment=True)
        self.com_agent_fee_adjustment_purchase()
        self.sales_balance_transfer(invoice)
        if self.com_agent_fee_invoice_id:
            self.breakdown(self.com_agent_fee_invoice_id,
                           sales_invoice=invoice)
        self.breakdown(invoice)
        self.pl_transfer_setup_products(invoice)
        self.reconcile_all_balance_transfers()

    @api.multi
    def signal_validated_com_agent_fees_invoice(self, invoice):
        """Handle commercial agent fee invoices:
        - Create a balance transfer accounting document.
        - Reverse the provision flow.
        - Prepare a breakdown plan when sales invoice details are already
        available (otherwise, the commercial agent fee breakdown plan will be
        generated when sales invoices are validated).

        :type invoice: Odoo "account.invoice" record set.
        """

        self.com_agent_fee_balance_transfer(invoice)
        self.com_agent_fee_provision_flow_id.signal_workflow('reversed')
        self._update_flow_amounts('com_agent_fee')
        if self.sales_invoice_detail_ids:  # Optional at this point.
            self.breakdown(invoice)

    @api.multi
    def signal_validated_com_agent_fee_adjustments_invoice(self, invoice):
        """Handle commercial agent fee adjustment invoices:
        - Create a balance transfer accounting document.
        - Reverse the provision flow.
        - Prepare a breakdown plan.

        :type invoice: Odoo "account.invoice" record set.
        """

        self.com_agent_fee_balance_transfer(invoice)
        (
            self.com_agent_fee_adjustment_provision_flow_id
            .signal_workflow('reversed')
        )
        self._update_flow_amounts('com_agent_fee_adjustment')
        self.breakdown(invoice)

    @api.multi
    def signal_validated_purchase_order(self, purchase_order):
        """Handle the validation of purchase orders that are linked to this
        subscription file.
        Delegate to per-type methods when there are for further processing.

        :type purchase_order: Odoo "purchase.order" record set.
        """

        self.ensure_one()

        # Strip the "subscription_" type prefix.
        porder_type = purchase_order.type
        if not porder_type or not porder_type.startswith('subscription_'):
            return
        porder_type = porder_type.replace('subscription_', '', 1)

        # Delegate to per-type methods when there are for further processing.
        per_type_method = getattr(
            self,
            'signal_validated_%s_purchase_order' % porder_type,
            None,
        )
        if per_type_method:
            per_type_method(purchase_order)

    @api.multi
    def signal_validated_production_order_purchase_order(self, purchase_order):
        """Handle the validation of production orders that are linked to this
        subscription file:
        - Update turnover entries.
        - When there are deltas, generate provisions.
        - When all turnover entries have been appreciated, generate a final
        invoice / refund and reverse provisions.

        :type purchase_order: Odoo "purchase.order" record set.
        """

        LOG_PREFIX = '%s - Production order %s: ' % (
            self.name, purchase_order.name,
        )
        log.info(LOG_PREFIX + 'Start.')

        # Find an accounting period around the purchase order date.
        # No need to check the result; the "find" method throws when no period
        # can be found.
        podate = purchase_order.date_order
        period = self.env['account.period'].find(dt=podate)

        for poline in purchase_order.order_line:
            product = poline.product_id

            # Ensure the product requires turnover tracking.
            if (
                product.tracking_type != 'actual' or
                product.type != 'subscription'
            ):
                log.warning('No turnover tracking for the "%s" product.' %
                            product.name)
                continue

            # Find a turnover entry for this product.
            turnover = self.env['subscription.turnover'].search([
                ('period_id', '=', period.id),
                ('product_id', '=', product.id),
                ('state', '!=', False),
                ('subscription_file_id', '=', self.id),
            ], limit=1)
            if not turnover:
                log.warning(
                    'No turnover entry for the "%s" product on the "%s" '
                    'period.' % (product.name, period.name)
                )
                continue

            if turnover.state == 'appreciated':
                raise exceptions.Warning(_(
                    'The turnover entry for the "%s" product on the "%s" '
                    'period for the "%s" subscription file has already been '
                    'appreciated.'
                ) % (product.name, period.name, self.name))

            # Update quantity data of the turnover entry.
            actual_quantity = poline.product_qty
            quantity_delta = actual_quantity - turnover.theoretical_quantity
            appreciation = quantity_delta * turnover.unit_price
            turnover.write({
                'actual_quantity': actual_quantity,
                'appreciation': appreciation,
                'quantity_delta': quantity_delta,
                'state': 'appreciated',
            })

            # When there is an appreciation amount, generate a provision.
            if appreciation:
                self.billed_revenue_provision(turnover, podate)

        # When all turnover entries have been appreciated, generate a final
        # invoice / refund and reverse provisions.
        if not self.env['subscription.turnover'].search_count([
            ('state', '=', 'to_appreciate'),
            ('subscription_file_id', '=', self.id),
        ]):
            log.info(LOG_PREFIX + (
                'All turnover entries have been appreciated; generating the '
                'final invoice / refund and reversing provisions.'
            ))

            self.final_turnover_invref(LOG_PREFIX, purchase_order.partner_id)

            (
                self.unbilled_revenue_provision_flow_id |
                self.overbilled_revenue_provision_flow_id
            ).signal_workflow('reversed')

            self._update_flow_amounts('unbilled_revenue')
            self._update_flow_amounts('overbilled_revenue')

        log.info(LOG_PREFIX + 'Done.')

    @api.multi
    def _update_flow_amounts(self, field_prefix):
        """Update provision, reversal, net amounts of the flow referenced by
        the specified name.
        It is assumed fields follow a specific naming convention.

        :type flow_name: String.
        """

        self.ensure_one()

        # Sometimes we add a "provision_" to flow field names.
        flow = getattr(self, '%s_flow_id' % field_prefix, None)
        if flow is None:
            flow = getattr(self, '%s_provision_flow_id' % field_prefix, None)
        if not flow:
            return

        provision_amount = 0.0
        reversal_amount = 0.0

        for doc in flow.document_ids:
            # Handle the "reversal_journal_id" trick we sometimes use.
            if doc.to_reverse or doc.reversal_journal_id:
                provision_amount += doc.amount
            else:
                reversal_amount += doc.amount

        setattr(self, '%s_provision_amount' % field_prefix, provision_amount)
        setattr(self, '%s_reversal_amount' % field_prefix, reversal_amount)
        setattr(self, '%s_net_amount' % field_prefix,
                provision_amount - reversal_amount)
