from collections import OrderedDict

from openerp import _
from openerp import api
from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class ProductTemplate(models.Model):
    """Customize products to include subscription settings.
    """

    _inherit = 'product.template'

    fee_rate = fields.Float(
        string='Fee rate (%)',
        digits=dp.get_precision('Account'),  # Like sales amounts.
        help=(
            'The rate to use when computing fees from this product; a '
            'percentage (between 0 and 100).'
        ),
    )

    @api.depends('type')
    @api.one
    def _get_requires_breakdown(self):
        self.requires_breakdown = self.type in ('subscription', 'maintenance')

    requires_breakdown = fields.Boolean(
        compute='_get_requires_breakdown',
        string='Financial breakdown',
        help='Whether sales for this product require a financial breakdown.',
        store=True,
    )

    tracking_type = fields.Selection(
        selection=[
            ('theoretical', 'Theoretical quantities'),
            ('actual', 'Actual quantities'),
        ],
        string='Tracking type',
        help=(
            'Whether the turnover is to be tracked based on actually '
            'delivered quantities (as opposed to ordered quantities).'
        ),
    )

    # Override to add subscription-related types.
    type = fields.Selection(
        selection_add=[
            ('subscription', 'Subscription'),
            ('setup', 'Setup'),
            ('maintenance', 'Maintenance'),
        ],
    )

    def fields_get(
        self, cr, uid, allfields=None, context=None, write_access=True,
    ):
        """Override to:
        - Properly translate added selection items (Odoo bug).
        """

        ADDED_SEL_ITEMS = {
            'type': {
                'subscription': _('Subscription'),
                'setup': _('Setup'),
                'maintenance': _('Maintenance'),
            },
        }

        ret = super(ProductTemplate, self).fields_get(
            cr, uid, allfields=allfields, context=context,
            write_access=write_access,
        )

        # ret: { field-name: { 'selection': sel-items } }

        for field_name, sel_items in ADDED_SEL_ITEMS.iteritems():

            field_info = ret.get(field_name)
            if field_info:

                sel_data = field_info.get('selection')
                if sel_data:

                    # sel_data is a list of tuples; convert to a dictionary to
                    # update it, then back to a list of tuples.
                    sel_data = OrderedDict(sel_data)
                    sel_data.update(sel_items)
                    field_info['selection'] = sel_data.items()

        return ret
