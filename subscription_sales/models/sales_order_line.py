from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models
from openerp.osv import fields as odoo7_fields

import openerp.addons.decimal_precision as dp


class SalesOrderLine(models.Model):
    """Adapt sales orders lines to subscription sales.
    """

    _inherit = 'sale.order.line'

    def _get_invoiced(self, cr, uid, ids, field_name, args, context=None):
        """Redefine not to consider advance invoices made for setup products as
        being complete invoices (that would not allow further invoices for the
        same sales order lines).

        Adapted from addons/sale/sale.py:890 ("_fnct_line_invoiced" function).
        """

        ret = {}

        for soline in self.browse(cr, uid, ids, context=context):
            invoiced = False

            invlines = soline.invoice_lines
            if invlines:
                for invline in invlines:
                    invoice = invline.invoice_id

                    if invoice.subscription_setup_type == 'advance':
                        continue  # Ignore setup product advances.

                    if invoice.state == 'cancel':
                        invoiced = False
                        break

                    invoiced = True

            ret[soline.id] = invoiced

        return ret

    def _invoices_to_solines(self, cr, uid, ids, context=None):
        """Delegate to the base method."""
        return self.pool['sale.order.line']._order_lines_from_invoice(
            cr, uid, ids, context=context,
        )

    _columns = {
        # Override the "invoiced" field to handle advance invoices made for
        # setup products (see the "_get_invoiced" method for details). Done in
        # Odoo 7 style as the base field is in Odoo 7 style, so as not to
        # disturb store function triggers.
        'invoiced': odoo7_fields.function(
            _get_invoiced,
            string='Invoiced',
            type='boolean',
            store={
                'sale.order.line': (
                    lambda self, cr, uid, ids, context=None: ids,
                    ['invoice_lines'], 10,
                ),
                'account.invoice': (
                    _invoices_to_solines,
                    ['state', 'subscription_setup_type'], 10
                ),
            },
        ),
    }

    free_month_count = fields.Integer(
        related=('subscription_frequency_id', 'free_month_count'),
        string='Free month count',
        help='The total amount of months where publications are free.',
        readonly=True,
        store=True,
    )

    paid_month_count = fields.Integer(
        related=('subscription_frequency_id', 'paid_month_count'),
        string='Paid month count',
        help='The total amount of months where publications are not free.',
        readonly=True,
        store=True,
    )

    @api.multi
    def _get_price_subtotal(self):
        """Provide the regular amount but multiply it by the paid month count
        when a subscription frequency table is defined.
        """

        # This is the regular Odoo7-style method; the 2 "None"s are for
        # "field_name" and "arg" arguments.
        amounts_per_line = self._amount_line(None, None)

        for soline in self:
            amount = amounts_per_line.get(soline.id, 0.0)

            paid_month_count = soline.paid_month_count
            if paid_month_count > 0:
                amount *= paid_month_count

            soline.price_subtotal = amount

    # Redefine to change how this field is computed.
    price_subtotal = fields.Float(
        compute=_get_price_subtotal,
        compute_sudo=True,
        string='Sub-total',
        digits=dp.get_precision('Account'),
        help=(
            'The sub-total of this line; corresponds to [paid-month-count *] '
            'quantity * unit-price.'
        ),
    )

    subscription_frequency_id = fields.Many2one(
        comodel_name='subscription.frequency',
        string='Frequency table',
        ondelete='restrict',
        help='The subscription frequency table to use for this line.',
    )

    @api.constrains('product_id', 'subscription_frequency_id')
    @api.one
    def _check_subscription_frequency(self):
        """Ensure a subscription frequency is provided for subscription
        products.
        """

        if (
            self.product_id.type == 'subscription' and
            not self.subscription_frequency_id
        ):
            raise exceptions.ValidationError(_(
                'A subscription frequency is required for subscription '
                'products.'
            ))

    def _prepare_order_line_invoice_line(
        self, cr, uid, line, account_id=False, context=None,
    ):
        """Override to:
        - Include subscription information into invoice lines generated from
        this sales order line.
        - Add analytics.
        """

        ret = super(SalesOrderLine, self)._prepare_order_line_invoice_line(
            cr, uid, line, account_id=False, context=None,
        )

        order = line.order_id

        # Include subscription information into invoice lines generated from
        # this sales order line.
        if order.is_for_subscription:
            ret.update({
                'subscription_frequency_id': line.subscription_frequency_id.id,
                'subscription_file_id': order.subscription_file_id.id,
            })

            # Propagate sales order analytics.
            # Let the "extract_values" method provide analytic field updates.
            ret.update(self.pool['analytic.structure'].extract_values(
                cr, uid, order, 'sale_order',
                dest_model='account_invoice_line', context=context,
            ))

            # Propagate product analytics.
            # Let the "extract_values" method provide analytic field updates.
            ret.update(self.pool['analytic.structure'].extract_values(
                cr, uid, line.product_id, 'product_template',
                dest_model='account_invoice_line', context=context,
            ))

        return ret
