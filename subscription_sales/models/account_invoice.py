from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class AccountingInvoice(models.Model):
    """Adapt accounting invoices to subscription sales.
    """

    _inherit = 'account.invoice'

    # Similar to the "amount_total" field of "account.invoice".
    expected_total_amount = fields.Float(
        string='Expected total amount',
        digits=dp.get_precision('Account'),
        help=(
            'Expected total amount of the invoice, including taxes. When this '
            'is defined, no delta will be accepted. The check occurs when '
            'validating the invoice.'
        ),
        readonly=True,
    )

    subscription_file_ids = fields.Many2many(
        comodel_name='subscription.file',
        relation='invoice_subscription_file_rel',
        column1='invoice_id',
        column2='subscription_file_id',
        string='Subscription files',
        readonly=True,
    )

    subscription_setup_type = fields.Selection(
        selection=[
            ('advance', 'Advance'),
            ('balance', 'Balance'),
        ],
        string='Subscriptions > Setup type',
        default='balance',
        help=(
            'Type specific to sales of "Setup" products to distinguish '
            'advance invoices from final ones.'
        ),
    )

    # Elements in this selection field should have a corresponding
    # "register_<sel>_invoice" method in subscription files (see
    # SubscriptionFile::register_invoice for the implementation).
    # Types are similar to those added to purchase orders and trackers.
    subscription_type = fields.Selection(
        selection=[
            ('sales', 'Generated from the sales order'),
            ('com_agent_fees', 'Commercial agent fees'),
            ('com_agent_fee_adjustments', 'Commercial agent fee adjustments'),

            # Specfic to invoices (not for purchase orders / trackers).
            ('final_turnover_invref', 'Final turnover invoice / refund'),
        ],
        string='Subscriptions > Type',
        help=(
            'Type within the subscription accounting process; empty when the '
            'invoice is not for subscriptions.'
        ),
        readonly=True,
    )

    @api.multi
    def write(self, vals):
        """Override to signal the change to subscription files, when there are.
        """

        ret = super(AccountingInvoice, self).write(vals)

        for invoice in self:
            for subsfile in invoice.subscription_file_ids:
                subsfile.signal_updated_invoice(invoice)

        return ret

    @api.multi
    def invoice_validate(self):
        """Override to:
        - Ensure the expected total amount is respected.
        - Signal the validation to subscription files, when there are.
        """

        ret = super(AccountingInvoice, self).invoice_validate()

        for invoice in self:

            # Ensure the expected total amount is respected.
            if (
                invoice.expected_total_amount and
                invoice.amount_total != invoice.expected_total_amount
            ):
                raise exceptions.Warning(_(
                    'Invoice validation: expected amount = %s ; total amount '
                    '= %s.'
                ) % (invoice.expected_total_amount, invoice.amount_total))

            # Signal the validation to subscription files, when there are.
            for subsfile in invoice.subscription_file_ids:
                subsfile.signal_validated_invoice(invoice)

        return ret

    @api.one
    def wkf_subscription_reg(self):
        """Register the invoice within the subscription accounting process.
        """

        self.env['subscription.file'].register_invoice(self)
