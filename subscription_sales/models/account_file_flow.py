from collections import OrderedDict
from openerp import _
from openerp import api
from openerp import fields
from openerp import models


class AccountingFlow(models.Model):
    """Add a "canceled" state to accounting flows.
    """

    _inherit = 'account.file.flow'

    # Override to add an element into the selection list.
    state = fields.Selection(
        selection_add=[('canceled', 'Canceled')],
    )

    def fields_get(
        self, cr, uid, allfields=None, context=None, write_access=True,
    ):
        """Override to:
        - Properly translate added selection items (Odoo bug).
        """

        ADDED_SEL_ITEMS = {
            'state': {
                'canceled': _('Canceled'),
            },
        }

        ret = super(AccountingFlow, self).fields_get(
            cr, uid, allfields=allfields, context=context,
            write_access=write_access,
        )

        # ret: { field-name: { 'selection': sel-items } }

        for field_name, sel_items in ADDED_SEL_ITEMS.iteritems():

            field_info = ret.get(field_name)
            if field_info:

                sel_data = field_info.get('selection')
                if sel_data:

                    # sel_data is a list of tuples; convert to a dictionary to
                    # update it, then back to a list of tuples.
                    sel_data = OrderedDict(sel_data)
                    sel_data.update(sel_items)
                    field_info['selection'] = sel_data.items()

        return ret

    @api.one
    def wkf_canceled(self):
        """- Reverse the accounting documents.
        - Switch the state to "canceled".
        - Signal the linked accounting file about this.
        """

        self.reverse_documents(reconcile=False)

        self.state = 'canceled'

        self.account_file_id.check_flow_workflows()
