from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models

import openerp.addons.decimal_precision as dp


class AccountingInvoiceLine(models.Model):
    """Adapt accounting invoice lines to subscription sales.
    """

    _inherit = 'account.invoice.line'

    begin_period_id = fields.Many2one(
        comodel_name='account.period',
        string='Beginning period',
        domain=[('special', '=', False), ('state', '=', 'draft')],
        ondelete='restrict',
        help='The accounting period this subscription line starts from.',
    )

    end_period_id = fields.Many2one(
        comodel_name='account.period',
        string='Ending period',
        ondelete='restrict',
        help=(
            'The accounting period this subscription line goes up to. Will be '
            'filled at invoice validation by the subscription file.'
        ),
        readonly=True,
    )

    free_month_count = fields.Integer(
        related=('subscription_frequency_id', 'free_month_count'),
        string='Free month count',
        help='The total amount of months where publications are free.',
        readonly=True,
        store=True,
    )

    paid_month_count = fields.Integer(
        related=('subscription_frequency_id', 'paid_month_count'),
        string='Paid month count',
        help='The total amount of months where publications are not free.',
        readonly=True,
        store=True,
    )

    @api.depends(
        'discount', 'invoice_id.currency_id', 'invoice_id.partner_id',
        'invoice_id.subscription_type', 'invoice_line_tax_id', 'price_unit',
        'product_id', 'quantity', 'subscription_frequency_id.paid_month_count',
    )
    @api.one
    def _get_price_subtotal(self):
        """Provide the regular amount but multiply it by the paid month count
        when a subscription frequency table is defined.
        """

        # Call the regular method we are overriding here.
        self._compute_price()

        if self.invoice_id.subscription_type == 'sales':
            paid_month_count = self.paid_month_count
            if paid_month_count > 0:
                self.price_subtotal *= paid_month_count

    # Redefine to change how this field is computed.
    price_subtotal = fields.Float(
        compute=_get_price_subtotal,
        compute_sudo=True,
        string='Amount',
        digits=dp.get_precision('Account'),
        help=(
            'The sub-total of this line; corresponds to [paid-month-count *] '
            'quantity * unit-price.'
        ),
        store=True,
    )

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',  # On purpose, to let other objects block dels.
        help='The subscription file this accounting invoice line is for.',
        readonly=True,
    )

    subscription_frequency_id = fields.Many2one(
        comodel_name='subscription.frequency',
        string='Frequency table',
        ondelete='restrict',
        help='The subscription frequency table to use for this line.',
    )

    @api.constrains('product_id', 'subscription_frequency_id')
    @api.one
    def _check_subscription_frequency(self):
        """Ensure a subscription frequency is provided for subscription
        products.
        """

        if (
            self.invoice_id.subscription_type in (
                'sales', 'com_agent_fees', 'com_agent_fee_adjustments',
            ) and
            self.product_id.type == 'subscription' and
            not self.subscription_frequency_id
        ):
            raise exceptions.ValidationError(_(
                'A subscription frequency is required for subscription '
                'products.'
            ))

    @api.model
    def create(self, vals):
        """Override to fill analytic codes when there is a subscription file.
        """

        self._fill_subscription_analytics(vals)
        return super(AccountingInvoiceLine, self).create(vals)

    @api.multi
    def write(self, vals):
        """Override to fill analytic codes when there is a subscription file.
        """

        self._fill_subscription_analytics(vals)
        return super(AccountingInvoiceLine, self).write(vals)

    @api.model
    def _fill_subscription_analytics(self, values):
        """Include analytic codes related to subscription files when there are.

        :param values: The data being updated.
        """

        lnfile_id = values.get('subscription_file_id')
        if not lnfile_id:
            return
        lnfile = self.env['subscription.file'].browse(lnfile_id)

        bound_analytics = lnfile.get_bound_analytics('account_invoice_line')
        if bound_analytics:
            values.update(bound_analytics)
