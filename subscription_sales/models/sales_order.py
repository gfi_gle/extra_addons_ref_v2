import logging

from openerp import _
from openerp import api
from openerp import fields
from openerp import models


log = logging.getLogger(__name__)


class SalesOrder(models.Model):
    """Adapt sales orders to subscription sales.
    """

    _inherit = 'sale.order'

    com_agent_fee_purchase_order_id = fields.Many2one(
        comodel_name='purchase.order',
        string='Commercial agent fee purchase order',
        ondelete='cascade',  # On purpose, to let purchases block dels.
        copy=False,
        help=(
            'The purchase order generated from this sales order to track '
            'commercial agent fee invoices.'
        ),
        readonly=True,
    )

    com_agent_partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Commercial agent',
        ondelete='set null',
        help='The commercial agent behind this subscription sales order.',
    )

    is_for_subscription = fields.Boolean(
        string='For a subscription',
        help=(
            'Whether this sales order is for a subscription; additional, '
            'subscription specific parameters will be available and a '
            'subscription file will be generated when the sales order is '
            'confirmed.'
        ),
    )

    queue_task_ids = fields.Many2many(
        comodel_name='queue.task',
        relation='ss_sales_order_queue_task_rel',
        column1='order_id',
        column2='task_id',
        string='Queue tasks',
        copy=False,
        help='The tasks related to this sales order that are run by a queue.',
        readonly=True,
    )

    subscription_file_id = fields.Many2one(
        comodel_name='subscription.file',
        string='Subscription file',
        ondelete='cascade',  # On purpose, to let the sales order block dels.
        copy=False,
        help='The subscription file generated from this sales order.',
        readonly=True,
    )

    @api.multi
    def action_cancel(self):
        """Override to also cancel the subscription file when there is one.
        """

        (
            self.mapped('subscription_file_id.account_file_id.flow_ids')
            .signal_workflow('canceled')
        )

        return super(SalesOrder, self).action_cancel()

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        """Override to:
        - Set the right accounting account.
        - Set the right payment terms.
        - Include subscription information into invoices generated from this
        sales order.
        """

        ret = super(SalesOrder, self)._prepare_invoice(
            cr, uid, order, lines, context=context,
        )

        if order.is_for_subscription:
            partner = order.partner_id

            ret.update({
                'account_id': partner.operational_client_account_id.id,
                'payment_term': partner.operational_client_payment_terms_id.id,
                'subscription_file_ids': [(4, order.subscription_file_id.id)],
                'subscription_type': 'sales',
            })

        return ret

    @api.multi
    def create_subsfile(self):
        print 'TT'*100
        """Launch the subscription file creation, along with its following
        elements (as specified through workflows). Run this here instead of
        using workflow magic) so the sales order workflow can catch invoice
        validation / payment events should they occur instantly.
        """

        self.ensure_one()

        # As this is run via a queued task, the sales order may have been
        # canceled in the meantime.
        if self.state == 'cancel':
            log.info('%s: The sales order has been canceled; no subscription '
                     'file creation.', self.name)
            return True

        self.signal_workflow('create_subsfile')

        return True

    @api.multi
    def display_com_agent_fee_purchase_order(self):
        """Display the commercial agent fee purchase order generated for this
        sales order.
        """

        self.ensure_one()

        cafpo = self.com_agent_fee_purchase_order_id

        return {
            'context': self.env.context,
            'name': cafpo.name,
            'res_id': cafpo.id,
            'res_model': 'purchase.order',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
        }

    @api.multi
    def display_subscription_file(self):
        """Display the subscription file generated for this sales order.
        """

        self.ensure_one()

        subsfile = self.subscription_file_id

        return {
            'context': self.env.context,
            'name': subsfile.name,
            'res_id': subsfile.id,
            'res_model': 'subscription.file',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'view_type': 'form',
        }

    @api.one
    def wkf_create_com_agent_fee_purchase(self):
        """Create a commercial agent fee purchase order based on this sales
        order.
        """

        log.info('%s: Creating the com agent fee purchase order.', self.name)

        partner = self.com_agent_partner_id
        if not partner:
            log.info('%s: No commercial agent.', self.name)
            return

        # Prepare some data.
        fiscal_position = partner.property_account_position
        payment_terms = partner.referrer_fees_payment_terms_id

        # Gather fees by sales order line by looking them up into the
        # subscription file detail table.
        fees_by_soline = {
            detail.sales_order_line_id.id: detail.theoretical_amount
            for detail in self.subscription_file_id.com_agent_fee_detail_ids
        }

        def addAnalytics(order_line, product):
            # Propagate sales order analytics.
            # Let the "extract_values" method provide analytic field updates.
            order_line.update(self.env['analytic.structure'].extract_values(
                self, 'sale_order', dest_model='purchase_order_line',
            ))

            # Propagate product analytics.
            # Let the "extract_values" method provide analytic field updates.
            order_line.update(self.env['analytic.structure'].extract_values(
                product, 'product_template', dest_model='purchase_order_line',
            ))

        def mapTaxes(taxes):
            """Map taxes through the fiscal position and format to insert."""
            return [(6, 0, fiscal_position.map_tax(taxes).ids)]

        # Prepare the line list.
        order_lines = []

        # Defaults for all order lines.
        order_line_defaults = {
            'date_planned': fields.Date.today(),
        }

        # Fill the line list. We propagate all sold products to the commercial
        # agent fee purchase order.
        for soline in self.order_line:
            order_line = order_line_defaults.copy()
            product = soline.product_id
            order_line.update({
                'name': product.name,
                'price_unit': fees_by_soline.get(soline.id, 0.0),
                'product_id': product.id,
                'product_qty': 1.0,
                'product_uom': product.uom_po_id.id,
                'subscription_frequency_id': (
                    soline.subscription_frequency_id.id
                ),
            })
            if fiscal_position:
                order_line['taxes_id'] = mapTaxes(product.supplier_taxes_id)
            addAnalytics(order_line, product)
            order_lines.append(order_line)

        # All set! Create a purchase order.
        self.com_agent_fee_purchase_order_id = (
            self.env['purchase.order']
            .with_context({'partner_id': partner.id})
            .create({
                'description': _('Commercial agent fee'),
                'fiscal_position': fiscal_position.id,
                'order_line': [(0, 0, line) for line in order_lines],
                'partner_id': partner.id,
                'payment_term_id': payment_terms.id,
                'subscription_file_id': self.subscription_file_id.id,
                'type': 'subscription_com_agent_fees',
            })
        )

        log.info('%s: Created the com agent fee purchase order.', self.name)

    @api.one
    def wkf_create_subscription_file(self):
        """Create a subscription file based on this sales order.
        """

        log.info('%s: Creating the subscription file.', self.name)

        self.subscription_file_id = (
            self.env['subscription.file'].create_from_sales_order(self)
        )

        self.message_post(
            body=_('Subscription file created.'),
            subject=_('Subscription file creation'),
        )

        log.info('%s: Created the subscription file.', self.name)

    @api.one
    def wkf_wait_subsfile(self):
        """Register a subscription file creation task, to be run by a queue.
        """

        self.queue_task_ids += self.env['queue.task'].create({

            'args_interpolation': """{{
                'order_id': {order_id!r},
            }}""".format(order_id=self.id),

            'queue_id': self.env.ref(
                'subscription_sales.ss_sales_order_create_subsfile_queue'
            ).id,
        })

        self.message_post(
            body=_(
                'Registered a delayed task to create the subscription file.'
            ),
            subject=_('Subscription file creation'),
        )
