-- Create sales order line computed columns; useful to avoid letting Odoo create them, which might
-- take too long on a database that already contains a lot of sales order lines.

ALTER TABLE sale_order_line ADD COLUMN free_month_count integer;
COMMENT ON COLUMN sale_order_line.free_month_count IS 'Free month count';

ALTER TABLE sale_order_line ADD COLUMN paid_month_count integer;
COMMENT ON COLUMN sale_order_line.paid_month_count IS 'Paid month count';
