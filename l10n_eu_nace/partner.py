# -*- encoding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2013, 2015 XCG Consulting (www.xcg-consulting.fr) for 7.0
#    Copyright (C) 2011 Numerigraphe SARL for original version
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, fields

class PartnerCategory(osv.osv):
    """Let users search on code without a dot. Also increase name column size."""
    _name = 'res.partner.category'
    _inherit = 'res.partner.category'

    _columns = {
        'name': fields.char('Category Name', required=True, size=144, translate=True),
    }

    def name_search(self, cr, uid, name, args=None, operator='ilike', context=None, limit=80):
        """When no results are found, try again with an additional "."."""
        results = super(PartnerCategory, self).name_search(cr, uid, name, args=args,
                               operator=operator, context=context, limit=limit)
        if not results and name and len(name)>2:
            # Add a "." after the 2nd character, in case that makes it a NACE code
            results = super(PartnerCategory, self).name_search(cr, uid, 
                    '%s.%s' % (name[:2], name[2:]),
                    args=args, operator=operator, context=context, limit=limit)
        return results

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
