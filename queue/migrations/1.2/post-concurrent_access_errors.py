import logging


log = logging.getLogger(__name__)


def migrate(cr, installed_version):
    """Now that we properly handle tasks in error due to concurrent access
    issues, migrate tasks that matter based on heuristics (as we store the
    error message). 
    """

    log.info('Start.')

    log.info('Marking tasks in error due to concurrent access issues as such.')
    cr.execute('''
    UPDATE queue_task
    SET in_error_concurrent_access = TRUE
    WHERE
        state = 'error' AND
        (
            message LIKE '%could not serialize access due to concurrent%' OR
            message LIKE '%could not obtain lock on row in relation%'
        );
    ''')

    log.info('Done.')
