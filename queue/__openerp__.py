# -*- coding: utf-8 -*-
##############################################################################
#
#    Queue for Odoo
#    Copyright (C) 2015 XCG Consulting (http://odoo.consulting)
#    Author: Vincent Hatakeyama
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': "Queue",
    'summary': "Queues and tasks",
    'version': '8.0.1.2',
    'author': 'XCG Consulting',
    'category': "Technical Settings",
    'description': """
Queue
=====

Queue for tasks to do (usually for those that take too much time to do
interactively).

    """,
    'website': 'http://odoo.consulting/',
    'init_xml': [],
    'depends': [
        'base',
    ],

    'data': [
        'security/ir.model.access.csv',

        'menu.xml',

        'data/cron_tasks/concurrent_access_restart_cron_task.xml',

        'views/queue.xml',
        'views/task.xml',
    ],

    'test': [
    ],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
