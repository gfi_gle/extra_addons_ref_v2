Models
======

Queue
-----

.. automodule:: openerp.addons.queue.models.queue
    :members:
    :undoc-members:

Task
----

.. automodule:: openerp.addons.queue.models.task
    :members:
    :undoc-members:

