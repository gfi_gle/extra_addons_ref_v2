# -*- coding: utf-8 -*-
###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################
from openerp.tests.common import TransactionCase
import openerp.tests
from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from mock import Mock
from openerp import api


def test_run_impl(cr, uid, task_args):
    """Method injected into the "queue" object, which we designate as
    the target of task runs.
    """

    pass


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class TestQueue(TestBase):
    """Tests around queues and tasks. Done in a similar fashion than OCA ones.
    """

    def setUp(self):
        """Create queues and tasks, to be used in tests.
        """

        super(TestQueue, self).setUp()
        self.memory = TestMemory()

    def initialize(self):

        # Create queues.
        (queue1, queue2) = self.createAndTest(
            'queue',
            [
                {
                    'name': 'Queue1',
                    'model': 'queue',
                    'function': 'create',
                    'args': (
                        '({\'name\':\'Queue Test\', \'model\': \'queue\'},)'
                    )
                },
                {
                    'name': 'Queue2',
                    'model': 'queue',
                    'function': 'create',
                    'args': (
                        '({\'name\':\'Queue %(name)s\','
                        ' \'model\': \'queue\'},)'
                    )
                },
            ]
        )

        self.memory.queue1 = queue1
        self.memory.queue2 = queue2

        # Create a Mock from 'queue2', to add a method on this object, which
        # will be used to test task execution on concurrent access errors.
        self.memory.mock = Mock(return_value=self.memory.queue2)
        self.memory.mock().test_run_impl = test_run_impl

        # Add tasks into queues.
        self.memory.queue2_tasks = self.createAndTest(
            'queue.task',
            [
                {
                    'queue_id': self.memory.queue2.id,
                    'args_interpolation': '{\'name\': \'task%d\'}' % (
                        task_index + 1
                    ),
                }
                for task_index in xrange(3)
            ]
        )

        for i, task in enumerate(self.memory.queue2_tasks):
            setattr(self.memory, 'task%d' % (i + 1), task)

        self.memory.company = self.env.ref('base.main_company')

        (partner1, partner2, partner3) = self.createAndTest(
            'res.partner',
            [
                {
                    'lastname': 'Partner 1',
                    'firstname': 'Test',
                },
                {
                    'lastname': 'Partner 2',
                    'firstname': 'Test',
                },
                {
                    'lastname': 'Partner 3',
                    'firstname': 'Test',
                },
            ]
        )

        self.memory.partner1 = partner1
        self.memory.partner2 = partner2
        self.memory.partner3 = partner3

        (user1, user2, user3) = self.createAndTest(
            'res.users',
            [
                {
                    'company_id': self.memory.company.id,
                    'partner_id': self.memory.partner1.id,
                    'login': 'test',
                    'active': True,
                    'alias_contact': 'everyone',
                    'groups_id': [
                        (
                            6,
                            0,
                            [
                                self.env.ref('base.group_no_one').id,
                                self.env.ref('base.group_erp_manager').id,
                            ]
                        ),
                    ],
                },
                {
                    'company_id': self.memory.company.id,
                    'partner_id': self.memory.partner2.id,
                    'login': 'test2',
                    'active': True,
                    'alias_contact': 'everyone',
                    'groups_id': [
                        (
                            6,
                            0,
                            [
                                self.env.ref('base.group_no_one').id,
                                self.env.ref('base.group_erp_manager').id,
                                self.env.ref('base.group_user').id,
                            ]
                        ),
                    ],
                },
                {
                    'company_id': self.memory.company.id,
                    'partner_id': self.memory.partner3.id,
                    'login': 'test3',
                    'active': True,
                    'alias_contact': 'everyone',
                    'groups_id': [
                        (
                            6,
                            0,
                            [
                                self.env.ref('base.group_user').id,
                            ]
                        )
                    ],
                },
            ]
        )

        self.memory.user1 = user1
        self.memory.user2 = user2
        self.memory.user3 = user3

    def test_0001_call(self):
        """Ensure the "call" method of "queue" can be called.
        """

        self.initialize()

        # TODO test that one more queue exists
        self.memory.queue1.call(None)
        self.assertTrue(
            True,
            "should not fail"
        )

    def test_0002_run(self):
        """Run tasks in a queue, respecting the run strategy.
        """

        self.memory.queue2.run()
        self.assertEqual(
            self.memory.task1.state,
            'done',
        )
        self.assertEqual(
            self.memory.task2.state,
            'todo',
        )
        self.assertEqual(
            self.memory.task3.state,
            'todo',
        )

    def test_0003_define_managing_groups(self):

        # Failure on 'write', when trying to add a managing group, if the user
        # is not a member of the Access Rights group.
        with self.assertRaises(openerp.exceptions.Warning):
            (
                self.memory.queue2.sudo(self.memory.user3.id).
                managing_group_ids
            ) = [(4, self.env.ref('base.group_user').id)]

        # Success on 'write', when trying to add a managing group, if the user
        # is a member of the Access Rights group.
        self.memory.queue2.sudo(self.memory.user1.id).managing_group_ids = \
            [(4, self.env.ref('base.group_user').id)]

    def test_0004_force_run(self):

        # Failure when trying to force execution on a task, if the user is not
        # a member of the queue's managing groups.
        with self.assertRaises(openerp.exceptions.Warning):
            self.memory.task1.sudo(self.memory.user1.id).force_run()

        # Success when trying to force execution on a task, if the user is a
        # member of the queue's managing groups.
        self.memory.task1.sudo(self.memory.user2.id).force_run()

    def test_0005_verify_sudo_on_force_run(self):

        # Check, if the task has been executed using the Administrator user.
        self.assertEqual(self.memory.task1.write_uid.id, self.env.uid)

    def test_0006_concurrent_access_error(self):
        """Simulate a concurrent access error when running a queue task and
        ensure we know how to handle it.
        """

        def scoped_test_run_impl(cr, uid, task_args):
            """Simulate a concurrent access error when running a queue task.

            Taken from openerp/tests/test_ir_sequence.py
            (test_ir_sequence_draw_twice_no_gap method).
            """

            uid = self.env.uid

            # Find an existing sequence.
            seq_id = self.env['ir.sequence'].search(
                [('implementation', '=', 'no_gap')], limit=1,
            )[0].id

            self.assertNotEqual(seq_id, 0, 'Error')

            # Ask for a new sequence code twice in 2 different transactions.
            cr0 = self.cursor()
            cr1 = self.cursor()
            cr1._default_log_exceptions = False  # Prevent logging a backtrace.
            self.env['ir.sequence'].with_env(
                api.Environment(cr0, uid, {})).next_by_id(seq_id)
            self.env['ir.sequence'].with_env(
                api.Environment(cr1, uid, {})).next_by_id(seq_id)
            cr0.close()
            cr1.close()

        # Set our custom run target during this test.
        self.memory.mock().function = 'test_run_impl'
        self.memory.mock().test_run_impl = scoped_test_run_impl

        self.memory.mock().with_context(mock=self.memory.mock()).run()

        # Set back the default run target.
        self.memory.mock().test_run_impl = test_run_impl

        # The queue, that has run should be in error, with the special flag
        # turned on.
        task = self.memory.task2
        self.assertEqual(task.state, 'error')
        self.assertTrue(task.in_error_concurrent_access)

        # Now run the method normally called by a planned task and ensure the
        # task works (as we have set back the default run target).
        self.memory.mock().with_context(mock=self.memory.mock()).\
            restart_tasks_in_concurrent_access_error()
        task = self.memory.task2
        self.assertEqual(task.state, 'done')
        self.assertFalse(task.in_error_concurrent_access)

    def test_0007_regular_error(self):
        """Simulate an error when running a queue task and ensure we know how
         to handle it.
       """

        def scoped_test_run_impl(cr, uid, task_args):
            """Simulate an error when running a queue task."""
            raise Exception('oops')

        # Set our custom run target during this test.
        self.memory.mock().test_run_impl = scoped_test_run_impl

        self.memory.mock().with_context(mock=self.memory.mock()).run()

        # Set back the default run target.
        self.memory.mock().test_run_impl = test_run_impl

        # The that that has run should be in error, with no special flag turned
        # on.
        task = self.memory.task3
        self.assertEqual(task.state, 'error')
        self.assertFalse(task.in_error_concurrent_access)
