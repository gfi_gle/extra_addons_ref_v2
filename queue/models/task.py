###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

from openerp import _
from openerp import api
from openerp import exceptions
from openerp import fields
from openerp import models


class Task(models.Model):
    _name = 'queue.task'

    @api.depends('queue_id.managing_group_ids')
    def get_is_manager(self):

        user = self.env.user

        for record in self:

            group_ids = record.queue_id.managing_group_ids.ids

            if any([(gid in group_ids) for gid in user.groups_id.ids]):
                record.is_manager = True

    state = fields.Selection(
        [
            ('todo', "To Do"),
            # XXX maybe add the selected state
            ('started', "Started"),
            ('error', "Error"),
            ('done', "Done"),
        ],
        default='todo',
    )
    args_interpolation = fields.Char()
    started_datetime = fields.Datetime(
        readonly=True,
    )
    done_datetime = fields.Datetime(
        readonly=True,
    )
    queue_id = fields.Many2one(
        'queue',
        string="Queue",
        required=True,
    )
    message = fields.Text(
        "Error Message",
        readonly=True,
    )
    is_manager = fields.Boolean(
        string='Manager user',
        compute=get_is_manager,
        readonly=True,
        store=False,
        default=False,
    )

    in_error_concurrent_access = fields.Boolean(
       string='In error because of a concurrent access issue',
       help=(
           'Shows whether this task is in error because of a concurrent '
           'access issue. It will be automatically restarted by a '
           'scheduled task, should the default one, proposed by this '
           'addon, have been set up (it is disabled by default).'
       ),
       readonly=True,
       default=False,
    )

    @api.multi
    def force_run(self):
        """Force a (re-)run of this task.
        """

        self.ensure_one()

        if self.is_manager:

            if self.state == 'started':
                raise exceptions.Warning(_('Already running.'))

            self.state = 'todo'
            self.sudo().run()

        else:

            raise exceptions.Warning(_(
                u"Only the queue's managing groups, defined by the Access "
                u"Rights group, can force execution on a task."
            ))

        return True

    @api.one
    def run(self):
        """Do the task, calling the queue with the resource id
        """

        context = self.env.context

        if self.state == 'todo':
            self.write(
                {
                    'state': 'started',
                    'started_datetime': fields.Datetime.now(),
                },
            )

        # is self update if todo?
        if self.state == 'started':
            call_error_info = self.queue_id.with_context(context).call(
                self.args_interpolation
            )

            if call_error_info[0]:
                error, concurrent_access_error = call_error_info[0]
                self.write(
                    {
                        'state': 'error',
                        'in_error_concurrent_access': concurrent_access_error,
                        'message': unicode(error),
                    },
                )
            else:
                self.write(
                    {
                        'state': 'done',
                        'done_datetime': fields.Datetime.now(),
                        'in_error_concurrent_access': False,
                        'message': '',
                    },
                )

    def run_n(self, n):
        """Run n tasks, take oldest first
        ::args n:: integer
        """
        pass
        # TODO

    # TODO add run_for(time, time_unit):
    # This should run any tasks until the time is elapsed.
