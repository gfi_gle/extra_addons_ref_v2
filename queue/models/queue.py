###############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
###############################################################################

import ast
import logging
from openerp import _
from openerp import api
from openerp import fields
from openerp import models
from openerp import exceptions
from openerp.addons.base.ir.ir_cron import str2tuple
import psycopg2  # Odoo req.
import openerp
import time

# PostgreSQL errors that result from concurrent access issues.
# Ref: <https://www.postgresql.org/docs/current/static/errcodes-appendix.html>.
CONCURRENT_ACCESS_PGODES = (
    '40001',  # serialization_failure
    '55P03',  # lock_not_available
)

_logger = logging.getLogger(__name__)


class Queue(models.Model):
    """Queue
    """
    _name = 'queue'

    name = fields.Char(required=True)
    active = fields.Boolean(default=True)
    # model should be 64 char long, as in ir_cron
    model = fields.Char("Object")
    # XXX user?
    function = fields.Char("Method")
    args = fields.Text("Arguments")

    # TODO add a priority system

    @api.depends('task_ids')
    @api.one
    def _get_task_count(self):
        self.task_count = self.env['queue.task'].search(
            [('queue_id', '=', self.id)], count=True
        )

    task_count = fields.Integer(
        compute=_get_task_count,
        string='Task count',
        help='The tasks linked to this queue.',
        store=True,
    )

    task_ids = fields.One2many(
        'queue.task',
        'queue_id',
        string="Tasks",
    )
    run_strategy = fields.Selection(
        [
            ('no_limit', 'No limit'),
            ('count', "Do n items"),
            # TODO implement ('time', "Spent an amount of time on tasks"),
        ],
        string='Run strategy',
        default='count',
        required=True,
    )
    # either number of items, or number of time_unit
    count = fields.Integer(
        default=1,
        string='Count',
    )
    # time_unit = fields.Selection(
    # TODO add count/run_strategy constraints

    managing_group_ids = fields.Many2many(
        comodel_name='res.groups',
        relation='queue_res_groups_rel',
        column1='queue_id',
        column2='group_id',
        string='Managing groups',
    )

    @api.one
    def call(self, args_interpolation=None):
        """Call the queue with the given args_interpolation.
        Inspired by the code of ir.cron.

        :return: Tuple with:
           - String: Error message when there is one, nothing on success.
           - Boolean: Whether the error is from a concurrent access issue to 
           the database.
        """

        context = self.env.context

        if args_interpolation:
            args_mod = ast.literal_eval(args_interpolation)
        else:
            args_mod = {}
        args = str2tuple(self.args % args_mod)
        _logger.info(args)
        openerp.modules.registry.RegistryManager.check_registry_signaling(
            self.env.cr.dbname)
        registry = openerp.registry(self.env.cr.dbname)
        if self.model in registry or 'mock' in context.keys():
            model = registry[self.model]
            if 'mock' in context.keys():
                model = context['mock']
            if hasattr(model, self.function):

                if _logger.isEnabledFor(logging.DEBUG):
                    start_time = time.time()

                # Set a savepoint to ensure the whole operation is rolled back
                # when there is an error (otherwise parts of it get
                # committed...).
                savepoint_name = 'queue_task_%s' % self.id

                try:
                    self.env.cr.execute('SAVEPOINT %s' % savepoint_name)

                    getattr(model, self.function)(
                        self.env.cr, self.env.uid, *args
                    )

                except psycopg2.Error as pgerror:
                    self.env.cr.execute(
                        'ROLLBACK TO SAVEPOINT %s' % savepoint_name
                    )
                    _logger.warning(pgerror)
                    return pgerror, pgerror.pgcode in CONCURRENT_ACCESS_PGCODES

                except Exception as error:
                    self.env.cr.execute('ROLLBACK TO SAVEPOINT %s' %
                                        savepoint_name)
                    _logger.warning(error)
                    return error, False

                if _logger.isEnabledFor(logging.DEBUG):
                    end_time = time.time()
                    _logger.debug(
                        '%.3fs (%s, %s)' % (
                            end_time - start_time, self.model, self.function))

                openerp.modules.registry.RegistryManager.signal_caches_change(
                    self.env.cr.dbname)
            else:
                msg = "Method `%s.%s` does not exist." % (
                    self.model, self.function)
                _logger.warning(msg)
                return msg, False
        else:
            msg = "Model `%s` does not exist." % self.model
            _logger.warning(msg)
            return msg, False

    @api.multi
    def open_tasks(self):
        """Display tasks related to this queue.
        """

        self.ensure_one()

        return {
            'context': self.env.context,
            'domain': [('queue_id', '=', self.id)],
            'name': _('Tasks'),
            'res_model': 'queue.task',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree,form',
            'view_type': 'form',
        }

    @api.model
    def restart_tasks_in_concurrent_access_error(self):
        """Restart tasks that have returned out an error because of a DB
        concurrent access issue.
        """

        def logThis(msg, *args):
            _logger.info(
                'queue: restart_tasks_in_concurrent_access_error: %s' % msg,
                *args
            )

        logThis('Start.')

        # Explicit ordering to ensure tasks are run in the right order.
        task_ids = self.env['queue.task'].search([
            ('in_error_concurrent_access', '=', True),
            ('state', '=', 'error'),
        ], order='id ASC')

        if not task_ids:
            logThis('No task - Done.')
            return

        logThis('%d tasks.', len(task_ids))

        for task in task_ids:
            task.force_run()

        logThis('Done.')

    @api.one
    def run(self):
        """Run tasks on the queue, using run_strategy
        """

        context = self.env.context

        # Only set a limit when the run strategy is based on that.
        task_search_limit = (
           self.count if self.run_strategy == 'count' else None
        )

        # Explicit ordering to ensure tasks are run in the right order.
        tasks = self.env['queue.task'].search(
            [
                ('queue_id', '=', self.id),
                ('state', 'not in', ['done']),
            ],
            limit=task_search_limit,
            order='id ASC',
        )


        tasks.with_context(context).run()

    @api.multi
    def write(self, vals):

        user = self.env.user
        authorized_group_id = self.env.ref('base.group_erp_manager').id

        if 'managing_group_ids' in vals:

            if all([
                (gid != authorized_group_id) for gid in user.groups_id.ids
            ]):
                raise exceptions.Warning(_(
                    u'Only the Access Rights group can define, which security '
                    u'group can manage a queue.'
                ))

        return super(Queue, self).write(vals)
