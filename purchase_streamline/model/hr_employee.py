# -*- coding: utf-8 -*-
from base64 import b64decode
from cStringIO import StringIO
from PIL import Image

from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp import SUPERUSER_ID


class hr_employee(osv.Model):

    _inherit = 'hr.employee'

    def is_purchase_user(
        self, cr, uid, ids, field_names, args, context=None
    ):
        res = {}
        pool = self.pool['ir.model.data']
        group = pool.get_object(
            cr,
            SUPERUSER_ID,
            'purchase',
            'group_purchase_user',
        )

        list_users = group.users
        list_users_ids = [user.id for user in list_users]

        for employee in self.browse(cr, SUPERUSER_ID, ids, context=context):
            res[employee.id] = {
                    'is_user': employee.user_id.id in list_users_ids
                }

        return res

    def get_employee_users(
        self, cr, uid, ids, context=None
    ):
        result = {}
        list_users_ids = []
        list_ids = self.pool['res.users'].search(
            cr, SUPERUSER_ID, [], context=context
        )
        for user in self.pool['res.users'].browse(
            cr, SUPERUSER_ID, list_ids, context=context
        ):
            if user.partner_id:
                list_users_ids.append(user.id)

        list_employee_ids = self.pool['hr.employee'].search(
            cr, SUPERUSER_ID, [], context=context
        )

        for employee in self.pool['hr.employee'].browse(
            cr, SUPERUSER_ID, list_employee_ids, context=context
        ):
            if employee.user_id.id in list_users_ids:
                result[employee.id] = True

        return result.keys()

    _columns = {

        'is_user': fields.function(
            is_purchase_user,
            readonly=True,
            type='boolean',
            string="Purchase user",
            store={
                'hr.employee': (
                    lambda self, cr, uid, ids, c={}: ids,
                    [], 10),
                'res.users': (get_employee_users, [], 10),
            },
            multi='val',
        ),
    }


class hr_vgroups(osv.Model):

    _inherit = "hr.vgroups"

    _columns = {

        'members': fields.many2many(
            'hr.employee',
            'hr_employee_hr_vgroups_rel',
            'vgroup_id',
            'employee_id',
            domain=[('is_user', '!=', False)],
            string="Group members",
        ),
    }

