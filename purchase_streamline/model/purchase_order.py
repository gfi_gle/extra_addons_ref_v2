# -*- coding: utf-8 -*-
##############################################################################
#
#    Purchase management improvements for Odoo
#    Copyright (C) 2013-2015 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from lxml import etree
from openerp import _
from openerp import api
from openerp import exceptions
from openerp import models, fields
from openerp.osv import fields as odoo7_fields

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic
from openerp.addons.purchase.purchase import purchase_order as base_po_class
from openerp.addons.purchase_streamline.util.odoo import default_context


# Defined here so it can be fiddled with by other modules.
type_selection = [
    ('standard', _('Standard')),
]


# Register a new purchase order state right before the current "approved" one.
for i, state_tuple in enumerate(base_po_class.STATE_SELECTION):
    if state_tuple[0] == 'approved':
        base_po_class.STATE_SELECTION.insert(i, (
            'confirmed_large_value',
            _(u"Waiting for large-value approval")
        ))
        break

for i, state_tuple in enumerate(base_po_class.STATE_SELECTION):
    if state_tuple[0] == 'cancel':
        base_po_class.STATE_SELECTION.insert(i + 1, (
            'rejected',
            _(u"Rejected")
        ))
        break


class PurchaseOrder(models.Model):
    """Customize purchase orders and add analytics.
    """

    __metaclass__ = MetaAnalytic

    _analytic = 'purchase_order'

    _inherit = 'purchase.order'

    def _default_employee(self, cr, uid, context=None):
        """
        :return: employee of user uid
        """
        if context is None:
            context = {}
        ids = self.pool.get('hr.employee').search(
            cr, uid, [
                ('user_id', '=', uid),
                '|', ('active', '=', True), ('active', '=', False),
            ], limit=1, context=context)
        if ids:
            return ids[0]
        return False

    def identify_employee(self, cr, uid, context=None):
        """
        :return: Employee of user uid
        even if she/he has purchase on behalf rights to create
        purchase orders for other employees.
        The consequence is that this does not need any call to the
        _check_employee_id_readonly method.
        """
        if context is None:
            context = {}

        ids = self.pool.get('hr.employee').search(
            cr, uid, [
                ('user_id', '=', uid),
            ], limit=1, context=context)
        if ids:
            return ids[0]
        return False

    def _default_operational_department(self, cr, uid, context=None):
        """Select the first (ie most prioritized) operational department of the
        employee object linked to the connected user.
        """

        return self._get_first_op_dep(
            cr, uid, self._default_employee(cr, uid, context=context), context
        )

    def _get_company_partner_id(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        user = self.pool.get('res.users').browse(cr, uid, uid, context=context)
        company_id = context.get('company_id', user.company_id.id)
        company_partner_id = self.pool.get('res.company').browse(
            cr, uid, company_id, context=context).partner_id
        return company_partner_id.id

    def _check_employee_id_readonly(
        self, cr, uid, ids, field_name, arg, context=None
    ):
        res = {}
        test = not self.user_has_groups(
            cr, uid, 'purchase_streamline.group_purchase_on_behalf',
            context=context,
        )

        for id_ in ids:
            res[id_] = test

        return res

    def is_purchase_manager(self, cr, uid, ids, field_names, args,
                            context=None):
        res = {}
        pool = self.pool['ir.model.data']
        l = pool.get_object(cr,
                            uid,
                            'purchase',
                            'group_purchase_manager'
                            )
        list_users = l.users
        m_users = [m.id for m in list_users]

        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {'is_pm': (uid in m_users)}
        return res

    def is_purchase_user(self, cr, uid, ids, field_names, args,
                              context=None):
        res = {}
        pool = self.pool['ir.model.data']
        n = pool.get_object(cr,
                            uid,
                            'purchase',
                            'group_purchase_user'
                            )
        list_users = n.users
        v_users = [v.id for v in list_users]

        for order in self.browse(cr, uid, ids, context=context):
            res[order.id] = {'is_user': (uid in v_users)}
        return res

    def is_vgroup_member(self, vgroup, user):
        is_member = False
        if vgroup.members:
            for mbr in vgroup.members:
                if mbr.id == user:
                    is_member = True
        return is_member

    def order_validation(self, cr, uid,
                         vgroup, user, high_value,
                         is_pm, is_user,
                         order, context=None):

        for_validate = False
        if vgroup.members and is_user:
            if self.is_vgroup_member(vgroup, user) or is_pm:
                for_validate = True
        elif is_pm:
            for_validate = True

        if for_validate:
            self.signal_workflow(cr, uid, order,
                                 'purchase_approve', context=context
                                 )
            return True
        else:
            if high_value:
                raise exceptions.Warning(
                                         _(
                                           u"You are not allowed to "
                                           u"validate large-value purchase "
                                           u"orders.")
                                         )
            else:
                raise exceptions.Warning(
                                         _(
                                           u"You are not a validator "
                                           u"for this purchase order.")
                                         )
                return False

    def action_purchase_approve(self, cr, uid, ids, context=None):

        # IDs of purchase orders for which this workflow step can be skipped.

        for order in self.browse(cr, uid, ids, context=context):
            l = []
            company = order.company_id
            my_employee = self.pool['purchase.order'].identify_employee(
                cr, uid, context)
            transmitter = order.employee_id

            high_value = False
            valid = False

            is_pm = order.is_pm
            is_user = order.is_user

            vgroup = transmitter.validation_group_1
            threshold = company.has_purchase_order_threshold

            l.append(order.id)

            valid = self.order_validation(cr, uid,
                                          vgroup, my_employee, high_value,
                                          is_pm, is_user,
                                          l, context)

            if threshold:
                if (order.amount_total < company.purchase_order_threshold):
                    valid = self.order_validation(cr, uid,
                                                  vgroup, my_employee,
                                                  high_value,
                                                  is_pm, is_user,
                                                  l, context)
            else:
                valid = self.order_validation(cr, uid,
                                              vgroup, my_employee, high_value,
                                              is_pm, is_user,
                                              l, context)

        return valid

    def action_purchase_approve_large_value(self, cr, uid, ids,
                                            context=None):

        for order in self.browse(cr, uid, ids, context=context):
            l = []
            user = self.pool['purchase.order'].identify_employee(
                cr, uid, context)
            transmitter = order.employee_id

            high_value = True
            valid = False

            is_pm = order.is_pm
            is_user = order.is_user

            vgroup = transmitter.validation_group_2

            l.append(order.id)

            valid = self.order_validation(cr, uid, vgroup, user,
                                          high_value,
                                          is_pm, is_user,
                                          l, context)

        return valid

    def action_purchase_reject(self, cr, uid, ids, context=None):

        for order in self.browse(cr, uid, ids, context=context):
            l = []
            user = self.pool['purchase.order'].identify_employee(
                cr, uid, context)
            transmitter = order.employee_id

            pm = order.is_pm
            is_user = order.is_user

            vgroup1 = transmitter.validation_group_1
            vgroup2 = transmitter.validation_group_2

            for_validate = False

            l.append(order.id)

            if vgroup1.members and is_user and order.state == "confirmed":
                if self.is_vgroup_member(vgroup1, user) or pm:
                    for_validate = True

            if (
                vgroup2.members and is_user and
                order.state == "confirmed_large_value"
            ):
                if self.is_vgroup_member(vgroup2, user) or pm:
                    for_validate = True
            if pm:
                for_validate = True

            if for_validate:
                return self.signal_workflow(cr, uid, l,
                                            'purchase_reject', context=context)
            else:
                raise exceptions.Warning(
                                         _(
                                           u"You are neither a validator nor "
                                           u"a manager in order to reject this"
                                           u" purchase order.")
                                         )

    _columns = {
        'create_uid': odoo7_fields.many2one(
            'res.users',
            string=u"Preparator",
            readonly=True,
        ),
        'contact_id': odoo7_fields.many2one(
            'res.partner',
            'Supplier contact',
            domain=[
                ('is_company', '=', False)
            ],
        ),
        'delivery_contact_id': odoo7_fields.many2one(
            'res.partner',
            'Delivery Contact',
        ),
        'company_partner_id': odoo7_fields.many2one(
            'res.partner',
            'Company address',
        ),
        'is_employee_id_readonly': odoo7_fields.function(
            _check_employee_id_readonly,
            method=True,
            type='boolean',
        ),
        'employee_id': odoo7_fields.many2one(
            'hr.employee',
            "Transmitter",
            ondelete='restrict',
        ),
        'user_id': odoo7_fields.many2one(
            'res.users',
            "User",
            ondelete='restrict',
            required=True,
        ),
        'department_id': odoo7_fields.many2one(
            'hr.department',
            "Transmitter Department",
            ondelete='restrict',
            readonly=True,
            required=True,
        ),
        'operational_department_id': odoo7_fields.many2one(
            'hr.operational_department',
            "Transmitter Department",
            ondelete='restrict',
            required=True,
            store=True,
        ),
        'description': odoo7_fields.char(
            'Description',
            size=100,
            required=True,
        ),
        # handy fields for workflow
        'user_valid': odoo7_fields.many2one(
            'res.users',
            'Validation By',
            readonly=True,
            track_visibility='onchange',
            select=True,
        ),
        'validator': odoo7_fields.many2one(
            'res.users',
            'Validated by',
            readonly=True,
            track_visibility='onchange',
            select=True,
        ),
        'is_pm': odoo7_fields.function(
            is_purchase_manager,
            readonly=True,
            type='boolean',
            string="Purchase manager",
            multi='pm',
        ),
        'is_user': odoo7_fields.function(
            is_purchase_user,
            readonly=True,
            type='boolean',
            string="Purchase user",
            multi='val',
        ),
        'hacked': odoo7_fields.integer(
            "Close/Open Again",
            readonly=True,
            track_visibility='onchange',
        ),
        # Utility field of use to derived classes.
        'type': odoo7_fields.selection(
            type_selection,
            "Type",
        ),
        'location_id': odoo7_fields.many2one(
            'stock.location',
            'Destination',
            required=True,
            domain=[('usage', '<>', 'view')],
            states={
                'confirmed': [('readonly', True)],
                'approved': [('readonly', True)],
                'done': [('readonly', True)]},
        ),
    }

    def _get_default_location(self, cr, uid, context=None):
        loc = self.pool['stock.location'].search(
            cr, uid, [('usage', '=', 'internal')], context=context, limit=1
        )
        return loc[0] if loc else False

    _defaults = {
        'invoice_method': 'manual',
        'employee_id': _default_employee,
        'user_id': (lambda cr, uid, id, c={}: id),
        'company_partner_id': _get_company_partner_id,
        'operational_department_id': _default_operational_department,
        'description': "",
        'hacked': 0,
        'type': 'standard',
        'location_id': _get_default_location,
    }

    def default_get(self, cr, uid, fields, context=None):
        vals = super(PurchaseOrder, self).default_get(
            cr, uid, fields, context=context
        )
        vals['is_employee_id_readonly'] = self._check_employee_id_readonly(
            cr, uid, [0], None, None, context=context
        )[0]

        return vals

    @api.multi
    def write(self, vals):
        """Override to:
        - Fill the department from the selected operational department.
        - Fill line department analytics.
        """

        # Avoid the addition/deletion of order_lines when the order is approved
        data = self.read(['state'])
        for d in data:
            if d['state'] == 'approved' and 'order_line' in vals:
                for order_line in vals['order_line']:
                    # We assume that a one2many is a tuple
                    #  4 <=> existing lines, 1 <=> writing existing lines
                    if order_line[0] == 1 or order_line[0] == 4:
                        continue
                    raise exceptions.Warning(
                        _(u"You can't add/delete lines in"
                          u" an approved purchase order.")
                    )

        op_dep_id = vals.get('operational_department_id')
        if op_dep_id:
            op_dep_obj = self.env['hr.operational_department']
            vals['department_id'] = (
                op_dep_obj.browse(op_dep_id).department_id.id
            )

        ret = super(PurchaseOrder, self).write(vals)

        if 'department_id' in vals or 'order_line' in vals:
            # Fill line department analytics.
            self.fill_line_department_analytics()

        return ret

    def fields_view_get(
        self, cr, uid, view_id=None, view_type='form', context=None,
        toolbar=False, submenu=False
    ):
        """Override to handle analytics.
        """

        ret = super(PurchaseOrder, self).fields_view_get(
            cr, uid, view_id=view_id, view_type=view_type, context=context,
            toolbar=toolbar, submenu=submenu
        )

        ans_obj = self.pool['analytic.structure']
        data_obj = self.pool['ir.model.data']

        # Include analytic fields in the line list.
        ans_obj.analytic_fields_subview_get(
            cr, uid, 'purchase_order_line', ret['fields'].get('order_line')
        )

        # When lines contain department analytics, make these read-only (they
        # will be filled at record creation, based on the department of the
        # purchase order).

        # Find the analytic dimension bound to departments.
        # Try/except for no exception to be thrown if
        # .hr_department_analytic_dimension_id is not found
        try:
            department_dimension = data_obj.get_object(
                cr, uid,
                '',
                'hr_department_analytic_dimension_id',
                context=context
            )
        except ValueError:
            return ret

        # Find out whether the department dimension is used in lines.
        dep_line_dim = ans_obj.get_dimensions(
            cr, uid, 'purchase_order_line', context=context
        ).get(department_dimension.id)
        if not dep_line_dim:
            return ret

        # Got a department line! Find it in the view description and make it
        # read-only.

        line_list_view_def = (
            ret.get('fields', {}).get('order_line', {}).get('views', {})
            .get('tree')
        )
        if not line_list_view_def:
            return ret
        line_list_arch = line_list_view_def.get('arch')
        if not line_list_arch:
            return ret

        line_list_doc = etree.XML(line_list_arch)

        dep_line_node = line_list_doc.xpath(
            "//field[@name='a%s_id']" % dep_line_dim
        )
        if not dep_line_node:
            return ret
        dep_line_node = dep_line_node[0]

        dep_line_node.set('modifiers', '{"readonly": true}')

        line_list_view_def['arch'] = etree.tostring(line_list_doc)

        return ret

    @api.model
    def create(self, vals):
        """Override to:
        - Fill the department from the selected operational department.
        - Fill line department analytics.
        """

        op_dep_id = vals.get('operational_department_id')
        if not op_dep_id:
            # No op dep; this can happen when creating via code. In that case,
            # just find the default one.
            op_dep_id = self._default_operational_department()

        # Add a check in case no operational department could be found. Let
        # Odoo display the "required" message in that case.
        if op_dep_id:

            # Deduce the department.
            op_dep_obj = self.pool['hr.operational_department']
            vals['department_id'] = op_dep_obj.browse(
                self.env.cr, self.env.user.id, [op_dep_id],
                context=self.env.context
            )[0].department_id.id

        order = super(PurchaseOrder, self).create(vals)

        # Fill line department analytics.
        order.fill_line_department_analytics()

        return order

    def onchange_employee_id(self, cr, uid, ids, employee_id, context=None):
        """Change the operational department according to the selected
        employee.

        Select the first (ie most prioritized) operational department of the
        employee object linked to the specified user.
        """

        return {'value': {'operational_department_id': (
            self._get_first_op_dep(cr, uid, employee_id, context)
        )}}

    # we override onchange_partner_id to add a field to set when a product is
    # selected
    def onchange_partner_id(self, cr, uid, ids, partner_id, context=None):
        res = super(PurchaseOrder, self).onchange_partner_id(
            cr, uid, ids, partner_id
        )

        company_partner_id = self._get_company_partner_id(
            cr, uid, ids, context=context)
        res['value']['company_partner_id'] = company_partner_id
        return res

    def fill_line_department_analytics(self):
        """Fill department analytics in order lines, when they contain an
        analytic dimension bound to departments.
        """

        # Find the analytic dimension bound to departments.
        # Try/except for no exception to be thrown if
        # .hr_department_analytic_dimension_id is not found
        try:
            department_dimension = self.env.ref(
                '.hr_department_analytic_dimension_id'
            )
        except ValueError:
            return

        # Find out whether the department dimension is used in lines.
        dep_line_dim = self.env['analytic.structure'].get_dimensions(
            'purchase_order_line'
        ).get(department_dimension.id)
        if not dep_line_dim:
            return

        for order in self:

            # Find the analytic code bound to the department of the order.
            dep_anc = order.department_id.analytic_id
            if not dep_anc:
                raise exceptions.Warning(_(
                    'No analytic code found for the "%s" department.'
                ) % order.department_id.name)

            for order_line in order.order_line:

                # Don't overwrite already set analytics.
                if getattr(order_line, 'a%s_id' % dep_line_dim):
                    continue

                setattr(order_line, 'a%s_id' % dep_line_dim, dep_anc)

    def hack_reopen(self, cr, uid, ids, context=None):
        '''Danger. This is only to be used carefully
        '''
        assert not isinstance(ids, list) or len(ids) == 1
        # XXX could assert that it was hacked at least once
        cr.execute('''
            UPDATE purchase_order
            SET state = 'submitted_procurement'
            WHERE id = %s
            ''', ids)
        self.increment_hacked(cr, uid, ids, context)
        return True

    def hack_close_again(self, cr, uid, ids, context=None):
        '''Danger. This is only to be used carefully
        '''
        assert not isinstance(ids, list) or len(ids) == 1
        cr.execute('''
            UPDATE purchase_order
            SET state = 'approved'
            WHERE id = %s
            ''', ids)
        self.increment_hacked(cr, uid, ids, context)
        return True

    def increment_hacked(self, cr, uid, id, context=None):
        """Increment the hacked counter on id
        """
        n = self.read(cr, uid, id, ['hacked'])[0]['hacked'] + 1
        vals = {'hacked': n}
        return self.write(cr, uid, id, vals, context)

    def wkf_confirmed_large_value(self, cr, uid, ids, context=None):
        context = default_context(self, cr, uid, context)
        self.write(
            cr, uid, ids, {'state': 'confirmed_large_value'}, context=context
        )

    def wkf_reject(self, cr, uid, ids, context=None):
        context = default_context(self, cr, uid, context)
        self.write(
            cr, uid, ids, {'state': 'rejected'}, context=context
        )

    # TODO replace _get_first_op_dep with get_first_op_dep
    def get_first_op_dep(self, cr, uid, employee_id, context=None):
        return self._get_first_op_dep(cr, uid, employee_id, context=context)

    def _get_first_op_dep(self, cr, uid, employee_id, context=None):
        """Select the first (ie most prioritized) operational department of the
        employee object linked to the specified user.
        """

        if not employee_id:
            return False

        op_dep_obj = self.pool['hr.operational_department']
        op_dep_ids = op_dep_obj.search(
            cr, uid,
            [('employee_id.id', '=', employee_id)],
            limit=1,
            context=context
        )
        if not op_dep_ids:
            return False

        return op_dep_ids[0]

    # Override in order to propagate the purchase order id in the reception
    # order at generation of the latter.
    def action_picking_create(self, cr, uid, ids, context=None):
        for order in self.browse(cr, uid, ids):
            picking_vals = {
                # Line added for purchase order id propagation.
                'purchase_id': order.id,
                'picking_type_id': order.picking_type_id.id,
                'partner_id': order.partner_id.id,
                'date': order.date_order,
                'origin': order.name
            }
            picking_id = self.pool.get('stock.picking').create(cr, uid, picking_vals, context=context)
            self._create_stock_moves(cr, uid, order, order.order_line, picking_id, context=context)
        return picking_id


class PurchaseOrderLine(models.Model):
    """Customize purchase orders and add analytics.
    """

    __metaclass__ = MetaAnalytic

    _analytic = 'purchase_order_line'

    _inherit = 'purchase.order.line'

    @api.multi
    def unlink(self):
        """Override purchase.order.line::unlink, defined in the "purchase"
        addon, to fix the Odoo commit acd61f8f0efe0c1da2a0640ec65bfc48d6714645
        which allows deleting lines during the approval process and then
        actually approving a purchase order without any line.
        """

        for line in self:
            if line.state not in ['draft', 'cancel']:
                raise exceptions.Warning(_(
                    'Cannot delete a purchase order line which is at the "%s" '
                    'state.'
                ) % line.state)

        return super(PurchaseOrderLine, self).unlink()

    # For department, division and subcontractor's analytics injection in
    # purchase order lines.
    def onchange_product_id2(
        self, cr, uid, ids, pricelist_id,
        product_id, qty, uom_id, partner_id, operational_department_id,
        date_order=False, fiscal_position_id=False, date_planned=False,
        name=False, price_unit=False, state='draft', context=None
    ):
        """New function copied from the first Odoo 8 version to fill purchase
           order line analytics based on purchase order partner and department.
           An override was not possible, the signature including one more
           argument.
        """

        # TODO Switch to an Odoo 8 style change handler.
        ret = super(PurchaseOrderLine, self).onchange_product_id(
            cr, uid, ids, pricelist_id, product_id,
            qty, uom_id, partner_id,
            date_order, fiscal_position_id, date_planned, name, price_unit,
            context)

        # Injects the department, division and subcontractor's analytic codes
        # in the purchase order line.
        if product_id:
            # Lets the "extract_values" method provide department analytic
            # field updates.
            # For the subcontractor, there is only one field to seek. A simple
            # call is sufficient.

            partner = self.pool['res.partner'].browse(
                    cr, uid, [partner_id],
                    context=context
            )

            department = self.pool['hr.operational_department'].browse(
                    cr, uid, [operational_department_id],
                    context=context
            ).department_id

            product = self.pool['product.product'].browse(
                    cr, uid, [product_id],
                    context=context
            )

            analytic_values1 = self.pool['analytic.structure'].extract_values(
                    cr, uid, department, 'hr.department',
                    dest_model='purchase_order_line', context=context
            )
            analytic_values2 = self.pool['analytic.structure'].extract_values(
                    cr, uid, product.product_tmpl_id, 'product_template',
                    dest_model='purchase_order_line', context=context
            )

            if 'value' not in ret:
                ret['value'] = {}

            ret['value'].update(analytic_values1)
            ret['value'].update(analytic_values2)
            ret['value'].update({'a1_id': partner.a1_id.id})
            if not pricelist_id:
                ret['value'].update({
                    'price_unit': product.product_tmpl_id.standard_price
                })

        return ret

    def onchange_product_qty(
        self, cr, uid, ids, pricelist_id,
        product_id, qty, uom_id, partner_id, operational_department_id,
        date_order=False, fiscal_position_id=False, date_planned=False,
        name=False, price_unit=False, context=None
    ):
        """Don't change the description of the product line, the unit price, or
        the type of tax when changing the quantity if one was already set.
        """

        res = self.onchange_product_id2(
            cr, uid, ids, pricelist_id, product_id,
            qty, uom_id, partner_id, operational_department_id,
            date_order, fiscal_position_id, date_planned, name, price_unit,
            context
        )

        if name and 'name' in res['value']:
            res['value'].pop('name')
        if 'taxes_id' in res['value']:
            res['value'].pop('taxes_id')

        return res
