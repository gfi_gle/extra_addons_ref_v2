from openerp.osv import fields, osv


class res_company(osv.Model):
    """Inherit from res.company to add some fields.
    """

    _inherit = 'res.company'

    _columns = {
        'has_purchase_order_threshold': fields.boolean(
            u"Purchase Order Threshold",
            help=(
                u"Threshold beyond which purchase orders will have an "
                u"additional validation step."
            ),
        ),
        'purchase_order_threshold': fields.float(
            u"Purchase Order Threshold",
            digits=(12, 2),
            help=(
                u"Threshold beyond which purchase orders will have an "
                u"additional validation step."
            ),
        ),
        'purchase_order_large_amount_validator_id': fields.many2one(
            'hr.employee',
            u"Validator for purchase orders beyond the threshold",
        ),
    }

    _defaults = {
        'has_purchase_order_threshold': lambda *a: False,
    }
