"""Improve stock management when linked to purchase orders.
"""

from openerp.osv import fields
from openerp.osv import orm


class stock_move(orm.Model):
    """Improve stock management when linked to purchase orders.
    """

    _inherit = 'stock.move'

    _columns = {
        'order_line_description': fields.related(
            'purchase_line_id',
            'name',
            type='text',
            string='Description',
            readonly=True,
        ),
    }
