"""Improve stock management when linked to purchase orders.
"""

from openerp.osv import fields
from openerp.osv import orm


def _get_order_information(order, context):
    """Fetch information about a purchase order.
    :type order: Odoo browse-record object of a purchase order.
    :param context: Odoo context (passed in to ensure correct i18n).
    :return: Dictionary with order information.
    """

    return {
        'order_date': order.date_approve if order else False,
        'order_department_id': order.department_id.id if order else False,
        'order_description': order.description if order else False,
        'order_employee_id': order.employee_id.id if order else False,
    }


def _sp_order_information(obj, cr, uid, ids, field_names, args, context):
    """Fetch order information when picking objects are linked to purchase
    orders.
    """

    sp_obj = obj.pool['stock.picking']

    return {
        sp.id: _get_order_information(sp.purchase_id, context)
        for sp in sp_obj.browse(cr, uid, ids, context=context)
    }


class stock_picking(orm.Model):
    """Redefine fields added to stock.picking here.
    See purchase/stock/stock.py:174 and
    <https://bugs.launchpad.net/openobject-server/+bug/996816> for details.
    """

    _name = 'stock.picking'
    _inherit = 'stock.picking'

    # See purchase/stock/stock.py:174 and
    # <https://bugs.launchpad.net/openobject-server/+bug/996816> for details.
    _columns = {
        'purchase_id': fields.many2one(
            obj='purchase.order',
            string='Purchase Order',
            ondelete='set null',
            select=True,
        ),
        'order_date': fields.function(
            _sp_order_information,
            method=True,
            type='date',
            multi='order_information',
            string='Order date',
            help='Date of approval of the purchase order.',
        ),

        'order_department_id': fields.function(
            _sp_order_information,
            method=True,
            type='many2one',
            obj='hr.department',
            multi='order_information',
            string='Order department',
        ),

        'order_description': fields.function(
            _sp_order_information,
            method=True,
            type='char',
            multi='order_information',
            string='Order description',
        ),

        'order_employee_id': fields.function(
            _sp_order_information,
            method=True,
            type='many2one',
            obj='hr.employee',
            multi='order_information',
            string='Order transmitter',
        ),
    }

    _defaults = {
        'purchase_id': False,
    }

    def on_change_purchase_order_id(
        self, cr, uid, ids, purchase_order_id, context=None
    ):
        """When changing the purchase order, fetch related fields.
        """

        if purchase_order_id:
            order_obj = self.pool['purchase.order']
            order = order_obj.browse(
                cr, uid, [purchase_order_id], context=context
            )[0]
        else:
            order = None

        return {'value': _get_order_information(order, context)}

    def _get_partner_to_invoice(self, cr, uid, picking, context=None):
        """ Inherit the original function of the 'stock' module
            We select the partner of the sale order as the
             partner of the customer invoice
        """
        if picking.purchase_id:
            return picking.purchase_id.partner_id
        return super(stock_picking, self)._get_partner_to_invoice(
            cr, uid, picking, context=context
        )

    def _prepare_invoice(
            self, cr, uid, picking, partner,
            inv_type, journal_id, context=None
    ):
        """ Inherit the original function of the 'stock' module in order
         to override some values if the picking has been generated
         by a purchase order
        """
        invoice_vals = super(stock_picking, self)._prepare_invoice(
            cr, uid, picking, partner, inv_type, journal_id, context=context
        )
        if picking.purchase_id:
            invoice_vals['fiscal_position'] = (
                picking.purchase_id.fiscal_position.id
            )
            invoice_vals['payment_term'] = (
                picking.purchase_id.payment_term_id.id
            )
            # Fill the date_due on the invoice, for usability purposes.
            # Note that when an invoice with a payment term is validated, the
            # date_due is always recomputed from
            # the invoice date and the payment
            # term.
            if picking.purchase_id.payment_term_id and context.get('date_inv'):
                invoice_vals['date_due'] = (
                    self.pool.get('account.invoice')
                        .onchange_payment_term_date_invoice(
                            cr, uid, [],
                            picking.purchase_id.payment_term_id.id,
                            context.get('date_inv')
                        )['value'].get('date_due')
                )
        return invoice_vals

    def get_currency_id(self, cursor, user, picking):
        if picking.purchase_id:
            return picking.purchase_id.pricelist_id.currency_id.id
        else:
            return super(stock_picking, self).get_currency_id(
                cursor, user, picking
            )

    def _get_comment_invoice(self, cursor, user, picking):
        if picking.purchase_id and picking.purchase_id.notes:
            if picking.note:
                return picking.note + '\n' + picking.purchase_id.notes
            else:
                return picking.purchase_id.notes
        return super(stock_picking, self)._get_comment_invoice(
            cursor, user, picking
        )

    def _get_price_unit_invoice(self, cursor, user, move_line, type):
        if move_line.purchase_line_id:
            if move_line.purchase_line_id.order_id.invoice_method == 'picking':
                return move_line.price_unit
            else:
                return move_line.purchase_line_id.price_unit
        return super(stock_picking, self)._get_price_unit_invoice(
            cursor, user, move_line, type
        )

    def _get_discount_invoice(self, cursor, user, move_line):
        if move_line.purchase_line_id:
            return 0.0
        return super(stock_picking, self)._get_discount_invoice(
            cursor, user, move_line
        )

    def _get_taxes_invoice(self, cursor, user, move_line, type):
        if move_line.purchase_line_id:
            return [x.id for x in move_line.purchase_line_id.taxes_id]
        return super(stock_picking, self)._get_taxes_invoice(
            cursor, user, move_line, type
        )

    def _get_account_analytic_invoice(self, cursor, user, picking, move_line):
        if picking.purchase_id and move_line.purchase_line_id:
            return move_line.purchase_line_id.account_analytic_id.id
        return super(stock_picking, self)._get_account_analytic_invoice(
            cursor, user, picking, move_line
        )

    def _invoice_line_hook(self, cursor, user, move_line, invoice_line_id):
        if move_line.purchase_line_id:
            purchase_line_obj = self.pool.get('purchase.order.line')
            purchase_line_obj.write(
                cursor, user, [move_line.purchase_line_id.id], {
                    'invoiced': True,
                    'invoice_lines': [(4, invoice_line_id)],
                }
            )
        return super(stock_picking, self)._invoice_line_hook(
            cursor, user, move_line, invoice_line_id
        )

    def _invoice_hook(self, cursor, user, picking, invoice_id):
        purchase_obj = self.pool.get('purchase.order')
        if picking.purchase_id:
            purchase_obj.write(
                cursor, user, [picking.purchase_id.id],
                {'invoice_ids': [(4, invoice_id)]}
            )
        return super(stock_picking, self)._invoice_hook(
            cursor, user, picking, invoice_id
        )
