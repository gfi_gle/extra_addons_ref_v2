import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partners = self.createAndTest(
            'res.partner',
            [
                {
                    'name': genUuid(),
                },
            ],
        )

    def test_0001_setup_admin_account(self):
        """Set the admin account up:
        - Add an operational department for purchase order generaiton.
        """

        # Find the employee that should have been created at database setup.
        employee = self.env['hr.employee'].search([
            ('user_id', '=', self.env.user.id),
        ], limit=1)

        department, = self.createAndTest(
            'hr.department',
            [{'name': genUuid()}],
        )
        self.createAndTest(
            'hr.operational_department',
            [{
                'department_id': department.id,
                'employee_id': employee.id,
            }],
        )

    def test_0100_create_purchase_order(self):
        """Create purchase orders.
        """

        partner = self.memory.partners[0]

        self.memory.purchase_orders = self.createAndTest(
            'purchase.order',
            [
                {
                    'partner_id': partner.id,
                    'pricelist_id': (
                        partner.property_product_pricelist_purchase.id
                    ),
                },
            ],
        )
