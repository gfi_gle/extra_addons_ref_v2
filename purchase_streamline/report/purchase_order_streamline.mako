## -*- coding: utf-8 -*-
<style type="text/css">
${css}
</style>
%for i, order in enumerate(objects):
<%page expression_filter="entity"/>
<% setLang(order.partner_id.lang) %>

<div class="address">
    <div class="addressright">
        <table class="recipient">
            <tr><th class="addresstitle">${ _(u"FOURNISSEUR") } :</th></tr>
            %if order.partner_id.parent_id:
            <tr><td class="name">${order.partner_id.parent_id.name or ''}</td></tr>
            <% address_lines = order.partner_id.contact_address.split("\n")[1:] %>
            %else:
            <% address_lines = order.partner_id.contact_address.split("\n") %>
            %endif
            <tr><td class="name">${order.partner_id.title and order.partner_id.title.name or ''} ${order.partner_id.name }</td></tr>
            %for part in address_lines:
                %if part:
                <tr><td>${part}</td></tr>
                %endif
            %endfor
        </table>
    </div>
    <div class="addressleft">
        <table class="shipping">
            <tr><th class="addresstitle"></th></tr>
            %if order.company_id.partner_id.parent_id:
            <tr><td class="name">${order.company_id.partner_id.parent_id.name or ''}</td></tr>
            <% address_lines = order.company_id.partner_id.contact_address.split("\n")[1:] %>
            %else:
            <% address_lines = order.company_id.partner_id.contact_address.split("\n") %>
            %endif
            <tr><td class="name">${order.company_id.partner_id.title and order.company_id.partner_id.title.name or ''} ${order.company_id.partner_id.name }</td></tr>
            %for part in address_lines:
                %if part:
                <tr><td>${ part }</td></tr>
                %endif
            %endfor
       </table>
    </div>
</div>

<div class="orderinfo">
    <span class="text">
       <h1> ${ _(u'N° BON DE COMMANDE') }  ${order.name}</h1>
    </span>
</div>

<div class="basic_table">
    <center>
    <table class="basic_table">
    <thead>
        <tr>
            <th>${_("Date Commande")}</th>
            <th>${_("Emetteur")}</th>
            <th>${_("Date Approbation")}</th>
            <th>${_("Fournisseur")}</th>
        </tr>
        <tr>
            <td>${order.date_order or ''}</td>
            <td>${order.create_uid.title and order.create_uid.title.name or ''} ${order.create_uid.name }</td>
            <td>${order.date_approve or ''}</td>
            <td>${order.partner_id.name }</td>
        </tr>
    </thead>
    </table>
    </center>
</div>
<table class="list_table">
    <thead>
        <tr>
            <th>${ _("Produit") }</th>
            <th>${ _("Description") }</th>
            <th class="amount">${_("Quantité")}</th>
            <th class="amount">${_("Prix Unitaire")}</th>
            <th class="amount">${_("Montant HT ")}</th>
        </tr>
    </thead>
    <tbody>
    %for line in order.order_line:
        <tr class="line">
            <td style="text-align:center">${ line.product_id.name or '' }</td>
            <td style="text-align:center">${ line.name }</td>
            <td class="amount" width="8%" style="text-align:center">${ line.product_qty or ''}</td>
            <td class="amount" width="12%" style="text-align:center">${ formatLang(line.price_unit, currency_obj=order.currency_id) or '' }</td>
            <td class="amount" width="14%" style="text-align:center">${ formatLang(order.amount_untaxed, currency_obj=order.currency_id) or ''}</td>
        </tr>
    %endfor
    </tbody>
    </table>
    <p>&nbsp;</p>
    <table class="totaux">
    <tfoot class="totals">
        <tr>
            <td colspan="4"/>
            <td><b>${_("Total HT") } :</b></td>
            <td class="amount" style="white-space:nowrap">${ formatLang(order.amount_untaxed, currency_obj=order.currency_id) or ''}</td>
        </tr>
        <tr>
            <td colspan="4"/>
            <td><b>${_("Total VAT")} :</b></td>
            <td class="amount" style="white-space:nowrap">${ formatLang(order.amount_tax, currency_obj=order.currency_id) or ''}</td>
        </tr>
        <tr>
            <td colspan="4"/>
            <td><b>${_("Total TTC")} :</b></td>
            <td class="amount" style="white-space:nowrap">${ formatLang(order.amount_total, currency_obj=order.currency_id) or ''}</td>
        </tr>
    </tfoot>
    </table>

%if i < len(objects) - 1:
    <div style="page-break-after:always"></div>
%endif

%endfor


