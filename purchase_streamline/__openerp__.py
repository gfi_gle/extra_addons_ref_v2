# -*- coding: utf-8 -*-
##############################################################################
#
#    Purchase management improvements for Odoo
#    Copyright (C) 2013-2015 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    'name': "Purchase Streamline",
    'version': '8.0.1.20',
    'author': 'XCG Consulting',
    'category': "Purchase Management",
    'description': """Enhancements to the purchase module to streamline its
    usage.
    """,
    'website': 'http://odoo.consulting/',
    'depends': [
        'base',
        'purchase',
        'hr_streamline',
        'account_accountant',
        'account_streamline',
        'analytic_structure',
        'product_streamline',
        'report_webkit',
        'report_py3o',
    ],
    'data': [
        'report/purchase_order_streamline.xml',
        'report/purchase_order_streamline_py3o.xml',
        'security/security.xml',
        'security/record_rules.xml',
        'views/purchase_order.xml',
        'views/res_company.xml',
        'views/stock_move.xml',
        'views/stock_picking.xml',
        'views/board_purchase.xml',
        'workflows/purchase_order.xml',
    ],
    'demo': [
        'demo/purchase.order.csv'
    ],
    'test': [],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
