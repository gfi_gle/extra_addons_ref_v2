-- Fill the "operational_department_id" field added in purchase orders from the employees and departments.
BEGIN;
-- Put an operational department, even if it does not match the one in the
-- purchase order/rfq. 
-- Those with several op department will cause problem (random one written, see
-- next update for the fix)
UPDATE purchase_order AS porder
    SET operational_department_id = op_dep.id
    FROM hr_operational_department AS op_dep
    WHERE op_dep.employee_id = porder.employee_id;

-- Now overwrite those that match
UPDATE purchase_order AS porder
    SET operational_department_id = op_dep.id
    FROM hr_operational_department AS op_dep
    WHERE
        op_dep.department_id = porder.department_id AND
	        op_dep.employee_id = porder.employee_id;
COMMIT;

