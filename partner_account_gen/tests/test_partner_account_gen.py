import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partners = self.createAndTest(
            'res.partner',
            [
                {
                    'customer': False,
                    'name': genUuid(),
                    'supplier': False,
                },
                {
                    'customer': False,
                    'name': genUuid(),
                    'supplier': False,
                },
            ],
        )

    def test_0100_create_account_types(self):
        """Create account types, that will be used in other tests.
        """

        self.memory.test_account_type, = self.createAndTest(
            'account.account.type',
            [
                {
                    'code': 'test',
                    'name': 'test',
                },
            ],
        )

    def test_0110_create_account_generation_rules(self):
        """Create account generation rules, that will be used in other tests.
        """

        self.memory.account_gen_rule_client, = self.createAndTest(
            'account.generation_rule',
            [
                {
                    'account_type_id': self.memory.test_account_type.id,
                    'code_prefix': 'ABC',
                    'partner_account_field_id': self.browse_ref(
                        'account.field_res_partner_property_account_payable'
                    ).id,
                    'partner_condition_field_id': self.browse_ref(
                        'base.field_res_partner_customer'
                    ).id,
                },
            ],
        )

    def test_0120_account_generation_rule_generate_account(self):
        """Tests around the "generate_account" method of account generation
        rules.
        """

        account_gen_rule = self.memory.account_gen_rule_client

        account_code = 'testcode'
        account_name = 'testname'

        def clear_partner_accounts(partner):
            partner.write({
                'property_account_payable': False,
                'property_account_receivable': False,
            })

        partner = self.memory.partners[0]

        # First, a test run without setting the partner as a client.
        clear_partner_accounts(partner)
        account = account_gen_rule.generate_account(
            partner, account_code, account_name,
        )
        self.assertIsNone(account)

        # Now set the partner as a client and launch generation again.
        partner.customer = True
        clear_partner_accounts(partner)

        account = account_gen_rule.generate_account(
            partner, account_code, account_name,
        )

        # Ensure the account has been created as expected.
        self.assertIsInstance(account, openerp.models.BaseModel)
        self.assertEqual(account.code, 'ABCtestcode')
        self.assertEqual(account.name, 'testname')

        # Ensure a link to the account has been saved on the partner.
        self.assertEqual(partner.property_account_payable.id, account.id)

        # Try again with another partner and ensure the same account is
        # re-used.
        other_partner = self.memory.partners[1]
        other_partner.customer = True
        clear_partner_accounts(other_partner)
        other_account = account_gen_rule.generate_account(
            other_partner, account_code, account_name,
        )
        self.assertEqual(other_account.id, account.id)
        self.assertEqual(other_partner.property_account_payable.id, account.id)
