from openerp import fields
from openerp import models


class AccountGenerationRule(models.Model):
    """Rule defining how an accounting account is to be generated.
    """

    _name = 'account.generation_rule'

    _rec_name = 'partner_condition_field_id'

    internal_type = fields.Selection(
        [
            ('other', u"Regular"),
            ('payable', u"Payable"),
            ('receivable', u"Receivable"),
        ],
        string=u"Account Type",
        default='other'
    )

    account_type_id = fields.Many2one(
        comodel_name='account.account.type',
        string='Account type',
        ondelete='restrict',
        help='Type of the generated account.',
        required=True,
    )

    code_prefix = fields.Char(
        string='Code prefix',
        help='String prepended to the code of the generated account.',
    )

    code_suffix = fields.Char(
        string='Code suffix',
        help='String appended to the code of the generated account.',
    )

    parent_account_id = fields.Many2one(
        comodel_name='account.account',
        string='Parent',
        ondelete='restrict',
        help='Parent account of the generated account.',
    )

    partner_account_field_id = fields.Many2one(
        comodel_name='ir.model.fields',
        string='Partner account field',
        domain=[
            ('model', '=', 'res.partner'),
            ('relation', '=', 'account.account'),
            ('ttype', '=', 'many2one'),
        ],
        help=(
            'Account field of the partner on which to store a link to the '
            'generated account.'
        ),
        required=True,
    )

    partner_condition_field_id = fields.Many2one(
        comodel_name='ir.model.fields',
        string='Partner condition field',
        domain=[
            ('model', '=', 'res.partner'),
            ('ttype', '=', 'boolean'),
        ],
        help=(
            'Boolean field of the partner that has to be enabled for this '
            'rule to apply.'
        ),
        required=True,
    )

    def generate_account(self, partner, account_code, account_name):
        """Generate an account using the specified rule and additional
        parameters.

        :param partner: The partner on which to store a link to the generated
        account.
        :type partner: Odoo "res.partner" record-set.

        :param account_code: Code of the generated account; a prefix and a
        suffix will be added if set by the rule.
        :type account_code: String.

        :param account_name: Name of the generated account
        :type account_name: String.

        :return: The generated account.
        :rtype: Odoo "account.account" record-set.
        """

        self.ensure_one()

        if not getattr(partner, self.partner_condition_field_id.name):
            # Generation is disabled on the partner for the specified rule.
            return

        if getattr(partner, self.partner_account_field_id.name):
            # An account has already been specified; don't generate a new one.
            return

        def ensureStr(text):
            """Ensure the specified variable is read as a string."""
            return text or u''

        # Build the account code.
        account_code = (
            ensureStr(self.code_prefix) +
            ensureStr(account_code) +
            ensureStr(self.code_suffix)
        )

        # Create the account, unless one already exists with the desired code.
        account_obj = self.env['account.account']
        account = (
            account_obj.search([('code', '=', account_code)]) or
            account_obj.create({
                'code': account_code,
                'name': ensureStr(account_name),
                'parent_id': self.parent_account_id.id,
                'reconcile': True,
                'type': self.internal_type,
                'user_type': self.account_type_id.id,
            })
        )

        # Store a link to the generated account on the partner.
        setattr(partner, self.partner_account_field_id.name, account)

        return account
