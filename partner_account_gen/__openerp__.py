##############################################################################
#
#    Partner account generation for Odoo
#    Copyright (C) 2016 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Partner account generation',
    'description': '''
Partner account generation
==========================

Generation rules for accounting account linked to Odoo partners.
''',
    'version': '0.1',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'account_streamline',
    ],

    'data': [
        'security/ir.model.access.csv',

        'views/account_generation_rule.xml',
    ],

    'installable': True,
}
