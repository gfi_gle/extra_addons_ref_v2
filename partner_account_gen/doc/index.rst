Welcome to this addon's documentation!
======================================

.. include:: manifest

Contents:

.. toctree::
   :maxdepth: 2

   README
   NEWS
   models
   tests
   TODO


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

