openerp.addons.base_res_users_streamline package
================================================

Submodules
----------

openerp.addons.base_res_users_streamline.loginaudit module
----------------------------------------------------------

.. automodule:: openerp.addons.base_res_users_streamline.loginaudit
    :members:
    :undoc-members:
    :show-inheritance:

openerp.addons.base_res_users_streamline.res_users module
---------------------------------------------------------

.. automodule:: openerp.addons.base_res_users_streamline.res_users
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: openerp.addons.base_res_users_streamline
    :members:
    :undoc-members:
    :show-inheritance:
