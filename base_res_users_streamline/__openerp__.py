# -*- coding: utf-8 -*-
##############################################################################
#     
#    Better User login for Odoo
#    Copyright (C) 2013 XCG Consulting (http://odoo.consulting)
#    Author: Florent AIDE, <florent.aide@xcg-consulting.fr>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "base_res_users_streamline",
    "version": "8.0.1.3",
    "author": "XCG Consulting",
    "category": '',
    "description": """
    An addon you'll need to install if you plan to have a heavy number of users
    in your system.

    This will fix the following error:

     TransactionRollbackError: could not serialize access due to concurrent
     update
     CONTEXT:  SQL statement "SELECT 1 FROM ONLY "public"."res_users" x WHERE
     "id" OPERATOR(pg_catalog.=) $1 FOR KEY SHARE OF x"


    ATTENTION though: this module will render the "last login" column on your
    users definition useless since it disables the SQL updates associated to
    the user login phase to obtain better performances.
    """,
    'website': 'http://odoo.consulting',
    'init_xml': [],
    "depends": [
        'base',
    ],
    'images': [
    ],
    "data": [
        'security/ir.model.access.csv',
        'views/login_audit.xml',
        'views/res_users.xml',
        'menu.xml',
    ],
    'demo_xml': [
    ],
    'test': [
    ],
    'installable': True,
    'active': True,
    'css': [],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
