*******
Changes
*******

.. _1.3:


1.3
---

* Add view, menu item for "login_audit" + translations

.. _1.2:

1.2
---

* Hide login_date on tree view as it is not updated anymore

.. _1.1:

1.1
---

* Add a global ACL for the login_audit model

