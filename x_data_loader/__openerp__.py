# -*- coding: utf-8 -*-
##############################################################################
#
#    External data loader for Odoo
#    Copyright (C) 2013 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'External data loader',
    'description': '''
External data loader for Odoo
=============================

Load data from external systems into Odoo.


Description
-----------

This addon provides a way to load data in a transactional way into Odoo. The
data to be loaded is stored in a Redis server. This avoids multiple RPC calls
and ensures transaction concepts are respected when loading multiple related
elements at once.


Python dependencies
-------------------

The Python environment in which Odoo runs needs:

- msgpack-python (available on pypi)::

    $ pip install msgpack-python

- redis (available on pypi)::

    $ pip install redis


Supported Odoo versions
-----------------------

This addon has been tested to work on Odoo 7 & Odoo 8.
''',
    'version': '1.0-PL',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',

        'view/xbus_event_type.xml',
        "menu.xml",
    ],
    'installable': True,
    'external_dependencies': {
        'python': ['redis', 'msgpack'],
    },
}
