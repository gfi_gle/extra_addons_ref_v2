External data loader for Odoo
=============================

Load data from external systems into Odoo.


Description
-----------

This addon provides a way to load data in a transactional way into Odoo. The
data to be loaded is stored in a Redis server. This avoids multiple RPC calls
and ensures transaction concepts are respected when loading multiple related
elements at once.


Python dependencies
-------------------

The Python environment in which Odoo runs needs:

- msgpack-python (available on pypi)::

    $ pip install msgpack-python

- redis (available on pypi)::

    $ pip install redis


Supported Odoo versions
-----------------------

This addon has been tested to work on Odoo 7 & Odoo 8.
