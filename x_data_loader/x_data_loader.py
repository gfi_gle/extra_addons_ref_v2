# encoding: utf-8
import datetime
import json
import logging
import msgpack
import redis

from openerp import exceptions
from openerp.osv import osv


log = logging.getLogger(__name__)


class XDataLoader(osv.AbstractModel):
    _name = 'x_data_loader'
    _description = 'External data loader'

    def _redis_connection(self, r_host, r_port):
        """ Just a simple Redis connection with a ping test.
        """
        r = redis.StrictRedis(host=r_host, port=r_port)
        try:
            if not r.ping():
                return None
        except Exception:
            return None
        return r

    def error(self, index, e):
        return {'index': index, 'message': e}

    def _write_errors(self, event_id, redis_con, errors):
        msg = msgpack.packb(errors)
        key = "odooloader:errors:" + event_id
        redis_con.rpush(key, msg)

    def _read_objects_gen(self, redis, ref_dict, r_key, models):

        for model in models:
            model_key = r_key + model
            i = 0

            try:
                # Here is the core, we unpackb, try to create, and if an error
                # occurs, we rollback so we can continue to test other records.
                while 1:
                    msg = redis.lindex(model_key, i)
                    i += 1
                    if msg is None:
                        break

                    obj = msgpack.unpackb(msg)
                    if type(obj) is not dict:
                        err = "[{}] Message cannot be parsed".format(model_key)
                        yield None, self.error(i, err), None, None
                    indices = obj.pop('.index', None)
                    ref = obj.pop('.ref', None)

                    yield model, obj, ref, indices

            except Exception as e:
                err = u"[{}] {}: {}".format(
                    model_key, type(e).__name__, unicode(e)
                )
                raise e
                yield None, self.error(i, err), None, None

    def _orm_error(self, cr, errors, e, indices, head=None):
        cr.rollback()
        if head:
            err_msg = u"{}: {}".format(head, unicode(e))
        else:
            err_msg = str(e)
        log.info('ORM error; rolled back - %s', err_msg)
        errors.append((indices, err_msg))

    def load_data(
        self, cr, uid, event_id, r_host='localhost', r_port=6379,
        create=None, write=None, delete=None, rollback=False
    ):
        """ This function try a connection with a redis server, read the
        stream with key=odoo_evtdata:evt_id, and create related model records.
        This function rely on the fact that the redis stream
        is not mal-formatted
        """

        log.info('x_data_loader::load_data: Start.')
        start_time = datetime.datetime.now()

        re = self._redis_connection(r_host, r_port)
        if re is None:
            log.error('Error connecting to the Redis server')
            return self.error(0, 'Error connecting to the Redis server')

        log.info('Redis connection successful')

        # We collect errors so we can push it in the redis stream
        errors = []
        # TODO: use a more fitting type ; numpy.zeros(n, dtype=numpy.int32)?
        ref_dict = {}

        context = {
            # Disable parent link recomputations while loading.
            'defer_parent_store_computation': True,

            # Signal the fact this loading is external.
            'external_load': True,
        }

        log.info('Starting loop')

        nmspc = 'odooloader-{}'.format(cr.dbname)
        c_key = '{}:create:{}:'.format(nmspc, event_id)
        create_generator = self._read_objects_gen(re, ref_dict, c_key, create)
        for model, obj, ref, indices in create_generator:
            if model is None:
                log.error('Create rollback')
                cr.rollback()
                return obj
            try:
                orm = self.pool[model]
                rec_id = orm.create(cr, uid, obj, context=context)
                log.info('Record {} created.'.format(rec_id))
            except Exception as e:
                self._orm_error(cr, errors, e, indices, "Error on creation")
                rec_id = False
            if ref is not None:
                ref_dict[-ref] = rec_id

        w_key = '{}:write:{}:'.format(nmspc, event_id)
        write_generator = self._read_objects_gen(re, ref_dict, w_key, write)
        for model, obj, ref, indices in write_generator:
            if model is None:
                log.error('Update rollback')
                cr.rollback()
                return obj
            try:
                orm = self.pool[model]
                if ref is not None and ref < 0:
                    ref = ref_dict[-ref]
                orm.write(cr, uid, ref, obj, context=context)
                log.info('Record {} updated.'.format(ref))
            except Exception as e:
                self._orm_error(cr, errors, e, indices, "Error on update")

        d_key = '{}:delete:{}:'.format(nmspc, event_id)
        delete_generator = self._read_objects_gen(re, ref_dict, d_key, delete)
        for model, obj, ref, indices in delete_generator:
            if model is None:
                log.error('Delete rollback')
                cr.rollback()
                return obj
            try:
                orm = self.pool[model]
                if ref is not None and ref < 0:
                    ref = ref_dict[-ref]
                orm.unlink(cr, uid, ref, context=context)
                log.info('Record {} deleted.'.format(ref))
            except Exception as e:
                self._orm_error(cr, errors, e, indices, "Error on delete")

        log.info('Loop done')

        if errors:
            log.error('Got errors; rolling back')
            cr.rollback()
            self._write_errors(event_id, re, errors)
            return False

        if rollback:
            log.info('Rolling back as requested by the caller')
            cr.rollback()
        else:
            log.info('Computing the parent/child hierarchy')
            for model in set(create + write + delete):
                orm = self.pool[model]
                orm._parent_store_compute(cr)
            log.info('Done computing the parent/child hierarchy')

        log.info('x_data_loader::load_data: Done - Time spent: %s.',
                 datetime.datetime.now() - start_time)
        return True

    def run_calls(self, cr, uid, redis_url, redis_db, redis_key):
        """Run calls found within the specified Redis key through a single
        transaction.
        """

        prefix = 'x_data_loader::run_calls - %s: ' % redis_key
        log.info(prefix + 'Start url: <%s> db: %d.', redis_url, redis_db)
        start_time = datetime.datetime.now()

        redis_conn = redis.from_url(redis_url, db=redis_db)
        if not redis_conn.ping():
            raise exceptions.Warning(
                prefix + 'Cannot connect to Redis on <%s> (DB #%d).' %
                (redis_url, redis_db),
            )
        log.info(prefix + 'Successfully connected to Redis on <%s> (DB #%d).',
                 redis_url, redis_db)

        context = {
            # Disable parent link recomputations while loading.
            'defer_parent_store_computation': True,

            # Signal the fact this loading is external.
            'external_load': True,
        }

        # Store affected models so we can refresh their parent hierarchy once
        # done.
        affected_models = {}

        # Grab the calls to run from Redis.
        json_calls = redis_conn.lrange(redis_key, 0, -1)
        log.info(prefix + "%d calls to run.", len(json_calls))

        # Call data is JSON-encoded; decode then run calls.
        for json_call in json_calls:
            call = json.loads(json_call)
            event_type = call.get('event-type')
            model = call.get('odoo-model')
            method = call.get('odoo-method')
            json_data = call.get('data')
            if not event_type or not model or not method or not json_data:
                raise exceptions.Warning(prefix + (
                    'Incomplete call information (need "event-type", '
                    '"odoo-model", "odoo-method" and "data" keys).'
                ))
            affected_event_type = affected_models.get(event_type, [])
            affected_models.update({
                event_type: affected_event_type,
            })
            affected_event_type.append({
                "event_type": event_type,
                "model": model,
                "method": method,
                "data": json.loads(json_data),
            })

        # Call models accordingly to their priorities
        xbus_event_type_ids = self.pool["xbus_event_type"].search(
            cr, uid, [], context=context
        )
        xbus_event_types = self.pool["xbus_event_type"].browse(
            cr, uid, xbus_event_type_ids, context=context
        )
        ordered_xbus_event_types = sorted(
            xbus_event_types, key=lambda r: r.priority
        )

        for event_type in ordered_xbus_event_types:
            if event_type.name in affected_models:
                messages = affected_models.pop(event_type.name, None)
                for m in messages:
                    getattr(self.pool[m.get("model")], m.get("method"))(
                        cr, uid, m.get("data"), context=context,
                    )
                    # Refresh parent hierarchies when applicable.
                    self.pool[m.get("model")]._parent_store_compute(cr)

        if affected_models.keys():
            raise exceptions.Warning(prefix + (
                    "No event type priority defined for {}. "
                    "Please check in odoo 'Configuration/Xbus Event Type' "
                    "that all events are set up."
                ).format(affected_models.keys())
            )

        # All good! Delete the Redis key.
        redis_conn.delete(redis_key)

        log.info(prefix + 'Done - Time spent: %s.',
                 datetime.datetime.now() - start_time)
        return True
