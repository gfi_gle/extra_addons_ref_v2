from openerp.osv import fields, osv


class XbusEventType(osv.Model):
    """An event type received from xbus.
    """

    _name = "xbus_event_type"

    _order = "priority ASC"

    _columns = {
        "name": fields.char(
            u"Xbus Event Type",
            required=True,
        ),
        "priority": fields.integer(
            u"Priority",
            help=(
                u"Number to handle xbus event types priority. "
                u"The lowest is processed first."
            ),
        )
    }
