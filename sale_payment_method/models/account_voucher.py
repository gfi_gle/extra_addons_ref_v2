from openerp import api
from openerp import models


class AccountingVoucher(models.Model):
    """Customize accounting vouchers:
    - Propagate the sales payment method to accounting elements.
    """

    _inherit = 'account.voucher'

    @api.multi
    def proforma_voucher(self):
        """Override to:
        - Propagate the sales payment method to accounting elements.
        """

        ret = super(AccountingVoucher, self).proforma_voucher()

        # TODO Better way of fetching the default payment method.
        payment_method = self.env['crm.payment.mode'].search(
            [('technical_name', '=', 'VIR')], limit=1,
        )
        if not payment_method:
            return ret

        for voucher in self:

            # Get the accounting documents linked to the accounting entries,
            # and from there, all of their entries. This assumes some about
            # payment (no mixed payments).
            payment_docs = voucher.move_ids.mapped('move_id')
            payment_entries = payment_docs.mapped('line_id')

            # Ignore those that already have a payment method.
            payment_entries = payment_entries.filtered(
                lambda entry: not entry.sales_payment_method_id
            )

            # Include our payment method onto the accounting entries.
            payment_entries.write({
                'sales_payment_method_id': payment_method.id,
            })

        return ret
