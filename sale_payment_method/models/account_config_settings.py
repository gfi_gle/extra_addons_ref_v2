from openerp import api
from openerp import fields
from openerp import models


_ALLOW_ISPMU_KEY = (
    'sale_payment_method.allow_invoice_sales_payment_method_updates'
)


class AccountConfigSettings(models.TransientModel):
    """Inherit from account.config.settings to add a setting. This is only here
    for easier access; the setting is not actually stored by this (transient)
    model. Instead, it is kept in sync with the
    "sale_payment_method.allow_invoice_sales_payment_method_updates" global
    setting. See commends in the definition of the "res.config.settings" model
    for details.
    """

    _inherit = 'account.config.settings'

    allow_ispmu = fields.Boolean(
        string='Allow sales payment method updates in invoices',
        help=(
            'Whether the "sales payment method" field in invoices may be '
            'updated (as long as the invoice has not been validated). When '
            'disabled, the field is to be filled in from its related sales '
            'order.'
        ),
    )

    @api.model
    def get_default_allow_ispmu(self, fields):
        """Read the allow_ispmu setting. This function is called when the form
        is shown.
        """

        ret = {}

        if 'allow_ispmu' in fields:
            ret['allow_ispmu'] = self.read_allow_ispmu()

        return ret

    @api.model
    def read_allow_ispmu(self):
        """Read the allow_ispmu setting. Use the admin account to bypass
        security restrictions.
        """

        config_record = self.env['ir.config_parameter'].sudo().search(
            [('key', '=', _ALLOW_ISPMU_KEY)],
            limit=1,
        )
        return config_record and config_record.value == '1'

    @api.multi
    def set_allow_saml_ispmu(self):
        """Update the allow_ispmu setting. This function is called when saving
        the form.
        """

        setting_value = '1' if self.allow_ispmu else '0'

        existing_config_record = self.env['ir.config_parameter'].search(
            [('key', '=', _ALLOW_ISPMU_KEY)],
            limit=1,
        )

        if existing_config_record:
            existing_config_record.value = setting_value

        else:
            # The setting doesn't exist; create it.
            self.env['ir.config_parameter'].create(
                {'key': _ALLOW_ISPMU_KEY, 'value': setting_value},
            )
