from openerp import fields
from openerp import models


class PaymentMode(models.Model):
    """Add settings into payment methods.
    """

    _inherit = 'crm.payment.mode'

    technical_name = fields.Char(
        string='Technical name',
        help='The technical name of this payment method; used in exports.',
    )

    debit_payment = fields.Boolean(
        string='Debit payment',
        help='This payment method is used in debit payments.',
    )
