from openerp import api
from openerp import fields
from openerp import models


class AccountingInvoice(models.Model):
    """Propagate the sales payment method to accounting elements.
    """

    _inherit = 'account.invoice'

    @api.one
    def _get_allow_spmethod_updates(self):
        self.allow_spmethod_updates = (
            self.env['account.config.settings'].read_allow_ispmu()
        )

    # Utility field to propagate a setting into views.
    allow_spmethod_updates = fields.Boolean(
        compute=_get_allow_spmethod_updates,
        string='Allow sales payment method updates',
    )

    sales_payment_method_id = fields.Many2one(
        comodel_name='crm.payment.mode',
        string='Sales payment method',
        ondelete='restrict',
        help=(
            'The payment method linked to the sales order this invoice is '
            'for.'
        ),
        readonly=True,
    )

    @api.multi
    def finalize_invoice_move_lines(self, move_lines):
        """Override this method, conceived as a hook, to set payment method
        information onto invoice accounting entries.

        As we want access to invoice settings and we also want to affect
        partner accounting entries, no better hook is available.
        """

        self.ensure_one()

        ret = (
            super(AccountingInvoice, self)
            .finalize_invoice_move_lines(move_lines)
        )

        payment_method = self.sales_payment_method_id
        if payment_method:
            for entry_iter in ret:  # entry_iter: (0, 0, {entry-values}).
                # Extract accounting entry creation values.
                if not isinstance(entry_iter, (tuple, list)):
                    continue
                if len(entry_iter) < 2:
                    continue
                entry_values = entry_iter[2]
                if not entry_values or not isinstance(entry_values, dict):
                    continue
                entry_values['sales_payment_method_id'] = payment_method.id

        return ret

    @api.multi
    def confirm_paid(self):
        """Override to set payment method information onto invoice payment
        accounting entries.
        """

        ret = super(AccountingInvoice, self).confirm_paid()

        for invoice in self:

            payment_method = invoice.sales_payment_method_id
            if not payment_method:
                continue

            # The values to update accounting entries with.
            update_values = {'sales_payment_method_id': payment_method.id}

            # Get the accounting documents linked to the accounting entries.
            payment_docs = invoice.payment_ids.mapped('move_id')

            # Include our payment method onto all entries of the accounting
            # documents. This assumes some about payment (no mixed payments).
            for payment_entry in payment_docs.mapped('line_id'):
                payment_entry.write(update_values)

        return ret
