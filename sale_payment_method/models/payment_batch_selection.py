from openerp import api
from openerp import models


class PaymentBatchSelection(models.TransientModel):
    _inherit = 'account.payment_batch.selection'

    @api.multi
    def sale_payment_method_filter(self, entry_search_domain):
        """Use the hook prepared in Account Streamline, in order to filter
        accounting document lines on payment methods, which are associated to
        debit payments.
        """

        self.ensure_one()

        if 'unit_tests' not in self.env.context:
            entry_search_domain.append(
                ('sales_payment_method_id.debit_payment', '=', True)
            )

        return entry_search_domain
