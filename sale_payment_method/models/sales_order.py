from openerp import fields
from openerp import models


class SalesOrder(models.Model):
    """Add a payment method selector to sales orders.
    """

    _inherit = 'sale.order'

    payment_method_id = fields.Many2one(
        comodel_name='crm.payment.mode',
        string='Payment method',
        ondelete='restrict',
        help=(
            'The payment method used to pay this sales order. May influence '
            'certain parts of the validation / accounting process of this '
            'sales order.'
        ),
        readonly=True,
        states={'draft': [('readonly', False)]},
    )

    def _prepare_invoice(self, cr, uid, order, lines, context=None):
        """Override to propagate the sales payment method into invoices
        generated from this sales order.
        """

        ret = super(SalesOrder, self)._prepare_invoice(
            cr, uid, order, lines, context=context,
        )

        payment_method = order.payment_method_id
        if payment_method:
            ret['sales_payment_method_id'] = payment_method.id

        return ret
