from openerp import fields
from openerp import models


class AccountingEntry(models.Model):
    """- Propagate the sales payment method to accounting elements.
    """

    _inherit = 'account.move.line'

    # This field is meant to be read-only but we leave it open to allow data
    # imports. It is not shown in any view, but were it so, it would then be
    # read-only there.
    sales_payment_method_id = fields.Many2one(
        comodel_name='crm.payment.mode',
        string='Sales payment method',
        ondelete='restrict',
        help=(
            'The payment method linked to the sales order this accounting '
            'entry is for.'
        ),
    )
