Sales payment methods
=====================

Link sales orders to payment methods (using the "payment mode" list provided by
the CRM addon). The payment method is propagated to accounting elements.


UI
--

* Sales orders: Payment method.

* Invoices: Sales payment method.

* Settings > Accounting > Sales payment methods:

    - Allow sales payment method updates in invoices.
