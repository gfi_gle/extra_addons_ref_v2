##############################################################################
#
#    Sales payment methods for Odoo
#    Copyright (C) 2016 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Sales payment methods',
    'description': '''
Sales payment methods
=====================

Link sales orders to payment methods (using the "payment mode" list provided by
the CRM addon). The payment method is propagated to accounting elements.


UI
--

* Sales orders: Payment method.

* Invoices: Sales payment method.

* Settings > Accounting > Sales payment methods:

    - Allow sales payment method updates in invoices.
''',
    'version': '0.1',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'crm',  # For payment modes.
        'sale_streamline',
        'account_invoice_streamline',
        'account_streamline',
    ],

    'data': [
        'data/ir_config_parameter.xml',

        'views/account_config_settings.xml',
        'views/account_invoice.xml',
        'views/crm_payment_mode.xml',
        'views/sales_order.xml',
    ],

    'installable': True,
}
