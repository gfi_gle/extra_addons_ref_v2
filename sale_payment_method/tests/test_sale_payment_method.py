import openerp.tests

from .util.odoo_tests import TestBase
from .util.singleton import Singleton
from .util.uuidgen import genUuid


class TestMemory(object):
    """Keep records in memory across tests."""
    __metaclass__ = Singleton


@openerp.tests.common.at_install(False)
@openerp.tests.common.post_install(True)
class Test(TestBase):

    def setUp(self):
        super(Test, self).setUp()
        self.memory = TestMemory()

    def test_0000_create_payment_methods(self):
        """Create payment methods and store them for further use.
        """

        self.memory.payment_method, = self.createAndTest(
            'crm.payment.mode',
            [
                {
                    'name': genUuid(),
                },
            ],
        )

    def test_0001_create_partners(self):
        """Create simple partners, that will be used in other tests.
        """

        self.memory.partner, = self.createAndTest(
            'res.partner',
            [
                {
                    'customer': True,
                    'name': genUuid(),
                },
            ],
        )

    def test_0010_create_sales_order(self):
        """Ensure a sales order can be created, with a payment method.
        """

        sales_order, = self.createAndTest(
            'sale.order',
            [
                {
                    'partner_id': self.memory.partner.id,
                    'payment_method_id': self.memory.payment_method.id,
                },
            ],
        )
