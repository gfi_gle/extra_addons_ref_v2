Accounting Export Sender
========================

* Category: Accounting

* Quick summary:
Send accounting export files to external systems.

* Version:
8.0.1.0

* Dependencies:
  account_export

* Repo owner or admin: XCG Consulting

Python environment dependencies
-------------------------------

- scp (available on pypi)::

    pip install scp


Configuration
-------------

This addon creates a cron task, inactive by default. Configure it, then set it 
as active.

Sender cron task
~~~~~~~~~~~~~~~~~

Arguments of the sender methods:

- Dictionary - Arguments sent to `paramiko's "connect" function`_.

- String - Where to send files on the remote server.

.. _paramiko's "connect" function:
    http://docs.paramiko.org/en/latest/api/client.html
    #paramiko.client.SSHClient.connect
