# -*- coding: utf-8 -*-
##############################################################################
#
#    Accounting Export Sender, for OpenERP
#    Copyright (C) 2017 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Accounting Export Sender",
    "version": '8.0.1.0',
    "author": "XCG Consulting",
    "category": 'Accounting',
    "description": """
Accounting Export Sender
========================

Send accounting export files to external systems.

Python environment dependencies
-------------------------------

- scp (available on pypi)::

    pip install scp


Configuration
-------------

This addon creates several cron tasks, all inactive by default. Configure them
then set them as active.

Sender cron tasks
~~~~~~~~~~~~~~~~~

Arguments of the sender methods:

- Dictionary - Arguments sent to `paramiko's "connect" function`_.

- String - Where to send files on the remote server.

.. _paramiko's "connect" function:
    http://docs.paramiko.org/en/latest/api/client.html
    #paramiko.client.SSHClient.connect

Receiver cron tasks
~~~~~~~~~~~~~~~~~~~

Arguments of the receiver methods:

- Dictionary - Arguments sent to `paramiko's "connect" function`_.

- String - Where files reside on the remote server.

.. _paramiko's "connect" function:
    http://docs.paramiko.org/en/latest/api/client.html
    #paramiko.client.SSHClient.connect
""",
    'website': 'http://odoo.consulting/',
    'init_xml': [],
    "depends": [
        'account_export',
    ],
    "data": [
        'data/cron_tasks/acc_entry_batch_sender.xml',
        'data/cron_tasks/partner_info_batch_sender.xml',

        'views/account_move_line_batch.xml',
        'views/partner_info_batch.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
