from openerp import models
from openerp import api

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


class SaleOrder(models.Model):
    """- Include analytic capabilities to sales orders.
    - Disallow copies thanks to "base_no_copy".
    """

    __metaclass__ = MetaAnalytic

    _inherit = ['sale.order', 'base.no_copy_mixin']
    _name = 'sale.order'

    _analytic = True

    @api.model
    def create(self, vals):
        """Override to:
        - Propagate partner analytics.
        """

        partner_id = vals.get('partner_id')
        if partner_id:
            partner = self.env['res.partner'].browse(partner_id)
            vals.update(self.env['analytic.structure'].extract_values(
                partner, 'res_partner', dest_model='sale_order',
            ))

        return super(SaleOrder, self).create(vals)

    @api.multi
    def write(self, vals):
        """Override to:
        - Propagate partner analytics.
        """

        ret = super(SaleOrder, self).write(vals)

        if 'partner_id' in vals:
            for sorder in self:
                sorder_values = self.env['analytic.structure'].extract_values(
                    sorder.partner_id, 'res_partner', dest_model='sale_order',
                )
                if sorder_values:
                    sorder.write(sorder_values)

        return ret

    def fields_view_get(
        self, cr, uid, view_id=None, view_type='form', context=None,
        toolbar=False, submenu=False
    ):
        """Override to handle analytics.
        """

        ret = super(SaleOrder, self).fields_view_get(
            cr, uid, view_id=view_id, view_type=view_type, context=context,
            toolbar=toolbar, submenu=submenu
        )

        self.pool['analytic.structure'].analytic_fields_subview_get(
            cr, uid, 'sale_order_line', ret['fields'].get('order_line')
        )

        return ret

    @api.multi
    def action_button_confirm_order_multi(self):
        """Call the sale.order action_button_confirm method on each active id.
        """

        # Support multi-selections from lists. IDs are in that case sent
        # through "active_ids" via the context...
        record_ids = self.env.context.get('active_ids')
        records = self.browse(record_ids) if record_ids else self

        for this in records:
            super(SaleOrder, this).action_button_confirm()

        return True
