import datetime
from openerp import _
from openerp import api
from openerp import exceptions
from openerp import models
from openerp import SUPERUSER_ID
from openerp.tools import (
    DEFAULT_SERVER_DATE_FORMAT,
    DEFAULT_SERVER_DATETIME_FORMAT,
)
import time

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


class SaleOrderLine(models.Model):
    """Include analytic capabilities to sales orders.
    """

    __metaclass__ = MetaAnalytic

    _inherit = 'sale.order.line'

    _analytic = True

    @api.model
    def create(self, vals):
        """Override to:
        - Propagate product analytics.
        """

        product_id = vals.get('product_id')
        if product_id:
            product = self.env['product.product'].browse(product_id)
            vals.update(self.env['analytic.structure'].extract_values(
                product.product_tmpl_id, 'product_template',
                dest_model='sale_order_line',
            ))

        return super(SaleOrderLine, self).create(vals)

    @api.multi
    def write(self, vals):
        """Override to:
        - Propagate product analytics.
        """

        ret = super(SaleOrderLine, self).write(vals)

        if 'product_id' in vals:
            for soline in self:
                soline_values = self.env['analytic.structure'].extract_values(
                    soline.product_id.product_tmpl_id, 'product_template',
                    dest_model='sale_order_line',
                )
                if soline_values:
                    soline.write(soline_values)

        return ret

    def product_id_change(
        self, cr, uid, ids, pricelist, product, qty=0, uom=False, qty_uos=0,
        uos=False, name='', partner_id=False, lang=False, update_tax=True,
        date_order=False, packaging=False, fiscal_position=False, flag=False,
        context=None,
    ):
        """Copied from sale.sale.sale_order_line. Changes:
        - Code formatting.
        - Fill sales order line analytics based on products.
        - Add context arguments when calling pricelist rule getters.
        - Ensure the date is always a date, without time.
        """

        # TODO Switch to an Odoo 8 style change handler.

        if context is None:
            context = {}
        initial_context = context.copy()

        lang = lang or context.get('lang', False)
        if not partner_id:
            raise exceptions.Warning(_(
                'No Customer Defined. - Before choosing a product, select a '
                'customer in the sales form.'
            ))
        warning = False
        product_uom_obj = self.pool.get('product.uom')
        partner_obj = self.pool.get('res.partner')
        product_obj = self.pool.get('product.product')
        context = {'lang': lang, 'partner_id': partner_id}
        partner = partner_obj.browse(cr, uid, partner_id)
        lang = partner.lang
        context_partner = context.copy()
        context_partner.update({'lang': lang, 'partner_id': partner_id})

        if not product:
            return {
                'domain': {'product_uom': [], 'product_uos': []},
                'value': {'th_weight': 0, 'product_uos_qty': qty},
            }

        if date_order:
            # Sometimes we receive dates containing times, which make the
            # pricelist checker fail. Ensure we always have only dates...
            try:
                date_order = datetime.datetime.strptime(
                    date_order, DEFAULT_SERVER_DATETIME_FORMAT
                ).strftime(DEFAULT_SERVER_DATE_FORMAT)
            except ValueError:
                date_order = None

        if not date_order:
            date_order = time.strftime(DEFAULT_SERVER_DATE_FORMAT)

        result = {}
        warning_msgs = ''
        product_obj = product_obj.browse(
            cr, uid, product, context=context_partner
        )

        uom2 = False
        if uom:
            uom2 = product_uom_obj.browse(cr, uid, uom)
            if product_obj.uom_id.category_id.id != uom2.category_id.id:
                uom = False
        if uos:
            if product_obj.uos_id:
                uos2 = product_uom_obj.browse(cr, uid, uos)
                if product_obj.uos_id.category_id.id != uos2.category_id.id:
                    uos = False
            else:
                uos = False

        fpos = False
        if not fiscal_position:
            fpos = partner.property_account_position or False
        else:
            fpos = self.pool['account.fiscal.position'].browse(
                cr, uid, fiscal_position
            )

        if uid == SUPERUSER_ID and context.get('company_id'):
            taxes = product_obj.taxes_id.filtered(
                lambda r: r.company_id.id == context['company_id']
            )
        else:
            taxes = product_obj.taxes_id
        result['tax_id'] = self.pool['account.fiscal.position'].map_tax(
            cr, uid, fpos, taxes, context=context,
        )

        if not flag:
            result['name'] = self.pool['product.product'].name_get(
                cr, uid, [product_obj.id], context=context_partner
            )[0][1]
            if product_obj.description_sale:
                result['name'] += '\n' + product_obj.description_sale
        domain = {}
        if (not uom) and (not uos):
            result['product_uom'] = product_obj.uom_id.id
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
                uos_category_id = product_obj.uos_id.category_id.id
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
                uos_category_id = False
            result['th_weight'] = qty * product_obj.weight
            domain = {
                'product_uom': [('category_id', '=',
                                 product_obj.uom_id.category_id.id)],
                'product_uos': [('category_id', '=', uos_category_id)],
            }
        elif uos and not uom:  # only happens if uom is False
            result['product_uom'] = (
                product_obj.uom_id and product_obj.uom_id.id
            )
            result['product_uom_qty'] = qty_uos / product_obj.uos_coeff
            result['th_weight'] = (
                result['product_uom_qty'] * product_obj.weight
            )
        elif uom:  # whether uos is set or not
            default_uom = product_obj.uom_id and product_obj.uom_id.id
            q = product_uom_obj._compute_qty(cr, uid, uom, qty, default_uom)
            if product_obj.uos_id:
                result['product_uos'] = product_obj.uos_id.id
                result['product_uos_qty'] = qty * product_obj.uos_coeff
            else:
                result['product_uos'] = False
                result['product_uos_qty'] = qty
            # Round the quantity up
            result['th_weight'] = q * product_obj.weight

        if not uom2:
            uom2 = product_obj.uom_id
        # get unit price

        if not pricelist:
            warn_msg = _(
                'You have to select a pricelist or a customer in the sales '
                'form ! Please set one before choosing a product.'
            )
            warning_msgs += _('No Pricelist. ') + warn_msg + '\n\n'
        else:
            pricelist_context = initial_context.copy()
            pricelist_context.update({
                'date': date_order,
                'uom': uom or result.get('product_uom'),
            })
            price = self.pool['product.pricelist'].price_get(
                cr, uid, [pricelist], product, qty or 1.0, partner_id,
                pricelist_context
            )[pricelist]
            if price is False:
                warn_msg = _(
                    'Cannot find a pricelist line matching this product and '
                    'quantity. You have to change either the product, the '
                    'quantity or the pricelist.'
                )

                warning_msgs += (
                    _('No valid pricelist line found. ') + warn_msg + '\n\n'
                )
            else:
                price = self.pool['account.tax']._fix_tax_included_price(
                    cr, uid, price, taxes, result['tax_id'],
                )
                result.update({'price_unit': price})
                if context.get('uom_qty_change', False):
                    values = {'price_unit': price}
                    if result.get('product_uos_qty'):
                        values['product_uos_qty'] = result['product_uos_qty']
                    return {'value': values, 'domain': {}, 'warning': False}
        if warning_msgs:
            warning = {
                'title': _('Configuration Error!'),
                'message': warning_msgs,
            }

        # Let the "extract_values" method provide analytic field updates.
        result.update(self.pool['analytic.structure'].extract_values(
            cr, uid, product_obj.product_tmpl_id, 'product_template',
            dest_model='sale_order_line', context=context
        ))

        return {'value': result, 'domain': domain, 'warning': warning}
