Sales enhancements for Odoo
===========================

- Apply general sales enhancements.

- Include analytics into sales orders.

- Disallow copies thanks to ``base_no_copy``.
