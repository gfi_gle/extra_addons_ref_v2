##############################################################################
#
#    Sales enhancements for Odoo
#    Copyright (C) 2015 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Sales enhancements',
    'description': '''
Sales enhancements for Odoo
===========================

- Apply general sales enhancements.

- Include analytics into sales orders.

- Disallow copies thanks to ``base_no_copy``.
''',
    'version': '8.0.1.0.3-prodready',
    'category': 'Sales',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'base',
        'sale',
        'analytic_structure',
        'base_no_copy',
        'product_streamline',
    ],

    'data': [
        'views/sale_order.xml',
        'views/sale_order_line.xml',
    ],

    'installable': True,
}
