==================================
Use the whole available screen width when displaying sheets
==================================

Origin
------

This is a Mercurial proxy of <https://github.com/OCA/web/tree/123bfbcb4d75d6c8750e564abc26c9c31d42bb89/web_sheet_full_width>.


Description
-----------

This addon displays sheets making use of the whole screen, thereby avoiding
to narrow columns in ie. sale orders or purchase orders.

