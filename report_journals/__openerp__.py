# -*- coding: utf-8 -*-
##############################################################################
#
#    Journals, for Odoo
#    Copyright (C) 2016 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "Journals",
    "version": '1.0',
    "author": "XCG Consulting",
    "category": 'Accounting',
    "description": u"""
        Journals
    """,
    'website': 'http://odoo.consulting/',
    'init_xml': [],
    "depends": [
        'base',
        'account',
        'account_report',
        'external_job',
        'queue',
    ],
    "data": [
        'data/cron_tasks/queue.csv',
        'data/job_definitions/journals.xml',
        'data/cron_tasks/report_journals.xml',
        'views/journals_view.xml',
        'wizard/journals_view.xml',
        'security/ir.model.access.csv',
        'security/security.xml',
    ],
    # 'demo_xml': [],
    'test': [],
    'installable': True,
    'active': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
