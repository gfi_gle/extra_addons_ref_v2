# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2015-2016 XCG Consulting (http://www.xcg-consulting.fr/)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import datetime
import json
import tempfile
import codecs

from babel.dates import format_date, format_datetime
from babel.numbers import format_decimal

from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp.tools import config
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT

from sqlalchemy import create_engine, and_
from sqlalchemy.schema import MetaData, Table
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import select


options = [
    ('posted', 'All Posted Entries'),
    ('all', 'All Entries'),
]


class Journals(osv.Model):
    """This is used to store the result of the wizard.
    """

    _name = 'report_journals'

    _columns = {
        'name': fields.char(
            size=512,
            string="Name",
        ),

        'job_log_id': fields.many2one(
            'external_job.job_log',
            u"External job",
            readonly=True,
            ondelete='cascade',
        ),

        'filename': fields.related(
            'job_log_id',
            'filename',
            type='char',
            string=u"Output",
            size=512,
            readonly=True,
        ),

        'file': fields.related(
            'job_log_id',
            'out_file',
            type='binary',
            string=u"File Data",
            readonly=True,
        ),

        'company_id': fields.many2one(
            'res.company',
            string='Company',
            readonly=True
        ),

        'fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            'Fiscal Year',
            help='Keep empty for all open fiscal year',
            required=True,
            readonly=True
        ),

        'period_from': fields.many2one(
            'account.period',
            'Start Period',
            required=True,
            readonly=True
        ),

        'period_to': fields.many2one(
            'account.period',
            'End Period',
            required=True,
            readonly=True
        ),

        'journal_ids': fields.many2many(
            'account.journal',
            string='Journals',
            required=True,
            readonly=True
        ),

        'moves_to_include': fields.selection(
            options,
            'Moves to include',
            required=True,
            readonly=True
        ),

        'user': fields.many2one(
            'res.users',
            string="Creator",
            required=True,
            readonly=True
        ),

        'lang_id': fields.many2one(
            'res.lang',
            string="Creator's language",
            required=True,
            readonly=True
        ),
    }

    def get_period_ids(self, cr, uid, data, context):
        period_osv = self.pool['account.period']
        edge_periods = period_osv.read(
            cr, uid, [
                data.period_from.id,
                data.period_to.id
            ], ['date_start'], context=context
        )
        if data.period_from.id == data.period_to.id:
            edge_periods.append(edge_periods[0])
        period_ids = self.pool['account.period'].search(
            cr, uid, [
                ('date_start', '<=', edge_periods[1]['date_start']),
                ('date_start', '>=', edge_periods[0]['date_start']),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
                ('special', '=', False),
            ], context=context
        )
        opening_period_id = period_osv.search(
            cr, uid, [
                ('special', '=', True),
                ('fiscalyear_id', '=', data['fiscalyear_id'].id),
            ], limit=1, context=context
        )[0]

        if data.period_from.id == opening_period_id:
            period_ids.append(opening_period_id)
            period_ids = sorted(period_ids)

        return period_ids

    def get_dimension_ids(self, cr, uid, context):

        anc_obj = self.pool['analytic.structure']

        analytic_structure_ids = anc_obj.search(
            cr, uid,
            [('model_name', '=', 'account_move_line')],
            context=context
        )

        analytic_structures = anc_obj.browse(
            cr, uid, analytic_structure_ids, context=context
        )

        dimension_ids = [anc.ordering for anc in analytic_structures]
        dimensions = [anc.nd_id.name for anc in analytic_structures]

        return (dimension_ids, dimensions)

    def group_lines_by_journals_and_periods(self, lines):

        # TODO Document this.

        res = {}
        for line in lines:

            # TODO Use collections.defaultdict.

            journal_key = (line['Journal Code'], line['Journal Name'])
            if journal_key not in res.keys():
                res[journal_key] = {}

            # TODO Better variable name than just "a".

            a = res[journal_key]

            period_key = (line['Period end date'], line['Period Name'])
            if period_key not in a.keys():
                a[period_key] = []

            a[period_key].append(line)

        return res

    def generate_report(self, cr, uid, report_journal_id, context=None):

        data_obj = self.pool['ir.model.data']

        if context is None:
            context = {}

        select_all = _(u"All")

        # The method will return a JSON format dictionary, for the Report
        # Runner module to produce a PDF document containing all the data.
        json_output = {}

        report_journal = self.browse(
            cr, uid, report_journal_id, context=context
        )

        user = report_journal.user

        period_ids = self.get_period_ids(
            cr, uid, report_journal, context=context
        )

        (dimension_ids, dimensions) = self.get_dimension_ids(
            cr, uid, context=context
        )

        journal_ids = [journal.id for journal in report_journal.journal_ids]

        journals_generator = JournalsGenerator(
            cr, report_journal.company_id.id, report_journal.fiscalyear_id.id,
            journal_ids, period_ids, dimension_ids,
            report_journal.moves_to_include,
        )

        lines = list(journals_generator())

        now = datetime.datetime.now()

        lang = report_journal.lang_id.code

        json_output['metadata'] = {
            'lang': lang,
            'created': format_datetime(
                now, 'short', locale=lang
            ),
            'creator': user.name
        }

        def format_amount(amount):
            """Format the specified amount according to the locale."""
            return format_decimal(amount, u'#,##0.00', locale=lang)

        option = [
            entry[1]
            for entry in options
            if entry[0] == report_journal.moves_to_include
        ][0]

        json_output['options'] = {
            'company': report_journal.company_id.name,
            'fiscalyear': report_journal.fiscalyear_id.name,
            'currency': report_journal.company_id.currency_id.name,
            'period_from': report_journal.period_from.name,
            'period_to': report_journal.period_to.name,
            'account': select_all,
            'moves': option,
            'reconcile': select_all,
            'analytic_structures': dimensions
        }

        json_output['tables'] = []

        # TODO Document the format of "journal_dict".
        journal_dict = self.group_lines_by_journals_and_periods(lines)

        # TODO Must be a better way to iterate on a dict without having to
        # build a sorting list beforehand.

        # TODO Document what the [0] is for.
        order = journal_dict.keys()
        order.sort(key=lambda tuple: tuple[0])

        # Init global counters.
        total_debit = 0.0
        total_credit = 0.0

        journals_names = ''
        i = 0  # TODO Better variable name than just "i".

        # TODO Document what goes on here (up to 4 imbricated loops).

        for journal_tuple in order:

            period_dict = journal_dict[journal_tuple]

            # TODO Document what this does.
            if i != 0:
                journals_names += ', '
            journals_names += journal_tuple[1]
            i = 1

            period_lines = []

            # TODO Must be a better way to iterate on a dict without having to
            # build a sorting list beforehand.

            suborder = period_dict.keys()
            # Order by period end date (first element of "period_info").
            suborder.sort(key=lambda period_info: period_info[0])

            # Init per-journal counters.
            debit_by_journal = 0.0
            credit_by_journal = 0.0

            for period in suborder:
                lines = period_dict[period]

                period_name = period[1]

                account_move_lines = []

                # Init per-period counters.
                debit_by_period = 0.0
                credit_by_period = 0.0
                balance_by_period = 0.0

                for line in lines:

                    # Gather amounts.
                    debit = line['Line Debit']
                    credit = line['Line Credit']

                    # Update counters.
                    total_debit += debit
                    total_credit += credit
                    debit_by_journal += debit
                    credit_by_journal += credit
                    debit_by_period += debit
                    credit_by_period += credit
                    balance_by_period += debit - credit

                    account_move_line = {
                        'date': format_date(
                            datetime.datetime.strptime(
                                line['Move Date'], DEFAULT_SERVER_DATE_FORMAT
                            ),
                            'short',
                            locale=lang
                        ),
                        'entry': line['Move Reference'],
                        'description': line['Line Description'],
                        'account_code': line['Account Code'],
                        'account_name': line['Account Name'],
                        'partner': (
                            line['Partner Name']
                            if line['Partner Name'] is not None
                            else ""
                        ),
                        'debit': format_amount(debit),
                        'credit': format_amount(credit),
                        'analytic': [
                            line['Analytic Code %s' % dimension_id]
                            if (
                                line['Analytic Code %s' % dimension_id]
                                is not None
                            ) else ''
                            for dimension_id in dimension_ids
                        ]
                    }

                    account_move_lines.append(account_move_line)

                period_lines.append({
                    'balance': format_amount(balance_by_period),
                    'credit': format_amount(credit_by_period),
                    'debit': format_amount(debit_by_period),
                    'moves': account_move_lines,
                    'period': period_name,
                })

            json_output['tables'].append({
                'credit': format_amount(credit_by_journal),
                'debit': format_amount(debit_by_journal),
                'journal_code': journal_tuple[0],
                'journal_name': journal_tuple[1],
                'periods': period_lines,
            })

        json_output.update({
            'credit': format_amount(total_credit),
            'debit': format_amount(total_debit),
        })

        json_output['options']['journals'] = journals_names

        json_temp = tempfile.mkstemp(suffix='.json')

        with codecs.open(json_temp[1], 'wb', "utf-8") as json_file:
            json.dump(
                json_output, json_file, skipkeys=True, indent=True,
                encoding="utf-8"
            )

        json_file.close()

        journals_job = data_obj.get_object(
            cr, uid,
            'report_journals',
            'job_definition_journals',
            context=context
        )

        job_log_id = journals_job.run_job(
            in_file_name=json_temp[1], context=context
        )['res_id']

        report_journals = {
            'job_log_id': job_log_id,
        }

        report_journal.with_context(context).write(report_journals)

        return True


class JournalsGenerator(object):
    def __init__(
        self, cr, company_id, fiscalyear_id, journal_ids,
        period_ids, dimension_ids, moves_to_include
    ):
        self.cr = cr
        self.fiscalyear_id = fiscalyear_id
        self.journal_ids = journal_ids
        self.company_id = company_id
        self.period_ids = period_ids
        self.dimension_ids = dimension_ids
        self.moves_to_include = moves_to_include
        self.tables = {}
        self.query = None
        self.period_query = None
        self.query_fields = []
        self.session = self.__get_session_maker()()
        self.meta = MetaData(bind=self.session.bind)
        self.__init_model()
        self.__init_period_query()
        self.__init_query_fields()
        self.__init_query()
        self.__init_where()
        self.__init_orderby()

        # Useful for tests:
        # self.query = self.query.limit(400)

        super(JournalsGenerator, self).__init__()

    def __get_session_maker(self):
        engine = create_engine(
            "postgres://%s:%s@%s:%d/%s" % (
                config.get('db_user'),
                config.get('db_password'),
                config.get('db_host'),
                config.get('db_port'),
                self.cr.dbname,
            )
        )
        return sessionmaker(bind=engine)

    def __init_model(self):

        tables = {
            'move_line': Table(
                'account_move_line',
                self.meta,
                autoload=True,
            ),
            'move': Table(
                'account_move',
                self.meta,
                autoload=True,
            ),
            'journal': Table(
                'account_journal',
                self.meta,
                autoload=True,
            ),
            'period': Table(
                'account_period',
                self.meta,
                autoload=True,
            ),
            'partner': Table(
                'res_partner',
                self.meta,
                autoload=True,
            ),
            'account': Table(
                'account_account',
                self.meta,
                autoload=True,
            ),
        }

        if self.dimension_ids:
            for i in self.dimension_ids:
                tables['analytic_code_%s' % i] = \
                    Table(
                        'analytic_code',
                        self.meta,
                        autoload=True,
                    ).alias(
                        'analytic_code_%s' % i
                    )

        self.tables.update(tables)

    def __init_query_fields(self):

        query_fields = [
            self.tables['journal'].c.code.label('Journal Code'),
            self.tables['journal'].c.name.label('Journal Name'),
            self.tables['period'].c.date_stop.label('Period end date'),
            self.tables['period'].c.name.label('Period Name'),
            self.tables['move'].c.date.label('Move Date'),
            self.tables['move'].c.name.label('Move Reference'),
            self.tables['move_line'].c.name.label('Line Description'),
            self.tables['account'].c.code.label('Account Code'),
            self.tables['account'].c.name.label('Account Name'),
            self.tables['partner'].c.name.label('Partner Name'),
            self.tables['move_line'].c.debit.label('Line Debit'),
            self.tables['move_line'].c.credit.label('Line Credit'),
        ]

        if self.dimension_ids:
            for i in self.dimension_ids:
                query_fields.append(
                    self.tables['analytic_code_%s' % i].c.description.label(
                        'Analytic Code %s' % i
                    )
                )

        self.query_fields.extend(query_fields)

    def __init_query(self):

        query = \
            self.tables['move_line'].outerjoin(
                self.tables['account'],
                self.tables['account'].c.id ==
                self.tables['move_line'].c.account_id,
            ).outerjoin(
                self.tables['move'],
                self.tables['move'].c.id ==
                self.tables['move_line'].c.move_id,
            ).outerjoin(
                self.tables['period'],
                self.tables['period'].c.id ==
                self.tables['move_line'].c.period_id,
            ).outerjoin(
                self.tables['partner'],
                self.tables['partner'].c.id ==
                self.tables['move_line'].c.partner_id,
            ).outerjoin(
                self.tables['journal'],
                self.tables['journal'].c.id ==
                self.tables['move_line'].c.journal_id,
            )

        if self.dimension_ids:
            for i in self.dimension_ids:
                query = query.outerjoin(
                    self.tables['analytic_code_%s' % i],
                    self.tables['analytic_code_%s' % i].c.id ==
                    eval("self.tables['move_line'].c.a%s_id" % i),
                )

        self.query = select(self.query_fields).select_from(query)

    def __init_period_query(self):
        self.period_query = select(
            [self.tables['period'].c.id]
        ).select_from(
            self.tables['period']
        ).where(
            and_(
                self.tables['period'].c.fiscalyear_id == self.fiscalyear_id,
                self.tables['period'].c.company_id == self.company_id,
            ),
        )

    def __init_where(self):
        and_args = (
            self.tables['journal'].c.id.in_(self.journal_ids),
            self.tables['journal'].c.company_id == self.company_id,
            self.tables['move_line'].c.period_id.in_(self.period_query),
            self.tables['period'].c.id.in_(self.period_ids),
        )

        # Check if we care about draft moves
        move_states = ('posted',)
        if self.moves_to_include == 'all':
            move_states += ('draft',)

        and_args += (self.tables['move'].c.state.in_(move_states),)

        self.query = self.query.where(and_(*and_args))

    def __init_orderby(self):
        self.query = self.query.order_by(
            self.tables['journal'].c.code,
            self.tables['period'].c.date_stop,
            self.tables['move'].c.date,
            self.tables['move'].c.name,
            self.tables['account'].c.code,
        )

    def __call__(self):
        # Establish connection, execute query and yield resulting rows
        conn = self.session.bind.connect()
        for result in conn.execute(self.query):
            yield result

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
