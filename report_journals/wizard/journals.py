# -*- coding: utf-8 -*-
##############################################################################
#
#    Trial Balance, for OpenERP
#    Copyright (C) 2015 XCG Consulting (http://odoo.consulting)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
import time

from openerp.osv import fields, osv
from openerp.osv.orm import browse_record
from openerp.tools.translate import _


options = [
    ('posted', 'All Posted Entries'),
    ('all', 'All Entries'),
]


class journals(osv.osv_memory):
    """Copied and adapted from Odoo7 Account module and its
    account.common.report class in wizard/account_report_common.py.
    """

    _name = "wizard.journals"
    _description = "Journals"

    _columns = {
        'company_id': fields.many2one(
            'res.company',
            string='Company',
            readonly=True
        ),

        'fiscalyear_id': fields.many2one(
            'account.fiscalyear',
            'Fiscal Year',
            help='Keep empty for all open fiscal year',
            required=True
        ),

        'period_from': fields.many2one(
            'account.period',
            'Start Period',
            required=True
        ),

        'period_to': fields.many2one(
            'account.period',
            'End Period',
            required=True
        ),

        'journal_ids': fields.many2many(
            'account.journal',
            string='Journals',
            required=True
        ),

        'moves_to_include': fields.selection(
            options,
            'Moves to include',
            required=True
        ),
    }

    def get_company(self, cr, uid, ids, context=None):

        user = self.pool['res.users'].browse(cr, uid, uid, context=context)

        return user.company_id.id

    def _check_company_id(self, cr, uid, ids, context=None):
        for wiz in self.browse(cr, uid, ids, context=context):
            company_id = wiz.company_id.id
            if (
                wiz.fiscalyear_id and
                company_id != wiz.fiscalyear_id.company_id.id
            ):
                return False
            if wiz.period_from and company_id != wiz.period_from.company_id.id:
                return False
            if wiz.period_to and company_id != wiz.period_to.company_id.id:
                return False
        return True

    _constraints = [
        (
            _check_company_id,
            u'The fiscal year and periods of account chosen have'
            u' to belong to the same company.',
            ['fiscalyear_id', 'period_from', 'period_to']
        ),
    ]

    def _get_fiscalyear(self, cr, uid, context=None):

        if context is None:
            context = {}

        now = time.strftime('%Y-%m-%d')
        company_id = False

        ids = context.get('active_ids', [])

        if ids and context.get('active_model') == 'account.account':
            company_id = self.pool.get('account.account').browse(
                cr, uid, ids[0], context=context
            ).company_id.id
        else:  # use current company id
            company_id = self.pool.get('res.users').browse(
                cr, uid, uid, context=context
            ).company_id.id
        domain = [
            ('company_id', '=', company_id),
            ('date_start', '<', now),
            ('date_stop', '>', now)
        ]
        fiscalyears = self.pool.get('account.fiscalyear').search(
            cr, uid, domain, limit=1
        )
        return fiscalyears and fiscalyears[0] or False

    def _get_all_journal(self, cr, uid, ids, context=None):
        company_id = self.get_company(cr, uid, ids, context=context)
        return self.pool.get('account.journal').search(
            cr, uid, [('company_id', '=', company_id)], context=context
        )

    _defaults = {
        'fiscalyear_id': _get_fiscalyear,
        'company_id': get_company,
        'moves_to_include': 'posted',
    }

    def generate_cron_task(self, cr, uid, ids, context=None):

        data_obj = self.pool['ir.model.data']

        wizard = self.browse(cr, uid, ids, context=context)[0]

        lang_id = self.pool['res.lang'].search(
            cr, uid, [('code', '=', context['lang'])], context=context
        )[0]

        report_journals = {
            'name': u"Journals Report " + fields.datetime.now(),
            'lang_id': lang_id,
            'user': uid,
        }

        for key in wizard._columns.keys():
            val = getattr(wizard, key)
            if isinstance(val, list):
                out = []
                for journal in val:
                    out.append(journal.id)
                report_journals[key] = [(6, 0, out)]
            elif isinstance(val, browse_record):
                report_journals[key] = val.id
            else:
                report_journals[key] = val

        report_journal_id = self.pool['report_journals'].create(
            cr, uid, report_journals, context=context
        )

        journals_queue_id = data_obj.get_object(
            cr, uid,
            'report_journals',
            'report_journals_queue',
            context=context
        ).id

        self.pool['queue.task'].create(
            cr, uid,
            {
                'state': 'todo',
                'args_interpolation': "{'report_journal_id': %s}" % (
                    report_journal_id
                ),
                'queue_id': journals_queue_id,
            },
            context=context
        )

        return {
            'type': 'ir.actions.act_window',
            'name': _('Journals'),
            'res_model': 'report_journals',
            'res_id': report_journal_id,
            'view_mode': 'form',
            'view_type': 'form',
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
