from openerp import models

from openerp.addons.analytic_structure.MetaAnalytic import MetaAnalytic


class ProductTemplate(models.Model):
    """- Include analytic capabilities to products.
    - Disallow copies thanks to "base_no_copy".
    """

    __metaclass__ = MetaAnalytic

    _inherit = _inherit = ['product.template', 'base.no_copy_mixin']
    _name = 'product.template'

    _analytic = 'product_template'
