from openerp import models


class ProductProduct(models.Model):
    """Show analytics from the product template.
    """

    _inherit = 'product.product'

    def fields_get(
        self, cr, uid, allfields=None, context=None, write_access=True,
    ):
        """Override to include analytics from the product template."""

        ret = super(ProductProduct, self).fields_get(
            cr, uid, allfields=allfields, context=context,
            write_access=write_access,
        )

        self.pool['analytic.structure'].analytic_fields_get(
            cr, uid, 'product_template', ret, context=context,
        )

        return ret

    def fields_view_get(
        self, cr, uid, view_id=None, view_type='form', context=None,
        toolbar=False, submenu=False,
    ):
        """Override to include analytics from the product template."""

        ret = super(ProductProduct, self).fields_view_get(
            cr, uid, view_id=view_id, view_type=view_type, context=context,
            toolbar=toolbar, submenu=submenu,
        )

        self.pool['analytic.structure'].analytic_fields_view_get(
            cr, uid, 'product_template', ret, context=context,
        )

        return ret
