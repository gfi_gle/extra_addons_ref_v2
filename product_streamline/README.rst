Product management improvements
===============================

Improvements on top of the base Odoo product management:

- Analytics.

- Disallow copies thanks to ``base_no_copy``.
