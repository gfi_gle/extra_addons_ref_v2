##############################################################################
#
#    Product management improvements for Odoo
#    Copyright (C) 2017 XCG Consulting <http://odoo.consulting>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

{
    'name': 'Product management improvements',
    'description': '''
Product management improvements
===============================

Improvements on top of the base Odoo product management:

- Analytics.

- Disallow copies thanks to ``base_no_copy``.
''',
    'version': '1.2',
    'category': '',
    'author': 'XCG Consulting',
    'website': 'http://odoo.consulting/',
    'depends': [
        'analytic_structure',
        'base_no_copy',
        'product',
        'stock',
    ],

    'data': [
        'views/product_template.xml',

        # Safer to load this one after product_template.xml.
        'views/product_product.xml',
    ],

    'demo': [
        'demo/analytic.dimension.csv',
        'demo/analytic.structure.csv',
        'demo/analytic.code.csv',
        'demo/product.template.csv',
    ],

    'installable': True,
}
